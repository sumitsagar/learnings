package main

import "fmt"

func main() {
	input := []int{2, 7, 9, 3, 1}
	r := rob(input)
	fmt.Println(r)
}

func rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	money := make([]int, len(nums)+1)
	money[0] = 0
	money[1] = nums[0]

	for i := 1; i < len(nums); i++ {
		money[i+1] = getMax(money[i-1]+nums[i], money[i])
	}
	return money[len(nums)]
}

func getMax(a, b int) int {
	if a > b {
		return a
	}

	return b
}
