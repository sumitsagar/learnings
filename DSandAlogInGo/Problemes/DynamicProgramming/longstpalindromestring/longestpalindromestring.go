package main

import "fmt"

func main() {
	s := longestPalindrome("uytvryutrfyutfruurftuyfrtuyrvtyu")
	fmt.Println(s)
}

func longestPalindrome(s string) string {
	longest := ""

	// isodd := len(s)%2 == 1

	// centreindex := 0
	// leftindex := 0
	// rightindex := 0

	// if isodd {
	// 	centreindex = len(s) / 2
	// 	leftindex = centreindex
	// 	rightindex = centreindex
	// } else {
	// 	leftindex = (len(s) / 2) - 1
	// 	rightindex = leftindex + 1
	// }
	leftindex := 1
	rightindex := leftindex + 1

	for h := 0; h < len(s)-1; h++ {
		leftindex = leftindex + h
		rightindex = leftindex + 1
		for i := 0; i < leftindex && rightindex+1 < len(s); i++ {
			if leftindex-i != rightindex+i {
				//is plaindrome
				if s[leftindex-i] == s[rightindex+i] {
					st := s[leftindex-i : rightindex+i+1]
					if len(st) > len(longest) {
						longest = st
					}
				} else {
					break
				}
			}
		}

	}

	return longest
}

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}
