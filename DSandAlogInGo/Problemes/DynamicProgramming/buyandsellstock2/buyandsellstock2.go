package main

import "fmt"

var alreadypurchaseddates map[int]int

func main() {
	alreadypurchaseddates = make(map[int]int)
	r := maxProfit([]int{7, 1, 5, 3, 6, 4})
	fmt.Println(r)
}
func maxProfit(prices []int) int {
	minsofar := prices[0]
	purchaseedindex := 0
	totalprofit := 0
	localprofit := 0

	for h := 1; h < len(prices); h++ {
		for i := 1; i < len(prices); i++ {
			if IsAlreadyPurchased(i) {
				continue
			}
			if prices[i] < minsofar {
				minsofar = prices[i]
				purchaseedindex = i
			}
			profitinsellingthis := prices[i] - minsofar
			if profitinsellingthis > localprofit {
				localprofit = profitinsellingthis
			}
		}
		if localprofit < 0 {
			localprofit = 0
		} else {
			totalprofit = totalprofit + localprofit
		}
		alreadypurchaseddates[purchaseedindex] = 1
	}
	return totalprofit
}

func IsAlreadyPurchased(days int) bool {
	if _, ok := alreadypurchaseddates[days]; ok {
		return true
	}
	return false
}
