package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(numDecodings("12"))
	fmt.Println(numDecodings("226"))
	fmt.Println(numDecodings("111111111111111111111111111111111111111111111"))

}

func numDecodings(s string) int {
	memo := make(map[string]int)
	return numDecodingsProcess(s, &memo)
}

func numDecodingsProcess(s string, memo *map[string]int) int {
	if len(s) == 0 {
		return 1
	}
	if len(s) == 1 && s[0] != '0' {
		return 1
	}
	if s[0] == '0' {
		return 0
	}
	if val, ok := (*memo)[s]; ok {
		return val
	}
	x := numDecodingsProcess(s[1:], memo)

	arr := string(s[1:])
	(*memo)[arr] = x

	if s[:2] <= "26" {
		ways := numDecodingsProcess(s[2:], memo)
		arr2 := string(s[2:])
		(*memo)[arr2] = ways
		x = x + ways
	}
	return x
}

func numDecodingsbyme(str string, selection int) int {

	if strings.Index(str, "0") == 0 {
		return 0
	}
	if len(str) == 1 {
		return 1
	}
	if len(str) == 2 && selection == 2 {
		return 0
	}

	ways := 0
	for i := 0; i < len(str); i++ {
		substr := str[i:]
		if len(substr) > 1 && string(substr[0]) != "0" { // 0,03,04 etc.. shouldnt be considered
			leftstr := substr[1:]
			if len(leftstr) > 0 {
				can_decode_startwith1char := numDecodingsbyme(leftstr, 1) //compute rest excluding 1 char
				ways += can_decode_startwith1char
			} else if len(leftstr) == 0 { // mymistakes -- this should be covered in base case
				break
			}
		}
		if len(substr) >= 2 && strings.Index(string(substr[0:2]), "0") != 0 { // 0,03,04 etc.. shouldnt be considered
			leftstr := substr[2:]
			if len(leftstr) > 0 {
				can_decode_startwith2char := numDecodingsbyme(leftstr, 2) //compute rest excluding 2 char
				ways += can_decode_startwith2char
			} else {
				ways++
			}
		}
	}
	return ways
}
