package main

import "fmt"

func main() {
	input := []int{1, 2, 5}
	r := coinChange(input, 11)
	fmt.Println(r)
}

func coinChange(coins []int, amount int) int {

	dp := make([]int, amount+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = amount + 1 //setting some default value
	}

	dp[0] = 0
	for i := 1; i <= amount; i++ {
		for _, coin := range coins {
			if i-coin >= 0 {
				//i-coin is the excess amount needed to reach amount i which we can get in dp[i-coin]
				dp[i] = Min(dp[i], 1+dp[i-coin]) //this is called recurrent relationship
			}
		}
	}
	if dp[amount] != amount+1 {
		return dp[amount]
	} else {
		return -1
	}
}

func Min(a, b int) int {
	if a < b {
		return a
	}

	return b
}
