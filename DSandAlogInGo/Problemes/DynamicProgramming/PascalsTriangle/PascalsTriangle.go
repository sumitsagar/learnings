package main

import "fmt"

func main() {
	arr := generate(1)
	fmt.Println(arr)
}

func generate(numRows int) [][]int {
	arr := [][]int{}
	fr := []int{1}
	arr = append(arr, fr)
	if numRows == 1 {
		return arr
	}
	Solution(numRows, 1, []int{1}, &arr)
	return arr
}

func Solution(numRows int, currow int, prevarr []int, output *[][]int) {
	nextarr := []int{}
	for i := 0; i < len(prevarr)+1; i++ {
		if i == 0 {
			nextarr = append(nextarr, prevarr[i])
		} else if i == len(prevarr) {
			nextarr = append(nextarr, prevarr[i-1])
		} else {
			nextarr = append(nextarr, prevarr[i-1]+prevarr[i])
		}
	}
	*output = append(*output, nextarr)
	currow = currow + 1
	if currow < numRows {
		Solution(numRows, currow, nextarr, output)
	}
}
