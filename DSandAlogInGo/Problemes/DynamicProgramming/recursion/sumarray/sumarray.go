package main

import "fmt"

func main() {
	i := []int{1, -2, 3, 4}
	fmt.Println(sumarr2(i))
}

func sumarr1(arr []int) int {
	if len(arr) == 0 {
		return 0
	}
	if len(arr) == 1 {
		return arr[0]
	}

	sum := arr[len(arr)-1] + sumarr1(arr[0:len(arr)-1])
	return sum
}

func sumarr2(arr []int) int {
	if len(arr) == 0 {
		return 0
	}
	sum := arr[0] + sumarr2(arr[1:])
	return sum
}
