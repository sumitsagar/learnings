package main

import "fmt"

func main() {
	input := []int{2, 7, 9, 3, 1}
	r := rob(input)
	fmt.Println(r)
}

func rob(nums []int) int {
	arrlen := len(nums)

	if arrlen == 0 {
		return 0
	}
	if arrlen == 1 {
		return nums[0]
	}
	excluding_first := nums[1:arrlen]
	excluding_last := nums[0 : arrlen-1]
	r1 := process(excluding_first)
	r2 := process(excluding_last)
	if r1 > r2 {
		return r1
	} else {
		return r2
	}
}

func process(nums []int) int {

	money := make([]int, len(nums)+1)
	money[0] = 0
	money[1] = nums[0]

	for i := 1; i < len(nums); i++ {
		money[i+1] = getMax(money[i-1]+nums[i], money[i])
	}
	return money[len(nums)]
}

func getMax(a, b int) int {
	if a > b {
		return a
	}

	return b
}
