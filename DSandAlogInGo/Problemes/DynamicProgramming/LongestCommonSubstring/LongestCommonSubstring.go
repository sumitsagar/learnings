package main

import "fmt"

func main() {
	fmt.Println(longestCommonSubsequence("sumitsagar", "mits"))
}
func longestCommonSubsequence(text1 string, text2 string) int {
	longeststring := ""
	parentstartindex := 0
	childstartindex := 0
	windowlength := 1

	for parentstartindex+windowlength <= len(text1) && childstartindex+windowlength <= len(text2) {
		if text1[parentstartindex:parentstartindex+windowlength] == text2[childstartindex:childstartindex+windowlength] {
			longeststring = text1[parentstartindex : parentstartindex+windowlength]
			windowlength = windowlength + 1 //increase window size from end
		} else {
			parentstartindex = parentstartindex + 1
			childstartindex = 0
			windowlength = 1
		}
	}

	return len(longeststring)
}
