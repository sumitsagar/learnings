package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	r := countBits(5)
	fmt.Println(r)
}

func countBits(n int) []int {

	retval := []int{}
	for i := 0; i <= n; i++ {
		n := strconv.FormatInt(int64(i), 2)
		//asint, _ := strconv.Atoi(n)
		retval = append(retval, strings.Count(n, "1"))
	}
	return retval
}
