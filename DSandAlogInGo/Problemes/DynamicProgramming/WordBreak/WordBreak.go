package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(wordBreak("applepenapple", []string{"apple", "pen"}))
}
func wordBreak(word string, item []string) bool {
	if word == "" {
		return true
	}
	for i := 0; i < len(item); i++ {
		if strings.Index(word, item[i]) == 0 {
			//means word start with this
			leftstring := strings.Split(word, item[i])[1]
			if wordBreak(leftstring, item) {
				return true
			}
		}
	}
	return false
}
