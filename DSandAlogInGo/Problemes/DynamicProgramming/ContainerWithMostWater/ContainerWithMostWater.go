package main

import "fmt"

func main() {
	fmt.Println(maxArea([]int{1, 8, 6, 2, 5, 4, 8, 3, 7}))
}
func maxArea(height []int) int {
	max_area := 0
	i := 0
	j := len(height) - 1
	for i < len(height) && j > 0 && i < j {
		h := Min(height[i], height[j])
		area := h * (j - i)
		if area > max_area {
			max_area = area
		}
		if height[i] < height[j] {
			i = i + 1
		} else {
			j = j - 1
		}
	}
	return max_area
}

func Min(a, b int) int {
	if a > b {
		return b
	} else {
		return a
	}
}
