package main

import "fmt"

func main() {
	r := maxProfit([]int{2, 1, 2, 1, 0, 1, 2})
	fmt.Println(r)
}

//bruteforce
func maxProfit1(prices []int) int {
	maxprofint := 0
	for i := 0; i < len(prices)-1; i++ {
		for j := i + 1; j <= len(prices)-1; j++ {
			profilt := prices[j] - prices[i]
			if profilt > maxprofint {
				maxprofint = profilt
			}
		}
	}
	return maxprofint
}
func maxProfit(prices []int) int {
	minsofar := prices[0]
	profit := 0
	for i := 1; i < len(prices); i++ {
		if prices[i] < minsofar {
			minsofar = prices[i]
		}
		profitinsellingthis := prices[i] - minsofar
		if profitinsellingthis > profit {
			profit = profitinsellingthis
		}
	}
	if profit < 0 {
		profit = 0
	}
	return profit
}
