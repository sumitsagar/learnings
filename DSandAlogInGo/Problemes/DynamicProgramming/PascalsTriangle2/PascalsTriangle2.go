package main

import "fmt"

func main() {
	arr := getRow(3)
	fmt.Println(arr)
}

func getRow(rowIndex int) []int {
	fr := []int{1}
	if rowIndex == 0 {
		return fr
	}
	row := Solution(rowIndex, 1, []int{1})
	return *row
}

func Solution(rowindex int, currow int, prevarr []int) *[]int {

	nextarr := []int{}
	for i := 0; i < len(prevarr)+1; i++ {
		if i == 0 {
			nextarr = append(nextarr, prevarr[i])
		} else if i == len(prevarr) {
			nextarr = append(nextarr, prevarr[i-1])
		} else {
			nextarr = append(nextarr, prevarr[i-1]+prevarr[i])
		}
	}
	if currow == rowindex {
		return &nextarr
	} else {
		currow++
	}
	nextarr = *Solution(rowindex, currow, nextarr)
	return &nextarr
}
