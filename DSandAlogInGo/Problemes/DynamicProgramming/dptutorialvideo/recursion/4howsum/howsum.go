package main

import "fmt"

func main() {
	memo := make(map[int][]int)
	fmt.Println(howsum(7, []int{2, 3}, &memo))
	fmt.Println(howsum(7, []int{5, 3, 4, 7}, &memo))
	fmt.Println(howsum(7, []int{1, 4}, &memo))
	fmt.Println(howsum(8, []int{2, 3, 5}, &memo))
	fmt.Println(howsum(3000, []int{7, 14}, &memo))
}

func howsum(target int, numbers []int, memo *map[int][]int) []int {
	if target == 0 {
		return []int{}
	}
	if target < 0 {
		return nil
	}
	if val, ok := (*memo)[target]; ok {
		return val
	}
	//below code is called branching
	for i := 0; i < len(numbers); i++ {
		num := numbers[i]
		remainder := target - num
		r := howsum(remainder, numbers, memo)
		(*memo)[target] = append(r, num)
		if r != nil { //means we got the target to 0
			final := (append(r, num))
			return final
		}
	}
	return nil
}
