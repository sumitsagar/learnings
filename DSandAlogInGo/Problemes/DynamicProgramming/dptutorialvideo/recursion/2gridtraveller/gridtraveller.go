package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println(gridtraveller(3, 7))
}

func gridtraveller(m int, n int) int {

	memo := make(map[string]int)
	return process(m, n, &memo)

}

func process(m int, n int, memo *map[string]int) int {

	if m == 1 || n == 1 {
		return 1
	}
	if m == 0 || n == 0 {
		return 0
	}
	if val, ok := (*memo)[strconv.Itoa(m)+"_"+strconv.Itoa((n))]; ok {
		return val //(*memo)[strconv.Itoa(m)+"_"+strconv.Itoa((n))]
	}

	(*memo)[strconv.Itoa(m)+"_"+strconv.Itoa((n-1))] = process(m, n-1, memo)
	(*memo)[strconv.Itoa((m-1))+"_"+strconv.Itoa(n)] = process(m-1, n, memo)
	return (*memo)[strconv.Itoa(m)+"_"+strconv.Itoa((n-1))] + (*memo)[strconv.Itoa(m-1)+"_"+strconv.Itoa((n))]
}

// func Process(m int, n int) bool {
// 	pathcount := 0
// 	for i := 0; i <= m; i++ {
// 		for j := 0; j < n; j++ {
// 			canreach  := CanReach(i, j)
// 			if canreach{
// 				pathcount++
// 			}
// 	}
// 	return canreach
// }

// func CanReach(m int, n int) int {
// 	r := 0
// 	if m == 0 || n == 0 {
// 		return false
// 	}
// 	if m == 1 && n == 1 {
// 		return 2
// 	}
// 	return r
// }
