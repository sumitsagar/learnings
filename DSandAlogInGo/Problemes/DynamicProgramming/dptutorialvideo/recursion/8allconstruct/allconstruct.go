package main

import (
	"fmt"
	"strings"
)

func main() {
	output := [][]string{}
	fmt.Println(AllConstruct("purple", []string{"purp", "p", "ur", "le", "purpl"}, &output))
	// fmt.Println(AllConstruct("sumitsagar", []string{"sum", "it", "gar", "sa"}, &output))
	// fmt.Println(AllConstruct("sssssssssssssssssssssssss", []string{"sum", "it", "gar", "sa", "s", "ss"}, &output))
	//fmt.Println(output)
}

func AllConstruct(word string, input []string, output *[][]string) [][]string {

	if word == "" {
		return [][]string{}
	}

	result := [][]string{}

	for i := 0; i < len(input); i++ {
		//branching
		if strings.Index(word, input[i]) == 0 {
			//word exists
			//resultcandidate = append(resultcandidate, input[i])
			remainderword := strings.Split(word, input[i])[1]
			res := AllConstruct(remainderword, input, output)
			//fmt.Println(res)
			if len(res) == 0 {
				res = append(res, []string{input[i]})
				result = append(result, res...)
			} else {
				res2 := [][]string{}
				for _, item := range res {
					item := append(item, input[i])
					res2 = append(res2, item)
				}
				result = append(result, res2...)
			}
		}
	}
	return result //retrning blank array
}
