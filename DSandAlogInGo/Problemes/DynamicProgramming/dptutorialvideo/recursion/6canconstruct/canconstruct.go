package main

import (
	"fmt"
	"strings"
)

func main() {
	memo := make(map[string]bool)
	fmt.Println(canconstruct("sumitsagar", []string{"sum", "it", "gar", "sa"}, &memo))
	fmt.Println(canconstruct("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
		[]string{"e", "ee", "eeee", "eeeeee"}, &memo))
}

func canconstruct(word string, item []string, memo *map[string]bool) bool {
	if word == "" {
		return true
	}
	if _, ok := (*memo)[word]; ok {
		return true
	}
	for i := 0; i < len(item); i++ {
		if strings.Index(word, item[i]) == 0 {
			//means word start with this
			leftstring := strings.Split(word, item[i])[1]
			if canconstruct(leftstring, item, memo) {
				(*memo)[word] = true
				return true
			}
		}
	}
	(*memo)[word] = false
	return false
}
