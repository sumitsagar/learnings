package main

import "fmt"

func main() {

	memo := make(map[int][]int)
	fmt.Println(bestsum(7, []int{5, 3, 4, 7}, &memo))
	fmt.Println(bestsum(8, []int{2, 3, 5}, &memo))
	fmt.Println(bestsum(8, []int{1, 4, 5}, &memo))
	fmt.Println(bestsum(100, []int{1, 2, 5, 25}, &memo))
	// fmt.Println(memo[8])
}

func bestsum(target int, numbers []int, memo *map[int][]int) []int {
	if val, ok := (*memo)[target]; ok {
		return val
	}
	if target == 0 {
		return []int{}
	}
	if target < 0 {
		return nil
	}

	bestsumresult := []int{}

	for i := 0; i < len(numbers); i++ {
		num := numbers[i]
		reminder := target - numbers[i]
		result := bestsum(reminder, numbers, memo)

		if result != nil {
			if len(bestsumresult) > len(result)+1 {
				bestsumresult = append(result, num)

			} else {
				bestsumresult = append(result, num)
			}
		}
	}
	(*memo)[target] = bestsumresult
	return bestsumresult
}
