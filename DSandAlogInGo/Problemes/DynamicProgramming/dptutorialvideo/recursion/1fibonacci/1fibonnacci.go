package main

func main() {
	//println(fib(60))
	dp := make([]int, 600+1)
	println(fibdp(600, &dp))
}

func fib(cnt int) int {
	if cnt == 0 {
		return 0
	}
	if cnt == 1 {
		return 1
	}
	return fib(cnt-1) + fib(cnt-2)
}

func fibdp(cnt int, dp *[]int) int {
	if (*dp)[cnt] != 0 {
		return (*dp)[cnt]
	}
	if cnt == 0 {
		return 0
	}
	if cnt == 1 {
		return 1
	}
	(*dp)[cnt] = fibdp(cnt-1, dp) + fibdp(cnt-2, dp)
	return (*dp)[cnt]
}
