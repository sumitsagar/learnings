package main

import "fmt"

func main() {
	memo := make(map[int]bool)
	fmt.Println(cansum(7, []int{2, 3}, &memo))
	fmt.Println(cansum(7, []int{5, 3, 4, 7}, &memo))
	fmt.Println(cansum(7, []int{1, 4}, &memo))
	fmt.Println(cansum(8, []int{2, 3, 5}, &memo))
	fmt.Println(cansum(300, []int{7, 14}, &memo))
}

func cansum(target int, numbers []int, memo *map[int]bool) bool {
	if target == 0 {
		return true
	}
	if target < 0 {
		return false
	}
	if val, ok := (*memo)[target]; ok {
		return val
	}
	//below code is called branching
	for i := 0; i < len(numbers); i++ {
		num := numbers[i]
		remainder := target - num
		cansum := cansum(remainder, numbers, memo)
		if cansum {
			(*memo)[target] = true
			return true
		}
		(*memo)[target] = false
	}
	return false
}
