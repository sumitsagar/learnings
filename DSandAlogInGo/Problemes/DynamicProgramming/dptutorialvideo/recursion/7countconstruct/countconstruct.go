package main

import (
	"fmt"
	"strings"
)

func main() {
	memo := make(map[string]int)
	fmt.Println(countconstruct("sumitsagar", []string{"sum", "it", "gar", "sa"}, &memo))
	fmt.Println(countconstruct("purple", []string{"purp", "p", "ur", "le", "purpl"}, &memo))
	fmt.Println(countconstruct("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
		[]string{"e", "ee", "eeee", "eeeeee"}, &memo))
}

func countconstruct(word string, item []string, memo *map[string]int) int {
	if word == "" {
		return 1
	}
	if _, ok := (*memo)[word]; ok {
		return 1
	}
	total := 0
	for i := 0; i < len(item); i++ {
		if strings.Index(word, item[i]) == 0 {
			//means word start with this
			leftstring := strings.Split(word, item[i])[1]
			if countconstruct(leftstring, item, memo) == 1 {
				total = total + 1
				(*memo)[word] = total
			}
		}
	}
	(*memo)[word] = total
	return total
}
