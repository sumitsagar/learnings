package main

import "fmt"

func main() {
	fmt.Println(fib(10))
}

func fib(index int) int {
	dp := make([]int, index+1)
	dp[0] = 0
	dp[1] = 1
	for i := 2; i <= index; i++ {
		dp[i] = dp[i-1] + dp[i-2]
	}
	return dp[index]
}
