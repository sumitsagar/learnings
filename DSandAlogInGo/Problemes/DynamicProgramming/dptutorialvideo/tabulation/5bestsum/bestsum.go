package main

import "fmt"

func main() {
	fmt.Println(bestsum(7, []int{2, 3}))
	fmt.Println(bestsum(7, []int{5, 3, 4, 7}))
	fmt.Println(bestsum(7, []int{1, 4}))
	fmt.Println(bestsum(8, []int{2, 3, 5}))
	fmt.Println(bestsum(301, []int{7, 14}))
}
func bestsum(target int, numbers []int) []int {
	dp := make([][]int, target+1)
	dp[0] = []int{}
	bestsumarr := []int{}
	for i := 0; i <= target; i++ {
		for j := 0; j < len(numbers); j++ {
			if dp[i] != nil && i+numbers[j] <= target {
				dp[i+numbers[j]] = append(dp[i], numbers[j])
				if i+numbers[j] == target {
					//return dp[i+numbers[j]]
					if len(bestsumarr) == 0 || len(bestsumarr) > len(dp[i+numbers[j]]) {
						bestsumarr = dp[i+numbers[j]]
					}
				}
			}
		}
	}
	return bestsumarr
}
