package main

import (
	"fmt"
)

func main() {
	fmt.Println(gridtraveller(3, 3))
}

func gridtraveller(m int, n int) int {
	dp := constructArr(n+1, m+1)
	dp[0][0] = 0
	dp[1][0] = 0
	dp[0][1] = 0
	dp[1][1] = 1
	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			current := dp[i][j]
			if i+1 <= m {
				dp[i+1][j] += current
			}
			if j+1 <= n {
				dp[i][j+1] += current
			}
		}
	}
	return dp[m][n]
}

func constructArr(m, n int) [][]int {
	matrix := make([][]int, n)
	for i := 0; i < n; i++ {
		matrix[i] = make([]int, m)
	}
	return matrix
}
