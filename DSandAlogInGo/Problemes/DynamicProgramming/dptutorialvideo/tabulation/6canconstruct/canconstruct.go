package main

import (
	"fmt"
)

func main() {
	fmt.Println(canconstruct("sumitsagar", []string{"sum", "it", "gar", "sa"}))
	fmt.Println(canconstruct("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
		[]string{"e", "ee", "eeee", "eeeeee"}))
}

func canconstruct(word string, item []string) bool {
	return true
}
