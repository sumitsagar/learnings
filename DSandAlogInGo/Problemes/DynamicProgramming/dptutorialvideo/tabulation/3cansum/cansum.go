package main

import "fmt"

func main() {
	fmt.Println(cansum(7, []int{2, 2}))
	fmt.Println(cansum(7, []int{5, 3, 4, 7}))
	fmt.Println(cansum(7, []int{1, 4}))
	fmt.Println(cansum(8, []int{2, 3, 5}))
	fmt.Println(cansum(300, []int{7, 14}))
}

func cansum(target int, numbers []int) bool {
	dp := make([]bool, target+1)
	dp[0] = true
	for i := 0; i <= target; i++ {
		for j := 0; j < len(numbers); j++ {
			if dp[i] && i+numbers[j] <= target {
				dp[i+numbers[j]] = true
			}
		}
	}
	return dp[target]
}
