package main

import "fmt"

func main() {
	fmt.Println(longestCommonSubsequence("abcde", "ace"))
}
func longestCommonSubsequence(text1 string, text2 string) int {
	m, n := len(text1), len(text2)
	dp := make([][]int, m+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}

	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			if text1[i-1] == text2[j-1] {
				dp[i][j] = dp[i-1][j-1] + 1
			} else {
				dp[i][j] = max(dp[i-1][j], dp[i][j-1])
			}
		}
	}
	return dp[m][n]
}

//when the max func will be the builtin func in golang, i hate writing the stupid func everytime!!!
func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

// func longestCommonSubsequence(text1 string, text2 string) int {
// 	if text1 == "" || text2 == "" || len(text1) < len(text2) {
// 		return 0
// 	}
// 	foundsubsequence := ""
// 	parentindex := 0
// 	childstartindex := 0
// 	// parentwindowlength := 1
// 	childwindowlength := 1

// 	for parentindex < len(text1) && childstartindex+childwindowlength <= len(text2) {
// 		parentstringtochkfor := foundsubsequence + string(text1[parentindex])
// 		if parentstringtochkfor == text2[childstartindex:childstartindex+childwindowlength] {
// 			foundsubsequence = parentstringtochkfor
// 			parentindex = parentindex + 1             //increase window size from end
// 			childwindowlength = childwindowlength + 1 //increase window size from end
// 		} else {
// 			parentindex = parentindex + 1
// 		}
// 	}

// 	return len(foundsubsequence)
// }
