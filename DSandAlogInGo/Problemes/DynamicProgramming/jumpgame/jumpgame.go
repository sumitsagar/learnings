package main

import "fmt"

func main() {
	v := canJump([]int{2, 3, 1, 1, 4})
	fmt.Println(v)
}

func canJump(nums []int) bool {
	if len(nums) <= 1 {
		return true
	}

	m := 0
	for i, n := range nums {
		if i > m {
			return false
		}
		m = max(m, n+i)
		if m >= len(nums)-1 {
			return true
		}
	}

	return false
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
