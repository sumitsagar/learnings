package main

import "fmt"

func main() {
	fmt.Println(maxProduct([]int{2, 3, -2, 4}))
	// fmt.Println(maxProduct([]int{-2, 4}))
	//fmt.Println(maxProduct([]int{-2, 0, -1}))
}

func maxProduct(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	max := nums[0]
	min := nums[0]
	result := nums[0]
	for i := 1; i < len(nums); i++ {
		num := nums[i]
		oldmax := max
		oldmin := min
		max = maxOfThree(num, num*oldmax, num*oldmin)
		min = minOfThree(num, num*oldmax, num*oldmin)
		result = maxOfTwo(result, max)
	}

	return result
}

func maxOfTwo(a, b int) int {
	if a > b {
		return a
	}

	return b
}

func maxOfThree(a, b, c int) int {
	return maxOfTwo(maxOfTwo(a, b), c)
}

func minOfTwo(a, b int) int {
	if a < b {
		return a
	}

	return b
}

func minOfThree(a, b, c int) int {
	return minOfTwo(minOfTwo(a, b), c)
}

func maxProductByMe(nums []int) int {
	if len(nums) == 0 {
		fmt.Println("error")
	}
	if len(nums) == 1 {
		return nums[0]
	}
	product := nums[0]
	for i := 0; i <= len(nums)-2; i++ {
		subarr := nums[i+1:]
		subarrayprod := maxProductByMe(subarr)
		if subarrayprod > product {
			product = subarrayprod
		}
		subarrayprodfinal := nums[i] * subarrayprod
		if subarrayprodfinal > product {
			product = subarrayprod
		}
	}
	return product
}

// func reverseArray(arr []int) []int {
// 	arr2 := make([]int, len(arr))
// 	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
// 		arr2[i], arr2[j] = arr[j], arr[i]
// 	}
// 	return arr2
// }
