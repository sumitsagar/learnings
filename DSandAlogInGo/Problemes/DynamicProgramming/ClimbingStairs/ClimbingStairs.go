package main

import "fmt"

var processed map[int]int

func main() {
	ways := climbStairs(2)
	fmt.Println(ways)
}

func climbStairs(n int) int {
	processed = make(map[int]int)
	ways := Solution(n)
	return ways
}

func Solution(n int) int {
	if n == 0 {
		return 0
	}
	if n == 1 {
		return 1
	} else if n == 2 {
		return 2
	}
	if val, ok := processed[n]; ok {
		return val
	}
	ways := Solution(n-1) + Solution(n-2)
	processed[n] = ways
	return ways
}
