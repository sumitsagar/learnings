package main

func main() {
	dp := make([]int, 600+1)
	println(fibdp(600, &dp))
}

func fibdp(cnt int, dp *[]int) int {
	if (*dp)[cnt] != 0 {
		return (*dp)[cnt]
	}
	if cnt == 0 {
		return 0
	}
	if cnt == 1 {
		return 1
	}
	(*dp)[cnt] = fibdp(cnt-1, dp) + fibdp(cnt-2, dp)
	return (*dp)[cnt]
}
