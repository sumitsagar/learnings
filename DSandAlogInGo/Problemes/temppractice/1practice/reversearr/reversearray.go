package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5}
	reversed := Reverse(arr)
	fmt.Println(reversed)
}

func Reverse(input []int) []int {
	len := len(input)
	reversed := []int{}
	for i := len - 1; i >= 0; i-- {
		reversed = append(reversed, input[i])
	}

	return reversed
}
