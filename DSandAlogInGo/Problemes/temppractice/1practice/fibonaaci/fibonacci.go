package main

import "fmt"

func main() {
	fmt.Println(printnthfib(5))
}

func printnthfib(val int) int {
	if val == 1 {
		return 1
	}
	if val == 0 {
		return 0
	}
	r := printnthfib(val-1) + printnthfib(val-2)
	return r
}
