package main

import "fmt"

func mainnn() {
	retval := productExceptSelf([]int{1, 2, 3, 4})
	fmt.Println(retval)
}

func productExceptSelf(nums []int) []int {

	output := make([]int, len(nums))

	productofLeft := []int{1}

	current_left := 1
	for i := 0; i < len(nums)-1; i++ {
		current_left = current_left * nums[i]
		productofLeft = append(productofLeft, current_left)
	}

	productofRight_temp := []int{1}
	current_right := 1
	for i := len(nums) - 1; i > 0; i-- {
		current_right = current_right * nums[i]
		productofRight_temp = append(productofRight_temp, current_right)
		output[i] = productofLeft[i] * current_right
	}

	return output
}
