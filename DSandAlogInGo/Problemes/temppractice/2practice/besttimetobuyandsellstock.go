package main

import "fmt"

func main1() {
	retval := maxProfit([]int{7, 1, 5, 3, 6, 4})
	fmt.Println(retval)
}
func maxProfit(prices []int) int {
	minsofar := prices[0]
	profit := 0
	for i := 1; i < len(prices); i++ {
		if prices[i] < minsofar {
			minsofar = prices[i]
		}
		profitinsellingthis := prices[i] - minsofar
		if profitinsellingthis > profit {
			profit = profitinsellingthis
		}
	}
	if profit < 0 {
		profit = 0
	}
	return profit
}
