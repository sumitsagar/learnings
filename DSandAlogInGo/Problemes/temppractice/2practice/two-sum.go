package main

import "fmt"

func main() {
	retval := twoSum([]int{3, 2, 4}, 6)
	fmt.Println(retval)
}
func twoSum(nums []int, target int) []int {
	var output []int
	//convert slice to map
	numsMap := make(map[int]int)
	for key, value := range nums {
		numsMap[value] = key
	}
	//find in map
	for key, value := range nums {
		remaining := target - value
		if nextKey, exist := numsMap[remaining]; exist && nextKey != key {
			output = append(output, key, nextKey)
			break
		}
	}

	return output
}
