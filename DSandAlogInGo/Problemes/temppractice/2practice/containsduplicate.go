package main

import "fmt"

func main111() {
	retval := containsDuplicate([]int{1, 2, 3, 4})
	fmt.Println(retval)
}
func containsDuplicate(nums []int) bool {
	m := make(map[int]int)
	for _, val := range nums {
		if _, ok := m[val]; !ok {
			m[val] = 1
		} else {
			return true
		}
	}
	return false
}
