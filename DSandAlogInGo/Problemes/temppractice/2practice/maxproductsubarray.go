package main

import "fmt"

func main11111() {
	retval := maxProduct([]int{-2, 3, -4})
	fmt.Println(retval)
}

func maxProduct(nums []int) int {
	forward := ProcessmaxProduct(nums)
	revesed := reverseInts(nums)
	backward := ProcessmaxProduct(revesed)
	if forward > backward {
		return forward
	}
	return backward
}

func ProcessmaxProduct(nums []int) int {
	prod_sofar := nums[0]
	current_prod := nums[0]

	for i := 1; i < len(nums); i++ {
		currnum := nums[i]
		current_prod *= currnum
		if current_prod > prod_sofar {
			prod_sofar = current_prod
		}
		if currnum > current_prod {
			current_prod = currnum
			if currnum > prod_sofar {
				prod_sofar = currnum
			}
		}
	}
	return prod_sofar
}

func reverseInts(input []int) []int {
	if len(input) == 0 {
		return input
	}
	return append(reverseInts(input[1:]), input[0])
}
