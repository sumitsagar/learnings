package main

import "fmt"

func main11313() {
	retval := maxSubArray([]int{-1, -2})
	fmt.Println(retval)
}

func maxSubArray(nums []int) int {
	sum_sofar := nums[0]
	current_sum := nums[0]

	for i := 1; i < len(nums); i++ {
		currnum := nums[i]
		current_sum += currnum
		if current_sum > sum_sofar {
			sum_sofar = current_sum
		}
		if currnum > current_sum {
			current_sum = currnum
			if currnum > sum_sofar {
				sum_sofar = currnum
			}
		}
	}

	return sum_sofar
}
