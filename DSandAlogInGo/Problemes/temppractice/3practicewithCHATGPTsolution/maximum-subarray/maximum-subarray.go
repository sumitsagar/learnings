package main

import "fmt"

func main() {

	result := maxSubArrayGPT([]int{-2, 1, -3, 4, -1, 2, 1, -5, 4})
	fmt.Println(result)

}

// . The key to solving this problem efficiently is to use dynamic programming, particularly Kadane's algorithm.
// This algorithm goes through the array once, maintaining a running sum of the current subarray and updating the
// maximum subarray sum found so far.

// With this implementation:

// The function max simply returns the larger of two integers.
// maxSum keeps track of the largest subarray sum found so far.
// currentSum keeps track of the sum of the current subarray that ends at nums[i].
// This version should be easier to read and understand, while still solving the problem efficiently with a time complexity of
// �
// (
// �
// )
// O(n).

func maxSubArrayGPT(nums []int) int {
	maxSum := nums[0]     // Initialize maxSum with the first element of the array.
	currentSum := nums[0] // Initialize currentSum with the first element of the array.

	for i := 1; i < len(nums); i++ {
		// Update currentSum by either adding nums[i] to it or starting a new subarray at nums[i].
		currentSum = max(nums[i], currentSum+nums[i])

		// Update maxSum if a larger subarray sum is found.
		maxSum = max(maxSum, currentSum)
	}

	return maxSum
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
