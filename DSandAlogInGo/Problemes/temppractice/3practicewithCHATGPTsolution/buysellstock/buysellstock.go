package main

import "fmt"

func main() {
	// fmt.Println(maxProfit([]int{7, 1, 5, 3, 2, 4}))
	fmt.Println(maxProfit_gpt([]int{7, 1, 5, 3, 2, 4}))
}

func maxProfit_gpt(prices []int) int {
	// Check if the array has less than 2 elements
	// In that case, we can't make any profit as we need at least two days to buy and sell
	if len(prices) < 2 {
		return 0
	}

	// Initialize minPrice with the first price in the array
	// This will keep track of the lowest price we have seen so far
	minPrice := prices[0]

	// Initialize maxProfit as 0
	// This will keep track of the maximum profit we can make
	maxProfit := 0

	// Loop through the prices array starting from index 1
	for i := 1; i < len(prices); i++ {

		// If we find a price lower than minPrice, update minPrice
		// This gives us a new minimum price to consider for future transactions
		if prices[i] < minPrice {
			minPrice = prices[i]
		} else {
			// Calculate the profit we would make if we sold at prices[i]
			// and bought at the lowest price we have seen so far (minPrice)
			currentProfit := prices[i] - minPrice

			// If the current profit is greater than maxProfit, update maxProfit
			// This ensures that maxProfit always contains the highest profit we can achieve
			if currentProfit > maxProfit {
				maxProfit = currentProfit
			}
		}
	}

	// Return the maximum profit we can achieve
	return maxProfit
}

// this is using sliding window technique
func maxProfit(prices []int) int {
	profit := prices
	windowsstartindex := 0
	windowendindex := 1
	maxprofit := 0
	for {

		if windowendindex >= len(profit) {
			break
		}

		if profit[windowendindex] < profit[windowsstartindex] {
			windowsstartindex = windowendindex
			windowendindex = windowsstartindex + 1

		} else {
			profitnow := profit[windowendindex] - profit[windowsstartindex]
			if profitnow > maxprofit {
				maxprofit = profitnow
			}
			if windowendindex == len(profit) { //reached end
				if windowsstartindex < len(profit)-2 {
					windowsstartindex = windowsstartindex + 1
					windowendindex = windowsstartindex + 1
				} else { //all exausted
					break
				}
			} else {
				windowendindex = windowendindex + 1
			}
		}
	}
	return maxprofit
}
