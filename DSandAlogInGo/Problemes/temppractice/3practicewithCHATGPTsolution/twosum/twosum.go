package main

import "fmt"

func main() {
	input := []int{11, 33, 2, 7, 11, 15}
	target := 9

	for index, j := range input {
		for k := index + 1; k < len(input)-1; k++ {
			if j+input[k] == target {
				fmt.Println([]int{index, k})
				break
			}
		}
	}
}
