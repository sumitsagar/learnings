package main

import "fmt"

func main() {
	// Call the productExceptSelf function and store the result in 'result'
	result := productExceptSelf([]int{1, 2, 3, 4, 5})
	fmt.Println(result) // Output should be [120, 60, 40, 30, 24]
}

// Function to compute product of all elements except self for each index in 'nums'
func productExceptSelf(nums []int) []int {
	// Initialize 'retval' as an integer slice to store the final result
	retval := make([]int, len(nums))

	// Initialize 'productarr' to store the running product from left to right
	productarr := make([]int, len(nums))

	// Compute running product from left to right
	for i := 0; i < len(nums); i++ {
		// If it's the first element, directly copy it to 'productarr'
		if i == 0 {
			productarr[i] = nums[i]
		} else {
			// Else, multiply the previous running product with the current element
			productarr[i] = productarr[i-1] * nums[i]
		}
	}

	// Initialize 'productarr_rev' to store the running product from right to left
	productarr_rev := make([]int, len(nums))

	// Compute running product from right to left
	for i := len(nums) - 1; i >= 0; i-- {
		// If it's the last element, directly copy it to 'productarr_rev'
		if i == len(nums)-1 {
			productarr_rev[i] = nums[i]
		} else {
			// Else, multiply the next running product with the current element
			productarr_rev[i] = productarr_rev[i+1] * nums[i]
		}
	}

	// Populate 'retval' with the final result
	for i := 0; i < len(nums); i++ {
		// Handle the edge case for the first element
		if i == 0 {
			retval[i] = productarr_rev[i+1]
		} else if i == len(nums)-1 { // Handle the edge case for the last element
			retval[i] = productarr[i-1]
		} else { // Handle all the middle elements
			retval[i] = productarr[i-1] * productarr_rev[i+1]
		}
	}

	// Return the result
	return retval
}
