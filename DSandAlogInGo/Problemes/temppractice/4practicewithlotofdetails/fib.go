package practice

import "fmt"

func main() {
	memo := make(map[int]int)
	fmt.Println(fib(55, memo))
}

func fib(index int, memo map[int]int) int {

	val, exists := memo[index]
	if exists {
		return val
	}
	if index <= 2 {
		return 1
	}
	memo[index] = fib(index-1, memo) + fib(index-2, memo)
	return memo[index]
}
