package practice

import "sort"

// For the 3Sum problem, where you need to find triplets in an array that add up to zero without duplicates,
// let's evaluate the suitability of various algorithmic techniques:

// Brute Force:

// Usefulness: Can be used.
// Reason: A brute force approach would involve checking all possible triplets in the array to see if they sum to zero.
// This is straightforward but highly inefficient, especially for large arrays, as it has a time complexity of O(n³),
//  where n is the number of elements in the array.
// Sliding Window:

// Usefulness: Not suitable.
// Reason: Sliding window is typically used for contiguous subarray problems (e.g., finding the longest subarray with a given sum).
//  The 3Sum problem requires considering non-contiguous elements and checking their sum, making the sliding window technique inappropriate here.
// Dynamic Programming:

// Usefulness: Not suitable.
// Reason: Dynamic programming is used for optimization problems involving overlapping subproblems and optimal substructure,
//  usually with a recursive nature. The 3Sum problem doesn't have overlapping subproblems that can be reused, so dynamic programming
//  doesn't provide an advantage.
// Divide and Conquer:

// Usefulness: Not typically used.
// Reason: While divide and conquer can be applied to some extent (like initially sorting the array),
// the problem doesn't naturally break down into smaller instances of the same problem, which can be independently solved and then combined.
// Hence, it's not the most efficient approach for this problem.
// Greedy Algorithm:

// Usefulness: Not suitable.
// Reason: Greedy algorithms make the locally optimal choice at each step with the hope of finding a global optimum. In the 3Sum problem,
// local decisions (like picking the smallest or largest elements) do not necessarily lead to an overall solution, as it requires considering all
// combinations to find the triplets.

// The most efficient known approach for the 3Sum problem involves sorting the array and then using a two-pointer technique,
// which is a variant of the brute force approach but with reduced complexity. This method does not strictly fall into the categories of greedy,
//  dynamic programming, sliding window, or divide and conquer techniques. It's more of an optimized brute force approach,
// where sorting helps in skipping duplicates and reducing the search space effectively.

func ThreeSum(nums []int) [][]int {
	sort.Ints(nums)
	var triplets [][]int

	for i := 0; i < len(nums)-2; i++ {
		// Skip duplicates for the first number
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}

		left := i + 1
		right := len(nums) - 1

		for left < right {
			sum := nums[i] + nums[left] + nums[right]
			if sum < 0 {
				left++
			} else if sum > 0 {
				right--
			} else {
				triplets = append(triplets, []int{nums[i], nums[left], nums[right]})
				// Skip duplicates for the second and third numbers
				for left < right && nums[left] == nums[left+1] {
					left++
				}
				for left < right && nums[right] == nums[right-1] {
					right--
				}
				left++
				right--
			}
		}
	}

	return triplets
}
