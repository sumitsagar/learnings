/*
Sliding Window: This technique involves maintaining a window that slides over the data to track a subset of it. However, in this problem,

	since we are looking at the global maximum profit rather than a local one, the sliding window technique might not be the best fit.

Brute Force: Iterate over each pair of days and calculate the profit for each pair. This approach, while straightforward,
is not efficient for large arrays as it has a time complexity of O(n²), where n is the number of days.

Dynamic Programming: This technique involves breaking down a problem into simpler subproblems and storing

	the solution to each subproblem to avoid redundant calculations.

Greedy Algorithm: This is the approach used in the solution I provided. A greedy algorithm makes the optimal choice at each step as it attempts to find the overall optimal way to solve the entire problem. In this case, the algorithm greedily updates the minimum price and the maximum profit at each step.

	This method is efficient and has a time complexity of O(n).

Divide and Conquer: This approach divides the problem into two halves, finds the maximum profit in each half, and then combines the results. The maximum profit could come from either half or a combination of a buying price in the first half and a selling price in the second half. While this method can be used, it's more complex and doesn't

	provide a significant advantage over the greedy approach for this problem.
*/
package practice

// this is solution using greedy method
func MaxProfit(prices []int) int {
	if len(prices) == 0 {
		return 0
	}

	minPrice := prices[0]
	maxProfit := 0

	for _, price := range prices {
		if price < minPrice {
			minPrice = price
		} else if profit := price - minPrice; profit > maxProfit {
			maxProfit = profit
		}
	}

	return maxProfit
}
