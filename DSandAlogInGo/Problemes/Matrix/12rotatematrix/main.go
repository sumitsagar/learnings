package main

import "fmt"

func main() {
	matrizsize := 3
	input := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	print(input, matrizsize)
}

func print(arr []int, size int) {
	cntr := 0
	curindex := 0
	for curindex < len(arr) {
		fmt.Print(arr[curindex], "     ")
		curindex = curindex + 1
		cntr = cntr + 1
		if cntr == size {
			fmt.Println()
			cntr = 0
		}
	}
}
