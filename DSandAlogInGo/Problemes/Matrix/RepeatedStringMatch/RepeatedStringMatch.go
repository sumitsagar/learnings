package main

import (
	"fmt"
	"strings"
)

func main() {
	//fmt.Println(repeatedStringMatch("abc", "wxyz"))
	fmt.Println(repeatedStringMatch("sumit", "sumitsumitsum"))
}
func repeatedStringMatch(A string, B string) int {
	max := len(B)/len(A) + 2
	tmp := strings.Repeat(A, max)
	if !strings.Contains(tmp, B) {
		return -1
	}
	for {
		tmp = strings.Repeat(A, max)
		if !strings.Contains(tmp, B) {
			return max + 1
		}
		max--
	}
}

func repeatedStringMatchByMe(a string, b string) int {
	return repeatedStringMatchProcessByMe(a, b, "")
}
func repeatedStringMatchProcessByMe(a string, b string, flag string) int {
	if len(b) == 0 {
		return 0
	}
	if len(b) < len(a) {
		if flag == "P" {
			if strings.Index(a, b)+len(b) == len(a) {
				return 1
			} else {
				return -1
			}
		} else if flag == "S" {
			if strings.Index(a, b) == 0 {
				return 1
			} else {
				return -1
			}
		} else if strings.Contains(a, b) {
			return 1
		}
	}
	count := 0
	startindex := strings.Index(b, a)
	if startindex >= 0 {
		count++
		remainingstart := b[0:startindex]
		remainingend := b[startindex+len(a):]
		p := repeatedStringMatchProcessByMe(a, remainingstart, "P")
		s := repeatedStringMatchProcessByMe(a, remainingend, "S")
		if p == -1 || s == -1 {
			return -1
		} else {
			count += (p + s)
		}
	}
	if count > 0 {
		return count
	} else {
		return -1
	}
}
