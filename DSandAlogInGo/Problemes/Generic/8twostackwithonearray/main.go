package main

const size = 10

var arr [size]int32
var size_arr1 int32
var size_arr2 int32

func int() {
	size_arr1 = 0
	size_arr2 = 0
}
func main() {
	push_arr1(1)
	push_arr1(2)
	push_arr1(3)
	pop_arr1()
	push_arr1(4)

	push_arr2(1)
	push_arr2(2)
	push_arr2(3)
	pop_arr2()
	push_arr2(4)

	PrintArraItem()
}

func push_arr1(data int32) {
	indextopushdata := size_arr1
	arr[indextopushdata] = data
	size_arr1 += 1
}
func push_arr2(data int32) {
	indextopushdata := (size - 1) - size_arr2
	arr[indextopushdata] = data
	size_arr2 += 1
}

func pop_arr1() {
	indextoremovedata := size_arr1 - 1
	arr[indextoremovedata] = 0
	size_arr1 -= 1
}
func pop_arr2() {
	indextoremovedata := (size - size_arr2) - 1
	arr[indextoremovedata] = 0
	size_arr2 -= 1
}

func PrintArraItem() {
	for _, val := range arr {
		print(val)
	}
}
