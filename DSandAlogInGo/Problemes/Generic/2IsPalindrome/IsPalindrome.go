package main

import (
	commonimpl "dsalgo/Problemes/Generic/CommonImpl"
	"fmt"
)

func main() {
	r := IsPalindrome("hamah")
	fmt.Println(r)
}

func IsPalindrome(s string) bool {
	r := commonimpl.ReverseString(s)
	if s == r {
		return true
	}
	return false
}
