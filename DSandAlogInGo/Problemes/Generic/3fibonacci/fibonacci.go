package main

import (
	"fmt"
)

func main() {
	//for getting nth fibonacci number
	n := uint(8)
	fmt.Println(fib(n))

	//for printing fibonacci series using recursion
	PrintFibonacci()
}

func fib(n uint) uint {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return fib(n-1) + fib(n-2)
	}
}

// for printing
func PrintFibonacci() {
	beginnig1 := 0.0
	beginnig2 := 1.0
	maxval := 100.0
	println(beginnig1)
	println(beginnig2)
	PrintNextFibonacci(beginnig1, beginnig2, maxval)
}

func PrintNextFibonacci(val, val2, maxval float64) {
	val3 := val + val2
	fmt.Println(int(val3))
	if val3 < maxval {
		PrintNextFibonacci(val2, val3, maxval)
	}
}
