package main

import (
	queue "dsalgo/DataStructure/Queue"
	"fmt"
)

func main() {
	q := queue.Queue{}
	q.Enqueue(1)
	q.Enqueue(2)
	q.Enqueue(3)
	q.Enqueue(4)
	fmt.Println(q.DEqueue())
	fmt.Println(q.DEqueue())
	fmt.Println(q.DEqueue())
	fmt.Println(q.DEqueue())
}
