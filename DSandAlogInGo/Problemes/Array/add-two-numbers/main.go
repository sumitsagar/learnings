package main

import (
	linkedlist "dsalgo/DataStructure/LinkedList"
	"fmt"
	"strconv"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	lst1 := NumberToLinkedList(249)
	lst2 := NumberToLinkedList(5649)
	l1 := lst1.Head
	l2 := lst2.Head
	currentnode := addTwoNumbers(l1, l2)

	for {
		fmt.Println(currentnode.Val)
		if currentnode.Next != nil {
			currentnode = currentnode.Next
		} else {
			break
		}
	}
}

func addTwoNumbers(l1 *linkedlist.Node, l2 *linkedlist.Node) *ListNode {

	num1 := strconv.Itoa(l1.Val)
	curnode := l1
	for {
		if curnode.Next != nil {
			curnode = curnode.Next
			num1 = num1 + strconv.Itoa(curnode.Val)
		} else {
			break
		}
	}

	num2 := strconv.Itoa(l2.Val)
	curnode = l2
	for {
		if curnode.Next != nil {
			curnode = curnode.Next
			num2 = num2 + strconv.Itoa(curnode.Val)
		} else {
			break
		}
	}

	println("n1", num1)
	println("n2", num2)

	num1asint, _ := strconv.Atoi(num1)
	num2asint, _ := strconv.Atoi(num2)
	sum := num1asint + num2asint
	println("sum", sum)
	numasStr := strconv.Itoa(sum)
	var firstnode *ListNode
	var lastaddednode *ListNode

	for i := len(numasStr) - 1; i >= 0; i-- {
		var listNode ListNode
		asint, _ := strconv.Atoi(string(numasStr[i]))
		listNode.Val = asint
		if i == len(numasStr)-1 {
			firstnode = &listNode

		} else {
			lastaddednode.Next = &listNode
		}
		lastaddednode = &listNode
	}

	return firstnode
}

func NumberToLinkedList(num int) linkedlist.LinkedList {

	lst := linkedlist.LinkedList{}
	numasStr := strconv.Itoa(num)
	for _, char := range numasStr {

		var node1 linkedlist.Node
		asint, _ := strconv.Atoi(string(char))
		node1.Val = asint
		lst.AddNode(&node1)
	}
	return lst

}
