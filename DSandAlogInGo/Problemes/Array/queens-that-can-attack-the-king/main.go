package main

import "fmt"

func main() {
	//r := queensAttacktheKing([][]int{{0, 1}, {1, 0}, {4, 0}, {0, 4}, {3, 3}, {2, 4}}, []int{0, 0})
	r := queensAttacktheKing([][]int{{0, 1}, {0, 4}}, []int{0, 0})
	fmt.Println(r)
}
func queensAttacktheKing(queens [][]int, king []int) [][]int {
	rowmap := make(map[int][]int)
	colmap := make(map[int][]int)

	var diagnalq [][]int
	var canattack [][]int

	for _, i := range queens {
		isdiagonalelement := isDiagnalElement(i, king)
		if i[0] == king[0] || i[1] == king[1] || isdiagonalelement {

			rownum := i[1]
			colnum := i[0]

			if isdiagonalelement {
				diagnalq = append(diagnalq, i)
			} else {
				if rownum == king[1] {
					if rowmap[rownum] != nil { // process queen on same column
						if rowmap[rownum][1] > i[1] {
							rowmap[rownum] = i
						}
					} else {
						rowmap[rownum] = i
					}
				}

				if colnum == king[0] {
					if colmap[colnum] != nil { // process queen on same row
						if colmap[colnum][0] > i[0] {
							colmap[colnum] = i
						}
					} else {
						colmap[colnum] = i
					}
				}
			}
		}
	}

	if rowmap != nil {
		for _, val := range rowmap {
			if !Contains(canattack, val) {
				canattack = append(canattack, val)
			}
		}
	}

	if colmap != nil {
		for _, val := range colmap {
			if !Contains(canattack, val) {
				canattack = append(canattack, val)
			}
		}
	}

	return canattack
}

func isDiagnalElement(queen []int, king []int) bool {
	if queen[0]-king[0] == queen[1]-king[1] {
		return true
	} else {
		return false
	}
}

func Contains(arr [][]int, i []int) bool {
	for _, itm := range arr {
		if itm[0] == i[0] && itm[1] == i[1] {
			return true
		}
	}
	return false
}
