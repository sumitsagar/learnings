package main

func main() {
	intput := []int{1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 6, 7, 8}
	valandcountmap := make(map[int]int)

	// lets keep a map of found values
	for _, val := range intput {
		_, ok := valandcountmap[val] //check if key exists
		if !ok {
			valandcountmap[val] = 1 //set value 1
		} else {
			valandcountmap[val] += 1 //increment
		}
	}

	for k, v := range valandcountmap {
		if v == 1 {
			println(k)
			break
		}
	}
}
