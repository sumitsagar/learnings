package main

import "fmt"

func main() {
	li := maxArea([]int{1, 8, 6, 2, 5, 4, 8, 3, 7})
	fmt.Println(li)
}

func maxArea(height []int) int {

	maxarea := 0
	a := 0
	b := 0

	for i1, val1 := range height {
		for i2, val2 := range height {
			if i1 < i2 {
				width := i2 - i1
				height := 0
				if val1 > val2 {
					height = val2
				} else {
					height = val1
				}
				area := width * height
				if area > maxarea {
					maxarea = area
					a = i1
					b = i2
				}
			}
		}
	}
	fmt.Println(a, " ", b)
	return maxarea
}
