package main

import "fmt"

func main() {
	arr := []int{4, 5, 1, 8, 2}
	fmt.Println(arr)
	productExceptSelf(arr)
}

func productExceptSelf(nums []int) []int {

	output := []int{}

	productofLeft := []int{1}
	productofRight := []int{}

	current_left := 1
	for i := 0; i < len(nums)-1; i++ {
		current_left = current_left * nums[i]
		productofLeft = append(productofLeft, current_left)
	}

	productofRight_temp := []int{1}
	current_right := 1
	for i := len(nums) - 1; i > 0; i-- {
		current_right = current_right * nums[i]
		productofRight_temp = append(productofRight_temp, current_right)
	}

	for i := len(productofRight_temp) - 1; i >= 0; i-- {
		productofRight = append(productofRight, productofRight_temp[i])
	}

	for i := 0; i < len(nums); i++ {

		output = append(output, productofLeft[i]*productofRight[i])
	}

	fmt.Println("left ", productofLeft)
	fmt.Println("right ", productofRight)
	fmt.Println("output ", output)
	return output
}
