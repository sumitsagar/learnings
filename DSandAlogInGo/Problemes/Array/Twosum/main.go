package main

import "fmt"

func main() {
	r := twoSum([]int{3, 2, 4}, 6)
	fmt.Println(r)
}

func twoSum(nums []int, target int) []int {
	for i, n := range nums {
		for j, m := range nums {
			if i != j && n+m == target {
				return []int{i, j}
			}
		}
	}
	return []int{}
}
