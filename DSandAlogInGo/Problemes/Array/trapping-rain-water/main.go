package main

import "fmt"

func main() {
	li := trap([]int{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1})
	fmt.Println(li)
}

func trap(height []int) int {
	trap := 0
	maxindex := len(height) - 1
	for trapindex := 0; trapindex <= maxindex-1; trapindex++ {
		lmax := 0
		rmax := 0
		//calculating max on left of current index
		for left := trapindex - 1; left >= 0; left-- {
			if height[left] > lmax {
				lmax = height[left]
			}
		}
		//calculating max on right of current index
		for right := trapindex + 1; right <= maxindex; right++ {
			if height[right] > rmax {
				rmax = height[right]
			}
		}
		//add the min heigh to trap value minus the heigh of pillar of current trap index
		if lmax < rmax {
			if lmax > height[trapindex] {
				trap = trap + lmax - height[trapindex]
			}
		} else {
			if rmax > height[trapindex] {
				trap = trap + rmax - height[trapindex]
			}
		}
	}

	return trap
}

// func trap(height []int) int {
// 	trap := 0
// 	bucketstartindex := 0
// 	for i1, val1 := range height {
// 		if bucketstartindex > 0 && i1 <= bucketstartindex {
// 			//do nothing..
// 		} else {
// 			if val1 > 0 {
// 				bucketStartHeigh := val1
// 				for i2, val2 := range height {
// 					if i2 > i1 {
// 						if val2 >= val1 {
// 							bucketstartindex = i2
// 							break
// 						}
// 						trap = trap + (bucketStartHeigh - val2)
// 					}
// 				}
// 			}
// 		}
// 	}
// 	return trap
// }
