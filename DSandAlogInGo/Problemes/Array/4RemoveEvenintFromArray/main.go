package main

import "fmt"

func main() {
	input := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	output := RemoveEven(input)
	fmt.Println(output)
}
func RemoveEven(input []int) []int {
	output := []int{}
	for _, val := range input {
		if val%2 != 0 {
			output = append(output, val)
		}
	}
	return output
}
