package main

func main() {
	input := []int{1, -2147483648, 2}
	thirdMax := thirdMax(input)
	println(thirdMax)
}
func thirdMax(nums []int) int {
	max := -2147483649
	max_2nd := -2147483649
	max_3rd := -2147483649
	thirdrdmaxset := false

	for _, val := range nums {
		if val == max {
			//do nothing
		} else if val > max {

			//now propagate down max values
			max_2nd_temp := max_2nd
			if max_2nd < max {
				max_2nd = max
			}
			if max_3rd < max_2nd_temp {
				thirdrdmaxset = true
				max_3rd = max_2nd_temp
			}
			max = val

		} else if val == max_2nd {
			//do nothing
		} else if val > max_2nd {
			//now propagate down max values
			if max_3rd < max_2nd {
				thirdrdmaxset = true
				max_3rd = max_2nd
			}
			max_2nd = val

		} else if val >= max_3rd {
			thirdrdmaxset = true
			max_3rd = val
		}
	}

	if max_3rd == -2147483649 && !thirdrdmaxset {
		return max // incase there is no third max then we have to return max
	}
	return max_3rd
}
