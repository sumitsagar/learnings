package main

import "fmt"

func main() {
	arr1 := []int{1, 2, 3, 0, 0, 0}
	arr2 := []int{2, 5, 6}
	merge(arr1, 3, arr2, 3)

	output := []int{}

	index_arr1 := 0
	index_arr2 := 0

	for true {
		if index_arr1 >= len(arr1) { //means this array is full read already
			if index_arr2 < len(arr2) {
				output = append(output, arr2[index_arr2])
				index_arr2 += 1
			} else {
				break
			}
		} else if index_arr2 >= len(arr2) { //means this array is full read already
			if index_arr1 < len(arr1) {
				output = append(output, arr1[index_arr1])
				index_arr1 += 1
			} else {
				break
			}
		} else if arr1[index_arr1] <= arr2[index_arr2] {
			output = append(output, arr1[index_arr1])
			index_arr1 += 1
		} else {
			output = append(output, arr2[index_arr2])
			index_arr2 += 1
		}
	}

	fmt.Println(output)

}

//ignore below code.. this was for leet code ..
func merge(arr1 []int, m int, arr2 []int, n int) {
	output := []int{}

	index_arr1 := 0
	index_arr2 := 0

	for true {
		if index_arr1 >= m { //means this array is full read already
			if index_arr2 < n {
				output = append(output, arr2[index_arr2])
				index_arr2 += 1
			} else {
				break
			}
		} else if index_arr2 >= n { //means this array is full read already
			if index_arr1 < m {
				output = append(output, arr1[index_arr1])
				index_arr1 += 1
			} else {
				break
			}
		} else if arr1[index_arr1] <= arr2[index_arr2] {
			output = append(output, arr1[index_arr1])
			index_arr1 += 1
		} else {
			output = append(output, arr2[index_arr2])
			index_arr2 += 1
		}
	}

	fmt.Println(output)
}
