package main

import (
	"fmt"
)

func countTriplets(arr []int64, r int64) int64 {

	// 1 3 9 9 27 81
	x := int64(0)
	p := make(map[int64]int64)
	s := make(map[int64]int64)

	for i := len(arr) - 1; i >= 0; i-- {
		item := arr[i]
		next := item * r
		if v, ok := p[next]; ok {
			x += v
		}
		if v, ok := s[next]; ok {
			p[item] += v
		}

		s[item]++
	}
	return x
}

// Complete the countTriplets function below.
func countTripletsByMe(arr []int64, r int64) int64 {
	triplets := 0
	if len(arr) <= 3 {
		return 0
	}

	m := make(map[int]int)

	for i := 0; i < len(arr)-2; i++ {
		_, exists := m[i]
		if exists && arr[i]%r != 0 {
			m[i] = 0
			break

		}
		for j := i + 1; j < len(arr)-1; j++ {

			_, exists := m[j]
			if !exists && arr[j]%r != 0 {
				m[j] = 0
				break

			}
			for k := j + 1; k < len(arr); k++ {
				_, exists := m[k]
				if exists && arr[k]%r != 0 {
					m[k] = 0
					break
				}
				if arr[k]/arr[j] == r && arr[j]/arr[i] == r {
					triplets++
				}
			}
		}
	}
	return int64(triplets)
}

func main() {

	arr := []int64{1, 3, 7, 9, 9, 27, 81}
	ans := countTriplets(arr, 3)
	fmt.Println(ans)
}
