package main

import "fmt"

func main() {
	li := FindLuckInt([]int{1, 2, 2, 3, 3, 3})
	fmt.Println(li)
}

func FindLuckInt(arr []int) int {
	cntmap := make(map[int]int)
	retval := -1

	for _, v := range arr {
		cntmap[v]++
	}

	for i, v := range cntmap {
		if i == v {
			if i > retval {
				retval = i
			}
		}
	}
	return retval
}
