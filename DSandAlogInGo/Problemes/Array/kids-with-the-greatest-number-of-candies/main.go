package main

import "fmt"

func main() {
	r := kidsWithCandies([]int{3, 2, 4}, 6)
	fmt.Println(r)
}

func kidsWithCandies(candies []int, extraCandies int) []bool {

	max := GetMaxVal(candies)
	retval := []bool{}

	for _, i := range candies {
		if i+extraCandies >= max {
			retval = append(retval, true)
		} else {
			retval = append(retval, false)
		}
	}
	return retval

}

func GetMaxVal(candies []int) int {
	max := 0
	for _, i := range candies {
		if i > max {
			max = i
		}
	}
	return max
}
