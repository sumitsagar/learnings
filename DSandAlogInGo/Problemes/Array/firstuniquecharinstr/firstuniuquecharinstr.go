package main

func main() {
	s := "leetcode"
	indexoffirstuniquechar := firstUniqChar(s)
	println("index of unuqie char", indexoffirstuniquechar)
}

func firstUniqChar(s string) int {
	intput := []string{} //input as char array
	for _, ch := range s {
		intput = append(intput, string(ch))
	}
	valandcountmap := make(map[string]int)

	// lets keep a map of found values
	for _, val := range intput {
		_, ok := valandcountmap[val] //check if key exists
		if !ok {
			valandcountmap[val] = 1 //set value 1
		} else {
			valandcountmap[val] += 1 //increment
		}
	}

	for index, ch := range s {
		println(index)
		if valandcountmap[string(ch)] == 1 {
			return index
		}
	}
	return -1
}
