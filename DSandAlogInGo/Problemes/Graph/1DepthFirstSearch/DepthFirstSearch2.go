package main

import "fmt"

func main() {
	graph := make(map[string][]string)
	graph["a"] = []string{"b", "c"}
	graph["b"] = []string{"d"}
	graph["c"] = []string{"e"}
	graph["d"] = []string{"f"}
	graph["e"] = []string{}
	graph["f"] = []string{}
	fmt.Println(DFS(&graph, "a"))
}

func DFS(graph *map[string][]string, firstnode string) []string {
	retval := []string{firstnode}
	stack := (*graph)[firstnode]
	for len(stack) > 0 {
		popitem := stack[len(stack)-1]
		stack = stack[0 : len(stack)-1] //remove last item
		// print(popitem)
		retval = append(retval, popitem)
		neighbours := (*graph)[popitem]
		stack = append(stack, neighbours...)
	}

	return retval

}
