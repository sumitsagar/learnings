package main

import "fmt"

func main1() {
	graph := make(map[string][]string)
	graph["a"] = []string{"b", "c"}
	graph["b"] = []string{"d"}
	graph["c"] = []string{"e"}
	graph["d"] = []string{"f"}
	graph["e"] = []string{}
	graph["f"] = []string{}
	fmt.Println(DFS1(&graph, "a"))
	//fmt.Println(DFSRecursive(&graph, "a"))
}

// iterative approach
// star with start and push neighbour on stack
func DFS1(graph *map[string][]string, firstnodekey string) []string {
	firstnodeval := (*graph)[firstnodekey]
	result := []string{firstnodekey} // push start node to result
	stack := firstnodeval
	for len(stack) > 0 {
		item := stack[len(stack)-1]     //pop from top .. i.e. last item
		result = append(result, item)   // push to result
		stack = stack[0 : len(stack)-1] //remove last item
		neighbours := (*graph)[item]
		stack = append(stack, neighbours...) //push neighbours to stack
	}
	return result
}

// func getFirstNode(graph *map[string][]string) (string, []string) {
// 	for k, val := range *graph {
// 		return k, val
// 	}
// 	return "", []string{}
// }

//recursive approach
// func DFSRecursive(graph *map[string][]string, node string) []string {
// 	result := []string{}
// 	neighbours := (*graph)[node]
// 	result = append(result, node) //append parent node
// 	for _, val := range neighbours {
// 		resultFromSubGraph := DFSRecursive(graph, val) // here val is new node
// 		result = append(result, resultFromSubGraph...)
// 	}
// 	return result
// }
