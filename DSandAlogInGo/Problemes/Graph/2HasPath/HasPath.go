package main

import "fmt"

func main() {
	graph := make(map[string][]string)
	graph["a"] = []string{"b", "c"}
	graph["b"] = []string{"d"}
	graph["c"] = []string{"e"}
	graph["d"] = []string{"f"}
	graph["e"] = []string{}
	graph["f"] = []string{}
	fmt.Println(HasPath(&graph, "a", "e"))
	fmt.Println(HasPath(&graph, "b", "e"))
	fmt.Println(HasPathBFS(&graph, "a", "e"))
	fmt.Println(HasPathBFS(&graph, "b", "e"))
}

//defth first apprach
func HasPath(graph *map[string][]string, src, dest string) bool {
	if src == dest {
		return true
	}
	for _, neighbour := range (*graph)[src] {
		haspth := HasPath(graph, neighbour, dest)
		if haspth {
			return true
		}
	}
	return false
}

func HasPathBFS(graph *map[string][]string, src, dest string) bool {
	queue := []string{src}
	for len(queue) > 0 {
		item := queue[0]
		if len(queue) > 1 {
			queue = queue[1:]
		} else {
			queue = []string{}
		}
		for _, neighbour := range (*graph)[item] {
			if neighbour == dest {
				return true
			}
			queue = append(queue, neighbour)
		}
	}
	return false
}
