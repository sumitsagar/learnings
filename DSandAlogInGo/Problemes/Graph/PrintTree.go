package main

import "fmt"

type TreeNode struct {
	Value string
	Left  *TreeNode
	Right *TreeNode
}

// printTree calls the recursive helper function to print the tree
func PrintTree(root *TreeNode) {
	depth := maxDepth(root)
	printTreeHelper(root, 0, depth)
}

// printTreeHelper is the recursive function to print the tree nodes
func printTreeHelper(node *TreeNode, space int, height int) {
	if node == nil {
		return
	}

	// Increase distance between levels
	space += height

	// Process right child first
	printTreeHelper(node.Right, space, height)

	// Print current node after space count
	fmt.Println()
	for i := height; i < space; i++ {
		fmt.Print(" ")
	}
	fmt.Println(node.Value)

	// Process left child
	printTreeHelper(node.Left, space, height)
}

// maxDepth calculates the maximum depth of the tree
func maxDepth(node *TreeNode) int {
	if node == nil {
		return 0
	}
	leftDepth := maxDepth(node.Left)
	rightDepth := maxDepth(node.Right)
	if leftDepth > rightDepth {
		return leftDepth + 1
	}
	return rightDepth + 1
}
