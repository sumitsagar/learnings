package main

import "fmt"

func main() {

	input := "eceba"
	m := make(map[string]int)

	for i := 0; i < len(input); i++ {
		c := string(input[i])
		// fmt.Println(c)
		_, exist := m[c]
		if exist {
			m[c] = m[c] + 1
		} else {
			m[c] = 1
		}
	}

	fmt.Println(m)

}
