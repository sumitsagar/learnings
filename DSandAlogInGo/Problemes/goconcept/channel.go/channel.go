package main

import "fmt"

func main() {
	ch := make(chan int, 5)

	go func() {
		fmt.Println(<-ch)
	}()
	ch <- 5
}
