package main

import "fmt"

func main() {
	g := []int{10, 9, 8, 7}
	s := []int{5, 6, 7, 8}
	r := findContentChildren(g, s)
	fmt.Println(r)
}

func findContentChildren(g []int, s []int) int {
	g = Sort(g)
	s = Sort(s)
	contentchilderen := 0
	nextchildtofeed := 0
	for i := 0; i < len(s); i++ {
		if s[i] >= g[nextchildtofeed] {
			contentchilderen++
			nextchildtofeed++
		}
		if nextchildtofeed >= len(g) {
			break
		}
	}
	return contentchilderen
}

func Sort(arr []int) []int {

	swapped := false
	paas := 0

	for {
		paas = paas + 1
		//fmt.Println(paas)
		swapped = false
		for index, val := range arr {
			if index < len(arr)-1 {
				if val > arr[index+1] {
					temp := arr[index+1]
					arr[index+1] = val
					arr[index] = temp
					swapped = true
				}
			}
		}
		if !swapped {
			break
		}
	}
	return arr

}
