package main

import (
	"dsalgo/DataStructure/BinarySearchTree"
)

func main() {

	input := []int{50, 30, 20, 40, 70, 60, 80}
	rootnode := BinarySearchTree.CreateFromArray(input)
	k := 2
	FindKthLargest(rootnode, k)
}

func FindKthLargest(rootnode *BinarySearchTree.TreeNode, kthlargest int) {

	BinarySearchTree.ReverseOfInorderRecursive(rootnode)
	BinarySearchTree.PrintKthLargest(rootnode, 2)
}

// 	node := rootnode
// 	for i := 0; i < kthlargest; i++ {
// 		if node.Right == nil {
// 			return node.Val
// 		}
// 		node = node.Right
// 	}
// 	return node.Val
// }
