package main

import (
	"dsalgo/DataStructure/BinarySearchTree"
)

func main() {

	input := []int{3, 5, 6, 7, 8, 2, 11, 19, 18}
	rootnode := BinarySearchTree.CreateFromArray(input)
	minval := FindMinumumInBST(rootnode)
	print(minval)
}

func FindMinumumInBST(rootnode *BinarySearchTree.TreeNode) int {
	node := rootnode
	for node.Left != nil {
		node = node.Left
	}
	return node.Val
}
