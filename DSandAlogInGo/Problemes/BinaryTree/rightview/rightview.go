package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

var arr []int64
var m map[int]bool

func main() {
	m = make(map[int]bool)

	root := &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}

	rightSideView(root)
}
func rightSideView(root *binarytree.BinaryNode) []int64 {
	arr = []int64{}
	if root == nil {
		return arr
	}
	arr = append(arr, root.Data)
	Traverse(root, 1)
	fmt.Println(arr)
	return arr
}

func Traverse(node *binarytree.BinaryNode, level int) {

	//traverse right subtree
	if node.Right != nil {
		if !m[level] {
			arr = append(arr, node.Right.Data)
		}
		m[level] = true
		level = level + 1
		Traverse(node.Right, level)

		//traverse left subtree
	} else if node.Left != nil {
		if !m[level] {
			arr = append(arr, node.Left.Data)
		}
		m[level] = true
		level = level + 1
		Traverse(node.Left, level)
	}
}
