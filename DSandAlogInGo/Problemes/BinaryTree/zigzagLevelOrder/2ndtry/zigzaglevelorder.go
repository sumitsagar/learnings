package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	t1 := &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	t1.Left = &binarytree.BinaryNode{Data: 9, Left: nil, Right: nil}
	t1.Right = &binarytree.BinaryNode{Data: 20, Left: nil, Right: nil}
	t1.Right.Left = &binarytree.BinaryNode{Data: 15, Left: nil, Right: nil}
	t1.Right.Right = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}
	r := zigzagLevelOrder(t1)
	fmt.Println(r)
}

func zigzagLevelOrder(root *binarytree.BinaryNode) [][]int {
	return zigzagLevelOrderProcess(root, true)
}
func zigzagLevelOrderProcess(root *binarytree.BinaryNode, ltr bool) [][]int {
	if root == nil {
		return [][]int{}
	}
	result := [][]int{}
	if ltr {
		data := []int{}
		data = append(data, int(root.Left.Data))
		data = append(data, int(root.Right.Data))
		result = append(result, data)
	} else {
		data := []int{}
		data = append(data, int(root.Right.Data))
		data = append(data, int(root.Left.Data))
		result = append(result, data)
	}
	dtl := zigzagLevelOrderProcess(root.Left, !ltr)
	dtr := zigzagLevelOrderProcess(root.Right, !ltr)
	result = append(result, dtl...)
	result = append(result, dtr...)
	return result
}
