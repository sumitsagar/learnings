package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	t1 := &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	t1.Left = &binarytree.BinaryNode{Data: 9, Left: nil, Right: nil}
	t1.Right = &binarytree.BinaryNode{Data: 20, Left: nil, Right: nil}
	t1.Right.Left = &binarytree.BinaryNode{Data: 15, Left: nil, Right: nil}
	t1.Right.Right = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}
	r := LevelOrderZigzag(t1)
	fmt.Println(r)
}

func LevelOrderZigzag(root *binarytree.BinaryNode) [][]int {

	finalval := [][]int{}
	if root == nil {
		return finalval
	}

	currlevelnodes := []*binarytree.BinaryNode{root}
	reverse := true
	for len(currlevelnodes) > 0 {
		// if reverse {
		// 	currlevelnodes = reverseArray(currlevelnodes)
		// }
		nextlevelnodes := []*binarytree.BinaryNode{}
		currlevelnodesdata := []int{}
		for _, node := range currlevelnodes {
			if node == nil {
				continue
			}
			currlevelnodesdata = append(currlevelnodesdata, int(node.Data))
			if node.Left != nil {
				nextlevelnodes = append(nextlevelnodes, node.Left)
			}
			if node.Right != nil {
				nextlevelnodes = append(nextlevelnodes, node.Right)
			}
		}
		if reverse {
			currlevelnodesdata = reverseArray(currlevelnodesdata)
		}
		reverse = !reverse
		finalval = append(finalval, currlevelnodesdata)
		currlevelnodes = nextlevelnodes
	}
	return finalval
}
func reverseArray(arr []int) []int {
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]
	}
	return arr
}
