package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	t1 := &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}
	t1.Left = &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	t1.Right = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	t1.Left.Left = &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}

	t2 := &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	t2.Left = &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}
	t2.Right = &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	t2.Left.Right = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	t2.Right.Right = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}

	merged := mergeTrees(t1, t2)
	fmt.Println(*merged)

}

func mergeTrees(root1 *binarytree.BinaryNode, root2 *binarytree.BinaryNode) *binarytree.BinaryNode {
	return Merge(root1, root2)

}

func Merge(t1 *binarytree.BinaryNode, t2 *binarytree.BinaryNode) *binarytree.BinaryNode {
	if t1 == nil && t2 == nil {
		return nil
	}
	if t1 == nil && t2 != nil {
		return t2
	}
	if t1 != nil && t2 == nil {
		return t1
	}
	node := binarytree.BinaryNode{Data: t1.Data + t2.Data}
	node.Left = Merge(t1.Left, t2.Left)
	node.Right = Merge(t1.Right, t2.Right)
	return &node
}
