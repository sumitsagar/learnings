package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	root := &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 11, Left: nil, Right: nil}
	root.Left.Left.Left = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}
	root.Left.Left.Right = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}
	root.Right.Left = &binarytree.BinaryNode{Data: 13, Left: nil, Right: nil}
	root.Right.Right = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Right.Right.Left = &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}
	root.Right.Right.Right = &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}

	o := pathSum(root, 22)
	fmt.Print(o)
}
func pathSum(root *binarytree.BinaryNode, sum int) int {
	ret := 0
	pathSumHelper(root, sum, 0, &ret, []int{})
	return ret
}

func pathSumHelper(root *binarytree.BinaryNode, target int, cursum int, found *int, buf []int) {

	if root == nil {
		return
	}
	cursum = cursum + int(root.Data)
	tmp := make([]int, len(buf)+1)
	copy(tmp, buf)

	for i := 0; i < len(tmp); i++ {
		tmp[i] += int(root.Data)
		if tmp[i] == target {
			*found = *found + 1
		}
	}
	if root.Left != nil {
		pathSumHelper(root.Left, target, cursum, found, tmp)
	}
	if root.Right != nil {
		pathSumHelper(root.Right, target, cursum, found, tmp)
	}

}
