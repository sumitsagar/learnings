package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	root := &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 11, Left: nil, Right: nil}
	root.Left.Left.Left = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}
	root.Left.Left.Right = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}
	root.Right.Left = &binarytree.BinaryNode{Data: 13, Left: nil, Right: nil}
	root.Right.Right = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Right.Right = &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}

	b := hasPathSum(root, 22)
	fmt.Print(b)
}
func hasPathSum(root *binarytree.BinaryNode, targetSum int) bool {
	return HasSum(root, targetSum, 0)
}

func HasSum(root *binarytree.BinaryNode, targetSum int, cursum int) bool {
	if root == nil {
		return false
	}
	newsum := int(root.Data) + cursum
	if newsum == targetSum && root.Left == nil && root.Right == nil {
		return true
	}
	leftsum := false

	if root.Left != nil {
		leftsum = HasSum(root.Left, targetSum, newsum)
	} else {
		leftsum = false
	}

	rightssum := false

	if root.Right != nil {
		rightssum = HasSum(root.Right, targetSum, newsum)
	} else {
		rightssum = false
	}

	if leftsum || rightssum {
		return true
	}
	return false
}
