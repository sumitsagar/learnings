package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	root := &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 11, Left: nil, Right: nil}
	root.Left.Left.Left = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}
	root.Left.Left.Right = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}
	root.Right.Left = &binarytree.BinaryNode{Data: 13, Left: nil, Right: nil}
	root.Right.Right = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Right.Right.Left = &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}
	root.Right.Right.Right = &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}

	o := pathSum(root, 22)
	fmt.Print(o[0])
}
func pathSum(root *binarytree.BinaryNode, targetSum int) [][]int {
	output := [][]int{}
	c := []int{}
	pathSumHelper(root, targetSum, 0, c, &output)
	return output
}

func pathSumHelper(root *binarytree.BinaryNode, targetSum int, cursum int, temp []int, output *[][]int) {
	if root == nil {
		return
	}
	newsum := int(root.Data) + cursum

	//crete a copy of temp and mndify that else temp added in output will be modified
	cp := append([]int{}, temp...)
	cp = append(cp, int(root.Data))
	temp = cp

	if root.Left == nil && root.Right == nil {
		if newsum == targetSum {
			*output = append(*output, temp)
		}
		return
	}

	if root.Left != nil {
		pathSumHelper(root.Left, targetSum, newsum, temp, output)
	}

	if root.Right != nil {
		pathSumHelper(root.Right, targetSum, newsum, temp, output)
	}

}
