package main

import "dsalgo/DataStructure/BinarySearchTree"

func main() {

}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func invertTree(root *BinarySearchTree.TreeNode) *BinarySearchTree.TreeNode {
	if root == nil {
		return nil
	}

	temp := root.Right
	root.Right = root.Left
	root.Left = temp

	if root.Left != nil {
		invertTree(root.Left)
	}
	if root.Right != nil {
		invertTree(root.Right)
	}
	return root
}
