package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

var m map[int]bool
var arr []int64

func main() {
	m = make(map[int]bool)

	root := &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}

	arr := LeftSideView(root)
	fmt.Println(arr)
}

func LeftSideView(node *binarytree.BinaryNode) []int64 {
	arr = []int64{}
	if node == nil {
		return arr
	}
	Traverse(node, 1)
	return arr
}

func Traverse(node *binarytree.BinaryNode, h int) {
	if node.Left != nil {
		if !m[h] {
			arr = append(arr, node.Left.Data)
			m[h] = true
		}
		h = h + 1
		Traverse(node.Left, h)
	} else {
		if node.Right != nil {
			if !m[h] {
				arr = append(arr, node.Right.Data)
				m[h] = true
			}
			h = h + 1
			Traverse(node.Right, h)
		}
	}
}
