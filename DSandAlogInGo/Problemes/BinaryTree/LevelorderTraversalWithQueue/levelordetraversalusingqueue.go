package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
)

func main() {

	root := &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Right = &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	res := levelOrder(root)
	println(res)
}

func levelOrder(root *binarytree.BinaryNode) [][]int {
	var result [][]int
	if root == nil {
		return result
	}

	curLevel := []*binarytree.BinaryNode{root}

	for len(curLevel) > 0 {
		var thisArray []int                    // put the result
		var nextLevel []*binarytree.BinaryNode // put the TreeNodes of next leve.

		for _, node := range curLevel {
			thisArray = append(thisArray, int(node.Data)) // append the result
			//set the nodes of next level
			if node.Left != nil {
				nextLevel = append(nextLevel, node.Left)
			}
			if node.Right != nil {
				nextLevel = append(nextLevel, node.Right)
			}
		}
		curLevel = nextLevel
		result = append(result, thisArray) // note: thisArry is a element of result.
	}
	return result
}
