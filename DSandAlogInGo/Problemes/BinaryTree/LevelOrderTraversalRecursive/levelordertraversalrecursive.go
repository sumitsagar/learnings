package main

import (
	"dsalgo/DataStructure/BinarySearchTree"
	"fmt"
)

func main() {
	data := []int{11, 4, 15, 6, 2, 14, 7}
	rootNode := BinarySearchTree.CreateFromArray(data)

	h := BinarySearchTree.HeightOfTree(rootNode)
	fmt.Println("height:", h)
	LevelOrder(rootNode)

}

func LevelOrder(root *BinarySearchTree.TreeNode) {
	if root == nil {
		return
	}
	var height = BinarySearchTree.HeightOfTree(root)
	fmt.Printf("Height of a tree : %d \n", height)
	fmt.Printf("Level Order Traversal : \n")

	for i := 1; i <= height; i++ {
		PrintLevel(root, i)
	}
	fmt.Printf("\n")
}

func PrintLevel(root *BinarySearchTree.TreeNode, h int) {
	if root == nil {
		return
	}
	if h == 1 {
		fmt.Printf("%d ", root.Val)
		return
	}
	if h > 1 {
		PrintLevel(root.Left, h-1)
		PrintLevel(root.Right, h-1)
	}
}
