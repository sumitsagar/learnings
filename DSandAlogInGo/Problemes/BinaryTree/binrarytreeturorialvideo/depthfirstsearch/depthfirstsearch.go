package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	//  		1
	//		 /	 	\
	//		2		 3
	//	   /\		 /\
	//    /  \	    /  \
	//	 4    5    6    7
	//		   \         \
	//		    8         9

	root := &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Left.Right = &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}
	root.Left.Right.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}

	root.Right = &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	root.Right.Left = &binarytree.BinaryNode{Data: 6, Left: nil, Right: nil}
	root.Right.Right = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}
	root.Right.Right.Right = &binarytree.BinaryNode{Data: 9, Left: nil, Right: nil}

	arr := DFS(root)
	fmt.Println(arr)
}

func DFS(node *binarytree.BinaryNode) []int64 {
	if node == nil { //retunr if node is blank
		return []int64{}
	}
	stack := []*binarytree.BinaryNode{node}
	result := []int64{} //use array as queue

	for len(stack) > 0 {
		popitem := stack[len(stack)-1] //pop last item form array i..e top item from stack
		if len(stack) > 1 {
			stack = stack[0 : len(stack)-1]
		} else {
			stack = []*binarytree.BinaryNode{}
		}
		result = append(result, popitem.Data)

		if popitem.Right != nil {
			stack = append(stack, popitem.Right) //push right first
		}
		if popitem.Left != nil {
			stack = append(stack, popitem.Left) //push left next
		}
	}
	return result
}
