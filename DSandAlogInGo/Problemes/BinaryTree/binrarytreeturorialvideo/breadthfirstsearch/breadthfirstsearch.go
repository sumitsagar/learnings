package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	//  		1
	//		 /	 	\
	//		2		 3
	//	   /\		 /\
	//    /  \	    /  \
	//	 4    5    6    7
	//		   \         \
	//		    8         9

	root := &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Left.Right = &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}
	root.Left.Right.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}

	root.Right = &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	root.Right.Left = &binarytree.BinaryNode{Data: 6, Left: nil, Right: nil}
	root.Right.Right = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}
	root.Right.Right.Right = &binarytree.BinaryNode{Data: 9, Left: nil, Right: nil}

	arr := BFS(root)
	fmt.Println(arr)
}

func BFS(node *binarytree.BinaryNode) []int64 {
	if node == nil { //retunr if node is blank
		return []int64{}
	}
	result := []int64{}                     //use array as queue
	queue := []*binarytree.BinaryNode{node} //put first item in queue
	for len(queue) > 0 {
		popitem := queue[0]
		if len(queue) > 1 {
			queue = queue[1:]
		} else {
			queue = []*binarytree.BinaryNode{}
		}
		result = append(result, popitem.Data)
		if popitem.Left != nil {
			queue = append(queue, popitem.Left)
		}
		if popitem.Right != nil {
			queue = append(queue, popitem.Right)
		}
	}
	return result
}
