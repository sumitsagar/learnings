package main

import (
	binarytree "dsalgo/DataStructure/BinaryTree"
	"fmt"
)

func main() {
	//  		1
	//		 /	 	\
	//		2		 3
	//	   /\		 /\
	//    /  \	    /  \
	//	 4    5    6    7
	//		   \         \
	//		    8         9

	root := &binarytree.BinaryNode{Data: 1, Left: nil, Right: nil}
	root.Left = &binarytree.BinaryNode{Data: 2, Left: nil, Right: nil}
	root.Left.Left = &binarytree.BinaryNode{Data: 4, Left: nil, Right: nil}
	root.Left.Right = &binarytree.BinaryNode{Data: 5, Left: nil, Right: nil}
	root.Left.Right.Right = &binarytree.BinaryNode{Data: 8, Left: nil, Right: nil}

	root.Right = &binarytree.BinaryNode{Data: 3, Left: nil, Right: nil}
	root.Right.Left = &binarytree.BinaryNode{Data: 6, Left: nil, Right: nil}
	root.Right.Right = &binarytree.BinaryNode{Data: 7, Left: nil, Right: nil}
	root.Right.Right.Right = &binarytree.BinaryNode{Data: 9, Left: nil, Right: nil}

	res := MaxPathSum(root)
	fmt.Println(res)
}

func MaxPathSum(node *binarytree.BinaryNode) int64 {
	if node == nil {
		return -9999999
	}
	leftsum := int64(0)
	rightsum := int64(0)
	if node.Left == nil && node.Right == nil {
		return node.Data
	} else {
		if MaxPathSum(node.Left) > MaxPathSum(node.Right) {
			return leftsum + node.Data + MaxPathSum(node.Left)
		} else {
			return rightsum + node.Data + MaxPathSum(node.Right)
		}
	}
}
