package main

import (
	"fmt"
)

func main() {
	sorted := Sort([]int{1, 3, 4, 3, 6, 2, 9, 4})
	fmt.Println(sorted)
}

func Sort(arr []int) []int {
	sorted := false
	for !sorted {
		sorted = true
		for i := 0; i <= len(arr)-2; i++ {
			if arr[i+1] < arr[i] {
				//swap
				temp := arr[i+1]
				arr[i+1] = arr[i]
				arr[i] = temp
				sorted = false
			}
		}
	}
	return arr
}
