package HeapSort

import "fmt"

type minheap struct {
	arr []int
}

func NewMinHeap(arr []int) *minheap {
	minheap := &minheap{
		arr: arr,
	}
	return minheap
}

func (m *minheap) LeftchildIndex(index int) int {
	return 2*index + 1
}

func (m *minheap) RightchildIndex(index int) int {
	return 2*index + 2
}

func (m *minheap) Swap(first, second int) {
	temp := m.arr[first]
	m.arr[first] = m.arr[second]
	m.arr[second] = temp
}

func (m *minheap) Leaf(index int, size int) bool {
	if index >= (size/2) && index <= size {
		return true
	}
	return false
}

func (m *minheap) DownHeapify(current int, size int) {
	if m.Leaf(current, size) {
		return
	}
	smallest := current
	leftChildIndex := m.LeftchildIndex(current)
	rightRightIndex := m.RightchildIndex(current)
	if leftChildIndex < size && m.arr[leftChildIndex] < m.arr[smallest] {
		smallest = leftChildIndex
	}
	if rightRightIndex < size && m.arr[rightRightIndex] < m.arr[smallest] {
		smallest = rightRightIndex
	}
	if smallest != current {
		m.Swap(current, smallest)
		m.DownHeapify(smallest, size)
	}
}

func (m *minheap) BuildMinHeap(size int) {
	for index := ((size / 2) - 1); index >= 0; index-- {
		m.DownHeapify(index, size)
	}
}

func (m *minheap) Sort(size int) {
	m.BuildMinHeap(size)
	for i := size - 1; i > 0; i-- {
		// Move current root to end
		m.Swap(0, i)
		m.DownHeapify(0, i)
	}
}

func (m *minheap) Print() {
	for _, val := range m.arr {
		fmt.Println(val)
	}
}
