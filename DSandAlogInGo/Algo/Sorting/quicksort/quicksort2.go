package main

import (
	"fmt"
)

func main() {
	fmt.Println(QuickSort([]int{5, 7, 2, 1, 9, 8, 3, 5, 1}))
}

func QuickSort(arr []int) []int {
	if len(arr) < 2 {
		return arr
	}

	// QuickSort Helper function
	return quickSortHelper(arr, 0, len(arr)-1)
}

func quickSortHelper(arr []int, low, high int) []int {
	if low < high {
		// Partition the array and get the pivot index
		pi := partition(arr, low, high)

		// Recursively sort the sub-arrays
		quickSortHelper(arr, low, pi-1)
		quickSortHelper(arr, pi+1, high)
	}
	return arr
}

func partition(arr []int, low, high int) int {
	pivot := arr[high]
	i := low - 1

	for j := low; j < high; j++ {
		if arr[j] < pivot {
			i++
			arr[i], arr[j] = arr[j], arr[i]
		}
	}

	arr[i+1], arr[high] = arr[high], arr[i+1]
	return i + 1
}
