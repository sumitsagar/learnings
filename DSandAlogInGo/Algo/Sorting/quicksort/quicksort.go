package main

import (
	"fmt"
	"math/rand"
)

func main1() {
	fmt.Println(sortArray([]int{5, 7, 2, 1, 9, 8}))
	//fmt.Println(Sort([]int{5, 1, 1, 2, 0, 0}))
}

func sortArray(nums []int) []int {
	quickSort(nums, 0, len(nums)-1)
	return nums
}
func quickSort(nums []int, lo, hi int) {
	if lo >= hi {
		return
	}
	k := rand.Intn(hi-lo+1) + lo
	tmp := nums[hi]
	nums[hi] = nums[k]
	nums[k] = tmp

	pivot := nums[hi] //last item as pivot
	j := lo - 1
	for i := lo; i < hi; i++ {
		if nums[i] <= pivot {
			j++
			tmp := nums[j]
			nums[j] = nums[i]
			nums[i] = tmp
		}
	}
	tmp = nums[j+1]
	nums[j+1] = nums[hi]
	nums[hi] = tmp

	quickSort(nums, lo, j)
	quickSort(nums, j+1, hi)
}

func Sort(arr []int) []int {
	//2 base case
	if len(arr) == 0 {
		return arr
	}
	if len(arr) == 1 {
		return arr
	}
	if len(arr) == 2 {
		if arr[0] > arr[1] {
			temp := arr[0]
			arr[1] = arr[0]
			arr[0] = temp
			return arr
		} else {
			return arr
		}
	}
	pivotvalue := arr[len(arr)-1]
	lowindex := 0
	highindex := len(arr) - 1
	for lowindex < highindex {
		if arr[lowindex] > pivotvalue && arr[highindex] < pivotvalue {
			//swap
			temp := arr[lowindex]
			arr[lowindex] = arr[highindex]
			arr[highindex] = temp
			lowindex++
			highindex--
		} else {
			if arr[lowindex] <= pivotvalue {
				lowindex++
			}
			if arr[highindex] >= pivotvalue {
				highindex--
			}
		}
	}
	middleindex := (len(arr) - 1) / 2
	leftsplit := arr[0:middleindex]
	rightsplit := arr[middleindex:]
	sortleft := Sort(leftsplit)
	sortright := Sort(rightsplit)
	return append(sortleft, sortright...)
}
