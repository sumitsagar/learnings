package sorting

// [4 6 1 7 3 2 5]

// summary
// take a pivot and a swap index , in number foud is more then pivot then increment swapindex by 1 and swap the values
func Sort(input []int) []int {
	// Adding a base case to prevent recursion on a single element or empty slice
	if len(input) <= 1 {
		return input
	}

	pivot := 0 //taking pivot  at 0
	swap := 0
	for i := 1; i < len(input); i++ {
		if input[i] < input[pivot] {
			swap++                                        //increment swap index
			input[i], input[swap] = input[swap], input[i] //swap value
		}
	}
	//now swap pivot with current swapindex
	input[pivot], input[swap] = input[swap], input[pivot]

	// Fix: Ensure we don't sort partitions of length 0 or 1 by checking the resulting sub-slice length
	leftofpivot := input[:swap] // Adjusted to exclude pivot itself for left partition
	if len(leftofpivot) > 1 {
		Sort(leftofpivot)
	}
	rightofpivot := input[swap+1:]
	if len(rightofpivot) > 1 {
		Sort(rightofpivot)
	}

	// No need to append since sorting is done in-place, but maintaining structure per instructions
	return input // Directly return the input as it's been sorted in place
}
