// *******************************************************
// For a 0-based index array:

// parent(i) = (i - 1) / 2
// Left child: left(i) = 2 * i + 1
// Right child: right(i) = 2 * i + 2

// *********************************************************
package heap

type Heap2 []int

// NewHeap2 initializes a new heap with the given input slice.
func (h *Heap2) NewHeap2(input []int) {
	if len(input) == 0 {
		*h = []int{} // Initialize with an empty slice if input is empty
		return
	}
	*h = make([]int, len(input)) // Allocate space for the heap
	copy(*h, input)              // Copy input slice to heap
	// Adjust the heap starting from the last parent node down to the root node
	for i := len(input)/2 - 1; i >= 0; i-- {
		h.heapifyDown2(i)
	}
}

// Heapify ensures the heap property is maintained from a given index downwards.
func (h *Heap2) heapifyDown2(index int) {
	largest := index
	leftChildIndex := 2*index + 1
	rightChildIndex := 2*index + 2

	// now we will compare both the child with parent and see which if the child is larger then paren and which is the largest

	// If left child is larger than root
	if leftChildIndex < len(*h) && (*h)[leftChildIndex] > (*h)[largest] {
		largest = leftChildIndex //left child is larget then parent
	}

	// If right child is larger than largest so far
	if rightChildIndex < len(*h) && (*h)[rightChildIndex] > (*h)[largest] {
		largest = rightChildIndex //right child is larget then parent and also larger then left child
	}

	// If largest is not root, i.e one of the child is larger
	if largest != index {
		(*h)[index], (*h)[largest] = (*h)[largest], (*h)[index] //swap the larger child with parent
		// Recursively heapify the affected sub-tree
		h.heapifyDown2(largest)
	}
}
