package heap

type Heap3 []int

func (h *Heap3) NewHeap3(input []int) {
	if len(input) == 0 {
		*h = []int{} // Initialize with an empty slice if input is empty
		return
	}
	*h = make([]int, len(input)) // Allocate space for the heap
	copy(*h, input)
	for i := len(input)/2 - 1; i >= 0; i-- {
		h.HeapifyDown(i)
	}
}

func (h *Heap3) HeapifyDown(parentindex int) {
	leftchild := parentindex*2 + 1
	rightchild := parentindex*2 + 2

	largerindex := parentindex

	if leftchild < len(*h) && (*h)[leftchild] > (*h)[parentindex] {
		largerindex = leftchild
	}
	if rightchild < len(*h) && (*h)[rightchild] > (*h)[largerindex] {
		largerindex = rightchild
	}
	// If the largest is not the parent, swap and continue heapifying down
	if largerindex != parentindex {
		(*h)[parentindex], (*h)[largerindex] = (*h)[largerindex], (*h)[parentindex]
		h.HeapifyDown(largerindex)
	}
}
