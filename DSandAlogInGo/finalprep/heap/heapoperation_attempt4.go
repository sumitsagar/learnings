package heap

type Heap4 []int

func NewHeap4(data []int) *Heap4 {
	h := Heap4([]int{data[0]}) // Convert data to Heap4 type.
	for i := 1; i < len(data); i++ {
		h = append(h, data[i]) //append and heapify
		h.HeapifyUp(i)
	}
	return &h
}

func (heap *Heap4) HeapifyUp(index int) {
	childitemindex := index
	parentindex := (childitemindex - 1) / 2

	for parentindex >= 0 && (*heap)[parentindex] < (*heap)[childitemindex] {
		(*heap)[parentindex], (*heap)[childitemindex] = (*heap)[childitemindex], (*heap)[parentindex]

		childitemindex = parentindex
		parentindex = (childitemindex - 1) / 2
	}
}

func (heap *Heap4) ExtractMax() int {

	retval := (*heap)[0]
	(*heap)[0] = (*heap)[len(*heap)-1]
	(*heap) = (*heap)[0 : len(*heap)-2]
	heap.HeapifyDown(0)
	return retval
}

func (heap *Heap4) HeapifyDown(parentindex int) {
	for {
		leftchild := (parentindex * 2) + 1
		rightchild := (parentindex * 2) + 2
		largest := parentindex

		if leftchild < len(*heap) && (*heap)[leftchild] > (*heap)[largest] {
			largest = leftchild
		}

		if rightchild < len(*heap) && (*heap)[rightchild] > (*heap)[largest] {
			largest = rightchild
		}

		if largest == parentindex {
			break
		}

		(*heap)[parentindex], (*heap)[largest] = (*heap)[largest], (*heap)[parentindex]
		parentindex = largest
	}
}
