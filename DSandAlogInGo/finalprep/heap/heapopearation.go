// *******************************************************
// For a 0-based index array:

// parent(i) = (i - 1) / 2
// Left child: left(i) = 2 * i + 1
// Right child: right(i) = 2 * i + 2

//*********************************************************

package heap

type Heap []int

// NewHeap initializes a new heap with the given input slice.
func (h *Heap) NewHeap(input []int) {
	// Initialize heap with the input for efficiency.
	*h = input
	// Heapify process to ensure the heap property is met.
	n := len(*h)
	for i := n/2 - 1; i >= 0; i-- {
		h.heapifyDown(i, n)
	}
}

// Insert adds a new element to the heap.
func (h *Heap) Insert(v int) {
	*h = append(*h, v)       // Always append the new value to the end.
	h.heapifyUp(len(*h) - 1) // Adjust the heap starting from the new element.
}

// heapifyUp adjusts the heap starting from index i to maintain the heap property.
func (h *Heap) heapifyUp(i int) {
	for i != 0 {
		parent := (i - 1) / 2
		if (*h)[i] < (*h)[parent] {
			(*h)[i], (*h)[parent] = (*h)[parent], (*h)[i]
			i = parent
		} else {
			break
		}
	}
}

// heapifyDown adjusts the heap starting from index i to maintain the heap property after removal or during construction.
func (h *Heap) heapifyDown(i, n int) {
	smallest := i
	left := 2*i + 1
	right := 2*i + 2
	if left < n && (*h)[left] < (*h)[smallest] {
		smallest = left
	}
	if right < n && (*h)[right] < (*h)[smallest] {
		smallest = right
	}
	if smallest != i {
		(*h)[i], (*h)[smallest] = (*h)[smallest], (*h)[i]
		h.heapifyDown(smallest, n)
	}
}
