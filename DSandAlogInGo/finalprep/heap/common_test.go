package heap

import (
	"fmt"
	"testing"
)

func TestNewHeap(t *testing.T) {
	// h := Heap{}
	// h.NewHeap([]int{3, 5, 6, 3, 8, 4, 9, 2, 1})
	// fmt.Println(h)

	// h2 := Heap2{}
	// h2.NewHeap2([]int{3, 5, 6, 3, 8, 4, 9, 2, 1})
	// fmt.Println(h2)

	h4 := NewHeap4([]int{3, 5, 6, 3, 8, 4, 9, 2, 1})
	fmt.Println(h4)
	fmt.Println(h4.ExtractMax())
	fmt.Println(h4.ExtractMax())
	fmt.Println(h4.ExtractMax())
	fmt.Println(h4.ExtractMax())
	fmt.Println(h4)
	//expected :
	// 9,8,6,3,5,4,3,2,1
	// 9 6 8 3 3 4 5 2

}
