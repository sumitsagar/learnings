package problems

func FindDuplicates(input []int) []int {
	retval := []int{}

	dict_repeated := make(map[int]int)

	dict := make(map[int]int)
	for _, val := range input {
		_, exists := dict[val]
		if exists {
			dict_repeated[val] = 0
		} else {
			dict[val] = 0
		}
	}

	for k, _ := range dict_repeated {
		retval = append(retval, k)
	}

	return retval
}
