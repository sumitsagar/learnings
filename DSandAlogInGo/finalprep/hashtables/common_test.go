package problems

import (
	"fmt"
	"testing"
)

func TestFindDuplicates(t *testing.T) {
	duplicates := FindDuplicates([]int{4, 3, 2, 7, 8, 2, 3, 1})
	fmt.Println(duplicates)
}

func Test_NonRepeated_Char(t *testing.T) {
	fmt.Println(FirstNonRepeatingChar("sumitsagar"))
}

func Test_SubarraySum(t *testing.T) {
	fmt.Println(SubarraySum([]int{1, 2, 3}, 5))
}

func TestLongestConsecutiveSequence(t *testing.T) {
	fmt.Println(LongestConsecutiveSequence([]int{100, 4, 200, 1, 2, 3}))
}
