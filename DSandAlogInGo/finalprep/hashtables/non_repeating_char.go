package problems

func FirstNonRepeatingChar(input string) string {
	countDict := make(map[rune]int)
	// Populate the count dictionary with character counts
	for _, char := range input {
		countDict[char]++
	}

	// Find the first character with a count of 1
	for _, char := range input {
		if countDict[char] == 1 {
			return string(char)
		}
	}

	// No non-repeating character found
	return ""
}
