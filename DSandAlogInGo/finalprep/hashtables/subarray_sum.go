package problems

func SubarraySum(nums []int, k int) int {
	// Initialize variables
	count := 0 // This will store the count of subarrays that sum to k
	sum := 0   // This will keep track of the cumulative sum of the subarrays

	// The sumMap keeps track of how many times each cumulative sum has been seen
	// Initialize it with sum = 0 seen once, to handle cases where a subarray starting
	// from the beginning of the array sums to k
	sumMap := make(map[int]int)
	sumMap[0] = 1

	// Iterate over the array
	for _, num := range nums {
		// Add the current number to the cumulative sum
		sum += num

		// Check if there is a previous cumulative sum such that
		// current sum - previous sum = k.
		// If so, it means we've found a subarray that sums to k.
		// We add the count of such previous sums to our total count.
		count += sumMap[sum-k]

		// Update the sumMap with the current sum.
		// If this sum has been seen before, increment the count, otherwise set it to 1.
		if _, found := sumMap[sum]; found {
			sumMap[sum] += 1
		} else {
			sumMap[sum] = 1
		}
	}

	// Return the total count of subarrays that sum up to k
	return count
}

// func SubArraySum(nums []int, target int) []int {
// 	sum := 0
// 	start := 0
// 	end := 0
// 	for i, val := range nums {

// 		if val >= target {
// 			start = i
// 			end = i
// 		} else {
// 			sum += val
// 			if sum == target {
// 				end = i
// 			}
// 		}

// 	}
// 	return []int{start, end}
// }
