package problems

func LongestConsecutiveSequence(input []int) int {
	maxconsecutivecount := 0
	dic := make(map[int]int)
	for i := 0; i < len(input); i++ {
		dic[input[i]] = 1
	}

	for k := range dic {
		if _, exists := dic[k-1]; !exists { // Check if k is the start of a sequence else ignore it
			maxconsecutivecount_local := 1 // Initialize to 1 because k is part of the sequence
			nextvaltocheck := k + 1
			for Exists(nextvaltocheck, &dic) {
				maxconsecutivecount_local++
				nextvaltocheck++
			}
			if maxconsecutivecount_local > maxconsecutivecount {
				maxconsecutivecount = maxconsecutivecount_local
			}
		}
	}
	return maxconsecutivecount
}

func Exists(key int, dict *map[int]int) bool {
	_, exists := (*dict)[key]
	return exists
}
