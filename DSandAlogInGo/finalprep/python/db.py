import mysql.connector
from mysql.connector import Error

# Configuration for database connection
DB_CONFIG = {
    'host_shard_1': {'host': 'localhost', 'port': 3307},
    'host_shard_2': {'host': 'localhost', 'port': 3308}
}
USERNAME = 'root'
PASSWORD = 'password'
DATABASE_NAME = 'testdb'

def create_server_connection(host_name, port, database_name=''):
    """Create and return a database connection."""
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=USERNAME,
            passwd=PASSWORD,
            database=database_name,
            port=port
        )
        print(f"Connection to MySQL Server on {host_name}:{port} successful")
        return connection
    except Error as e:
        print(f"The error '{e}' occurred")
        return None

def execute_query(connection, query):
    """Execute a given query on the provided connection."""
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query executed successfully")
    except Error as e:
        print(f"The error '{e}' occurred")

def read_query(connection, query):
    """Execute a read query and return the results."""
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        return cursor.fetchall()
    except Error as e:
        print(f"The error '{e}' occurred")
        return None

def get_shard_connection(userid):
    """Return a connection to the appropriate shard based on userid."""
    shard_key = 'host_shard_2' if userid % 2 == 0 else 'host_shard_1'
    config = DB_CONFIG[shard_key]
    return create_server_connection(config['host'], config['port'], DATABASE_NAME)

def insert_user(userid, name, phone):
    """Insert a user record into the appropriate shard."""
    query = f"INSERT INTO users (userid, name, phone) VALUES ({userid}, '{name}', '{phone}')"
    connection = get_shard_connection(userid)
    if connection:
        execute_query(connection, query)
        connection.close()

def get_user(userid):
    """Retrieve a user record from the appropriate shard."""
    query = f"SELECT * FROM users WHERE userid = {userid}"
    connection = get_shard_connection(userid)
    if connection:
        user = read_query(connection, query)
        connection.close()
        return user

def setup_databases():
    """Create databases and tables as necessary on all shards."""
    query = """
    CREATE TABLE IF NOT EXISTS users (
        userid INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        phone VARCHAR(20)
    )
    """
    for config in DB_CONFIG.values():
        connection = create_server_connection(config['host'], config['port'], DATABASE_NAME)
        if connection:
            execute_query(connection, f"CREATE DATABASE IF NOT EXISTS {DATABASE_NAME}")
            execute_query(connection, query)
            connection.close()

# Setup databases and tables
setup_databases()

# Example usage
insert_user(21, 'John Doe', '1234567890')
insert_user(22, 'John Doe', '1234567890')
insert_user(23, 'John Doe', '1234567890')
insert_user(24, 'John Doe', '1234567890')
user = get_user(1)
print(user)
