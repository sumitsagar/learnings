from flask import Flask, jsonify
from handlers import say_hello, say_goodbye,Hello_Sumit

app = Flask(__name__)

@app.route('/hello', methods=['GET'])
def hello():
    return jsonify(say_hello())

@app.route('/goodbye', methods=['GET'])
def goodbye():
    return jsonify(say_goodbye())

@app.route("/hellosumit",methods=["GET"])
def hellosumit():
    return jsonify(Hello_Sumit())

if __name__ == '__main__':
    app.run(debug=True)
