
from helper import *

class MyClass:
    def __init__(self,value):
        self.value = value


    def Method1(self):
        print(self)
        print("method 1")

        def SubMethod():
            print("sub method")
            return "exited"

        return SubMethod
    
    def mutablefefaltarg(self,val,arr = []):
        arr.append(val)
        return arr
    
    def UnPack(self):
        x = [1,2,3,4]
        print(x)
        print(*x)

    def RaiseException(self):
        try:
            raise Exception("error")
        except Exception as e:
            print(e)

    def lambdatest(self,x,y):
        q = lambda x,y : x+y
        print(q(x,y)) 

class DerivedRomMyClass(MyClass):
    def __init__(self,value): 
        super.__init__(self,value)
        print("hi fromdevired class")

    def DerivedClassMEthod(self):
        print("derived class method")



obj = MyClass(10)
print(obj.value)
print(obj.Method1()())

print("--------------------------- mutable defalt ard example ")
print(obj.mutablefefaltarg(1))
print(obj.mutablefefaltarg(2))

print("-----------------------un pack test")
obj.UnPack()

print("---------------------------------------raise error")
obj.RaiseException()

print("lambda test-------------------------------")
obj.lambdatest(1,2)

print(" import another file ---------------------------------")
helper1()


