from abc import ABC, abstractmethod

class DatabaseConnection(ABC):
    @abstractmethod
    def connect(self):
        pass

class SQLDatabaseConnection(DatabaseConnection):
    def connect(self):
        print("Connecting to a SQL Database.")

class NoSQLDatabaseConnection(DatabaseConnection):
    def connect(self):
        print("Connecting to a NoSQL Database.")

class DatabaseConnectionFactory(ABC):
    @abstractmethod
    def create_connection(self) -> DatabaseConnection:
        pass

class SQLConnectionFactory(DatabaseConnectionFactory):
    def create_connection(self) -> DatabaseConnection:
        return SQLDatabaseConnection()

class NoSQLConnectionFactory(DatabaseConnectionFactory):
    def create_connection(self) -> DatabaseConnection:
        return NoSQLDatabaseConnection()


def client_code(factory: DatabaseConnectionFactory):
    # The factory could be any subclass of DatabaseConnectionFactory.
    db_connection = factory.create_connection()
    db_connection.connect()

# Example usage
if __name__ == "__main__":
    sql_factory = SQLConnectionFactory()
    client_code(sql_factory)

    nosql_factory = NoSQLConnectionFactory()
    client_code(nosql_factory)
