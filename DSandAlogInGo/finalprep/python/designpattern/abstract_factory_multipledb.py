from abc import ABC,abstractmethod

class Connection(ABC):
    @abstractmethod
    def Connect(self):
        pass
class Command(ABC):
    def CreateCommand(self):
        pass


class SQLConnection(Connection):
    def Connect(self):
        print("connecting to sql") 

class MongoConnection(Connection):
    def Connect(self):
        print("connecting to mongo") 


class SQLCommand(Command):
    def CreateCommand(self):
        print("command sql") 

class MongoCommand(Command): 
    def CreateCommand(self):
        print("command for mongo")


class DatabseFactory(ABC):
    @abstractmethod
    def Connection(self):
        pass
    @abstractmethod
    def CreateCommand(self):
        pass


class SQLDBFactory(DatabseFactory):
    def Connection(self):
        return SQLConnection()
    def CreateCommand(self):
        return SQLCommand()
    


class MongoDBFactory(DatabseFactory):
    def Connection(self):
        return MongoConnection()
    def CreateCommand(self):
        return MongoCommand()
    

class APP:
    def GetDB(self,db:str):
        if db == "sql":
            return SQLDBFactory()
        else:
            return MongoDBFactory()



app = APP()
db = app.GetDB("sql")
connection = db.Connection()
print(connection.Connect())

print(db.CreateCommand().CreateCommand())




































# # Abstract Products
# class Connection:
#     def connect(self):
#         pass

# class Command:
#     def execute(self, query):
#         pass

# # Concrete Products
# class SQLiteConnection(Connection):
#     def connect(self):
#         print("Connecting to SQLite database.")

# class SQLiteCommand(Command):
#     def execute(self, query):
#         print(f"Executing '{query}' on SQLite database.")

# class PostgreSQLConnection(Connection):
#     def connect(self):
#         print("Connecting to PostgreSQL database.")

# class PostgreSQLCommand(Command):
#     def execute(self, query):
#         print(f"Executing '{query}' on PostgreSQL database.")

# # Abstract Factory Interface
# class DatabaseFactory:
#     def create_connection(self):
#         pass

#     def create_command(self):
#         pass

# # Concrete Factories
# class SQLiteFactory(DatabaseFactory):
#     def create_connection(self):
#         return SQLiteConnection()

#     def create_command(self):
#         return SQLiteCommand()

# class PostgreSQLFactory(DatabaseFactory):
#     def create_connection(self):
#         return PostgreSQLConnection()

#     def create_command(self):
#         return PostgreSQLCommand()


# # Usage
# def database_operations(factory: DatabaseFactory):
#     # The client code works with factories and products only
#     # through abstract types: DatabaseFactory, Connection, and Command.
#     connection = factory.create_connection()
#     connection.connect()

#     command = factory.create_command()
#     command.execute("SELECT * FROM users")

# # Example: Client code choosing a database at runtime
# db_type = input("Enter the database type (SQLite/PostgreSQL): ")
# if db_type == "SQLite":
#     factory = SQLiteFactory()
# elif db_type == "PostgreSQL":
#     factory = PostgreSQLFactory()
# else:
#     print("Unsupported database type.")
#     factory = None

# if factory:
#     database_operations(factory)
