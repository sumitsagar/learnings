from abc import ABC

class Button(ABC):
    def Paint(self):
        pass

class CheckBox(ABC):
    def Paint(self):
        pass


class WindowsButton(Button):
        def Paint(self):
             print("this is windows button")


class MacButton(Button):
        def Paint(self):
             print("this is mac button")

class WindowsCheckBox(CheckBox):
        def Paint(self):
             print("this is windows checkBox")


class MacheckBox(CheckBox):
        def Paint(self):
             print("this is mac checkBox")




class GUIFacory(ABC):
      def CreateButton(self) -> Button:
            pass      
      def CreateCheckBox(self) -> CheckBox:
            pass


class WindowsFactory(GUIFacory):
    def CreateButton(self) -> Button:
        return WindowsButton()

    def CreateCheckBox(self) -> Button:
        return WindowsCheckBox()
    
class MacFactory(GUIFacory):
    def CreateButton(self) -> Button:
        return MacButton()

    def CreateCheckBox(self) -> Button:
        return MacheckBox()
    

class App:
    def GetUIElements(self,guifactory:GUIFacory):
        btn = guifactory.CreateButton()
        cb = guifactory.CreateCheckBox()
        print(btn.Paint())    
        print(cb.Paint())    

app = App()
guifacotry = WindowsFactory()
app.GetUIElements(guifacotry)        