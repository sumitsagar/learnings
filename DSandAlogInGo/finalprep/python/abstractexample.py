from abc import ABC, abstractmethod

class MyInterface(ABC):
    @abstractmethod
    def my_method(self):
        pass

class ConcreteClass(MyInterface):
    def my_method(self):
        print("Implementing the abstract method!")

 


class Interface2(ABC):
    @abstractmethod
    def method1(self):
        pass 

class concreteClass2(Interface2):
    def method1(self):
        try:
            print(1/0)
            print("hello from method 1")
        except Exception as e:
            print(str(e),"error occured")


# This will work
instance = ConcreteClass()
instance.my_method()

instance2 = concreteClass2()
instance2.method1()