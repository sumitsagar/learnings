import pika

# Connection setup
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

# Declare a headers exchange
exchange_name = 'headers_exchange'
channel.exchange_declare(exchange=exchange_name, exchange_type='headers')

# Declare a queue
queue_name = 'headers_queue'
channel.queue_declare(queue=queue_name)

# Bind the queue to the exchange with header matching
headers = {'x-match': 'all', 'format': 'pdf', 'type': 'report'}
channel.queue_bind(exchange=exchange_name, queue=queue_name, arguments=headers)

print(f"Queue {queue_name} is bound to exchange {exchange_name} with headers {headers}")



# Declare a headers exchange
channel.exchange_declare(exchange=exchange_name, exchange_type='headers')

# Send a message with headers
headers = {'format': 'pdf', 'type': 'report'}
message = 'A PDF report.'
channel.basic_publish(exchange=exchange_name, routing_key='', body=message, properties=pika.BasicProperties(headers=headers))

print("Sent a message with headers.")
connection.close()
