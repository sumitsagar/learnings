import pika
import sys

# Establish connection
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()


# creating queues with binding and exchange ****************************************

# Declare a direct exchange
exchange_name = 'direct_exchange'
channel.exchange_declare(exchange=exchange_name, exchange_type='direct')

# Declare a queue
queue_name = 'error'
channel.queue_declare(queue=queue_name)
# Bind the queue to the exchange with a routing key
routing_key = 'error'
channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key=routing_key)
print(f"Queue {queue_name} is bound to exchange {exchange_name} with routing key {routing_key}")

# Declare the 'info' queue before trying to bind it
queue_name = 'info'
channel.queue_declare(queue=queue_name)  # Missing in your script for 'info' and 'log'
routing_key = 'info'
channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key=routing_key)
print(f"Queue {queue_name} is bound to exchange {exchange_name} with routing key {routing_key}")


# Declare the 'log' queue before trying to bind it
queue_name = 'log'
channel.queue_declare(queue=queue_name)  # Missing in your script for 'log'
routing_key = 'log'
channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key=routing_key)

print(f"Queue {queue_name} is bound to exchange {exchange_name} with routing key {routing_key}")


# creating queues end ********************************************************

# Declare the direct exchange 
channel.exchange_declare(exchange=exchange_name, exchange_type='direct')

# Routing key and message from command line (or default values)
severity = sys.argv[1] if len(sys.argv) > 1 else 'info'
message = ' '.join(sys.argv[2:]) or 'Hello World!'

# Publish message
channel.basic_publish(exchange=exchange_name, routing_key=severity, body=message)
print(f" [x] Sent {severity}:{message}")

# Close connection
connection.close()
