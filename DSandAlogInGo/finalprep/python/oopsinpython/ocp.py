class Discount:
    def __init__(self,amount) -> None:
        self.amount = amount

    def GetDiscount(self) -> float:
        return self.amount * .1 
    

class VIPOffer(Discount):
    def GetVIPDiscount(self):
        return super().GetDiscount() + (self.amount * .4)    
    

class WeekendOffer(Discount):
    def GetWeekendDiscount(self):
        return super().GetDiscount() + (self.amount * .2)      




vipoffer  = VIPOffer(100)
print(vipoffer.GetVIPDiscount())


wo  = WeekendOffer(100)
print(wo.GetWeekendDiscount())