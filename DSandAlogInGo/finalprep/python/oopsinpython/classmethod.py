class TestClass:

    count = 0
    def Hello(self):
        print("hello")
        TestClass.count+=1 

    @classmethod
    def GetCount(cls):
        return(cls.count)
    

tc = TestClass()
tc.Hello()

tc2 = TestClass()
tc2.Hello()

print(TestClass.GetCount())
# TestClass.Hello()