from abc import ABC,abstractmethod


class Switchable(ABC):
    @abstractmethod
    def Switch_ON():
        pass

    
    @abstractmethod
    def Switch_OFF():
        pass

class BulbSwitch(Switchable):
    def Switch_ON(self):
        print("turn on")

    def Switch_OFF1(self):
        print("turn off")



c = BulbSwitch()
c.Switch_ON()
c.Switch_OFF()

