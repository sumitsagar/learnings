from abc import ABC, abstractmethod

class UserRepo(ABC):
    @abstractmethod
    def GetUser(self) -> str:
        pass

class SqlRepo(UserRepo):
    def GetUser(self) -> str:
        return "sumit from sql"

class MongoRepo(UserRepo):
    def GetUser(self) -> str:
        return "sumit from mongo"


class InvalidRepo(UserRepo):
    pass

class Repository:
    def __init__(self, repo: UserRepo) -> None:
        if not isinstance(repo, UserRepo):
            raise ValueError("repo must be an instance of UserRepo")
        self.repo = repo

# This will work
sql_repo = SqlRepo()
repo = Repository(sql_repo)

# This will raise an error at runtime
# invalid_repo = InvalidRepo()
# repo = Repository(invalid_repo)
