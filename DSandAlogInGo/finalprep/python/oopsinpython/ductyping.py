class SqlRepo():
    def GetUser(self) -> str:
        return "sumit from sql"
    
class MongoRepo():
    def GetUser(self) -> str:
        return "sumit from mongo"
    

class Repository():
    def __init__(self,repo) -> None:
        self.repo = repo


sqlrepo = SqlRepo()
repo = Repository(sqlrepo)
user = repo.repo.GetUser()
print(user)



mongorepo = MongoRepo()
repo = Repository(mongorepo)
user = repo.repo.GetUser()
print(user)
    


class InvalidRepo():
    pass
testrepo = InvalidRepo()
repo = Repository(testrepo)    
    
        
