from abc import ABC,abstractmethod

class ReportGenerator(ABC):
    @abstractmethod
    def GenerateReport(self,content):
        pass


class PDFReportGenrator(ReportGenerator):
    def __init__(self,content) -> None:
        self.content = content
    def GenerateReport(self):
        return  self.content + "this is PDF report"
    

class HTMLReportGenrator(ReportGenerator):
    def __init__(self,content) -> None:
        self.content = content
    def GenerateReport(self):
        return self.content + "this is HTML report"
    

class ReportCreator:
    def __init__(self) -> None:
        pass
    def CreateReport(self,report_generator:ReportGenerator):
        report = report_generator.GenerateReport()   
        print(report)


pdfreportgenrator = PDFReportGenrator("this is data for report")
reportcreator = ReportCreator()
reportcreator.CreateReport(pdfreportgenrator)