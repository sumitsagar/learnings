import asyncio


async def AsyncDelay():
    await asyncio.sleep(1)


async def main():
    await asyncio.gather(
    AsyncDelay(),
    AsyncDelay(),
)
    
asyncio.run(main())   