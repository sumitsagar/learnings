from abc import ABC, abstractmethod

class ReportGenerator(ABC):
    @abstractmethod
    def generate_report(self, content):
        pass

class PDFReportGenerator(ReportGenerator):
    def generate_report(self, content):
        # Logic to generate a PDF report
        print(f"Generating PDF report with content: {content}")

class HTMLReportGenerator(ReportGenerator):
    def generate_report(self, content):
        # Logic to generate an HTML report
        print(f"Generating HTML report with content: {content}")

class ReportCreator:
    def __init__(self, report_generator: ReportGenerator):
        self.report_generator = report_generator

    def create_report(self, content):
        self.report_generator.generate_report(content)

# Usage
pdf_report_generator = PDFReportGenerator()
report_creator = ReportCreator(pdf_report_generator)
report_creator.create_report("Report Content")

html_report_generator = HTMLReportGenerator()
report_creator = ReportCreator(html_report_generator)
report_creator.create_report("Report Content")
