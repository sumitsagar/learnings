import asyncio

async def AMEthodWithTakesLong(data):
    print("processing ..." ,data)
    await asyncio.sleep(1)
    print("processed ..." ,data)


async def main():
    await asyncio.gather(
         AMEthodWithTakesLong("one"),
         AMEthodWithTakesLong("two"),
         AMEthodWithTakesLong("three"),
        )    
    
asyncio.run(main()) 


