import asyncio


async def AnAsyncMethod():
    print("hello!")
    await asyncio.sleep(1)
    print("world")


# asyncio.run(AnAsyncMethod())


async def Async_numgenerator(num):
    for i in range(num):
        yield i

async def AsyncForloop():
    async for i in Async_numgenerator(10): 
        print(f"hi this is index no {i}")

asyncio.run(AsyncForloop())