package tree

type BSTNode struct {
	Data  int
	Left  *BSTNode
	Right *BSTNode
}

type BinarySearchTree struct {
	Root *BSTNode
}

func (bst *BinarySearchTree) Insert(data int) {
	if bst.Root == nil {
		bst.Root = &BSTNode{Data: data}
	} else {
		InsertRecursively(bst.Root, data)
	}
}

func InsertRecursively(curruent *BSTNode, data int) {
	if curruent == nil {
		return
	}
	//if data is small, add to left, so traverse left
	if data < curruent.Data {
		if curruent.Left == nil {
			//set left node if null
			curruent.Left = &BSTNode{data, nil, nil}
		} else {
			InsertRecursively(curruent.Left, data)
		}
	} else {
		if curruent.Right == nil {
			//set left node if null
			curruent.Right = &BSTNode{data, nil, nil}
		} else {
			InsertRecursively(curruent.Right, data)
		}
	}
}

func (bst *BinarySearchTree) ContainsAVal(data int) bool {
	return ContainsAValRecursive(bst.Root, data)
}

func ContainsAValRecursive(root *BSTNode, data int) bool {
	if root == nil {
		return false
	}
	if root.Data == data {
		return true
	}
	if root.Left != nil && data < root.Left.Data {
		return ContainsAValRecursive(root.Left, data)
	} else if root.Right != nil {
		return ContainsAValRecursive(root.Right, data)
	}
	return false
}

func (bst *BinarySearchTree) CreateBST(data []int) {
	if len(data) == 0 {
		return
	}
	root := BSTNode{data[0], nil, nil}
	bst.Root = &root

	for i := 1; i < len(data); i++ {
		bst.Insert(data[i])
	}

}

func (bst *BinarySearchTree) DeleteNode(data int) {
	bst.Root = deleteRecursively(bst.Root, data)
}

func deleteRecursively(root *BSTNode, data int) *BSTNode {
	if root == nil {
		return root
	}

	if data < root.Data {
		root.Left = deleteRecursively(root.Left, data)
	} else if data > root.Data {
		root.Right = deleteRecursively(root.Right, data)
	} else {
		// Node with only one child or no child
		if root.Left == nil {
			return root.Right
		} else if root.Right == nil {
			return root.Left
		}

		// Node with two children: Get the inorder successor (smallest in the right subtree)
		root.Data = minValue(root.Right)

		// Delete the inorder successor
		root.Right = deleteRecursively(root.Right, root.Data)
	}

	return root
}

func minValue(node *BSTNode) int {
	current := node
	for current.Left != nil {
		current = current.Left
	}
	return current.Data
}
