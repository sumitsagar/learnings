package tree

import (
	"fmt"
	"testing"
)

// Example tree:
//
//	      5
//	     / \
//	    3   7
//	   / \ / \
//	  2  4 6  8
//	 /       \
//	1         9
//	           \
//	            10
func TestCreateBST(t *testing.T) {
	bst := BinarySearchTree{}
	bst.CreateBST([]int{4, 5, 6, 7, 2, 4})
	fmt.Println(bst.ContainsAVal(1))
	fmt.Println(bst.ContainsAVal(7))

}

func TestInorderTraversal(t *testing.T) {
	root := newNode(5)
	root.Left = newNode(3)
	root.Right = newNode(7)
	root.Left.Left = newNode(2)
	root.Left.Right = newNode(4)
	root.Right.Left = newNode(6)
	root.Right.Right = newNode(8)
	root.Left.Left.Left = newNode(1)
	root.Right.Right.Right = newNode(9)
	root.Right.Right.Right.Right = newNode(10)
	// fmt.Println(inorderTraversalRecusive(root))
	fmt.Println(inorderIterative(root))

}

func newNode(val int) *TreeNode {
	return &TreeNode{Val: val}
}
