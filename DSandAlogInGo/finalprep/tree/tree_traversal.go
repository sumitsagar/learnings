package tree

func inorderTraversalRecusive(root *TreeNode) []int {
	retval := []int{}
	if root == nil {
		return []int{}
	}
	leftarr := []int{}
	if root.Left != nil {
		leftarr = inorderTraversalRecusive(root.Left)
	}
	rightarr := []int{}
	if root.Right != nil {
		rightarr = inorderTraversalRecusive(root.Right)
	}
	retval = append(retval, leftarr...)
	retval = append(retval, root.Val)
	retval = append(retval, rightarr...)
	return retval
}

func inorderIterative(root *TreeNode) []int {
	var result []int
	stack := []*TreeNode{}
	current := root

	for current != nil || len(stack) > 0 {
		// Reach the left most Node of the current Node
		for current != nil {
			stack = append(stack, current)
			current = current.Left
		}

		// Current must be nil at this point
		current = stack[len(stack)-1]
		stack = stack[:len(stack)-1]

		// Visit the node
		result = append(result, current.Val)

		// We have visited the node and its left subtree.
		// Now, it's right subtree's turn
		current = current.Right
	}

	return result
}
