package mixedprobleme

import "fmt"

type DLLNode2 struct {
	Data int
	Next *DLLNode2
	Prev *DLLNode2
}

type DLL2 struct {
	Head *DLLNode2
	Tail *DLLNode2
}

func NewDLLNode2(data []int) *DLL2 {
	dll := &DLL2{}
	for _, d := range data {
		dll.Append(d)
	}
	return dll
}

func (dll *DLL2) Append(data int) *DLLNode2 {
	node := &DLLNode2{Data: data}
	if dll.Tail == nil {
		dll.Head = node
		dll.Tail = node
	} else { // this is only node
		currenttail := dll.Tail
		dll.Tail = node
		dll.Tail.Prev = currenttail
		currenttail.Next = dll.Tail
	}
	return node
}

func (dll *DLL2) AppBeginning(data int) *DLLNode2 {
	node := &DLLNode2{Data: data}
	if dll.Head == nil {
		dll.Head = node
		dll.Tail = node
	} else {
		currenthead := dll.Head
		dll.Head = node
		dll.Head.Next = currenthead
		currenthead.Prev = node
	}
	return node
}

func (dll *DLL2) InsertAt(position, data int) {
	curnode := dll.Head
	for i := 1; i <= position; i++ {
		if curnode != nil {
			curnode = curnode.Next
		} else {
			return
		}
	}
	newnode := &DLLNode2{Data: data}
	prev := curnode.Prev
	next := curnode.Next
	prev.Next = newnode
	newnode.Prev = prev
	next.Prev = newnode
	newnode.Next = curnode
}

func (dll *DLL2) Remove(node *DLLNode2) {
	prev := node.Prev
	next := node.Next
	if prev != nil {
		prev.Next = next
	}
	if next != nil {
		next.Prev = prev
	}
}

func (dll *DLL2) Print() {
	if dll.Head == nil {
		fmt.Println("list is empty")
		return
	} else {
		curnode := dll.Head
		for curnode != nil {
			fmt.Println(curnode.Data)
			curnode = curnode.Next
		}
	}
}

//***************************** LRU ************************************//

type LRU2 struct {
	Data map[string]*DLLNode2
	DLL  *DLL2
}

func NewLRU2() *LRU2 {
	lru := &LRU2{}
	lru.DLL = &DLL2{}
	lru.Data = make(map[string]*DLLNode2)
	return lru
}

func (lru *LRU2) Get(key string) *int {
	if val, exists := lru.Data[key]; exists {
		lru.DLL.Remove(val)
		lru.DLL.AppBeginning(val.Data)
		return &(val).Data
	}
	return nil
}

func (lru *LRU2) Set(key string, data int) bool {
	node := lru.DLL.AppBeginning(data)
	lru.Data[key] = node
	return true
}

func (lru *LRU2) Print() {
	for k, v := range lru.Data {
		fmt.Println(k, v)
	}
}
