//to do , this code is not convring all use cases

package mixedprobleme

type Trie struct {
	Root *Node
}

type Node struct {
	Data   string
	Childs []*Node
}

func Constructor() Trie {
	return Trie{Root: nil}
}

func (t *Trie) Insert(word string) {

	c := string(word[0])
	if t.Root == nil {
		t.Root = &Node{Data: c, Childs: []*Node{}}
		remainingword := word[1:]
		InsertRecursive(t.Root, remainingword)
	} else {
		if t.Root.Data != string(word[0]) {
		} else {
			remainingword := word[1:]
			InsertRecursive(t.Root, remainingword)
		}
	}
}

func InsertRecursive(node *Node, word string) {

	if len(word) == 0 {
		return
	}

	if len(node.Childs) > 0 {
		for _, childs := range node.Childs {
			if string(word[0]) == childs.Data {
				InsertRecursive(childs, word[1:])
			} else {
				newnode := &Node{string(word[0]), []*Node{}}
				node.Childs = append(node.Childs, newnode)
				InsertRecursive(newnode, word[1:])
			}
		}
	} else {
		newnode := &Node{string(word[0]), []*Node{}}
		node.Childs = append(node.Childs, newnode)
		InsertRecursive(newnode, word[1:])
	}

}

func (t *Trie) Search(word string) bool {
	found := searchRecursive(t.Root, word)
	return found
}

func searchRecursive(n *Node, word string) bool {
	if len(word) == 0 {
		return true
	}
	if n.Data == string(word[0]) {
		if len(word) == 1 {
			return true
		}
		for _, child := range n.Childs {
			found := searchRecursive(child, word[1:])
			return found
		}
	}
	return false
}

func (t *Trie) StartsWith(prefix string) bool {
	return true
}

/**
 * Your Trie object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(word);
 * param_2 := obj.Search(word);
 * param_3 := obj.StartsWith(prefix);
 */
