package mixedprobleme

import "fmt"

func TopKFrequent(words []string, k int) []string {

	d := make(map[string]int)
	for _, word := range words {
		d[word]++
	}

	fmt.Println(d)
	return []string{}
}
