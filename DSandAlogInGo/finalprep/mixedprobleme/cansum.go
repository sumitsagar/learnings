package mixedprobleme

func CanSum(input []int, target int, memo map[int]bool) bool {
	if target == 0 {
		return true
	}
	if target < 0 {
		return false
	}
	for i := 0; i < len(input); i++ {
		remainder := target - input[i]
		if _, exists := memo[remainder]; exists {
			return true
		}
		//dont return if false
		y := CanSum(input, remainder, memo)
		if y {
			memo[remainder] = true
			return true
		}
	}
	return false
}
