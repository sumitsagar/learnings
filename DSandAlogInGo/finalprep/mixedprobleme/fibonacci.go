package mixedprobleme

func PrintFib(index int, memo map[int]int) int {
	if index <= 2 {
		return 1
	}
	if val, exists := memo[index]; exists {
		return val
	}
	fib := PrintFib(index-1, memo) + PrintFib(index-2, memo)
	memo[index] = fib
	return fib
}
