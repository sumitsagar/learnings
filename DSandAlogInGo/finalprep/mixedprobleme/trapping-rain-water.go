package mixedprobleme

import "math"

func Trap(height []int) int {
	start := 0
	maxtrappedwater := 0
	trappedwater := 0
	bars := 0
	for i, h := range height {
		distance := i - start
		heightoftrap := math.Min(float64(start), float64(h))
		area := distance * int(heightoftrap)
		trappedwatterexcludingbars := trappedwater + (area - bars)
		bars += h
		if trappedwatterexcludingbars > maxtrappedwater {
			maxtrappedwater = trappedwatterexcludingbars
		}
		if h > height[start] {
			start = i
			bars = 0
			trappedwater = 0
		}
	}
	return maxtrappedwater
}
