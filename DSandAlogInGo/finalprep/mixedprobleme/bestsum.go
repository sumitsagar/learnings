package mixedprobleme

func BestSum(input []int, target int) *[]int {

	if target == 0 {
		return &[]int{}
	}
	if target < 0 {
		return nil
	}

	bestsumsofar := []int{}
	for i := 0; i < len(input); i++ {
		remainder := target - i
		bestsum := BestSum(input, remainder)
		if *bestsum != nil {
			*bestsum = append(*bestsum, input[i])
			// return &bestsumsofar
			if len(*bestsum) < len(bestsumsofar) || len(bestsumsofar) == 0 {
				bestsumsofar = *bestsum
			}
		}
	}

	return &bestsumsofar
}
