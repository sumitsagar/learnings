// todo using trie

package mixedprobleme

type WordDictionary struct {
	Dictionary map[string]bool
}

func WordDictionaryConstructor() WordDictionary {
	return WordDictionary{Dictionary: make(map[string]bool)}
}

func (t *WordDictionary) AddWord(word string) {
	t.Dictionary[word] = true
}

func (t *WordDictionary) Search(word string) bool {
	if _, exists := t.Dictionary[word]; exists {
		return true
	} else {
		return false
	}
}
