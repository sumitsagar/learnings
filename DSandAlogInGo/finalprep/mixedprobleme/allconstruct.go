package mixedprobleme

import "strings"

func AllConstruct(target string, words []string) *[][]string {
	if len(target) == 0 {
		return &[][]string{{}} // Return a pointer to a 2D array with one empty slice, representing a valid combination
	}
	var results [][]string
	for _, word := range words {
		if strings.HasPrefix(target, word) {
			suffix := target[len(word):]              // Get the remaining part of the target after the prefix
			suffixWays := AllConstruct(suffix, words) // Get all combinations for the suffix
			for _, way := range *suffixWays {
				newCombination := append([]string{word}, way...) // Prepend the current word to each combination
				results = append(results, newCombination)        // Add the new combination to the results
			}
		}
	}
	return &results // Return a pointer to the results
}
