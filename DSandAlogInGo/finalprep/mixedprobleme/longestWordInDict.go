package mixedprobleme

import "sort"

func LongestWord(words []string) string {
	sort.Strings(words) // Sort the words to ensure lexicographical order

	results := ""
	d := make(map[string]bool)
	for _, word := range words {
		d[word] = true
	}
	for _, word := range words {
		exists := true
		for i := len(word) - 1; i >= 1; i-- {
			_, e := d[word[:i]]
			if !e {
				exists = false
				break
			}
		}
		if exists {
			if len(word) > len(results) {
				results = word
			}
		}
	}
	return results
}
