package mixedprobleme

import "strings"

func CanConstruct(target string, words []string) bool {
	if len(target) == 0 {
		return true
	}
	for _, word := range words {
		if strings.HasPrefix(target, word) {
			cancons := CanConstruct(target[len(word):], words)
			if cancons {
				return cancons
			}
		}
	}
	return false
}
