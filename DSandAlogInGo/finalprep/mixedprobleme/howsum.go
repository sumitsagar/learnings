package mixedprobleme

func HowSum(input []int, target int) *[]int {
	if target == 0 {
		return &[]int{}
	}
	if target < 0 {
		return nil
	}
	for i := 0; i < len(input); i++ {
		remainder := target - input[i]
		b := HowSum(input, remainder)
		if b != nil {
			*b = append(*b, input[i])
			return b
		}
	}
	return nil
}
