package mixedprobleme

import (
	"container/heap"
)

// Task represents a single task with its identifier and frequency.
type Task struct {
	id        rune // Task identifier
	frequency int  // Number of times the task needs to be executed
}

// PriorityQueue implements heap.Interface and holds Tasks.
// This is used to always process the task with the highest frequency first.
type PriorityQueue []*Task

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// We want Pop to give us the highest frequency task, not the lowest.
	// This ensures that the task with the highest frequency is processed first.
	return pq[i].frequency > pq[j].frequency
}

func (pq PriorityQueue) Swap(i, j int) {
	// Swap tasks in the priority queue.
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *PriorityQueue) Push(x interface{}) {
	// Add a new task to the priority queue.
	item := x.(*Task)
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	// Remove and return the task with the highest frequency.
	old := *pq
	n := len(old)
	item := old[n-1]
	*pq = old[0 : n-1]
	return item
}

// leastInterval calculates the least number of intervals needed to complete all tasks with cooling periods.
func leastInterval(tasks []rune, n int) int {
	if n == 0 {
		// If there is no cooling period, the total intervals needed equals the number of tasks.
		return len(tasks)
	}

	// Count the frequency of each task.
	taskCounts := make(map[rune]int)
	for _, task := range tasks {
		taskCounts[task]++
	}

	// Initialize a priority queue and add tasks based on their frequencies.
	pq := make(PriorityQueue, len(taskCounts))
	i := 0
	for id, freq := range taskCounts {
		pq[i] = &Task{id, freq}
		i++
	}
	heap.Init(&pq)

	time := 0 // Track the total intervals needed to complete all tasks.
	// Process tasks until all are completed.
	for pq.Len() > 0 {
		var temp []*Task // Store tasks that still need to be executed.
		cycles := n + 1  // Tracks the number of cycles needed for the cooling period.

		// Try to fill the cycles with tasks.
		for cycles > 0 && pq.Len() > 0 {
			time++
			cycles--
			task := heap.Pop(&pq).(*Task)
			task.frequency--
			if task.frequency > 0 {
				temp = append(temp, task) // If a task is not completed, add it back to be processed later.
			}
		}

		// Re-add tasks to the priority queue for further processing.
		for _, task := range temp {
			heap.Push(&pq, task)
		}

		// If there are cycles left and the priority queue is empty, it means we ended with idle times.
		if pq.Len() == 0 && cycles > 0 {
			time += cycles // Add these cycles (idle times) to the total time.
		}
	}

	return time
}
