package mixedprobleme

func LongestCommonPrefix(strs []string) string {
	if len(strs) == 0 {
		return ""
	}

	// Start with the first string as the prefix
	prefix := strs[0]

	for _, s := range strs[1:] {
		for len(s) < len(prefix) || s[:len(prefix)] != prefix {
			// Reduce prefix size by one from the end until a match is found
			prefix = prefix[:len(prefix)-1]
			if prefix == "" {
				return ""
			}
		}
	}

	return prefix
}
