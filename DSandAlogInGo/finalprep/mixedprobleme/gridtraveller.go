package mixedprobleme

func GridTraveller(x, y int) int {
	if x == 1 && y == 1 {
		return 1
	}
	if x == 0 || y == 0 {
		return 0
	}
	return GridTraveller(x-1, y) + GridTraveller(x, y-1)
}
