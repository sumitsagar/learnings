package mixedprobleme

import (
	"fmt"
	"testing"
)

func TestTrie(t *testing.T) {
	tr := Trie{}
	tr.Insert("sumit")
	tr.Insert("sumeet")
	fmt.Println(tr.Search("sums"))
	fmt.Println(tr.Search("sumit"))
	fmt.Println(tr.Search("sumeet"))
	fmt.Println(tr.Search("sumeett"))
}

func TestLongestCommonPrefix(t *testing.T) {
	fmt.Println(LongestCommonPrefix([]string{"flow", "flower", "flight"}))
}

func TestSuggestedProducts(t *testing.T) {
	fmt.Println(SuggestedProducts([]string{"mobile", "mouse", "moneypot", "monitor", "mousepad"}, "mouse"))
	fmt.Println(SuggestedProducts([]string{"bags", "baggage", "banner", "box", "cloths"}, "mouse"))
}

func TestLongestWord(t *testing.T) {
	fmt.Println(LongestWord([]string{"w", "wo", "wor", "worl", "world"}))
	fmt.Println(LongestWord([]string{"a", "banana", "app", "appl", "ap", "apply", "apple"}))
	fmt.Println(LongestWord([]string{"b", "br", "bre", "brea", "break", "breakf", "breakfa", "breakfas", "breakfast", "l", "lu", "lun", "lunc", "lunch", "d", "di", "din", "dinn", "dinne", "dinner"}))
	fmt.Println(LongestWord([]string{"m", "mo", "moc", "moch", "mocha", "l", "la", "lat", "latt", "latte", "c", "ca", "cat"}))
	fmt.Println(LongestWord([]string{"k", "lg", "it", "oidd", "oid", "oiddm", "kfk", "y", "mw", "kf", "l", "o", "mwaqz", "oi", "ych", "m", "mwa"}))
}

func TestTopKFrequent(t *testing.T) {
	fmt.Println(TopKFrequent([]string{"i", "love", "leetcode", "i", "love", "coding"}, 2))
}

func TestDoubleLL(t *testing.T) {
	dll := &DLL{}
	dll.AppendEnd("4")
	dll.AppendEnd("5")
	dll.AppendEnd("6")
	dll.AppendEnd("7")
	dll.Print()
	fmt.Println("-------------------")
	dll.Remove("6")
	dll.Print()
}

func TestLRUCache(t *testing.T) {
	lru := NewCache()
	lru.AddCache("sumit")
	lru.AddCache("sagar")
	lru.AddCache("divik")
	lru.AddCache("viaan")
	lru.AddCache("malika")
	lru.AddCache("nayan")
	lru.LL.Print()
	fmt.Println("-------------------")
	lru.GetCache("viaan")
	lru.LL.Print()

}

func TestLRUCache2(t *testing.T) {
	// dll := NewDLLNode2([]int{10, 14, 17, 21, 24, 31, 41})
	// dll.Append(13)
	// dll.AppBeginning(0)
	// dll.InsertAt(5, 22)
	// dll.Print()

	lru := NewLRU2()
	lru.Set("ten", 10)
	lru.Set("eleven", 11)
	lru.Set("tweleve", 12)
	lru.Set("thirteen", 13)
	lru.Get("eleven")
	lru.DLL.Print()

}

func TestTrap(t *testing.T) {
	fmt.Println(Trap([]int{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}))
}

func TestPrintFib(t *testing.T) {
	fmt.Println(PrintFib(6, map[int]int{}))
	fmt.Println(PrintFib(50, map[int]int{}))
}

func TestGridTraveller(t *testing.T) {
	fmt.Println(GridTraveller(2, 3))
}
func TestCansum(t *testing.T) {
	fmt.Println(CanSum([]int{1, 4, 3, 5}, 3, map[int]bool{}))
	fmt.Println(CanSum([]int{1, 4, 3, 5}, 7, map[int]bool{}))
	fmt.Println(CanSum([]int{4, 8, 5}, 11, map[int]bool{}))
	fmt.Println(CanSum([]int{4, 8, 5}, 10111100, map[int]bool{}))
}
func TestHowSum(t *testing.T) {
	fmt.Println(HowSum([]int{1, 4, 3, 5}, 3))
	fmt.Println(HowSum([]int{4, 3, 5}, 7))
	fmt.Println(HowSum([]int{4, 8, 5}, 11))
}
func TestBestSum(t *testing.T) {
	fmt.Println(BestSum([]int{1, 4, 3, 5}, 3))
	fmt.Println(BestSum([]int{4, 3, 5}, 7))
	fmt.Println(BestSum([]int{4, 8, 5}, 11))
}

func TestCanConstruct(t *testing.T) {
	fmt.Println(CanConstruct("sumitsagar", []string{"suim", "sumit", "sagar", "gar"}))
	fmt.Println(CanConstruct("sumitsagar", []string{"suim", "sumita", "sagar", "gar"}))
}

func TestLeastInterval(t *testing.T) {
	fmt.Println(leastInterval([]rune{'A', 'A', 'A', 'B', 'B', 'B', 'B'}, 3))
}
