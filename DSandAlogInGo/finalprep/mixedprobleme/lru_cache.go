//completely written by me

package mixedprobleme

import (
	"fmt"
	"hash/fnv"
)

type DLLNode struct {
	Data       string
	Prev, Next *DLLNode
}

type DLL struct {
	Head, Tail *DLLNode
}

func (d *DLL) AppendEnd(data string) *DLLNode {
	node := &DLLNode{Data: data, Prev: nil, Next: nil}
	if d.Tail == nil {
		//means this is first node
		d.Head = node
		d.Tail = node
	} else {
		d.Tail.Next = node //current tail points to new node
		node.Prev = d.Tail
		d.Tail = node //new node becomes tails
	}
	return node
}

func (d *DLL) AddBegining(data string) *DLLNode {
	node := &DLLNode{Data: data, Prev: nil, Next: nil}
	if d.Head == nil {
		//means this is first node
		d.Head = node
		d.Tail = node
	} else {
		d.Head.Prev = node //current head points to new node
		node.Next = d.Head
		d.Head = node //new node becomes head
	}
	return node
}

func (ll *DLL) MoveNodeToEnd(node *DLLNode) {
	if ll.Tail == node { // Node is already the tail
		return
	}

	// If node is head
	if ll.Head == node {
		ll.Head = node.Next
		ll.Head.Prev = nil
	} else { // Node is in the middle
		node.Prev.Next = node.Next
		node.Next.Prev = node.Prev
	}

	// Move node to end
	ll.Tail.Next = node
	node.Prev = ll.Tail
	node.Next = nil
	ll.Tail = node
}

func (d *DLL) Remove(data string) {
	curnode := d.Head
	nodefound := true
	for curnode != nil && curnode.Next != nil && curnode.Next.Data != data {
		curnode = curnode.Next
		nodefound = true
	}
	if nodefound {
		skipnext := curnode.Next.Next
		curnode.Next = curnode.Next.Next
		if skipnext != nil {
			skipnext.Prev = curnode
		}
	}
}

func (d *DLL) Print() {
	if d.Head == nil {
		fmt.Println("empty list")
		return
	}
	curnode := d.Head
	for curnode != nil {
		fmt.Print(curnode.Data)
		if curnode.Next != nil {
			fmt.Print(" next ")
			fmt.Print(curnode.Next.Data)
		}
		if curnode.Prev != nil {
			fmt.Print(" prev ")
			fmt.Print(curnode.Prev.Data)
		}
		fmt.Println()
		curnode = curnode.Next
	}
}

//********************************************** LRU *************************

type LRU struct {
	LL        *DLL                //pointer to a linked list which will store the data
	HashTable map[uint64]*DLLNode //a map of int and ll node pointer , int will store hash of data and pointer to ll node
}

func NewCache() *LRU {
	return &LRU{LL: &DLL{}, HashTable: make(map[uint64]*DLLNode)}
}

func (c *LRU) AddCache(data string) {
	h := hashStringToInt(data)

	llnodeaddress, exists := c.HashTable[h]
	if !exists {
		node := c.LL.AppendEnd(data)
		c.HashTable[h] = node
	} else {
		c.LL.MoveNodeToEnd(llnodeaddress)
	}
}

func (c *LRU) GetCache(data string) string {
	h := hashStringToInt(data)
	llnodeaddress, exists := c.HashTable[h]
	if !exists {
		return ""
	} else {
		c.LL.MoveNodeToEnd(llnodeaddress)
	}
	return llnodeaddress.Data
}

func hashStringToInt(s string) uint64 {
	// Create a new 64-bit FNV hasher
	hasher := fnv.New64a()
	// Write the string to the hasher; ignoring error as hash.Write never returns an error
	hasher.Write([]byte(s))
	// Return the 64-bit hash as an unsigned integer
	return hasher.Sum64()
}
