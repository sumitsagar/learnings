package problmefromsheet

type MedianFinder struct {
	holder []int
}

func MFConstructor() MedianFinder {
	mf := MedianFinder{holder: []int{}}
	return mf

}

func (this *MedianFinder) AddNum(num int) {
	this.holder = append(this.holder, num)

}

func (this *MedianFinder) FindMedian() float64 {
	if len(this.holder)%2 == 0 {
		return float64(this.holder[len(this.holder)])
	} else {
		return 0
	}

}
