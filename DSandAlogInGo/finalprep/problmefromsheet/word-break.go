package problmefromsheet

import "fmt"

func wordBreak(s string, wordDict []string) bool {
	d := make(map[string]bool, len(wordDict))
	for _, val := range wordDict {
		d[val] = true
	}
	return wordBreakSol(s, d, map[string]bool{})
}

// this is solved by me
func wordBreakSol(s string, wordDict map[string]bool, memo map[string]bool) bool {
	if len(s) == 0 {
		return false
	}
	if _, exists := wordDict[s]; exists {
		memo[s] = true
		return true
	}
	if val, exists := memo[s]; exists {
		return val
	}
	for i := 1; i < len(s); i++ {
		currentword := s[0:i]
		remainingword := s[i:]
		fmt.Println(currentword)
		if wordDict[currentword] {
			sol := wordBreakSol(remainingword, wordDict, memo)
			if sol {
				memo[remainingword] = true
				return true
			} else {
				memo[remainingword] = false
			}
		}
	}
	return false
}

func wordBreakDP(s string, wordDict []string) bool {
	wordSet := make(map[string]struct{}) // Create a set from wordDict for O(1) lookups.
	for _, word := range wordDict {
		wordSet[word] = struct{}{}
	}

	dp := make([]bool, len(s)+1)
	dp[0] = true // Base case: an empty string is always segmentable.

	// Iterate over the string to fill dp array.
	for i := 1; i <= len(s); i++ {
		for j := 0; j < i; j++ {
			// If s[:j] can be segmented and s[j:i] is in the dictionary, then s[:i] can be segmented.
			if dp[j] && contains(wordSet, s[j:i]) {
				dp[i] = true
				break // Found a valid segmentation, no need to check further.
			}
		}
	}

	return dp[len(s)] // Return whether the entire string can be segmented.
}

// contains checks if the word is in the set.
func contains(set map[string]struct{}, word string) bool {
	_, exists := set[word]
	return exists
}

// leetcode
// "leet","code"
