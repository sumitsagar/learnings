package problmefromsheet

import "sort"

func threeSum(nums []int) [][]int {
	sort.Ints(nums) // Step 1
	result := [][]int{}

	for i := 0; i < len(nums)-2; i++ { // Step 2
		if i > 0 && nums[i] == nums[i-1] { // Skip duplicate for the first element
			continue
		}
		left, right := i+1, len(nums)-1
		for left < right {

		}
	}

	return result // Step 6
}

// func threesumSol(nums []int, target int, isfirstcall bool) []int {
// 	if target == 0 && !isfirstcall {
// 		return []int{}
// 	}
// 	if len(nums) == 1 && nums[0] == target {
// 		return nums
// 	}
// 	res := []int{}
// 	for i := 0; i < len(nums); i++ {
// 		remainder := target - nums[i]
// 		sum := threesumSol(nums[i+1:], remainder, false)
// 		res = append(res, sum...)
// 		if len(sum) == 0 {
// 			return res
// 		}
// 	}
// 	return res
// }
