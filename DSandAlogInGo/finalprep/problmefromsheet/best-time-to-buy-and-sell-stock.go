package problmefromsheet

func MaxProfit(prices []int) int {
	min := 0
	profit := 0
	for index, val := range prices {
		if val < prices[min] {
			min = index
		}
		currentprofil := val - prices[min]
		if currentprofil > profit {
			profit = currentprofil
		}

	}
	return profit
}

// func maxProfit(prices []int) int {
// 	profit := prices
// 	windowsstartindex := 0
// 	windowendindex := 1
// 	maxprofit := 0
// 	for {

// 		if windowendindex >= len(profit) {
// 			break
// 		}

// 		if profit[windowendindex] < profit[windowsstartindex] {
// 			windowsstartindex = windowendindex
// 			windowendindex = windowsstartindex + 1

// 		} else {

// 			profitnow := profit[windowendindex] - profit[windowsstartindex]
// 			if profitnow > maxprofit {
// 				maxprofit = profitnow
// 			}
// 			if windowendindex == len(profit) { //reached end
// 				if windowsstartindex < len(profit)-2 {
// 					windowsstartindex = windowsstartindex + 1
// 					windowendindex = windowsstartindex + 1
// 				} else { //all exausted
// 					break
// 				}
// 			} else {
// 				windowendindex = windowendindex + 1
// 			}
// 		}
// 	}
// 	return maxprofit
// }
