package problmefromsheet

import (
	"fmt"
	"testing"
)

func TestTwoSum(t *testing.T) {
	fmt.Println(TwoSum([]int{2, 7, 11, 15}, 9))
	fmt.Println(TwoSum([]int{2, 7, 11, 15}, 14))
}

func TestMaxProfit(t *testing.T) {
	fmt.Println(MaxProfit([]int{7, 1, 5, 3, 6, 4}))
	fmt.Println(MaxProfit([]int{7, 6, 4, 3, 1}))
}

func TestProductExceptSelf(t *testing.T) {
	fmt.Println(ProductExceptSelf([]int{1, 2, 3, 4}))
}

func TestMaxSubArray(t *testing.T) {
	fmt.Println(MaxSubArray([]int{-2, 1, -3, 4, -1, 2, 1, -5, 4}))
	fmt.Println(MaxSubArray([]int{-1, -2}))
}

func TestClimbstairs(t *testing.T) {
	fmt.Println(climbStairs(33))
}

func TestCoinChange(t *testing.T) {
	fmt.Println(coinChangeDP([]int{1, 2, 5}, 11))
}
func TestLengthOfLIS(t *testing.T) {
	fmt.Println(lengthOfLIS([]int{0, 1, 0, 3, 2, 3}))
}
func TestLongestCommonSubsequence(t *testing.T) {
	fmt.Println(longestCommonSubsequence("", ""))
}

func TestWordBreakSol(t *testing.T) {
	fmt.Println(wordBreakDP("goalspecial", []string{"go", "goal", "goals", "special"}))
	fmt.Println(wordBreakDP("catsandog", []string{"cats", "dog", "sand", "and", "cat"}))
	fmt.Println(wordBreakDP("catsanddog", []string{"cats", "dog", "sand", "and", "cat"}))
	fmt.Println(wordBreakDP("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab",
		[]string{"a", "aa", "aaa", "aaaa", "aaaaa", "aaaaaa", "aaaaaaa", "aaaaaaaa", "aaaaaaaaa", "aaaaaaaaaa"}))
}

func TestCanjump(t *testing.T) {
	fmt.Println(canJump([]int{2, 3, 1, 1, 4}))
	fmt.Println(canJump([]int{3, 2, 1, 0, 4}))
}

func TestRob(t *testing.T) {
	// fmt.Println(rob([]int{1, 2}))
	fmt.Println(rob([]int{2, 7, 9, 3, 1}))
}
func TestNumDecodings(t *testing.T) {
	// fmt.Println(numDecodings("12"))
	fmt.Println(numDecodings("2101"))
}

func TestThreesumSol(t *testing.T) {
	// fmt.Println(numDecodings("12"))
	fmt.Println(threeSum([]int{-1, 0, 1, 2, -1, -4}))
}

func TestMaxDepth(t *testing.T) {
	// fmt.Println(numDecodings("12"))
	root1 := &TreeNode{3,
		&TreeNode{9, nil, nil},
		&TreeNode{20, &TreeNode{15, nil, nil}, &TreeNode{7, nil, nil}}}
	fmt.Println("Example 1:", maxDepth(root1)) // Output: 3
	fmt.Println(maxDepth(root1))
}

func TestIsSameTree(t *testing.T) {
	// fmt.Println(numDecodings("12"))
	root1 := &TreeNode{3,
		&TreeNode{9, nil, nil},
		&TreeNode{20, &TreeNode{15, nil, nil}, &TreeNode{7, nil, nil}}}

	root2 := &TreeNode{3,
		&TreeNode{9, nil, nil},
		&TreeNode{20, &TreeNode{15, nil, nil}, &TreeNode{8, nil, nil}}}

	fmt.Println(isSameTree(root1, root2))
}

func TestInvertTree(t *testing.T) {
	// fmt.Println(numDecodings("12"))
	root1 := &TreeNode{3,
		&TreeNode{9, nil, nil},
		&TreeNode{20, &TreeNode{15, nil, nil}, &TreeNode{7, nil, nil}}}

	res := invertTree(root1)
	fmt.Println(res)
}

func TestLevelOrder(t *testing.T) {
	// fmt.Println(numDecodings("12"))
	// root1 := &TreeNode{3,
	// 	&TreeNode{9, nil, nil},
	// 	&TreeNode{20, &TreeNode{15, nil, nil}, &TreeNode{7, nil, nil}}}

	// res := levelOrder(root1)
	// fmt.Println(res)

	root2 := &TreeNode{1,
		&TreeNode{2,
			&TreeNode{3,
				&TreeNode{4,
					&TreeNode{5, nil, nil},
					nil},
				nil},
			nil},
		nil}

	res2 := levelOrder(root2)

	fmt.Println(res2)
}

func TestValidateBST(t *testing.T) {

	root1 := &TreeNode{2,
		&TreeNode{1, nil, nil},
		&TreeNode{4, nil, nil}}

	res1 := isValidBST(root1)
	fmt.Println(res1)

	root2 := &TreeNode{5,
		&TreeNode{4, nil, nil}, // Left child of root
		&TreeNode{6,
			&TreeNode{3, nil, nil}, // Left child of 6
			&TreeNode{7, nil, nil}, // Right child of 6
		},
	}
	res2 := isValidBST(root2)

	fmt.Println(res2)
}

func TestIsSubtree(t *testing.T) {
	// Tree 1
	root := &TreeNode{3,
		&TreeNode{4,
			&TreeNode{1, nil, nil}, // Left child of 4
			&TreeNode{2, nil, nil}, // Right child of 4
		},
		&TreeNode{5, nil, nil}, // Right child of root
	}

	subRoot := &TreeNode{4,
		&TreeNode{1, nil, nil},
		&TreeNode{2, nil, nil},
	}

	res := isSubtree(root, subRoot)
	fmt.Println("Test Case 1 Result:", res)
	if res != true {
		t.Errorf("Expected true, got %v", res)
	}
}

func TestIsValid(t *testing.T) {
	// fmt.Println(isValid("()[]{}"))
	// fmt.Println(isValid("){"))
	fmt.Println(isValid("(])"))
	fmt.Println(isValid("(])"))
}

func TestIsPalindrome(t *testing.T) {
	// fmt.Println(isValid("()[]{}"))
	// fmt.Println(isValid("){"))
	fmt.Println(isPalindrome("0P"))
	fmt.Println(isPalindrome("A man, a plan, a canal: Panama"))
	fmt.Println(isPalindrome("race a car"))

}

func TestLongestPalindrome(t *testing.T) {
	// fmt.Println(longestPalindrome("babad"))
	fmt.Println(longestPalindrome("cbbd"))

}

func newNode(val int) *TreeNode {
	return &TreeNode{Val: val}
}

func TestNumIslands(t *testing.T) {
	grid := [][]byte{
		{'1', '1', '1', '1', '0'},
		{'1', '1', '0', '1', '0'},
		{'1', '1', '0', '0', '0'},
		{'0', '0', '0', '0', '0'},
	}

	// fmt.Println(longestPalindrome("babad"))
	fmt.Println(numIslands(grid))

}

func TestBuildTreeFromPReorderAndInOrder(t *testing.T) {
	preorder := []int{3, 9, 20, 15, 7}
	inorder := []int{9, 3, 15, 20, 7}
	fmt.Println(buildTree(preorder, inorder))
}

func TestFindKthSmallestElement(t *testing.T) {
	// Constructing a binary tree with 10 nodes.
	// Example tree:
	//          5
	//         / \
	//        3   7
	//       / \ / \
	//      2  4 6  8
	//     /       \
	//    1         9
	//               \
	//                10
	root := newNode(5)
	root.Left = newNode(3)
	root.Right = newNode(7)
	root.Left.Left = newNode(2)
	root.Left.Right = newNode(4)
	root.Right.Left = newNode(6)
	root.Right.Right = newNode(8)
	root.Left.Left.Left = newNode(1)
	root.Right.Right.Right = newNode(9)
	root.Right.Right.Right.Right = newNode(10)

	// Call the kthSmallest function.
	k := 3 // For example, find the 3rd smallest element.
	fmt.Printf("The %dth smallest element in the tree is: %d\n", k, kthSmallest(root, k))
}

func TestTopKFrequent(t *testing.T) {
	// fmt.Println(topKFrequent([]int{1, 1, 1, 2, 2, 3}, 3))
	// fmt.Println(topKFrequent([]int{-1, -1}, 1))
	// fmt.Println(topKFrequent([]int{1, 2}, 2))
	fmt.Println(topKFrequent([]int{3, 0, 1, 0}, 1))
}

func TestFindMedian(t *testing.T) {
	mf := MFConstructor()
	mf.AddNum(3)
	mf.AddNum(2)
	mf.FindMedian()
	mf.AddNum(3)
	mf.FindMedian()

}

func TestFindMins(t *testing.T) {
	fmt.Println(findMin([]int{4, 5, 6, 7, 0, 1, 2}))
}

func TestSearch(t *testing.T) {
	fmt.Println(search([]int{4, 5, 6, 7, 0, 1, 2}, 0))
}
