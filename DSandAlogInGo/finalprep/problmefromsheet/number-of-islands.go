package problmefromsheet

func numIslands(grid [][]byte) int {
	islands := 0
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i]); j++ {

			// fmt.Println(i, j)
			if grid[i][j] == 0 {
				continue
			}

			l := false
			r := false
			u := false
			d := false

			if i == 0 {
				u = true
			} else if grid[i-1][j] == 0 {
				u = true
			}

			if j == 0 {
				l = true
			} else if grid[i][j-1] == 0 {
				l = true
			}
			if i == len(grid)-1 {
				d = true
			} else if grid[i+1][j] == 0 {
				d = true
			}
			if j == len(grid[i])-1 {
				r = true
			} else if grid[i][j+1] == 0 {
				r = true
			}

			if l && r && u && d {
				islands++
			}
		}
	}
	return islands
}
