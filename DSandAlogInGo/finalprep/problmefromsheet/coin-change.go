package problmefromsheet

import "math"

func coinChangeDP(coins []int, amount int) int {
	// DP array initialization: dp[i] will be storing the minimum number of coins
	// required for amount i. We initialize every value with amount+1 since this is
	// the maximum number of coins it can take to form any amount (all 1s).
	dp := make([]int, amount+1)
	for i := range dp {
		dp[i] = amount + 1
	}

	// Base case: 0 coins are needed to make the amount 0.
	dp[0] = 0

	// Build up the DP table from amount 1 to amount
	for a := 1; a <= amount; a++ {
		for _, coin := range coins {
			if a-coin >= 0 {
				// If the current coin can contribute to the current amount,
				// update the DP table with the minimum of the current value and
				// 1 plus the value at the index representing the remaining amount.

				//dp[a] will be intially = amount+1 but will be updated in each loop of coin
				// so basically we are using dp[a] to compare to check the value with prevous coins

				//why dp[a-coin]+1 , coz suppose we have to make 3 rs , and we have 1,2,5 coin , and suppose currrent loop is for
				//coin 2 , then we can make 3 rs using [one] 2 ruppee and rest of coin needed to make 3-2 i.e. [one] + dp[3-2]
				dp[a] = min(dp[a], dp[a-coin]+1)
			}
		}
	}

	// If dp[amount] is still amount+1, then it was not possible to form the amount
	// with the given coins. Return -1 in this case.
	if dp[amount] > amount {
		return -1
	}

	return dp[amount]
}

// Helper function to find the minimum of two integers.
func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

func CoinChangeRecursive(coins []int, amount int) int {
	memo := make(map[int]int)
	return coinChangeHelper(coins, amount, memo)
}

func coinChangeHelper(coins []int, amount int, memo map[int]int) int {
	// Check if the result is already in the memo.
	if val, ok := memo[amount]; ok {
		return val
	}
	// Base cases
	if amount == 0 {
		return 0
	}
	if amount < 0 {
		return -1
	}
	minCoins := math.MaxInt32
	//branching the problme
	for _, coin := range coins {
		res := coinChangeHelper(coins, (amount - coin), memo)
		//ending of the probleme
		if res < math.MaxInt32 && res >= 0 {
			minCoins = res + 1
		}
	}
	if minCoins == math.MaxInt32 {
		memo[amount] = -1
	} else {
		memo[amount] = minCoins
	}
	return memo[amount]
}
