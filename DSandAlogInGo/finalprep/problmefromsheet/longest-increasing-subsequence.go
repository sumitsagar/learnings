package problmefromsheet

func lengthOfLIS(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	dp := make([]int, len(nums)) // DP array for storing the lengths of LIS

	// Initially, each element is a subsequence of length 1 by itself
	for i := range dp {
		dp[i] = 1
	}

	// Populate the DP array
	for i := 1; i < len(nums); i++ {
		for j := 0; j < i; j++ {
			if nums[j] < nums[i] && dp[i] < dp[j]+1 {
				dp[i] = dp[j] + 1 // Update dp[i] if a longer subsequence ending at nums[i] is found
			}
		}
	}

	// Find the maximum length of LIS from the DP array
	maxLIS := 0
	for _, length := range dp {
		if length > maxLIS {
			maxLIS = length
		}
	}

	return maxLIS
}
