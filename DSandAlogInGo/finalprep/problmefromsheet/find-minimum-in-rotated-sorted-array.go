package problmefromsheet

// 0,1,2,4,5,6,7
// [4,5,6,7,0,1,2] if it was rotated 4 times.
// [0,1,2,4,5,6,7] if it was rotated 7 times.
// Main function to find minimum
func findMin(nums []int) int {
	return findMinHelper(nums, 0, len(nums)-1)
}

// Separately defined recursive helper function
func findMinHelper(nums []int, left, right int) int {
	// Base case: when the search space is reduced to one element
	if left == right {
		return nums[left]
	}

	// If the subarray is already sorted, return the leftmost element
	if nums[left] < nums[right] {
		return nums[left]
	}

	// Find the middle element
	mid := left + (right-left)/2

	// Decide which half to search in
	if nums[mid] >= nums[left] {
		// The left half is sorted, so the pivot is in the right half
		return findMinHelper(nums, mid+1, right)
	} else {
		// The pivot is in the left half
		return findMinHelper(nums, left, mid)
	}
}
