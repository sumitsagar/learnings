package problmefromsheet

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func buildTree(preorder []int, inorder []int) *TreeNode {
	if len(preorder) == 0 || len(inorder) == 0 {
		return nil
	}

	// The first element in preorder is always the root.
	rootVal := preorder[0]
	root := &TreeNode{Val: rootVal}

	// Find the index of the root in inorder slice.
	mid := indexOf(inorder, rootVal)

	// Recursively build the left and right subtrees.
	root.Left = buildTree(preorder[1:mid+1], inorder[:mid])

	//why mid:1:
	//The number of elements in the left subtree in the inorder list is the same as the number of elements
	//in the left subtree in the preorder list.
	//This is because both lists include exactly the same nodes in the left subtree, just in a different order.
	root.Right = buildTree(preorder[mid+1:], inorder[mid+1:])

	return root
}

// indexOf finds the index of a value in a slice. Returns -1 if not found.
func indexOf(slice []int, value int) int {
	for i, v := range slice {
		if v == value {
			return i
		}
	}
	return -1
}
