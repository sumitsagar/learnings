package problmefromsheet

func isValidBST(root *TreeNode) bool {
	return isValidBSTSol(root, nil, nil) // Call helper with no initial bounds
}

// Modified to use lower and upper limit pointers for minimal changes
func isValidBSTSol(root *TreeNode, lower *int, upper *int) bool {
	if root == nil {
		return true
	}
	// Check current node against bounds
	if lower != nil && root.Val <= *lower {
		return false
	}
	if upper != nil && root.Val >= *upper {
		return false
	}
	// Recursively validate subtrees with updated bounds
	// For left subtree, set upper bound to current node's value
	// For right subtree, set lower bound to current node's value
	return isValidBSTSol(root.Left, lower, &root.Val) && isValidBSTSol(root.Right, &root.Val, upper)
}
