package problmefromsheet

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxDepth(root *TreeNode) int {
	// If the current node is nil, return 0 as the depth
	if root == nil {
		return 0
	}

	// Recursively find the depth of the left and right subtrees
	leftDepth := maxDepth(root.Left)
	rightDepth := maxDepth(root.Right)

	// The depth of the current node is the max of leftDepth and rightDepth plus 1
	if leftDepth > rightDepth {
		return leftDepth + 1
	} else {
		return rightDepth + 1
	}
}
