package problmefromsheet

import (
	"strings"
	"unicode"
)

func isPalindrome(s string) bool {
	filteredchar := ""
	for _, c := range s {
		if unicode.IsLetter(c) || unicode.IsNumber(c) { // Corrected condition to include numbers
			filteredchar += strings.ToLower(string(c))
		}
	}
	// fmt.Println(filteredchar)
	strlen := len(filteredchar)
	if strlen == 1 {
		return true
	}
	for i := 0; i < strlen/2; i++ {
		if filteredchar[i] != filteredchar[strlen-i-1] {
			return false
		}
	}
	return true
}
