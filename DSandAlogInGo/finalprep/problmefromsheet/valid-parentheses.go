package problmefromsheet

func isValid(s string) bool {

	if len(s) == 1 {
		return false
	}

	openmap := make(map[string]string)
	openmap["{"] = "}"
	openmap["["] = "]"
	openmap["("] = ")"

	closenmap := make(map[string]struct{})
	closenmap["}"] = struct{}{}
	closenmap["]"] = struct{}{}
	closenmap[")"] = struct{}{}

	stack := []string{}

	for i := 0; i < len(s); i++ {
		if _, exists := openmap[string(s[i])]; exists {
			//add to stack
			stack = append(stack, string(s[i]))
		} else if _, exists := closenmap[string(s[i])]; exists {
			if len(stack) == 0 {
				return false
			}
			popitem := stack[len(stack)-1]
			//lets check if the popped item is a opening brace pair
			v, e := openmap[popitem]
			if !e {
				return false
			} else if string(s[i]) == v {
				stack = stack[0 : len(stack)-1]
			} else {
				return false
			}
		}
	}
	return len(stack) == 0
}
