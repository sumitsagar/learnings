package problmefromsheet

func canJump(nums []int) bool {
	dp := make(map[int]bool, len(nums))
	//first lets set if end can be react directly from each index
	for i := 0; i < len(nums); i++ {
		if nums[i] >= len(nums)-i-1 {
			dp[i] = true
		} else {
			dp[i] = false
		}
	}

	//now lets see if jumping to next position based on value has dp[i]= true,if true then reachable
	for i := len(nums) - 2; i >= 0; i-- {
		nextjump := nums[i]
		if dp[i+nextjump] {
			dp[i] = true
		}
	}

	return dp[0]

}

// func canJumpSol(nums []int, jumpneeded int) bool {
// 	if jumpneeded == 0 {
// 		return true
// 	}
// 	for i := 0; i < len(nums); i++ {
// 		jumpsleft := jumpneeded - nums[i]
// 		canjump := canJumpSol(nums[i+1:], jumpsleft)
// 		if canjump {
// 			return true
// 		}
// 	}
// 	return false
// }
