package problmefromsheet

func topKFrequent(nums []int, k int) []int {
	m := make(map[int]int)
	for _, val := range nums {
		m[val]++
	}

	bucket := make(map[int][]int, len(nums))
	for i := 0; i < len(nums); i++ {
		bucket[i] = []int{}
	}
	//here frerquency is key and number is value
	for k, v := range m {
		val := bucket[v]
		val = append(val, k)
		bucket[v] = val
	}

	response := []int{}
	for i := len(bucket); i >= 0; i-- {
		vals := bucket[i]

		for _, vl := range vals {
			response = append(response, vl)
			if len(response) == k {
				return response
			}
		}

	}
	return nil
}
