package problmefromsheet

// 11106
func numDecodings(s string) int {
	if len(s) == 0 || s[0] == '0' {
		return 0
	}

	// Dynamic programming array to store the number of ways to decode up to each index
	dp := make([]int, len(s)+1)
	dp[0], dp[1] = 1, 1 // Base cases

	for i := 2; i <= len(s); i++ {
		// Check if current digit is valid (not '0')
		if s[i-1] != '0' {
			dp[i] += dp[i-1]
		}

		// Check if two-digit number formed with current and previous digit is valid (10 to 26)
		twoDigit := (s[i-2]-'0')*10 + (s[i-1] - '0')
		if twoDigit >= 10 && twoDigit <= 26 {
			dp[i] += dp[i-2]
		}
	}

	return dp[len(s)]
}

// func numDecodings(s string) int {
// 	m := make(map[string]struct{}, 26)
// 	for i := 1; i <= 26; i++ {
// 		m[fmt.Sprint(i)] = struct{}{}
// 	}
// 	return numDecodingssol(s, m)
// }

// func numDecodingssol(s string, m map[string]struct{}) int {
// 	if len(s) == 0 {
// 		return 0
// 	}
// 	if len(s) == 1 {
// 		if _, exists := m[s]; exists {
// 			return 1
// 		}
// 	}
// 	if len(s) == 2 {
// 		w := 0

// 		if _, exists := m[string(s[0])]; !exists {
// 			return 0
// 		} else {
// 			w++
// 		}
// 		if _, exists := m[string(s[1])]; !exists {
// 			return w
// 		}
// 		if _, exists := m[string(s)]; exists {
// 			w++
// 		}
// 		return w
// 	}
// 	for i := 0; i < len(s); i++ {
// 		str := string(s[i])
// 		if _, exists := m[str]; exists {
// 			r := numDecodings(s[i+1:])
// 			return 1 + r
// 		}
// 	}
// 	return 0
// }

// func numDecodings(s string) int {
// 	m := make(map[string]struct{}, 26)
// 	for i := 1; i <= 26; i++ {
// 		m[fmt.Sprint(i)] = struct{}{}
// 	}
// 	ways := 0
// 	for i := 0; i < len(s); i++ {
// 		for j := i + 1; (j <= i+2) && (j < len(s)); j++ {
// 			str := s[i:j]
// 			if _, exists := m[str]; exists {
// 				ways++
// 			}
// 		}
// 	}
// 	// fmt.Println(m)
// 	return ways
// }
