package problmefromsheet

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
//i solved it :)
func isSameTree(p *TreeNode, q *TreeNode) bool {

	if p == nil && q == nil {
		return true
	}
	if (p == nil && q != nil) || (q == nil && p != nil) {
		return false
	}

	if p.Val != q.Val {
		return false
	}

	if (p.Left == nil && q.Left != nil) || (p.Right == nil && q.Right != nil) {
		return false
	}

	leftmatch := isSameTree(p.Left, q.Left)
	rightmatch := isSameTree(p.Right, q.Right)

	return leftmatch && rightmatch
}
