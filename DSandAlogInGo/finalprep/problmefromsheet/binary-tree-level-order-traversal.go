package problmefromsheet

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func levelOrder(root *TreeNode) [][]int {
	m := make(map[int][]int)
	levelOrderRecursive(root, 0, m)
	r := make([][]int, len(m))
	for k, v := range m {
		// r = append(r, v)
		r[k] = v
	}
	return r
}

func levelOrderRecursive(root *TreeNode, level int, res map[int][]int) {

	if root == nil {
		return
	}
	if val, exists := res[level]; exists {
		val = append(val, root.Val)
		res[level] = val
	} else {
		val := []int{root.Val}
		res[level] = val
	}
	level++
	levelOrderRecursive(root.Left, level, res)
	levelOrderRecursive(root.Right, level, res)
}
