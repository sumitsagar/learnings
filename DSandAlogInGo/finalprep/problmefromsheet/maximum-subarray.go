package problmefromsheet

// -2, 1, -3, 4, -1, 2, 1, -5, 4
func MaxSubArray(nums []int) int {
	if len(nums) == 0 {
		return 0 // or appropriate error handling
	}
	maxsumsofar := nums[0]           // Initialize to the first element
	sumsofar := nums[0]              // Initialize to the first element for correct handling of negative numbers
	for i := 1; i < len(nums); i++ { // Start from the second element
		if nums[i] > sumsofar+nums[i] {
			sumsofar = nums[i]
		} else {
			sumsofar += nums[i]
		}
		if sumsofar > maxsumsofar {
			maxsumsofar = sumsofar
		}
	}
	return maxsumsofar
}
