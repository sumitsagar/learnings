package problmefromsheet

// calculates it as the sum of the ways to climb n-1 steps and n-2 steps.
// This reflects the idea that to reach the nth step, one could have either come from
// the n-1th step (taking one step) or from the n-2th step (taking two steps)

func climbStairs(n int) int {
	return climbStairsRecusrsive(n, map[int]int{})
}
func climbStairsRecusrsive(n int, memo map[int]int) int {
	if n == 0 {
		return 0
	}
	if n == 1 {
		return 1
	}
	if n == 2 {
		return 2
	}
	if val, ok := memo[n]; ok {
		return val
	}

	ways := climbStairsRecusrsive(n-1, memo) + climbStairsRecusrsive(n-2, memo)
	memo[n] = ways
	return ways
}
