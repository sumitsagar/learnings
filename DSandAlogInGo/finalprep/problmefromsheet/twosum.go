package problmefromsheet

func TwoSum(nums []int, target int) []int {
	// Map to store the value and its index
	dic := make(map[int]int)

	for i, num := range nums {
		remainder := target - num
		if j, found := dic[remainder]; found {
			return []int{j, i} // Return the indices of the complement and the current number
		}
		dic[num] = i // Store the index of the current number
	}

	return nil // Return nil if no two numbers sum up to target
}

func TwoSumBrutforce(nums []int, target int) []int {
	for i := 0; i < len(nums); i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i]+nums[j] == target {
				return []int{i, j} // Return the indices of the two numbers
			}
		}
	}
	return nil // Return nil if no two numbers sum up to target
}
