package problmefromsheet

import "fmt"

func kthSmallest(root *TreeNode, k int) int {
	stack := []int{root.Val}
	smallest := root
	for root.Left != nil {
		smallest = root.Left
		stack = append(stack, smallest.Val)
		if root.Right != nil {
			stack = append(stack, root.Right.Val)
		}
		root = root.Left
	}
	fmt.Println(stack)

	return stack[len(stack)-k]
}
