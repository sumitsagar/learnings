package practice

func rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	if len(nums) == 1 {
		return nums[0]
	}
	dp := make([]int, len(nums)+1)
	for i := range dp {
		dp[i] = -1
	}
	dp[0] = nums[0]
	dp[1] = max(nums[0], nums[1])
	for i := 2; i < len(nums); i++ {
		dp[i] = max(dp[i-1], dp[i-2]+nums[i])
	}
	max := 0
	for _, val := range dp {
		if val > max {
			max = val
		}
	}
	return max
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

/*

The "House Robber" problem is a classic example of dynamic programming, where the solution to the problem is built up from solving
smaller subproblems. The intuition and framework for solving such problems involve understanding the constraints and breaking the
problem down into smaller, manageable pieces that can be solved iteratively or recursively. Here's how you can approach it:

Thought Process
Identify the Subproblem: In the "House Robber" scenario, the subproblem is deciding whether to rob a house based on the maximum
amount that can be robbed up to the previous house, taking into account that adjacent houses cannot be robbed.

Define the State: The state is the maximum amount of money you can rob up to the current house without alerting the police.
This state depends on the decisions made at the previous houses.

Recurrence Relation: The key is to find a relation that connects the current state with the previous states. For any house i,
you have two choices:

Rob it: In this case, you cannot rob house i-1, but you can take whatever was robbed up to house i-2 plus the current house's money.
Don't rob it: Simply take the maximum amount robbed up to house i-1.
This gives us the recurrence relation: dp[i] = max(dp[i-1], dp[i-2] + nums[i]), where dp[i] is the maximum amount of money that can be
robbed up to house i.

Base Cases: You need to define base cases for the dynamic programming solution. The base cases are the maximum amounts that can be
robbed with the first house and the maximum of the first two houses, as no houses were robbed before these.

Iterate and Fill the DP Table: Using the recurrence relation, iterate through the list of houses and fill the dynamic programming
table (or array) with the maximum money that can be robbed up to each house.

Answer to the Problem: The answer will be in dp[n-1], where n is the number of houses, as it contains the maximum amount of money
that can be robbed from the entire street of houses.

This Go solution follows the thought process outlined above, where dp[i] represents the maximum amount of money that can be robbed
up to the i-th house.
We use a helper function max to find the maximum between two numbers, aiding in deciding whether to rob the current house or not.

*/
