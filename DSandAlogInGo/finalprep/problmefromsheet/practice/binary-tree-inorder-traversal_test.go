package practice

import (
	"fmt"
	"testing"
)

func TestInorderTraversal(t *testing.T) {
	node3 := &TreeNode{Val: 3}
	node2 := &TreeNode{Val: 2, Left: node3}
	root := &TreeNode{Val: 1, Right: node2}
	inorderTraversal(root)
}

func inorderTraversal(root *TreeNode) []int {
	result := []int{}
	res := inorderTraversalRecursive(root, result)
	fmt.Println(res)
	return res
}

func inorderTraversalRecursive(root *TreeNode, result []int) []int {

	if root == nil {
		return result
	}

	if root.Left != nil {
		result = inorderTraversalRecursive(root.Left, result)
	}

	result = append(result, root.Val)
	if root.Right != nil {
		result = inorderTraversalRecursive(root.Right, result)
		return result
	}
	return result
}
