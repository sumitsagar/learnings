package practice

func maxProduct(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	maxSoFar := nums[0]
	minProduct := nums[0]
	maxProduct := nums[0]

	for i := 1; i < len(nums); i++ {
		if nums[i] < 0 {
			minProduct, maxProduct = maxProduct, minProduct
		}
		maxProduct = max(nums[i], maxProduct*nums[i])
		minProduct = min(nums[i], minProduct*nums[i])

		maxSoFar = max(maxSoFar, maxProduct)
	}

	return maxSoFar
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

/*
Solving the problem of finding the maximum product subarray involves understanding the dynamics of how products change
when multiplied by positive or negative numbers, especially considering the possibility of zero values which reset the
product calculations. Here’s a step-by-step framework to develop intuition and approach for solving this problem, followed
by a GoLang solution.

Intuition and Thought Process
Understand the Problem: Recognize that the problem requires finding a contiguous subarray within an array which yields
 the highest product of its elements. Key considerations include handling negative numbers and zeros.

Identify Subproblems: Note that multiplying two negative numbers results in a positive number, which can potentially
increase the product. A zero resets the product to 0, affecting the overall maximum product.

Track Minimum and Maximum Products: Since negative numbers can turn a small product into a larger one, maintain two
variables to track the minimum and maximum product up to the current element. The maximum product at the current
index can either come from the maximum product up to the previous index times the current element, the minimum product
 (if the current element is negative) times the current element, or the current element itself.

Handle Zeroes and Negative Numbers: Consider that a zero will reset your running product totals, so you need to start
over beyond a zero. For negative numbers, since they can flip the maximum and minimum, keep track of both.

Iterate Through the Array: As you iterate, update the maximum and minimum products based on the current element.
 The maximum product so far needs to be updated at each step.

Edge Cases: Pay attention to edge cases such as arrays with only one element, arrays with all negative numbers,
and arrays with zeros.

*/
