package problmefromsheet

// 1, 2, 3, 4
func ProductExceptSelf(nums []int) []int {
	leftproduct := make([]int, len(nums))
	rightproduct := make([]int, len(nums))
	productsofat := 1

	leftproduct[0] = 1
	for i := 1; i < len(nums); i++ {
		leftproduct[i] = productsofat * nums[i-1]
		productsofat = productsofat * nums[i-1]
	}
	productsofat = 1
	rightproduct[len(nums)-1] = 1
	for i := len(nums) - 2; i >= 0; i-- {
		rightproduct[i] = productsofat * nums[i+1]
		productsofat = productsofat * nums[i+1]
	}

	retval := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		retval[i] = leftproduct[i] * rightproduct[i]
	}
	return retval
}
