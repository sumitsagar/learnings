package problmefromsheet

// [ 2, 7, 9, 3, 1 ]
func rob(nums []int) int {

	if len(nums) == 0 {
		return 0
	}

	if len(nums) == 1 {
		return nums[0]
	}
	dp := make([]int, len(nums)+1)
	for i := range dp {
		dp[i] = -1
	}
	dp[0] = nums[0]
	dp[1] = max(nums[0], nums[1])
	for i := 2; i < len(nums); i++ {
		//i wrote this code but it doesn not consider the case where we can also skip more then 1 hose like skip 2,3 houses
		// dp[i] = nums[i] + dp[i-2]
		// below is the correct coce, where instead if just robbing the curren house we also check how much was robbed fom immdiate previous house
		whatcanberobbed := nums[i] + dp[i-2]
		maxrob := max(whatcanberobbed, dp[i-1])
		dp[i] = maxrob
	}
	max := 0
	for _, val := range dp {
		if val > max {
			max = val
		}
	}
	return max
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func rob_not_working(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	if len(nums) == 1 {
		return nums[0]
	}
	maxrob := 0
	for i := 0; i < len(nums)-1; i++ {
		remaininghouse := nums[i+2:]
		r := nums[i] + rob(remaininghouse)
		if r > maxrob {
			maxrob = r
		}
	}
	return maxrob
}
