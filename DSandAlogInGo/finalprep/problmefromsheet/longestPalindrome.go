package problmefromsheet

func longestPalindrome(s string) string {
	response := []string{}
	cur_palindrome := ""
	for i := 1; i < len(s); i++ {
		cur_palindrome = string(s[i])
		l := i - 1
		r := i + 1

		for l >= 0 && r < len(s) {
			if s[l] == s[r] {
				cur_palindrome = string(s[l]) + cur_palindrome + string(s[r])
			} else {
				break
			}
			l--
			r++
		}
		response = append(response, cur_palindrome)
	}
	maxpalindrome := ""
	for _, p := range response {
		if len(p) > len(maxpalindrome) {
			maxpalindrome = p
		}
	}
	return maxpalindrome
}
