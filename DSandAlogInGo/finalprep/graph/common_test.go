package graph

import "testing"

func TestGraphOperations(t *testing.T) {
	g := NewGraph()
	g.AddVertex(4)
	g.AddVertex(5)
	g.AddEdge(4, 5)
	g.PrintGraph()

}
