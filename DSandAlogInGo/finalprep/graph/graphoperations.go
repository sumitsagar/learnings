package graph

import "fmt"

type Graph struct {
	adjList map[int][]int
}

func NewGraph() *Graph {
	return &Graph{adjList: make(map[int][]int)}
}

func (g *Graph) AddVertex(v int) bool {
	if _, exists := g.adjList[v]; !exists {
		g.adjList[v] = []int{}
		return true
	}
	return false
}

func (g *Graph) RemoveVertex(v int) bool {
	_, exists := g.adjList[v]
	if exists {

		delete(g.adjList, v)
		return true
	}
	return false
}

func (g *Graph) AddEdge(v1, v2 int) bool {
	v1_adj, v1_exists := g.adjList[v1]
	v2_adj, v2_exists := g.adjList[v2]
	if v1_exists && v2_exists {
		g.adjList[v1] = append(v1_adj, v2)
		g.adjList[v2] = append(v2_adj, v1)
		return true
	}
	return false
}

func (g *Graph) RemoveEdge(v1, v2 int) bool {
	// Check if both vertices exist in the graph
	v1_adj, v1_exists := g.adjList[v1]
	v2_adj, v2_exists := g.adjList[v2]

	if !v1_exists || !v2_exists {
		// One or both vertices do not exist, so no edge to remove
		return false
	}

	// Remove v2 from v1's adjacency list
	foundV2 := false
	for i, vertex := range v1_adj {
		if vertex == v2 {
			g.adjList[v1] = append(v1_adj[:i], v1_adj[i+1:]...)
			foundV2 = true
			break // Stop searching once found
		}
	}

	// Remove v1 from v2's adjacency list
	foundV1 := false
	for i, vertex := range v2_adj {
		if vertex == v1 {
			g.adjList[v2] = append(v2_adj[:i], v2_adj[i+1:]...)
			foundV1 = true
			break // Stop searching once found
		}
	}

	// Return true if both vertices were found and removed, else false
	return foundV1 && foundV2
}

func (g *Graph) PrintGraph() {
	for k, v := range g.adjList {
		fmt.Println(k, v)
	}
}
