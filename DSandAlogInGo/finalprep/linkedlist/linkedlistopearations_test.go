package linkedlist

import (
	"fmt"
	"testing"
)

// go test -v -run TestCreateLinkedList
func TestCreateLinkedList(t *testing.T) {
	ll := LinkedList{}
	ll.CreateLinkedList([]int{1, 3, 5, 6, 7})
	fmt.Println("appenind now")
	ll.AppendEnd(1)
	ll.Print()
	fmt.Println("insert beginning")
	ll.InsertBeginning(9)
	ll.Print()
	fmt.Println("inseting in middle..")
	ll.InsertMiddle(22, 3)
	ll.Print()
}

func TestPop(t *testing.T) {
	ll := LinkedList{}
	ll.CreateLinkedList([]int{1})
	ll.Print()
	fmt.Println("popping now..")
	fmt.Println(ll.Pop())
	ll.Print()
}

func TestReverse(t *testing.T) {
	ll := LinkedList{}
	ll.CreateLinkedList([]int{1, 3, 5, 6, 7})
	ll.Print()
	fmt.Println("reversing now..")
	ll.ReverseLinkedList()
	ll.Print()
}
