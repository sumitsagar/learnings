package linkedlist

import (
	"fmt"
)

type LinkedList struct {
	Head *Node
}

// make sure to use pointer to nodes
func (ll *LinkedList) CreateLinkedList(nodes []int) *Node {
	head := &Node{nodes[0], nil}
	curnode := head
	for i := 1; i < len(nodes); i++ {
		newnode := &Node{nodes[i], nil}
		curnode.Next = newnode
		curnode = newnode
	}
	ll.Head = head
	return head
}

func (ll *LinkedList) AppendEnd(i int) {
	if ll.Head == nil {
		return
	} else {
		curnode := ll.Head
		for {
			if curnode.Next == nil {
				newnode := Node{i, nil}
				curnode.Next = &newnode
				break
			} else {
				curnode = curnode.Next
			}
		}
	}
}

func (ll *LinkedList) Pop() *Node {
	if ll.Head == nil {
		return nil
	}
	if ll.Head.Next == nil {
		popnode := ll.Head
		ll.Head = nil
		return popnode
	}
	curnode := ll.Head
	for curnode.Next != nil && curnode.Next.Next != nil {
		curnode = curnode.Next
	}
	popnode := curnode.Next
	curnode.Next = nil
	return popnode
}

func (ll *LinkedList) InsertBeginning(data int) {
	if ll.Head == nil {
		ll.Head = &Node{data, nil}
		return
	}
	next := ll.Head
	ll.Head = &Node{data, nil}
	ll.Head.Next = next
}

func (ll *LinkedList) InsertMiddle(data, position int) {
	if ll.Head == nil {
		fmt.Println("list is empty!")
		return
	}
	currentposition := 1
	curnode := ll.Head

	for currentposition < position-1 && curnode != nil {
		currentposition++
		curnode = curnode.Next
	}
	newnode := &Node{data, curnode.Next}
	curnode.Next = newnode
}

func (ll *LinkedList) ReverseLinkedList() {
	//if list is empty or has just 1 node then do nothing
	if ll.Head == nil || ll.Head.Next == nil {
		return
	}
	var prevnode *Node
	curnode := ll.Head

	for curnode != nil {
		nextnodeforloop := curnode.Next //store next node in a variable since after pointer chnages this will be lost
		curnode.Next = prevnode         //next pointer starts pointing to previous node
		prevnode = curnode              // in next loop curnode will be prvious node
		curnode = nextnodeforloop       //next node become curnode for looping
	}
	ll.Head = prevnode // Set the last node as the new head
}

func (ll LinkedList) Print() {
	if ll.Head == nil {
		fmt.Println("linked list is empty!")
		return
	}
	curnode := ll.Head
	for curnode != nil {
		fmt.Print(curnode.Data)
		curnode = curnode.Next
	}
}
