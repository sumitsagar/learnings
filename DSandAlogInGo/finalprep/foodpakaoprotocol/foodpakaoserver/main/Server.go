package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

func main() {
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		panic(err)
	}
	defer listener.Close()
	fmt.Println("FoodPakao Protocol Server running on port 8080")

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting connection:", err)
			continue
		}
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer conn.Close()

	reader := bufio.NewReader(conn)
	requestLine, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Error reading from connection:", err)
		return
	}

	// Parse the request
	requestParts := strings.Split(strings.TrimSpace(requestLine), ",")
	if len(requestParts) < 4 {
		fmt.Println("Invalid request:", requestLine)
		return
	}

	action, foodItem, quantity, contentType := requestParts[0], requestParts[1], requestParts[2], requestParts[3]
	response := processRequest(action, foodItem, quantity, contentType)

	// Send response
	fmt.Fprintf(conn, response+"\n")
}

func processRequest(action, foodItem, quantity, contentType string) string {
	switch action {
	case "ORDER_FOOD":
		return fmt.Sprintf("Your order for %s of %s (%s) has been placed successfully!", quantity, foodItem, contentType)
	case "DONATE_FOOD":
		return fmt.Sprintf("Thank you for donating %s of %s (%s). Your generosity is appreciated!", quantity, foodItem, contentType)
	default:
		return "Invalid action. Please use ORDER_FOOD or DONATE_FOOD."
	}
}
