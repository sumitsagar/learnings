package main

import (
	"fmt"
	"net"
	"os"
)

// Define the Request struct
type FoodPakaoRequest struct {
	Action      string
	FoodItem    string
	Quantity    string
	ContentType string
}

func main() {
	// Construct a sample request
	request := FoodPakaoRequest{
		Action:      "ORDER_FOOD",
		FoodItem:    "CHAI",
		Quantity:    "1 Cup",
		ContentType: "With_Sugar",
	}

	// Connect to the FPP server
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		fmt.Println("Error connecting to server:", err)
		os.Exit(1)
	}
	defer conn.Close()

	// Send the request
	sendRequest(conn, &request)

	// Read and print the response
	printResponse(conn)
}

// sendRequest constructs and sends a request to the server
func sendRequest(conn net.Conn, req *FoodPakaoRequest) {
	requestLine := fmt.Sprintf("%s,%s,%s,%s\n", req.Action, req.FoodItem, req.Quantity, req.ContentType)
	_, err := conn.Write([]byte(requestLine))
	if err != nil {
		fmt.Println("Error sending request:", err)
		os.Exit(1)
	}
}

// printResponse reads the server's response and prints it
func printResponse(conn net.Conn) {
	buf := make([]byte, 1024)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Error reading response:", err)
		os.Exit(1)
	}
	fmt.Print("Server response: ", string(buf[:n]))
}
