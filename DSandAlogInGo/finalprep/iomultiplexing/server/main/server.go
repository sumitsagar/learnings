package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func handleConnection(conn net.Conn) {
	defer conn.Close()
	remoteAddr := conn.RemoteAddr().String()
	fmt.Println("Connected to:", remoteAddr)

	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		text := scanner.Text()
		fmt.Println("Received from", remoteAddr+":", text)
		fmt.Fprintf(conn, "Echo: %s\n", text)
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading from", remoteAddr+":", err)
	}

	fmt.Println("Connection closed:", remoteAddr)
}

func main() {
	fmt.Println("Server starting on port 8080...")
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println("Error starting server:", err)
		os.Exit(1)
	}
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting connection:", err)
			continue
		}
		go handleConnection(conn)
	}
}
