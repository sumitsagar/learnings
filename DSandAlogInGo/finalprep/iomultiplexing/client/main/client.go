package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		fmt.Println("Error connecting to server:", err)
		os.Exit(1)
	}
	defer conn.Close()
	fmt.Println("Connected to server.")

	message := "Hello, Server!"
	fmt.Println("Sending:", message)
	fmt.Fprintf(conn, "%s\n", message)

	response, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		fmt.Println("Error reading response from server:", err)
		return
	}
	fmt.Print("Received:", response)
}
