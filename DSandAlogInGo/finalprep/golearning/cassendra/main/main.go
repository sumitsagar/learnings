package main

import (
	"fmt"
	"log"

	"github.com/gocql/gocql"
)

func main() {
	// Connect to Cassandra
	cluster := gocql.NewCluster("127.0.0.1") // Change this to your Cassandra's IP/hostname if different
	cluster.Port = 9042                      // Default Cassandra port
	cluster.Keyspace = "system"              // Connect to the 'system' keyspace initially
	cluster.Consistency = gocql.Quorum       // Set the consistency level
	session, err := cluster.CreateSession()
	if err != nil {
		log.Fatalf("Failed to connect to Cassandra: %v", err)
	}
	defer session.Close()

	// Create a new keyspace for our example
	err = session.Query(`CREATE KEYSPACE IF NOT EXISTS example WITH replication = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }`).Exec()
	if err != nil {
		log.Fatalf("Failed to create keyspace: %v", err)
	}

	// Switch to our new keyspace
	session.Close() // Close the existing session to switch keyspaces
	cluster.Keyspace = "example"
	session, err = cluster.CreateSession()
	if err != nil {
		log.Fatalf("Failed to connect to the 'example' keyspace: %v", err)
	}
	defer session.Close()

	// Create a table
	err = session.Query(`CREATE TABLE IF NOT EXISTS users (id UUID PRIMARY KEY, name text, email text)`).Exec()
	if err != nil {
		log.Fatalf("Failed to create table: %v", err)
	}

	// Insert data
	if err := session.Query(`INSERT INTO users (id, name, email) VALUES (?, ?, ?)`, gocql.TimeUUID(), "John Doe", "john.doe@example.com").Exec(); err != nil {
		log.Fatal(err)
	}

	// Query data
	var id gocql.UUID
	var name, email string
	if err := session.Query(`SELECT id, name, email FROM users LIMIT 1`).Consistency(gocql.One).Scan(&id, &name, &email); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("User: %v, %v, %v\n", id, name, email)
}
