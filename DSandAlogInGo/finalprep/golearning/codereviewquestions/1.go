package codereviewquestions

import (
	"fmt"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

// Question 1: Goroutine Leak

func processRequests(requests []int) {
	var wg sync.WaitGroup
	for _, req := range requests {
		wg.Add(1)
		go func() {
			defer wg.Done()
			// Simulate a long-running task
			time.Sleep(2 * time.Second)
			fmt.Println("Processed request", req)
		}()
	}
	// Missing synchronization here
}

// Answer 1: Goroutine Leak
// The issue here is the missing wg.Wait() call at the end of the processRequests function, which leads to a goroutine leak because
// the main goroutine may exit before the child goroutines have finished, causing the program to not wait for all the processing
//
//	to complete.
//
// ---------------------------------------------------------------------
func processRequests2(requests []int) {
	var wg sync.WaitGroup
	for _, req := range requests {
		wg.Add(1)
		go func(req int) { // Correctly passing `req` as a parameter to the goroutine
			defer wg.Done()
			// Simulate a long-running task
			time.Sleep(2 * time.Second)
			fmt.Println("Processed request", req)
		}(req) // Passing `req` here
	}
	wg.Wait() // Correctly waiting for all goroutines to finish
}

// Question 2: Incorrect Error Handling

func readFile(filename string) string {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return "" // Ignoring error
	}
	return string(data)
}

// Answer 2: Incorrect Error Handling
// The problem is that the function silently ignores the error and returns an empty string if reading the file fails. This practice can lead to difficult debugging and maintenance issues. Proper error handling should be implemented, either by returning the error to the caller or handling
//
//	it appropriately within the function.
func readFile2(filename string) (string, error) { // Now returns an error as well
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err // Returning the error to the caller
	}
	return string(data), nil
}

// -----------------------------------------------------------

// Question 3: Inefficient Use of Channels

func fetchData() <-chan int {
	ch := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			ch <- i // Sending data to channel without a consumer ready
		}
		close(ch)
	}()
	return ch
}

// Answer 3: Inefficient Use of Channels
// The potential issue is that sending data to the channel without ensuring there's a ready consumer can lead to a deadlock or slow down the producer goroutine. It's generally advisable to have a consumer ready or use buffered channels appropriately to mitigate this risk.

func fetchData2() <-chan int {
	ch := make(chan int, 10) // Making the channel buffered to prevent blocking
	go func() {
		for i := 0; i < 10; i++ {
			ch <- i // Now it's safe because the channel has a buffer
		}
		close(ch)
	}()
	return ch
}

// --------------------------------------------------

// Question 4: Misuse of defer in a Loop

func openFiles(filenames []string) {
	for _, filename := range filenames {
		f, err := os.Open(filename)
		if err != nil {
			fmt.Println("Error opening file:", err)
			continue
		}
		defer f.Close() // Defer inside a loop
	}
}

// Answer 4: Misuse of defer in a Loop
// Using defer inside a loop can lead to resource exhaustion or unexpected behavior, as deferred calls will only be executed at the end of the function, not at the end of each iteration. It's better to extract the file handling to a separate function where defer can be used safely.

// -------------------------------------------------

func openAndCloseFile(filename string) {
	f, err := os.Open(filename)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer f.Close() // Correctly using defer

	// Perform file operations here
}

func openFiles2(filenames []string) {
	for _, filename := range filenames {
		openAndCloseFile(filename) // Handling each file in its own function
	}
}

// Question 5: Ignoring Channel Capacity

func processWithBuffer() {
	ch := make(chan int, 1)
	ch <- 1 // Sending without checking capacity or having a receiver
	// More operations that might block or take time
	fmt.Println(<-ch)
}

// Answer 5: Ignoring Channel Capacity
// Sending a value to a buffered channel without checking if there's enough capacity or without having a corresponding receiver ready can cause the goroutine to block indefinitely if the channel is already full. This snippet assumes the channel will be consumed immediately, which might not always be the case.
// Proper synchronization or channel handling patterns should be employed.
func processWithBuffer2() {
	ch := make(chan int, 1)
	select {
	case ch <- 1: // Successfully sent
		fmt.Println("Sent 1 to channel")
	default:
		fmt.Println("Channel is full, unable to send")
	}
	// Now it's safe to proceed because we've handled the case where the channel might be full
	fmt.Println(<-ch) // Assuming this is part of a larger pattern where reads are expected
}
