package main

import "fmt"

type RefundRequest struct {
	TransactionId int64
}

type RefundResponse struct {
	RefundReferenceID int64
	Status            string
}
type Refund interface {
	Refund(RefundRequest) RefundResponse
}

type PaymentGateway interface {
	Refund
	CollectPayment(int) bool
}

//************************************************** concrete structs

type PaymentProcess struct {
}

func NewPaymentProcess() PaymentProcess {
	pp := PaymentProcess{}
	return pp
}

func (p *PaymentProcess) ProcessPaymet(amount int, pg PaymentGateway) bool {
	return pg.CollectPayment(amount)
}

type RefundProcess struct {
}

func (p *PaymentProcess) ProcessRefund(TransactionId uint64, pg PaymentGateway) RefundResponse {
	r := RefundRequest{TransactionId: int64(TransactionId)}
	return pg.Refund(r)
}

// ************** struct impementations
// example 1 : PG = Razorpay

type RazorPayPaymentGateway struct {
}

func (pg RazorPayPaymentGateway) CollectPayment(amount int) bool {
	fmt.Println("payment collected for amount by razor", amount)
	return true
}

func (pg RazorPayPaymentGateway) Refund(req RefundRequest) RefundResponse {
	fmt.Println("refund processed via razor")
	return RefundResponse{}
}

// ************** struct impementations
// example 1 : PG = Razorpay

type JuspayPaymentGateway struct {
}

func (pg JuspayPaymentGateway) CollectPayment(amount int) bool {
	fmt.Println("payment collected for amount by Juspay", amount)
	return true
}

func (pg JuspayPaymentGateway) Refund(req RefundRequest) RefundResponse {
	fmt.Println("refund processed via Juspay")
	return RefundResponse{}
}

func main() {

	razorPG := RazorPayPaymentGateway{}
	juspayPG := JuspayPaymentGateway{}

	pp := NewPaymentProcess()

	pp.ProcessPaymet(10, razorPG)
	pp.ProcessRefund(10, razorPG)

	pp.ProcessPaymet(10, juspayPG)
	pp.ProcessRefund(10, juspayPG)
}
