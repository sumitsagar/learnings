package main

import "fmt"

type Car interface {
	GetModel() string
}

func PrintModel(car Car) {
	fmt.Println(car.GetModel())
}

// *** tesla car ***************
type Tesla struct {
	Number string
}

func (Tesla) GetModel() string {
	return "S2"
}

//*** tesla car ***************

//*** toyta  car ***************

type Toyota struct {
	Number string
}

func (Toyota) GetModel() string {
	return "Toyota T1"
}

//*** toyta car ***************

func main() {

	c := &Toyota{}
	PrintModel(c)

	t := &Tesla{}
	PrintModel(t)
}
