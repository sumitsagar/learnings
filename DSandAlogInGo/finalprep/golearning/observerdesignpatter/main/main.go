package main

import (
	"container/list"
	"fmt"
)

// ********************************** observable *************************
type Observable struct {
	subs *list.List
}

func (o *Observable) Subscribe(x observer) {
	o.subs.PushBack(x)
}

func (o *Observable) UnSubscribe(x observer) {
	for z := o.subs.Front(); z != nil; z = z.Next() {
		if z.Value == x {
			o.subs.Remove(z)
		}
	}
}
func (o *Observable) Fire(data interface{}) {
	for z := o.subs.Front(); z != nil; z = z.Next() {
		z.Value.(observer).Notify(data)
	}
}

// ********************************** observable end *************************

//********************************** observer *************************

type observer interface {
	Notify(data interface{})
}

//********************************** observer end *************************

// concreter observable
type PatientObservable struct {
	Observable
	Name string
}

func NewPatient(name string) *PatientObservable {
	return &PatientObservable{Name: name, Observable: Observable{&list.List{}}}
}

func (p *PatientObservable) GotSick() {
	p.Fire(p)
}

// concrente observer
type DoctorObserver struct {
}

func (d *DoctorObserver) Notify(data interface{}) {
	fmt.Println("incoming alert!")
	fmt.Println(data)
}

func main() {
	p := NewPatient("sumit")
	doctor := &DoctorObserver{}
	p.Subscribe(doctor)
	p.Fire(p.Name + " is sick")
}
