package main

import (
	"fmt"
	"sync"
)

func main() {
	WaitGroupAndMutex()
}
func WaitGroupAndMutex() {

	wg := &sync.WaitGroup{}
	mutex := &sync.Mutex{}
	names := []string{}

	wg.Add(3)
	go func(w *sync.WaitGroup, m *sync.Mutex) {
		m.Lock()
		names = append(names, "sumit")
		m.Unlock()
		wg.Done()
	}(wg, mutex)

	go func(w *sync.WaitGroup, m *sync.Mutex) {
		m.Lock()
		names = append(names, "sagar")
		m.Unlock()
		wg.Done()
	}(wg, mutex)

	go func(w *sync.WaitGroup, m *sync.Mutex) {
		m.Lock()
		names = append(names, "divik")
		m.Unlock()
		wg.Done()
	}(wg, mutex)

	go func(w *sync.WaitGroup, m *sync.Mutex) {
		m.Lock()
		names = append(names, "viaan")
		m.Unlock()
	}(wg, mutex)

	wg.Wait()
	fmt.Println(names)

}
