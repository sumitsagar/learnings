package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/foo", handlefoo)
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func handlefoo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "GET request successful")
}
