package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
	"time"
)

func main() {
	// Connect to the server
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		fmt.Println("Error connecting:", err)
		os.Exit(1)
	}
	defer conn.Close()

	// Start listening for messages from the server
	go listenForServerMessages(conn)

	// Mocking user input and sending messages to the server.
	for {
		// Simulating user input. In a real application, you might read from stdin or an input field in a GUI.
		userInput := "Hello from client"
		fmt.Println("Sending message to server:", userInput)
		_, err := conn.Write([]byte(userInput + "\n"))
		if err != nil {
			fmt.Println("Error sending message to server:", err)
			break
		}

		// Sleep for demonstration purposes to simulate time between user inputs
		time.Sleep(5 * time.Second)

		// Example condition to break out of the loop, e.g., specific user input (this is just a placeholder).
		if strings.ToLower(strings.TrimSpace(userInput)) == "exit" {
			fmt.Println("Exiting...")
			break
		}
	}
}

// listenForServerMessages handles incoming messages from the server
func listenForServerMessages(conn net.Conn) {
	reader := bufio.NewReader(conn)
	for {
		message, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("Error reading from server:", err)
			break
		}
		fmt.Print("Server says: ", message)
	}
}
