package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {
	// Listen on TCP port 8080 on all interfaces.
	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer listener.Close()

	fmt.Println("Server is listening on port 8080")

	for {
		// Wait for a connection.
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}

		// Handle the connection in a new goroutine.
		// The loop then returns to accepting, so that
		// multiple connections may be served concurrently.
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	// Close the connection when the function exits
	defer conn.Close()

	// Read from the connection
	// NewReader returns a new Reader whose buffer has the default size.
	reader := bufio.NewReader(conn)
	msg, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println(err)
		return
	}

	// Print the message to the console
	fmt.Printf("Message received: %s", msg)

	// Write a response back to the client
	conn.Write([]byte("Message received.\n"))
}
