package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	C_CustomWriter()
}

func D_InbuiltConsoleWriter() {
	//file_reader implemments io.Read() method
	file_reader, _ := os.Open("test.txt")
	//copy just meed some thing which impents read and write
	n, err := io.Copy(os.Stdout, file_reader)
	fmt.Println("bytes Written :", n)
	fmt.Println("error:", err)
}

func C_CustomWriter() {
	//file_reader implemments io.Read() method
	file_reader, _ := os.Open("test.txt")
	//file_writer implements io.write() method
	file_writer := CustomTypeWithWriter{}
	//copy just meed some thing which impents read and write
	n, err := io.Copy(file_writer, file_reader)
	fmt.Println("bytes Written :", n)
	fmt.Println("error:", err)
}

func B_IOCopy() {
	//file_reader implemments io.Read() method
	file_reader, _ := os.Open("test.txt")
	//file_writer implements io.write() method
	file_writer, _ := os.Create("test_output.txt")
	//copy just meed some thing which impents read and write
	n, err := io.Copy(file_writer, file_reader)
	fmt.Println(n)
	fmt.Println(err)
}

func A_BasicReaderAndWriter() {
	file, _ := os.Create("test.txt")
	// w := io.Writer(file)
	file.Write([]byte("hello...."))
	file.Close()

	fr, _ := os.Open("test.txt")
	// reader := io.Reader(fr)
	b := make([]byte, 11)
	fr.Read(b)
}

type CustomTypeWithWriter struct {
}

func (CustomTypeWithWriter) Write(p []byte) (n int, err error) {
	fmt.Println(string(p))
	return len(p), nil
}
