package main

import (
	"fmt"
	"strconv"
	"time"
)

func main1111() {
	NormalChannel()
	TaskLimiter()
}

func NormalChannel() {
	c := make(chan string)
	go func() {
		fmt.Println(<-c)
	}()
	c <- "sample"
}

func TaskLimiter() {
	c := make(chan int, 3)
	totaltask := 100
	for i := 1; i <= totaltask; i++ {
		go Print(c, strconv.Itoa(i))
		c <- i
	}
}

func Print(c chan int, data string) {
	fmt.Println(data)
	time.Sleep(time.Millisecond * 1000)
	<-c
}
