package main

import (
	"fmt"
	"sync"
)

var test int

func main() {
	test = 3
	fmt.Println(test)
	basicchannel()
}

func basicchannel() {

	wg := &sync.WaitGroup{}
	m := &sync.Mutex{}

	data := []string{}

	wg.Add(4)
	go func() {
		defer wg.Done()
		m.Lock()
		data = append(data, "hello1")
		m.Unlock()
	}()
	go func() {
		defer wg.Done()
		m.Lock()
		data = append(data, "hello2")
		m.Unlock()
	}()
	go func() {
		defer wg.Done()
		m.Lock()
		data = append(data, "hello3")
		m.Unlock()
	}()
	go func() {
		defer wg.Done()
		m.Lock()
		data = append(data, "hello4")
		m.Unlock()
	}()
	wg.Wait()
	fmt.Println(data)

}
