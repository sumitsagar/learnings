package main

import (
	"fmt"
	"sync"
	"time"
)

func SimpleChannel() {
	c := make(chan string)
	go func() {
		fmt.Println(<-c)
	}()
	c <- "hello"
}

func BuferredChannel() {
	c := make(chan string, 5)
	go func() {
		for v := range c {
			fmt.Println(v)
		}
	}()
	c <- "1"
	c <- "2"
	c <- "3"
	c <- "4"
	c <- "5"
	c <- "6"

}

func Task(data int, c chan int) {
	time.Sleep(time.Millisecond * 1000)
	fmt.Println(data)
	<-c
}

func TaskLimit() {

	c := make(chan int, 3)
	for i := 0; i < 10; i++ {
		c <- i
		go Task(i, c)
	}

}

func UpdateSync() {

	sg := &sync.WaitGroup{}
	names := []string{}

	lock := &sync.Mutex{}

	sg.Add(3)
	go func() {
		lock.Lock()
		names = append(names, "sumit")
		lock.Unlock()
		sg.Done()
	}()
	go func() {
		lock.Lock()
		names = append(names, "sagar")
		lock.Unlock()
		sg.Done()
	}()
	go func() {
		lock.Lock()
		names = append(names, "sumit2")
		lock.Unlock()
		sg.Done()
	}()

	sg.Wait()
	fmt.Println(names)

}

func main11() {
	// SimpleChannel()
	// BuferredChannel()
	// TaskLimit()
	UpdateSync()
}
