package main

import (
	"fmt"
	"time"
)

func main1() {
	BufferChannelForTaskLimiting()
}

func BufferedChannel() {
	c := make(chan string, 5)
	c <- "11"
	c <- "22"
	c <- "33"
	c <- "44"
	close(c) //close channel onc writing is done
	for val := range c {
		fmt.Println(val)
	}
}

func SimpleExampleNonBuffered() {
	c := make(chan int)
	go func() {
		fmt.Println(<-c)
		// c <- 4
	}()
	c <- 4
	// fmt.Println(<-c)
	fmt.Println("completed")
}

/*********** BufferChannelForTaskLimiting  ***********************/
func BufferChannelForTaskLimiting() {
	c := make(chan int, 3)
	for i := 0; i <= 500; i++ {
		c <- i // Wait for availability in the buffer. This denotes a slot reservation for work., this blocking call is buffer is full
		go JustPrint(i, c)
	}
}

func JustPrint(id int, ch chan int) {
	defer func() { <-ch }() // Ensure that we always free a slot in the channel when done
	time.Sleep(time.Second)
	fmt.Println("work is done for id ", id)
}

/*********** BufferChannelForTaskLimiting  ***********************/
