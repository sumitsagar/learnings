package BinarySearchTree

import "fmt"

var rootNode *TreeNode

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

var nodecounter int

func CreateFromArray(data []int) *TreeNode {
	root := TreeNode{Val: data[0]}

	for i := 1; i < len(data); i++ {
		Insert(data[i], &root)
	}
	return &root
}

func Insert(data int, rootnode *TreeNode) *TreeNode {
	if rootnode == nil {
		newnode := &TreeNode{Val: data}
		return newnode
	}
	if data < rootnode.Val {
		rootnode.Left = Insert(data, rootnode.Left)
	} else {
		rootnode.Right = Insert(data, rootnode.Right)
	}
	return rootnode
}

func InorderRecursive(root *TreeNode) {
	if root == nil {
		return
	}
	InorderRecursive(root.Left)
	fmt.Printf("%d \n", root.Val)
	InorderRecursive(root.Right)
}

func ReverseOfInorderRecursive(root *TreeNode) {
	if root == nil {
		return
	}
	ReverseOfInorderRecursive(root.Right)
	fmt.Printf("%d \n", root.Val)
	ReverseOfInorderRecursive(root.Left)
}

func PrintKthLargest(root *TreeNode, k int) {
	if root == nil {
		return
	}
	PrintKthLargest(root.Right, k)
	nodecounter = nodecounter + 1
	if nodecounter == k {
		fmt.Println(nodecounter, " - ", root.Val)
	}
	PrintKthLargest(root.Left, k)
}

func HeightOfTree(root *TreeNode) int {
	if root == nil {
		return 0
	}
	leftSubtreeheight := HeightOfTree(root.Left)
	rightSubtreeheight := HeightOfTree(root.Right)

	if leftSubtreeheight > rightSubtreeheight {
		return leftSubtreeheight + 1
	} else {
		return rightSubtreeheight + 1
	}

}

// func HeightOfTree(root *TreeNode) int {
// 	if root == nil {
// 		return 0
// 	}

// 	var left = HeightOfTree(root.Left)
// 	var right = HeightOfTree(root.Right)

// 	if left > right {
// 		return left + 1
// 	} else {
// 		return right + 1
// 	}
// }
