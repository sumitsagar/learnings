package queue

//implementation of quee using stack

import (
	stack "dsalgo/DataStructure/Stack"
)

type Queue struct {
	queue       stack.Stack
	helperqueue stack.Stack
	//tail *Node
}

func init() {

}
func ConstructFromArray(arr []int) *Queue {
	q := Queue{}
	for _, val := range arr {
		q.queue.Push(val)
	}
	return &q
}

func (q *Queue) Enqueue(val int) {
	q.queue.Push(val)
}

func (q *Queue) Length() int {
	return q.queue.GetLength()
}

func (q *Queue) DEqueue() int {

	len := q.queue.GetLength()
	if len == 0 {
		return -1
	}
	//move all data in stack1 to stack2 except last item
	for i := 0; i <= len-2; i++ {
		itm := q.queue.PoP()
		if itm != -1 {
			q.helperqueue.Push(itm)
		} else {
			break
		}
	}

	itemtodeque := q.queue.PoP()

	//now put the items back to stack 1
	for i := 0; i < len-1; i++ {
		itm := q.helperqueue.PoP()
		if itm != -1 {
			q.queue.Push(itm)
		} else {
			break
		}
	}
	return itemtodeque
}
