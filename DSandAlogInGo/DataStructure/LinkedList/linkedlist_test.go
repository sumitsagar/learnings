package linkedlist

import (
	"fmt"
	"testing"
)

func TestAddNodeAndGetLength(t *testing.T) {

	lst := LinkedList{}
	var node1 Node
	node1.Val = 5
	lst.AddNode(&node1)

	var node2 Node
	node2.Val = 50
	lst.AddNode(&node2)

	l := lst.GetLength()
	fmt.Println(l)
	if l != 2 { //not sure what the fuck is wrong here
		t.Error("incorrect legth ,  expected 2 got ", l)
	}
}

func TestPrint(t *testing.T) {

	lst := LinkedList{}
	var node1 Node
	node1.Val = 5
	lst.AddNode(&node1)

	var node2 Node
	node2.Val = 50
	lst.AddNode(&node2)
	csv := lst.PrintAsCSV()
	fmt.Println("data in print node test:", csv)
	if csv != "5 50 " {
		t.Error("print fucntion test failure")
	}
}

func TestRemoveNode(t *testing.T) {

	lst := LinkedList{}
	var node1 Node
	node1.Val = 5
	lst.AddNode(&node1)

	var node2 Node
	node2.Val = 50
	lst.AddNode(&node2)

	lst.RemoveNode(&node2)

	csv := lst.PrintAsCSV()
	fmt.Println("data in removenode test:", csv)
	if csv != "5 " {
		t.Error("RemoveNode fucntion test failure")
	}
}
