package linkedlist

import (
	"fmt"
)

//List is linked list
type LinkedList struct {
	Head *Node
	//tail *Node
}

//Node is node if linked list
type Node struct {
	Val  int
	Next *Node
}

func (l *LinkedList) AddNode(node *Node) bool {
	if l.Head == nil { // if this is first req..then initilizd first node
		l.Head = node
	} else {
		curNode := l.Head
		for {
			if curNode.Next != nil {
				curNode = curNode.Next
			} else {
				curNode.Next = node
				return true
			}
		}
	}
	return false
}

func (l *LinkedList) RemoveNode(node *Node) bool {
	if l.Head == nil {
		return false
	}
	curNode := l.Head

	if curNode == node { // we need to remove head
		if curNode.Next != nil {
			l.Head = curNode.Next
		} else {
			l.Head = nil
		}
	}

	for {
		if curNode.Next != nil {
			if curNode.Next == node {
				curNode.Next = curNode.Next.Next
				return true
			} else {
				break
			}
		}
	}
	return false
}

func (l *LinkedList) GetLength() int {
	len := 0
	if l.Head == nil { // if this is first req..then initilizd first node
		return len
	}
	len++
	curNode := l.Head
	for {
		if curNode.Next != nil {
			curNode = curNode.Next
			len++
		} else {
			fmt.Println(len)
			return len
		}
	}
}

func (l *LinkedList) PrintAsCSV() string {

	csv := ""
	if l.Head == nil { // if this is first req..then initilizd first node
		return ""
	} else {
		curNode := l.Head
		for {
			if curNode != nil {
				str := fmt.Sprintf("%v", curNode.Val)
				csv = csv + str + " "
				curNode = curNode.Next
			} else {
				return csv
			}
		}
	}
}
