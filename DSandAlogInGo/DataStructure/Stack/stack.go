package stack

import "fmt"

type Stack struct {
	stack []int
}

func init() {
	stack := Stack{}
	stack.stack = make([]int, 0)
}

func (a *Stack) Push(s int) {
	a.stack = append(a.stack, s)
}

func (a *Stack) PoP() int {

	if a.stack == nil || len(a.stack) == 0 {
		return -1
	}

	itm := a.stack[len(a.stack)-1]
	a.stack = a.stack[:len(a.stack)-1]
	return itm
}

func (a *Stack) Top() int {

	if a.stack == nil || len(a.stack) == 0 {
		return -1
	}

	itm := a.stack[len(a.stack)-1]
	return itm
}

func (a *Stack) Print() {
	if a.stack != nil {
		for _, i := range a.stack {
			fmt.Println(i)
			fmt.Println("")
		}
	}
}

func (a *Stack) GetLength() int {
	return len(a.stack)
}
