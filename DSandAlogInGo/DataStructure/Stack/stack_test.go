package stack_test

import (
	stack "dsalgo/DataStructure/Stack"
	"testing"
)

func TestStackPush(t *testing.T) {
	s := stack.Stack{}
	s.Push(1)
	s.Push(2)
	if s.GetLength() != 2 {
		t.Error("expected 2 got ", s.GetLength())
	}
}

func TestStackPoP(t *testing.T) {
	s := stack.Stack{}
	s.Push(1)
	s.Push(2)
	s.PoP()
	if s.GetLength() != 1 && s.PoP() != 2 {
		t.Error("expected 2 got ", s.GetLength())
	}
}

func TestStackTop(t *testing.T) {
	s := stack.Stack{}
	s.Push(1)
	s.Push(2)
	s.Push(3)
	s.Push(4)

	if s.Top() != 4 {
		t.Error("expected 4 got ", s.Top())
	}
}
