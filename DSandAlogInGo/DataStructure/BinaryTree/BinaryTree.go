package binarytree

type BinaryNode struct {
	Left  *BinaryNode
	Right *BinaryNode
	Data  int64
}

type BinaryTree struct {
	Root *BinaryNode
}

func (t *BinaryTree) insert(Data int64) *BinaryTree {
	if t.Root == nil {
		t.Root = &BinaryNode{Data: Data, Left: nil, Right: nil}
	} else {
		t.Root.insert(Data)
	}
	return t
}

func (n *BinaryNode) insert(Data int64) {
	if n == nil {
		return
	} else if Data <= n.Data {
		if n.Left == nil {
			n.Left = &BinaryNode{Data: Data, Left: nil, Right: nil}
		} else {
			n.Left.insert(Data)
		}
	} else {
		if n.Right == nil {
			n.Right = &BinaryNode{Data: Data, Left: nil, Right: nil}
		} else {
			n.Right.insert(Data)
		}
	}

}

func CreateBinaryTreeFromArray(arr []int64) *BinaryTree {
	tree := &BinaryTree{}
	for _, val := range arr {
		tree.insert(val)
	}
	return tree
}
