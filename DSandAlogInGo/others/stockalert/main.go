package main

import (
	model "dsalgo/others/stockalert/Model"
	"dsalgo/others/stockalert/Utils"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"
)

func main() {

	symbols := []string{"WIPRO.NS", "TATASTEEL.NS", "DISHTV.NS"}
	yahoostockdataurl := "https://query1.finance.yahoo.com/v8/finance/chart/"
	for {
		for _, symbol := range symbols {
			Process(symbol, yahoostockdataurl)
		}
	}
	time.Sleep(5 * time.Second)
}

func Process(symbol, yahoostockdataurl string) {
	defer func() {
		if err := recover(); err != nil {
			log.Println("panic occurred:", err)
		}
	}()
	fmt.Println(symbol)
	databysymbolurl := yahoostockdataurl + symbol
	httpcode, jsonAsString := Utils.HttpRequest(databysymbolurl, "", nil, "GET")
	if httpcode != 200 {
		fmt.Println("yahoo stock api response for symbol " + symbol + " did not return correct data")
	} else {
		var stockDataStruct model.YahooStockDataResponse
		json.Unmarshal([]byte(jsonAsString), &stockDataStruct)
		stockvalue := stockDataStruct.Chart.Result[0].Meta.RegularMarketPrice

		header := make(map[string]string)
		header["Content-Type"] = "text/plain"
		normalizedsymbol := strings.Split(symbol, ".")[0]
		body := "stockrate " + fmt.Sprintf("%f", stockvalue) + "\n"
		pushgatewayurlwithlables := "http://weiggle.com:9091/metrics/job/stockrate/symbol/" + normalizedsymbol // +"/currency/inr"
		httpcode, body := Utils.HttpRequest(pushgatewayurlwithlables, body, header, "POST")
		fmt.Println("push gateway data : ", body)
		fmt.Println("pushgateway response code:", httpcode, "body", body)
	}
}
