# -----------lec 40 ----------------------------------
# keras syntax basics - part one - preparing the data
# -----------------------------------------------------

# %%
import pandas as pd
import numpy as nu
import seaborn as sns

# %% 
# first import data
df = pd.read_csv("D:\\projects\\talentcarpet\\learning\\MachineLearning\\udemy\\data\\fake_reg.csv")
print(df)
# %%
sns.pairplot(df)
# %%
# now create test train split
from sklearn.model_selection import train_test_split
# %%
# grab the feature that we will use ,  here   X is capital coz they are 2 dimesional
X = df[['feature1','feature2']].values
# %%
#label we will predict is Y  , here y is small coz they are 1 dimesional
y = df['price'].values
# %% 
#30% of total data as test data
X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.3, random_state=42)
# %%
X_train.shape
# %%
X_test.shape
# %%
#scaling or normalizing
from sklearn.preprocessing import MinMaxScaler

# %%
scaler = MinMaxScaler()
scaler.fit(X_train)
# %%
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)
# %%
# now both test and train data is scaled between 0 and 1
X_train.min()
X_train.max()
# %%
# ----------------- lec 41 ----------------------
# keras syntax basics - part two
## we will create mode , run model and generate predection
# ----------------------------------------------
# creating model using keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

# %%
# creating keras base model
# here dense simply means simple feed forward network NN densly-connected
# 4 neurons and activation fusnction relu
model = Sequential()
model.add(Dense(4,activation='relu')) # 4 neuron in this layer 
model.add(Dense(4,activation='relu')) # 4 neuron in this layer 
model.add(Dense(4,activation='relu')) # 4 neuron in this layer 
model.add(Dense(1)) #no activation and 1 neuron

# we are performing regression probleme , coz or lable is continiuos value so we use 
# MSE as loss function and will try to optimize via rmsprop optimizer
model.compile(optimizer='rmsprop',loss='mse')
# %%
# now we will train or fit model with training data
model.fit(x=X_train,y = y_train,epochs=250,verbose=1)
# %%
# we see the training history
loss_df = pd.DataFrame(model.history.history)
loss_df.plot()
# %%
# ----------------- lec 42 ----------------------
# keras syntax basics - part three 
## evaluation of model
# ----------------------------------------------
# this will give us mean squared error on test data
model.evaluate(X_test,y_test,verbose=0) # output -> 24.97
model.evaluate(X_train,y_train,verbose=0) # output -> 23.75

# %%
# lets try to predict from test data
test_predictions = model.predict(X_test)
# %%
test_predictions
# %%
# now lets compare models prediction with actual
test_predictions = pd.Series(test_predictions.reshape(300,))

# %%
pred_df = pd.DataFrame(y_test,columns = ['Expected Y'])
# %%
pred_df = pd.concat([pred_df,test_predictions],axis=1)
# %%
pred_df.columns = ['Expected Y', 'Predicted Y']
# %%
pred_df
# %%
sns.scatterplot(x = 'Expected Y',y = 'Predicted Y', data = pred_df)
# %%
# lets see how to get the errors of model
from sklearn.metrics import mean_absolute_error,mean_squared_error
mean_absolute_error(pred_df['Expected Y'],pred_df['Predicted Y']) #output -> 4.00561
# %%
# how to predict new data
new_gem = [[998,1000]]
new_gem = scaler.transform(new_gem)
# %%
model.predict(new_gem)
# %%
# how to save model
from tensorflow.keras.models import load_model
# %%
model.save('my_gem_model.h5')
# %%
# load the model
later_model = load_model('my_gem_model.h5')
later_model.predict(new_gem)
# %%
