# -----------lec 43 ----------------------------------
# exploratory data analysis
# -----------------------------------------------------

# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# %%
df = pd.read_csv("D:\\projects\\talentcarpet\\learning\\MachineLearning\\udemy\\data\\kc_house_data.csv")
df.shape
# %%
# find count of null for each column
df.isnull().sum()
# %%
# find mean ,min max etc...
df.describe().transpose()
# %%
# visualize now
sns.distplot(df['price'])
# price monstly ranges between 0 to 2 million.. beyond that is mostly outliers
# %%
# lets see how many bedrooms we have
sns.countplot(df['bedrooms'])
# here also we can remove bedroom beyond 8 as its very less
# %%
# lets check corelation.. this is highly recommeded
df.corr()['price'].sort_values()
# we see that sqft has very high corelation with price
# so lets scatterplt price vs sqft_living
sns.scatterplot(x='price',y='sqft_living',data=df)
# here ve can see a nerly linear relationship
# %%
# let see dribution of price by lat and long
plt.figure(figsize=(12,5))
sns.scatterplot(x='price',y='lat',data=df)
# %%
plt.figure(figsize=(12,7))
# this will color lat long based on distribution of price over lat long
sns.scatterplot(x='long',y='lat',data=df,hue='price')
df.head()
# let remove outliers prices to get better hue
# we will remove top 1% high priced houses

# %%
# first 216 rows
non_top_1_perc = df.sort_values('price',ascending=False).iloc[216:]
# %%

sns.scatterplot(x='long',y='lat',data=non_top_1_perc,alpha=0.2,edgecolor='none',hue='price')

# -----------lec 44 ----------------------------------
# exploratory data analysis.. continued
# -----------------------------------------------------


# %%
# drop id as its not needed
df
df = df.drop('id',axis=1)
# format date column
df['date'] = pd.to_datetime(df['date'])
df['date'] # not its date time .. it cam be used to extract year and month
# %%
# now we will do feature engineering to get more information
df['year'] = df['date'].apply(lambda date: date.year)
df['month'] = df['date'].apply(lambda date: date.month)
# %%
# let see relation beetween month and price
sns.boxplot(x= 'month',y = 'price',data = df)
# %%
# above diagram doesnt not seem very usefull so we will manuall see the mean of price 
# over month
df.groupby('month').mean()['price']
# we can plot this 
df.groupby('month').mean()['price'].plot()
# %%
df.groupby('year').mean()['price'].plot()
# now we will drop date as it is complex data type and cant be scaled
df = df.drop('date',axis=1)
# %%
# zip code has too much complexity so we will drip this
df = df.drop('zipcode',axis=1)
# %%
df['sqft_basement'].value_counts()
sns.scatterplot(x='sqft_basement',y='price',data=df)
# %%
# -----------lec 45 ----------------------------------
# creating a model
# -----------------------------------------------------
# now we will serate our lable from features
X = df.drop('price',axis=1).values
y = df['price'].values
# %%
# test cell
df.columns
# %%
# test train split
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3,random_state=101)
# %%
# scaling now
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()

# %%
# fit and transfor in oneline 
X_train= scaler.fit_transform(X_train)
# %%
# test data has not to be fit.. only transformation we will do
X_test = scaler.transform(X_test)
# %%
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation
from tensorflow.keras.optimizers import Adam
# %%
X_train.shape
# X_test.shape
# %%
# lets see shape
X_train.shape
# O : (15129, 19)
# we 19 feature so good to add 19 neurons
model = Sequential()

model.add(Dense(19,activation='relu'))
model.add(Dense(19,activation='relu'))
model.add(Dense(19,activation='relu'))
model.add(Dense(19,activation='relu'))
# now 1 neuron in output lable for predicting price
model.add(Dense(1))

model.compile(optimizer='adam',loss='mse')
# %%
y_train
# %%
# here we will also paas validation data .. it will help on checking prerformace, 
# note: bias is not changed based on validation data but only on training data
model.fit(x=X_train,y=y_train,
          validation_data=(X_test,y_test),
          batch_size=256,epochs=400)
# %%
# -----------lec 46 ----------------------------------
# model evaluation and prediction
# -----------------------------------------------------
# we paased evaluation data so we can compare training loss with loss with evaluation/test data
model.history.history
# we can create data fram
pd.DataFrame(model.history.history)
# we can plot also to visualize
pd.DataFrame(model.history.history).plot()

# we can see validation loss still going down so we can continue training 
# without overfitting
# anyways lets evaluate model
from sklearn.metrics import mean_squared_error,mean_absolute_error,explained_variance_score

# %%
predictions = model.predict(X_test)
# %%
# lets compare to see if the prediction are good
# root mean square error
np.sqrt(mean_squared_error(y_test,predictions))
# O : 168366.25380072463

# %%

mean_absolute_error(y_test,predictions)
# o: 103049.51017312886

# %%
# how to knwo based on mae how good is prediction ,
# we need to look at actuual data frame
df['price'].describe()
#  O : 
# count    2.159700e+04
# mean     5.402966e+05 = 540296
# std      3.673681e+05
# min      7.800000e+04
# 25%      3.220000e+05
# 50%      4.500000e+05
# 75%      6.450000e+05
# max      7.700000e+06

# here mean price is 540296 and mae is 103049 
# and 103049 is 20 % of 540296 so its not that great not horrible also

# %% lets see variance score
explained_variance_score(y_test,predictions)
# o : 0.78627160 best is 1 so its quite ok
# %%
# lets plot test y vs prediction y
plt.scatter(y_test,predictions)
plt.plot(y_test,y_test,'r')
# redline represets perfect prediction line
# from figure we can see the out liars are punishing the model
# expensive houses are causig probleme
# so we can retrain the model by removing top 1% expensive prices

# anyways lets try predict 1 house price

# %%
# lets grab first house from same data frame for prediction
single_house = df.drop('price',axis=1).iloc[0]
# %%
# our model is trained on scaled version so we need to transform , cant paas raw value
# here reshaping is done to convert 1d to 2d coz input has to be in 2d,
#  -1 mean keep old dimension along axis
single_house = scaler.transform(single_house.values.reshape(-1,19))
# %%
model.predict(single_house)
0: 276869.84

# %%
# actual price
df.head(1)['price']
# o: 221900.0
# so predicted is 221900 and actual price is 276869
# we really need to retrain our model removing expensive house who are outliers
