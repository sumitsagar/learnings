# -----------lec 40 ----------------------------------
# EDA and preprocessing
# -----------------------------------------------------

# task: based on tumer size we will find if it is 
# - WDBC-Malignant
# or
# - WDBC-Benign

# %%
import pandas as pd
import numpy as np
# %%
df = pd.read_csv("D:\\projects\\talentcarpet\\learning\\MachineLearning\\udemy\\data\\cancer_classification.csv")
df
# %%
# lets see nulls
df.info()
# %%
df.describe().transpose()
# %%
import seaborn as sns
import matplotlib.pyplot as plt
# %%

sns.countplot(x='benign_0__mal_1',data=df)
# graph shows class is well balanced
# %%
# lets see corelation between features and lables we are predecting
df.corr()['benign_0__mal_1'][:-1].sort_values().plot(kind='bar')
# %%
# this will show relation between every feature
sns.heatmap(df.corr())
# %%
# test train split
X = df.drop('benign_0__mal_1',axis=1).values
y = df['benign_0__mal_1'].values
# %%
from sklearn.model_selection import train_test_split
# %%
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.25,random_state=101)
# %%
from sklearn.preprocessing import MinMaxScaler
# %%
scaler = MinMaxScaler()
# %%
scaler.fit(X_train)
# %%
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)
# %%
# Creating the Model
# # For a binary classification problem
# model.compile(optimizer='rmsprop',
#               loss='binary_crossentropy',
#               metrics=['accuracy'])
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation,Dropout
# %%
X_train.shape
# %%
model = Sequential()

model.add(Dense(units=30,activation='relu'))
model.add(Dense(units=15,activation='relu'))
# For a binary classification last activation is sigmoid
model.add(Dense(units=1,activation='sigmoid'))
# For a binary classification problem
model.compile(loss='binary_crossentropy', optimizer='adam')
# %%
model.fit(x=X_train, 
          y=y_train, 
          epochs=600,
          validation_data=(X_test, y_test), verbose=1
          )
# %%
# lets plot training vs validation loss
losses = pd.DataFrame(model.history.history)
losses.plot()
# this is perfect example of over fitting
# key feature of overfitting is
# first both loss will decrease but later validation loss will increse
# and loss will keep on descrising
# %%
# lets redefine model and see how to do early stopping
model = Sequential()

model.add(Dense(units=30,activation='relu'))
model.add(Dense(units=15,activation='relu'))
# For a binary classification last activation is sigmoid
model.add(Dense(units=1,activation='sigmoid'))
# For a binary classification problem
model.compile(loss='binary_crossentropy', optimizer='adam')

# %%
from tensorflow.keras.callbacks import EarlyStopping
# %%
# patience means dont hard stop but wait for couple of epoc
# min mean we are trying to minimise the loss
# early stops helps us in stopping at correct epoc
early_stop = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=25)
# %%
model.fit(x=X_train, 
          y=y_train, 
          epochs=600,
          validation_data=(X_test, y_test), verbose=1,
          callbacks=[early_stop]
          )
# %%
losses = pd.DataFrame(model.history.history)
losses.plot()
# this is perfect plot we want to see 
# %%
from tensorflow.keras.layers import Dropout
# %%
# another thing we can do to prevent over fitting is adding dropoff layer
# lets create model again
model = Sequential()
model.add(Dense(units=30,activation='relu'))
# 0.5 is the fraction of which neuron we are turning off
# so it mean half of neuron of this layer in each epoc will be turned off and bias and weight wont be updated
model.add(Dropout(0.5))

model.add(Dense(units=15,activation='relu'))
model.add(Dropout(0.5))

model.add(Dense(units=1,activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam')

# %%
# we will use both early fitting and dropoff
model.fit(x=X_train, 
          y=y_train, 
          epochs=600,
          validation_data=(X_test, y_test), verbose=1,
          callbacks=[early_stop]
          )
# %%
model_loss = pd.DataFrame(model.history.history)
model_loss.plot()
# we can see significant imporvement
# %%
predictions = model.predict_classes(X_test)
# %%
# for classification we use confusion matrix and classification report for evaluation
from sklearn.metrics import classification_report,confusion_matrix
# %%
print(classification_report(y_test,predictions))
# accuracy is .97 which is very good
# %%
# we can check the performence via confusion matrix
print(confusion_matrix(y_test,predictions))
# CM for classification probleme is 
    # TP  FP
    # TN  TN
# only 1 wrong prediction
# %%
