# %%
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType
# %%
path = "D:\\projects\\Learning\\MachineLearning\\csv\\mlpaasdata.csv"

# %%
df = pd.read_csv(path,
                 warn_bad_lines=True, error_bad_lines=False)

# print(df)
# %%
df.head(10)
# print count of records
# print("total records: " + str(len(df.index)))
# %%
sns.set(rc={'figure.figsize': (12.7, 8.27)})
sns.countplot(x="lastused", data=df)
# %%
sns.countplot(x="lastused", data=df, hue="phonepe")
# %%
df['phonepe'].plot.hist()
# %%
df.drop("useid", axis=1, inplace=True)
# %%
df.head(5)
# %%
X = df.drop('lastused', axis=1)
y = df['lastused']
# %%
print(X)
print(y)

# %%
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.33, random_state=42)

# %%
# %%
logmode = LogisticRegression(max_iter=1000)
# %%
logmode.fit(X_train, y_train)
# %%
predict = logmode.predict(X_test)
# %%
# %%
# classification_report(y_test, predict)
# %%
# %%
# confusion_matrix(y_test, predict)
# %%
# %%
accuracy_score(y_test, predict)


# %%
# pickeling
pickle.dump(logmode, open('model.pkl', 'wb'))
modelFrompickel = pickle.load(open('model.pkl', 'rb'))
predict = modelFrompickel.predict(X_test)
accuracy_score(y_test, predict)
# %%
# onnx model
initial_type = [('float_input', FloatTensorType([None, 9]))]
onnx = convert_sklearn(logmode, initial_types=initial_type)
with open("artifacts/paymentrecomendation.onnx", "wb") as f:
    f.write(onnx.SerializeToString())

# %%
# df.head(2)
print(logmode.predict([[4, 1, 1, 1, 1, 1, 1, 1, 1]]))
# %%
