# %%
import graphviz
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType
# %%
path = "D:\\projects\\Learning\\MachineLearning\\csv\\mlpaasdata_decisiontree_large.csv"

# %%
df = pd.read_csv(path,
                 warn_bad_lines=True, error_bad_lines=False)

# print(df)
# %%
df.head(10)
# print count of records
# print("total records: " + str(len(df.index)))
# %%
sns.set(rc={'figure.figsize': (12.7, 8.27)})

# %%
# df.drop("useid", axis=1, inplace=True)
# %%
X = df.drop('lastused', axis=1)
y = df['lastused']

# %%
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.33, random_state=42)

# %%
# %%
logmode = DecisionTreeClassifier(
    criterion="gini", random_state=42, max_depth=15, min_samples_leaf=10)
# %%
logmode.fit(X_train, y_train)
# %%
predict = logmode.predict(X_test)
# %%
# %%
classification_report(y_test, predict)
# %%
# %%
confusion_matrix(y_test, predict)
# %%
# %%
accuracy_score(y_test, predict)
# %%
df.head(5)
print(X_test)
target = list(df['lastused'].unique())
feature_names = list(X.columns)

# %%
dot_data = tree.export_graphviz(logmode,
                                out_file=None,
                                feature_names=feature_names,
                                class_names=target,
                                filled=True, rounded=True,
                                special_characters=True)
graph = graphviz.Source(dot_data)
graph

# %%
initial_type = [('float_input', FloatTensorType([None, 12]))]
onnx = convert_sklearn(logmode, initial_types=initial_type)
with open("artifacts/paymoderecommend_decisiontree.onnx", "wb") as f:
    f.write(onnx.SerializeToString())

# %%
