
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def func(x):
    return (4 * np.power(x,4)) - (2 * np.power(x,3)) - ( 12 * np.power(x,2))  # 4x^4 + 2x^3 + 12x^2



def func_derivative(x):
    return (16 * np.power(x,3))  - (6 * np.power(x,2)) - (24 * x)  # d/dx of  4x^4 + 2x^3 + 12x^2 => 16x^3 + 6x^2 + 24x


inp  = np.arange(-10,10,.2)
func_out = []
func_out_derivative = []
 
for x in inp: 
    func_out = np.append(func_out, func(x))
    func_out_derivative = np.append(func_out_derivative, func_derivative(x)) 
 
plt.plot(inp,func_out)
plt.plot(inp,func_out_derivative) 
plt.grid(True)
plt.show()
 
