import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def sigmoid(sop):
    return 1.0 / (1 + np.exp(-1 *sop)) # (1/(1+e^-sop))

def error(predicted,target) :
    return np.power(predicted-target,2) # (pred-tar)^2 or X^2

def error_predicted_deriv(predicted,target):
    return 2 * (predicted - target) # 2*(pred-tar) or 2x

def activation_sop_deriv(sop):
    return sigmoid(sop) * (1.0 - sigmoid(sop)) # actual function

def sop_w_deriv(x):
    return x

def update_w(w,grad,learning_rate):
    return w - learning_rate * grad # uddate the weight



x = 0.1
target = 0.3
learning_rate = 0.01
w = np.random.rand()
print("initial W: ",w)


#for plotting
noofteration = 1000000
_iterations = np.arange(noofteration)
_predicted = np.zeros(noofteration)


for K in range(noofteration):
    #forward paas
    y = w * x
    predicted = sigmoid(y)
    err = error(predicted,target)

    #for plotting
    _predicted[K] = predicted

    #backward paas
    g1 = error_predicted_deriv(predicted,target)
    g2 = activation_sop_deriv(predicted)
    g3 = sop_w_deriv(x)

    grad = g3 * g2 * g1
    print(predicted)

    w = update_w(w,grad,learning_rate)


plt.plot(_iterations,_predicted)  #error vs prediction 
plt.show()


# df = pd.read_csv("../hourlysuccess.csv")
# print(df)
# plt.plot(df["hour"],df["status"])
# plt.show()