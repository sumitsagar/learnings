# %%
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
# %%
path = "D:\\projects\\talentcarpet\\learning\\MachineLearning\\udemy\\data\\txn_10k.csv"

# %%
df = pd.read_csv(path,
    warn_bad_lines=True, error_bad_lines=False)
df.isnull().sum()
# print(df)
# %%
sns.distplot(df['amount'])
# %%


sns.scatterplot(x='amount',y='country',data=df)

# %%
df['date'] = pd.to_datetime(df['requesttime'])

print(df['date'])
# %%
df['hour'] = df['date'].apply(lambda date : date.hour)
# %%
 
print(df['hour'])
# %%
df.groupby('hour').mean()['amount'].plot()