﻿using Microsoft.ML.OnnxRuntime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaaSML.ModelService.InferenceSessions.PaymentRecommendation
{
    public class PaymentRecommendation : InferenceSession
    {
        string _modelpath;
        public PaymentRecommendation(string modelpath) : base(modelpath)
        {
            _modelpath = modelpath;
        }

        public InferenceSession GetInstance()
        {
            return new InferenceSession(_modelpath);
        }
    }
}
