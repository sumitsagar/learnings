﻿using Microsoft.ML.OnnxRuntime.Tensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaaSML.ModelService.Model
{

    public class PaymentRecommendationAPIRequest
    {
        public float phonepe { get; set; }
        public float cc { get; set; }
        public float dc { get; set; }
        public float gpay { get; set; }
        public float addupi { get; set; }
        public float ola { get; set; }
        public float amazonpay { get; set; }
        public float paytm { get; set; }
        //public float nb { get; set; }
        public Tensor<float> AsTensor()
        {
            float[] data = new float[]
            {
                phonepe,cc,dc,gpay,addupi,ola,amazonpay,paytm
            };
            int[] dimensions = new int[] { 1, 8 };
            return new DenseTensor<float>(data, dimensions);
        }
    }
}
