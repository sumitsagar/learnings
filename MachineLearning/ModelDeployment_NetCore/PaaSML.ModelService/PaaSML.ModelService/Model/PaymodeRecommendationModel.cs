﻿using Microsoft.ML.OnnxRuntime.Tensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaaSML.ModelService.Model
{
    public class PaymodeRecommendationModel
    {
        public float phonepe { get; set; }
        public float cc { get; set; }
        public float dc { get; set; }
        public float gpay { get; set; }
        public float addupi { get; set; }
        public float ola { get; set; }
        public float qr { get; set; }
        public float airtel { get; set; }
        public float simpl { get; set; }
        public float amazonpay { get; set; }
        public float paytm { get; set; }
        public float nb { get; set; }
        public Tensor<float> AsTensor()
        {
            float[] data = new float[]
            {
                phonepe,cc,dc,gpay,addupi,ola,qr,airtel,simpl,amazonpay,paytm,nb
            };
            int[] dimensions = new int[] { 1, 12 };
            return new DenseTensor<float>(data, dimensions);
        }
    }
}
