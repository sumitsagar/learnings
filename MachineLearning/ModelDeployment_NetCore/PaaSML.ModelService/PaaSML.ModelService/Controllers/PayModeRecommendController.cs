﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.ML.OnnxRuntime;
using Microsoft.ML.OnnxRuntime.Tensors;
using PaaSML.ModelService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaaSML.ModelService.Controllers
{
    [ApiController]
    [Route("/PaymentRecommendation")]
    public class PayModeRecommendcontroller : Controller
    {
        private InferenceSession _session;

        public PayModeRecommendcontroller(InferenceSession session)
        {
            _session = session;
        }
        [HttpPost]
        public ActionResult Score([FromBody]PaymodeRecommendationModel data)
        {
            var input = new List<NamedOnnxValue>
            {
                NamedOnnxValue.CreateFromTensor("float_input", data.AsTensor())
            };
            var result = _session.Run(input);
            var res = result.First();
            Tensor<float> score = result.First().AsTensor<float>();
            //var prediction = new Prediction { PredictedValue = score.First() * 100000 };
            //result.Dispose();
            return Ok(res);
        }
    }
}
