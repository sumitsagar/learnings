import requests
import json

BASE_URL = "https://www.redbus.in/Home/SolarSearch"
unique_ids = set()
final_list = []

# List of cities to search for
cities =  [
    "Mumbai", "Delhi", "Bangalore", "Hyderabad", "Ahmedabad", "Chennai", "Kolkata", 
    "Surat", "Pune", "Jaipur", "Lucknow", "Kanpur", "Nagpur", "Indore", "Thane", 
    "Bhopal", "Visakhapatnam", "Pimpri-Chinchwad", "Patna", "Vadodara", "Ghaziabad", 
    "Ludhiana", "Agra", "Nashik", "Faridabad", "Meerut", "Rajkot", "Kalyan-Dombivali", 
    "Vasai-Virar", "Varanasi", "Srinagar", "Aurangabad", "Dhanbad", "Amritsar", 
    "Navi Mumbai", "Allahabad", "Ranchi", "Howrah", "Coimbatore", "Jabalpur", "Gwalior", 
    "Vijayawada", "Jodhpur", "Madurai", "Raipur", "Kota", "Guwahati", "Chandigarh", 
    "Solapur", "Hubli-Dharwad", "Bareilly", "Moradabad", "Mysore", "Gurgaon", "Aligarh", 
    "Jalandhar", "Tiruchirappalli", "Bhubaneswar", "Salem", "Mira-Bhayandar", "Warangal", 
    "Thiruvananthapuram", "Guntur", "Bhiwandi", "Saharanpur", "Gorakhpur", "Bikaner", 
    "Amravati", "Noida", "Jamshedpur", "Bhilai", "Cuttack", "Firozabad", "Kochi", 
    "Nellore", "Bhavnagar", "Dehradun", "Durgapur", "Asansol", "Rourkela", "Nanded", 
    "Kolhapur", "Ajmer", "Gulbarga", "Jamnagar", "Ujjain", "Loni", "Siliguri", "Jhansi", 
    "Ulhasnagar", "Jammu", "Mangalore", "Erode", "Belgaum", "Kurnool", "Rajahmundry", 
    "Tirupur", "Shiliguri", "Bilaspur", "Shahjahanpur", "Thrissur", "Alwar", 
    "Kakinada", "Panipat", "Bokaro", "Patiala", "Agartala", "Muzaffarnagar", "Mathura", 
    "Hisar", "Sonipat", "Imphal", "Ratlam", "Darbhanga", "Anantapur", "Karimnagar", 
    "Aizawl", "Ambattur", "Tirunelveli", "Baharampur", "Thanjavur", "Vellore", 
    "Puducherry", "Gandhinagar", "Shimoga", "Tumkur", "Mangalore", "Gangtok", "Rampur", 
    "Korba", "Kota", "Sambalpur", "Durg", "Bhilwara", "Mohali", "Rohtak", "Bardhaman", 
    "Rajapalayam", "Ambala", "Pali", "Sikar", "Thoothukudi", "Rewa", "Mirzapur", 
    "Raichur", "Pali", "Ramagundam", "Haridwar", "Vijayanagaram", "Tenali", 
    "Nagercoil", "Sri Ganganagar", "Karawal Nagar", "Mango", "Thanjavur", "Bulandshahr", 
    "Uluberia", "Katni", "Sambhal", "Singrauli", "Nadiad", "Secunderabad", "Naihati", 
    "Yamunanagar", "Bidhannagar", "Pallavaram", "Bidar", "Munger", "Panchkula", 
    "Burhanpur", "Raurkela Industrial Township", "Kharagpur", "Dindigul", "Gandhidham", 
    "Hospet", "Nangloi Jat", "English Bazar", "Ongole", "Deoghar", "Chapra", "Haldia", 
    "Khandwa", "Nandyal", "Chittoor", "Morena", "Amroha", "Anand", "Bhind", 
    "Bhalswa Jahangir Pur", "Madhyamgram", "Bhiwani", "Berhampore", "Ambala Sadar", 
    "Morbi", "Fatehpur", "Raebareli", "Maheshtala", "Sambalpur", "Durg", "Jhunjhunun", 
    "Neemuch", "Rewari", "Navsari"
]

# Loop through each city in the cities list
for city in cities:
    # Create the request URL
    url = f"{BASE_URL}?search={city}&parentLocationType=CITY"
    print(city)
    try:
        # Make the request
        response = requests.get(url)
        response.raise_for_status()
        
        # Load the JSON data
        data = response.json()
        docs = data.get('response', {}).get('docs', [])
        
        for doc in docs:
            # Check if the ID is unique
            if doc['ID'] not in unique_ids:
                unique_ids.add(doc['ID'])
                final_list.append(doc)
                
    except requests.RequestException as e:
        print(f"Error fetching data for search={city}. Error: {e}")

# Add the 'index' property to each item in the final list
for i, item in enumerate(final_list, 1):
    item['index'] = i

# Save the final list to a file
with open("output_data.json", "w") as f:
    json.dump(final_list, f, indent=4)

print("Data saved to output_data.json")
