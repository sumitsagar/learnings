import pymysql
import json
from datetime import datetime, timedelta

# Read JSON data from input_script.json
with open("input_script.json", "r", encoding="utf-8") as f:
    json_data = json.load(f)

# Connect to MySQL database
conn = pymysql.connect(host='localhost', user='root', password='password', database='ai')
cursor = conn.cursor()

# Prepare and execute the SQL insert statements
for item in json_data:
    script1 = json.dumps(item)  # Convert the entire JSON item to a string
    name = item.get("Title", "")  # Get the 'Title' field 
    current_time = datetime.now()
    scheduledate = current_time + timedelta(hours=24)  # Add 24 hours to the current time
    timestamp_str = current_time.strftime("%d-%m-%H-%M")
    outputfolder = f"./output/{name}-{timestamp_str}"  # Format the outputfolder

    sql_insert_query = """
    INSERT INTO tasks (name, script, outputfolder, scheduledate, isprocessed)
    VALUES (%s, %s, %s, %s, %s)
    """

    cursor.execute(sql_insert_query, (name, script1, outputfolder, scheduledate, 0))

# Commit the changes
conn.commit()

# Close the database connection
cursor.close()
conn.close()
