import pymysql
from datetime import datetime
from main import Run

# Connect to MySQL database
conn = pymysql.connect(host='localhost', user='root', password='password', database='ai')

def ProcessSchduledTask():
# SQL Query to fetch records
    sql_query = """
    SELECT * FROM tasks
    WHERE (scheduledate BETWEEN CURDATE() AND NOW() OR 
        scheduledate BETWEEN NOW() AND NOW() + INTERVAL 15 MINUTE)
        AND isprocessed = 0;
    """

    cursor = conn.cursor()
    cursor.execute(sql_query)

    # Fetch all rows
    rows = cursor.fetchall()

    # Loop through the rows and print each column value
    for row in rows:
        print("ID:", row[0])
        print("Name:", row[1])
        print("Script:", row[2])
        print("Subtitle:", row[3])
        print("Output Folder:", row[4])
        print("Schedule Date:", row[5])
        print("Action:", row[6])
        print("Is Processed:", row[7])
        print("------")
        issuccess,error = Run(row[2])
        update_task_status(row[0],issuccess,error)


    # Close the database connection
    cursor.close()
    conn.close()


def update_task_status(task_id, is_processed, error_message=None):
    try:
        # Connect to MySQL database 
        cursor = conn.cursor()

        # Current time
        current_time = datetime.now()

        # SQL Update Query
        sql_update_query = """
        UPDATE tasks
        SET isprocessed = %s,
            processedat = %s,
            error = %s
        WHERE id = %s
        """

        # Execute the update query
        cursor.execute(sql_update_query, (is_processed, current_time, error_message, task_id))

        # Commit the changes
        conn.commit()

    except Exception as e:
        print(f"An error occurred: {e}")
        if conn:
            conn.rollback()  # Rollback any changes if an error occurs

    finally:
        # Close the database connection
        if cursor:
            cursor.close()
        if conn:
            conn.close()


ProcessSchduledTask()