import base64
from decimal import Decimal
import json
import requests
import speech_recognition as sr
from pydub import AudioSegment
from google.cloud import speech_v1p1beta1 as speech
import os

# Convert WAV to base64
def convert_audio_to_base64(wav_file_path):
    with open(wav_file_path, "rb") as f:
        audio_data = f.read()
    return base64.b64encode(audio_data).decode("utf-8")

# Transcribe using Google Speech-to-Text REST API
def transcribe_audio_withapi(wav_file_path,audiolanguage): 
    audio_base64 = convert_audio_to_base64(wav_file_path=wav_file_path)
    # url = "https://speech.googleapis.com/v1/speech:recognize?key=" + "AIzaSyCq2HTpLBi__tIQ26Q0QXz26FjYM55B7Dc" #my key
    url = "https://speech.googleapis.com/v1/speech:recognize?key=" + "AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw" #this is free
    headers = {"Content-Type": "application/json"}
    payload = json.dumps({
        "audio": {
            "content": audio_base64
        },
        "config": {
            "encoding": "LINEAR16", 
            "languageCode": "en-US",
            "enableWordTimeOffsets" :"true"
        }
    })
    if audiolanguage == "hindi":
        payload = json.dumps({
            "audio": {
                "content": audio_base64
            },
            "config": {
                "encoding": "LINEAR16", 
                "languageCode": "hi-IN",
                "enableWordTimeOffsets" :"true"
            }
        })


    response = requests.post(url, headers=headers, data=payload)
    if response.status_code == 200:
        result = json.loads(response.text)
        if 'results' in result:
            words = result['results'][0]['alternatives'][0]['words']
            transcript = result['results'][0]['alternatives'][0]['transcript']
            return words,transcript
        else:
            return ""
    else:
        return f"API request failed with status {response.status_code}: {response.text}"


def transcribe_audio_with_googlelibrary(audio_file): #this seems to be free, i have not set any key
    recognizer = sr.Recognizer()
    try:
        audio_data = sr.AudioFile(audio_file)
        
        with audio_data as source:
            audio = recognizer.record(source)
            
        text = recognizer.recognize_google(audio),
        return text

    except sr.UnknownValueError:
        print("Could not understand audio")
    except sr.RequestError as e:
        print(f"Could not request results; {e}")
    except Exception as e:
        print(f"An error occurred: {e}")
    
    return ""



# Function to find silence segments in the audio
def find_silence(audio, silence_thresh=-40, min_silence_len=500, seek_step=100):
    silence_segments = []
    segment_start = None

    for i in range(0, len(audio), seek_step):
        segment = audio[i:i + seek_step]
        if segment.dBFS < silence_thresh:
            if segment_start is None:
                segment_start = i
        else:
            if segment_start is not None:
                if (i - segment_start) >= min_silence_len:
                    silence_segments.append((segment_start, i))
                segment_start = None

    return silence_segments
# Function to split audio based on silence segments
def split_audio(audio, silence_segments, min_chunk_len=2000, max_chunk_len=10000, min_silence_for_split=200):  # All lengths in milliseconds
    chunks = []
    start = 0
    
    # Helper function to find the next silence segment of at least 'min_silence_for_split' within a given audio chunk
    def find_next_split_point(chunk, start_index=0, min_silence=min_silence_for_split, silence_thresh=-40, seek_step=100):
        segment_start = None
        for i in range(start_index, len(chunk), seek_step):
            segment = chunk[i:i + seek_step]
            if segment.dBFS < silence_thresh:
                if segment_start is None:
                    segment_start = i
            else:
                if segment_start is not None:
                    if (i - segment_start) >= min_silence:
                        return (segment_start + i) // 2
                    segment_start = None
        return None
    
    for seg_start, seg_end in silence_segments:
        # Pick a point within the silence to split the audio
        split_point = (seg_start + seg_end) // 2
        chunk = audio[start:split_point]
        
        # If the chunk is smaller than min_chunk_len, merge with the previous chunk if available
        if len(chunk) < min_chunk_len and len(chunks) > 0:
            chunks[-1] += chunk
        else:
            # Further split the chunk if it's larger than max_chunk_len
            while len(chunk) > max_chunk_len:
                # Find the next point of silence in the chunk to split it further
                next_split = find_next_split_point(chunk, start_index=max_chunk_len - min_silence_for_split)
                if next_split is not None:
                    # Split the chunk at the point of silence
                    sub_chunk = chunk[:next_split]
                    chunks.append(sub_chunk)
                    chunk = chunk[next_split:]
                else:
                    # No suitable point of silence found, so we'll keep the chunk as is
                    break
            
            # Append the remaining chunk to the list of chunks
            if len(chunk) > 0:
                chunks.append(chunk)
        
        start = split_point
    
    # Handle remaining audio after the last silence segment
    remaining_chunk = audio[start:]
    while len(remaining_chunk) > max_chunk_len:
        next_split = find_next_split_point(remaining_chunk, start_index=max_chunk_len - min_silence_for_split)
        if next_split is not None:
            sub_chunk = remaining_chunk[:next_split]
            chunks.append(sub_chunk)
            remaining_chunk = remaining_chunk[next_split:]
        else:
            break

    if len(remaining_chunk) > 0:
        chunks.append(remaining_chunk)
        
    return chunks





def get_audio_length(audio_chunk):
    # Calculate the length of the audio in milliseconds
    audio_length_ms = len(audio_chunk)
    
    # Convert the length to seconds
    audio_length_s = audio_length_ms / 1000.0
    
    # Return the length as a Decimal for better arithmetic precision
    return Decimal(str(audio_length_s))

def GenerateSubtitle(folder,filename,filename_longsubtitle,audio_path,audiolanguage,minaudiochunklength=200,maxaudiochunklength=99999,minsilencelength=400):
    # Initialize an empty list to store the output
    output_longsubtitle = []

    # Load the audio file
    audio = AudioSegment.from_wav(audio_path)

    # Find silence segments in the audio
    silence_segments = find_silence(audio,min_silence_len=minsilencelength)

    # Split the audio into chunks
    audio_chunks = split_audio(audio, silence_segments,minaudiochunklength,maxaudiochunklength,minsilencelength)

    # Create output directory if it doesn't exist
    output_dir = 'audio_chunks'
    os.makedirs(output_dir, exist_ok=True)

    # Initialize start time as a Decimal for more accurate arithmetic
    start_time = Decimal("0.000")

    # Process each audio chunk
    for i, chunk in enumerate(audio_chunks):
        # Generate output path for each chunk
        output_path = os.path.join(output_dir, f"{i+1}.wav")

        # Export chunk to output path
        chunk.export(output_path, format="wav")

        # Get the length of the audio chunk in seconds, rounded to 3 decimal places
        length = get_audio_length(chunk)

        # Calculate end time based on actual chunk length, rounded to 3 decimal places
        end_time = start_time + length

        # Transcribe the audio chunk (placeholder)
        words,transcription = transcribe_audio_withapi(output_path,audiolanguage)  # Replace with your own API function
        add_and_update_timestamps(words, start_time)

        # print("speech to text - " + transcribed_text)
        os.remove(path=output_path)
        # Append the transcribed text and duration to the output list
        output_longsubtitle.append({
            "text": transcription,
            "duration": [float(start_time), float(end_time)]  # Convert Decimal back to float for JSON serialization
        })

        # Update start_time for the next segment
        start_time = end_time

    # Serialize the output list to JSON
    formateed = FormtDuration(global_words)
    formateed = combine_and_merge_formatted_words(formateed,3)
    output_json_str = json.dumps(formateed, indent=4, ensure_ascii=False)

    # Print or save the JSON string as needed
    print(output_json_str)
    subtitlepath = os.path.join(folder,filename)
    with open(subtitlepath, "w") as f:
        f.write(output_json_str)

    output_json_longsub = json.dumps(output_longsubtitle, indent=4, ensure_ascii=False)
    compressed_sub_path = os.path.join(folder,filename_longsubtitle)
    with open(compressed_sub_path, "w") as f:
        f.write(output_json_longsub)

    return output_json_str,output_json_longsub,get_audio_length(audio)



# Function to add and update timestamps
def add_and_update_timestamps(words, offset):
    global global_words  # Declare the variable as global if it's defined outside the function
    
    for word in words:
        start_time = float(Decimal(word['startTime'][:-1]) + Decimal(offset))
        end_time = float(Decimal(word['endTime'][:-1]) + Decimal(offset))
        word['startTime'] = f"{start_time}s"
        word['endTime'] = f"{end_time}s"
        
    global_words.extend(words)
# GenerateScript('/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/The Call from the Past: When Technology Horrifies_29-08-16-29',
#                '/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/The Call from the Past: When Technology Horrifies_29-08-16-29/concatenated_audio.wav')
def FormtDuration(global_words):
    converted_list = []
    
    for word in global_words:
        start_time = float(word['startTime'][:-1])
        end_time = float(word['endTime'][:-1])
        
        converted_list.append({
            "text": word['word'],
            "duration": [start_time, end_time]
        })
        
    return converted_list


# Function to combine N words and merge their durations from the formatted JSON
def combine_and_merge_formatted_words(formatted_words, word_count):
    combined_words = []
    i = 0
    
    while i < len(formatted_words):
        combined_text = ""
        start_time = formatted_words[i]['duration'][0]
        end_time = formatted_words[i]['duration'][1]
        
        for j in range(word_count):
            if i + j < len(formatted_words):
                combined_text += formatted_words[i + j]['text'] + " "
                end_time = formatted_words[i + j]['duration'][1]
        
        combined_text = combined_text.strip()
        combined_words.append({
            "text": combined_text,
            "duration": [start_time, end_time]
        })
        
        i += word_count
    
    return combined_words

global_words = []

    

