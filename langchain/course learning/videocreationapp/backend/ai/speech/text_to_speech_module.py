# Following pip packages need to be installed:
# !pip install git+https://github.com/huggingface/transformers sentencepiece datasets

from transformers import SpeechT5Processor, SpeechT5ForTextToSpeech, SpeechT5HifiGan
from datasets import load_dataset
import torch
import soundfile as sf
from datasets import load_dataset

def text_to_speech(text: str, output_filename: str = "speech1.wav"):
    # Initialize the processor, model, and vocoder
    processor = SpeechT5Processor.from_pretrained("microsoft/speecht5_tts")
    model = SpeechT5ForTextToSpeech.from_pretrained("microsoft/speecht5_tts")
    vocoder = SpeechT5HifiGan.from_pretrained("microsoft/speecht5_hifigan")
    
    # Process the input text
    inputs = processor(text=text, return_tensors="pt")
    
    # Load xvector containing speaker's voice characteristics from a dataset
    embeddings_dataset = load_dataset("Matthijs/cmu-arctic-xvectors", split="validation")
    speaker_embeddings = torch.tensor(embeddings_dataset[7306]["xvector"]).unsqueeze(0)
    
    # Generate the speech
    speech = model.generate_speech(inputs["input_ids"], speaker_embeddings, vocoder=vocoder)
    
    # Write the generated speech to a file
    sf.write(output_filename, speech.numpy(), samplerate=16000)

text_to_speech("As we dive into the clues, guess what? You'll have the golden opportunity to make electrifying choices! Will they lead you closer to cracking the code or plunge you into a dizzying labyrinth of bewilderment? The climax? Oh, that's the thrilling part you'll have to uncover yourself! So, what's it gonna be? Will you scour the bookshelf, peek under the table, or delve into the mystery of the door's bolt? Ah, the choice is all yours, my friend!  Let's do this!"
)
