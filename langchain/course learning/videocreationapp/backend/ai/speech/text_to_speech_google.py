import requests
import json
import base64
from pydub import AudioSegment
import io

def split_text(text, max_words=100):

    try:
        words = text.split(' ')
        segments = []
        segment = []
        word_count = 0

        for word in words:
            word_count += len(word.split())
            segment.append(word)
            
            if word_count >= max_words or word[-1] == '.':
                segments.append(' '.join(segment))
                segment = []
                word_count = 0
        
        if segment:
            segments.append(' '.join(segment))
            
        return segments
    
    except Exception as e:
        print("error")
        print(e)
        return [text]

def synthesize_text_to_audo(text:str, filename : str, language : str):
    audio_segments = []
    
    # Splitting the text into smaller parts if it's longer than 150 words
    text_segments = split_text(text)

    for segment in text_segments:
        url = "https://us-central1-texttospeech.googleapis.com/v1beta1/text:synthesize"
        headers = {
            "Content-Type": "application/json",
            "X-Goog-Api-Key": "AIzaSyCq2HTpLBi__tIQ26Q0QXz26FjYM55B7Dc"  # Your API key
        }

        payload = json.dumps({})
        
        if language == "english":
            payload = json.dumps({
                "audioConfig": {
                    "audioEncoding": "LINEAR16",
                    "effectsProfileId": [
                        "small-bluetooth-speaker-class-device"
                    ],
                    "pitch": 0,
                    "speakingRate": .85
                },
                "input": {
                    "text": segment
                },
                "voice": {
                    "languageCode": "en-US",
                    "name": "en-US-Neural2-J"
                }
            })

        if language == "hindi":
            payload = json.dumps({
                "audioConfig": {
                    "audioEncoding": "LINEAR16",
                    "pitch": -8,
                    "speakingRate": .9
                },
                "input": {
                    "text": segment
                },
                "voice": {
                    "languageCode": "hi-IN",
                    "name": "hi-IN-Neural2-C"
                }
            })

        response = requests.post(url, headers=headers, data=payload)

        if response.status_code == 200:
            response_data = json.loads(response.text)
            audio_content = base64.b64decode(response_data["audioContent"])
            
            # Convert binary audio content to pydub AudioSegment
            audio_segment = AudioSegment.from_wav(io.BytesIO(audio_content))
            audio_segments.append(audio_segment)
        else:
            print("Error:", response.content)
            return
    
    # Combine audio segments
    combined_audio = sum(audio_segments)
    combined_audio.export(filename, format="wav")
    print(f"Audio content written to {filename}")

if __name__ == "__main__":
    import io  # Importing IO for BytesIO
    
    long_text = "This is a sample text. It can be long enough to exceed 150 words so that we can test the functionality of splitting it into smaller segments based on full stops and then generating audio for each segment."
    synthesize_text_to_audo(long_text, "output.wav", "english")
