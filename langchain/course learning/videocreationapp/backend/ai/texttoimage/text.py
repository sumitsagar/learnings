from PIL import Image, ImageDraw, ImageFont

# Create an image with PIL
image = Image.new('RGB', (500, 300), color = (73, 109, 137))

# Initialize the drawing context
draw = ImageDraw.Draw(image)

# Define the text and font
text = "Hello, World!"
font = ImageFont.truetype("arial.ttf", 40)  # Change the font path as needed

# Calculate text size
text_width, text_height = draw.textlength(text, font=font)

# Print text size
print("Text width:", text_width)
print("Text height:", text_height)
