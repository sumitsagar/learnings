from diffusers import StableDiffusionPipeline
import torch
import os

# model_id = "runwayml/stable-diffusion-v1-5"
# pipe = StableDiffusionPipeline.from_pretrained(model_id, torch_dtype=torch.float16)
# pipe = pipe.to("cuda")

# prompt = "white boat in a river"
# image = pipe(prompt).images[0]  
    
# image.save("boat.png")
model_id = "runwayml/stable-diffusion-v1-5"
pipe = StableDiffusionPipeline.from_pretrained(model_id, torch_dtype=torch.float16)
pipe = pipe.to("cuda")
def generate_and_save_image(folder, file_name, prompt):
    
    image = pipe(prompt).images[0]
    
    # Create folder if it doesn't exist
    if not os.path.exists(folder):
        os.makedirs(folder)
    
    # Create the full file path
    full_file_path = os.path.join(folder, file_name)
    
    # Save the image
    image.save(full_file_path)
    print(f"Image saved at {full_file_path}")

# Example usage:
# generate_and_save_image("my_folder", "boat.png", "white boat in a river")
# generate_and_save_image("my_folder", "monkey.png", "monkey in a river")