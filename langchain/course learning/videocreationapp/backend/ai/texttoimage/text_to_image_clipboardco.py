# key 2a9578fdd94eecccd652c12f06a05e222c82108c08376523f159896d8417a8b7bb260d6ae8fb8780f21d1a4a36d50bb9
import requests

# https://clipdrop.co/apis/account
def generate_image_from_text(file_path, prompt_text, api_key="a976d951879bb60d358d57aaa866ce8e5425f6a9ef5f59a2a1a50de6f5c263f6044aebfb358ec5f89fca317e732d9b01"):
    """
    Generate an image from a text prompt using the Clipdrop API and save it to a file.

    Parameters:
    - file_path (str): The path where the generated image will be saved.
    - prompt_text (str): The text prompt based on which the image will be generated.
    - api_key (str): The API key for the Clipdrop API.

    Returns:
    - None
    """
    try:
        # Prepare the POST request
        r = requests.post(
            'https://clipdrop-api.co/text-to-image/v1',
            files={
                'prompt': (None, prompt_text, 'text/plain')
            },
            headers={'x-api-key': api_key}
        )

        # Check the response
        if r.ok:
            # Save the returned image to the specified file path
            with open(file_path, "wb") as f:
                f.write(r.content)
        else:
            r.raise_for_status()

    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")

# Example usage:
# generate_image_from_text("generated_image1.jpg", "prompt")
