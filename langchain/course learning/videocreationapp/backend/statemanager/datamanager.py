from datetime import datetime
import pymysql


# Global database configuration
HOST = 'localhost'
USER = 'root'
PASSWORD = 'password'
DATABASE = 'ai'
CHARSET = 'utf8mb4'

# def insert_script(requestid, script):
#     connection = pymysql.connect(host=HOST,
#                                  user=USER,
#                                  password=PASSWORD,
#                                  database=DATABASE,
#                                  charset=CHARSET)
#     cursor = connection.cursor()

#     current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#     cursor.execute("INSERT INTO scripts (requestid, script, createdon, updatedon) VALUES (%s, %s, %s, %s)",
#                    (requestid, script, current_time, current_time))
#     connection.commit()
#     last_inserted_id = cursor.lastrowid
#     connection.close()
#     return last_inserted_id

# def get_script_by_requestid(requestid):
#     connection = pymysql.connect(host=HOST,
#                                  user=USER,
#                                  password=PASSWORD,
#                                  database=DATABASE,
#                                  charset=CHARSET)
#     cursor = connection.cursor()
#     cursor.execute("SELECT * FROM scripts WHERE requestid=%s", (requestid,))
#     row = cursor.fetchone()
#     connection.close()
#     return row

# def update_script_by_requestid(requestid, new_script):
#     connection = pymysql.connect(host=HOST,
#                                  user=USER,
#                                  password=PASSWORD,
#                                  database=DATABASE,
#                                  charset=CHARSET)
#     cursor = connection.cursor()
#     current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#     cursor.execute("UPDATE scripts SET script=%s, updatedon=%s WHERE requestid=%s",
#                    (new_script, current_time, requestid))
#     connection.commit()
#     connection.close()
 

def insert_into_request_table(localfolderpath, s3path):
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET)
    cursor = connection.cursor()
    cursor.execute("INSERT INTO request (localfolderpath, s3path) VALUES (%s, %s)",
                   (localfolderpath, s3path))
    connection.commit()
    last_inserted_id = cursor.lastrowid
    connection.close()
    return last_inserted_id

def get_data_by_requestid_from_request_table(requestid):
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET,
                                 cursorclass=pymysql.cursors.DictCursor
                                 )
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM request WHERE requestid=%s", (requestid,))
    row = cursor.fetchone()
    connection.close()
    return row


def get_data_from_request_table(limit):
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET,
                                 cursorclass=pymysql.cursors.DictCursor
                                 )
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM request order by 1 desc limit %s", (limit,))
    row = cursor.fetchall()
    connection.close()
    return row

def update_request_table_by_requestid(requestid, columnname, columnvalue):
    # Validate that the column name is one of the allowed columns
    # allowed_columns = ['localfolderpath','script', 's3path',"sub","sub_com"]
    # if columnname not in allowed_columns:
    #     print("Invalid column name.")
    #     return False
    
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET)
    cursor = connection.cursor()

    query = f"UPDATE request SET {columnname} = %s WHERE requestid = %s"
    cursor.execute(query, (columnvalue, requestid))

    affected_rows = cursor.rowcount

    connection.commit()
    connection.close()

    return affected_rows > 0

# new_id = insert_script(1, "नमस्ते, दुनिया!")
# print(f"Inserted new record with id: {new_id}")

# bill *************************************************************************************************

# Insert into bill table
def insert_into_bill_table(requestid, price, type, vendor, note, createdon=datetime.now()):
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET)
    cursor = connection.cursor()
    cursor.execute("INSERT INTO bill (requestid, price, type, vendor, note, createdon) VALUES (%s, %s, %s, %s, %s, %s)",
                   (requestid, price, type, vendor, note, createdon))
    connection.commit()
    last_inserted_id = cursor.lastrowid
    connection.close()
    return last_inserted_id

# Get data by ID from bill table
def get_data_by_requestid_from_bill_table(id):
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET,
                                 cursorclass=pymysql.cursors.DictCursor)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM bill WHERE requestid=%s", (id,))
    row = cursor.fetchone()
    connection.close()
    return row

# Get all data from bill table
def get_data_from_bill_table_byrequestid(id):
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET,
                                 cursorclass=pymysql.cursors.DictCursor)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM bill WHERE requestid=%s", (id,))
    rows = cursor.fetchall()
    connection.close()
    return rows
# Get all data from bill table
def get_data_from_bill_table(limit):
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET,
                                 cursorclass=pymysql.cursors.DictCursor)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM bill ORDER BY id DESC LIMIT %s", (limit,))
    rows = cursor.fetchall()
    connection.close()
    return rows

# Update bill table by ID
def update_bill_table_by_id(id, columnname, columnvalue):
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET)
    cursor = connection.cursor()
    query = f"UPDATE bill SET {columnname} = %s WHERE id = %s"
    cursor.execute(query, (columnvalue, id))
    affected_rows = cursor.rowcount
    connection.commit()
    connection.close()
    return affected_rows > 0

# users ********************************************************************************
def insert_into_users(username, email, name, phone, isverified, creditbalance):
    connection = pymysql.connect(
        host=HOST,
        user=USER,
        password=PASSWORD,
        database=DATABASE,
        charset=CHARSET
    )
    cursor = connection.cursor()
    sql = "INSERT INTO users (username, email, name, phone, isverified, creditbalance) VALUES (%s, %s, %s, %s, %s, %s)"
    cursor.execute(sql, (username, email, name, phone, isverified, creditbalance))
    connection.commit()
    connection.close()

def read_user_by_field(field_type, value):
    valid_fields = ['id', 'email', 'phone']
    if field_type not in valid_fields:
        print("Invalid field type. Valid types are: 'id', 'email', 'phone'")
        return

    connection = pymysql.connect(
        host=HOST,
        user=USER,
        password=PASSWORD,
        database=DATABASE,
        charset=CHARSET,
        cursorclass=pymysql.cursors.DictCursor)
    
    cursor = connection.cursor()

    sql = f"SELECT * FROM users WHERE {field_type} = %s"
    cursor.execute(sql, (value,))
    
    row = cursor.fetchone()
    if row:
        print("User found:", row)
        return row
    else:
        print(f"No user found with {field_type} = {value}")
        return None

    connection.close()


def update_user_by_field(id, column_name, column_value):
    valid_fields = ['username', 'email', 'phone', 'name', 'isverified', 'creditbalance', 'createdon', 'updatedon']
    if column_name not in valid_fields:
        print("Invalid column name. Valid column names are: 'username', 'email', 'phone', 'name', 'isverified', 'creditbalance', 'createdon', 'updatedon'")
        return

    connection = pymysql.connect(
        host=HOST,
        user=USER,
        password=PASSWORD,
        database=DATABASE,
        charset=CHARSET
    )
    cursor = connection.cursor()

    sql = f"UPDATE users SET {column_name} = %s WHERE id = %s"
    cursor.execute(sql, (column_value, id))

    if cursor.rowcount > 0:
        print(f"Successfully updated {column_name} for user with id = {id}")
    else:
        print(f"No user found with id = {id}")

    connection.commit()
    connection.close()

def get_request_and_bill_info_by_userid(userid, page=1, per_page=10):
    # Calculate offset
    offset = (page - 1) * per_page

    connection = pymysql.connect(
        host=HOST,
        user=USER,
        password=PASSWORD,
        database=DATABASE,
        charset=CHARSET,
        cursorclass=pymysql.cursors.DictCursor)

    cursor = connection.cursor()

    sql = """
    SELECT 
        r.requestid, 
        r.script, 
        r.userid,
        b.id AS bill_id,
        b.price,
        b.type,
        b.vendor,
        b.note,
        b.createdon
    FROM (
        SELECT requestid, script, userid
        FROM request
        WHERE userid = %s
        ORDER BY requestid DESC
        LIMIT %s OFFSET %s
    ) AS r
    LEFT JOIN 
        bill AS b ON r.requestid = b.requestid
    """

    cursor.execute(sql, (userid, per_page, offset))

    rows = cursor.fetchall()

    if rows:
        print(f"Data found on page {page}:", rows)
        return rows
    else:
        print(f"No data found for userid = {userid} on page {page}")
        return None

    connection.close()