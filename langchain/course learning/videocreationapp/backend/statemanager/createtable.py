import pymysql


# Global database configuration
HOST = 'localhost'
USER = 'root'
PASSWORD = 'password'
DATABASE = 'ai'
CHARSET = 'utf8mb4'

def create_state_table():
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET)
    cursor = connection.cursor()
    create_table_query = """CREATE TABLE execution_states (
        id INT AUTO_INCREMENT,
        requestid INT,
        stateid INT,
        start_time TIMESTAMP,
        end_time TIMESTAMP,
        PRIMARY KEY (id)
    )CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"""

    cursor.execute(create_table_query)
    connection.commit()
    connection.close()


# Create metadata table
def create_metadata_table():
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET)
    cursor = connection.cursor()
    create_table_query = """CREATE TABLE process_metadata (
        id INT AUTO_INCREMENT,
        requestid INT,
        folder_path VARCHAR(255),
        timestamp TIMESTAMP,
        PRIMARY KEY (id)
    )CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"""
    cursor.execute(create_table_query)
    connection.commit()

 

def create_request_table():
    connection = pymysql.connect(host=HOST,
                                 user=USER,
                                 password=PASSWORD,
                                 database=DATABASE,
                                 charset=CHARSET)
    cursor = connection.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS request (
        requestid INT AUTO_INCREMENT PRIMARY KEY,
        localfolderpath TEXT,
        script TEXT,
        s3path TEXT,
        sub TEXT,
        userid INT,
        sub_com TEXT,
        image_prompt TEXT,
        inputtype VARCHAR(50),
        language VARCHAR(30),
        currentstatus VARCHAR(50),
        inputtext TEXT,
        audiopath TEXT,    
        video_url TEXT,     
        error TEXT       
    )CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;""")
    connection.commit()
    connection.close()

import pymysql

def create_bill_table():
    connection = pymysql.connect(
        host=HOST,
        user=USER,
        password=PASSWORD,
        database=DATABASE,
        charset=CHARSET
    )
    cursor = connection.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS bill (
        id INT AUTO_INCREMENT PRIMARY KEY,
        requestid INT,
        price DECIMAL(10, 10),
        type VARCHAR(50),
        vendor TEXT,
        note VARCHAR(500),
        createdon DATETIME
    ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
    """)
    connection.commit()
    connection.close()

def create_users_table():
    connection = pymysql.connect(
        host=HOST,
        user=USER,
        password=PASSWORD,
        database=DATABASE,
        charset=CHARSET
    )
    cursor = connection.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS users (
        id INT AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(50) UNIQUE,
        email VARCHAR(50) UNIQUE,
        name VARCHAR(100),
        phone VARCHAR(15),
        isverified BOOLEAN,
        creditbalance DECIMAL(20, 10),
        createdon DATETIME DEFAULT CURRENT_TIMESTAMP,
        updatedon DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
    """)
    connection.commit()
    connection.close()


# create_metadata_table()
# create_state_table() 
# create_request_table()
# create_bill_table()
create_users_table()


# ALTER TABLE request ADD audiopath VARCHAR(150);
#  ALTER TABLE request MODIFY image_prompt TEXT;
#  ALTER TABLE request DROP COLUMN topic; 
# ALTER TABLE request CHANGE topictext inputtext TEXT;


