import pymysql
from datetime import datetime
import json

# Global variable for database connection
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='password',
                             database='ai',
                             charset='utf8mb4')

def get_last_executed_state(requestid):
    global connection
    cursor = connection.cursor()
    cursor.execute(f"SELECT MAX(stateid) FROM execution_states WHERE requestid = {requestid} and end_time is not null")
    result = cursor.fetchone()
    return result[0] if result else None

def save_state(requestid, stateid):
    global connection
    cursor = connection.cursor()
    start_time = datetime.now()
    cursor.execute("INSERT INTO execution_states (requestid, stateid, start_time) VALUES (%s, %s, %s)", (requestid, stateid, start_time))
    connection.commit()
    last_inserted_id = cursor.lastrowid
    return last_inserted_id


def update_end_time(id,requestid, stateid):
    global connection
    cursor = connection.cursor()
    end_time = datetime.now()
    cursor.execute("UPDATE execution_states SET end_time = %s WHERE id = %s ", (end_time,id))
    connection.commit()


def insert_metadata(requestid, folder_path):
    global connection
    cursor = connection.cursor()
    timestamp = datetime.now()
    cursor.execute("INSERT INTO process_metadata (requestid, folder_path, timestamp) VALUES (%s, %s, %s)",
                   (requestid, folder_path, timestamp))
    connection.commit()

# Method to read metadata
def read_metadata_by_requestid(requestid):
    global connection
    cursor = connection.cursor()
    cursor.execute("SELECT folder_path, timestamp FROM process_metadata WHERE requestid = %s", (requestid,))
    result = cursor.fetchone()
    return result