import asyncio
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

from main import Run

# Initialize Firebase
cred = credentials.Certificate('aitexttovideo-firebase.json')
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://aitexttovideo-acf41-default-rtdb.asia-southeast1.firebasedatabase.app/'  # Your Realtime Database URL
})

# Reference to the database root
root = db.reference()

# Define the listener function to print event details
def listener(event):
    try:
        print("Event Type:", event.event_type)
        print("Path:", event.path)

        # Ensure the data is a dictionary
        if isinstance(event.data, dict) and event.data.get('name') == 'requestid':
            value_of_requestid = event.data.get('value')
            print("Value of requestid:", value_of_requestid)
            
            # Schedule Run() to run asynchronously
            asyncio.create_task(Run("", value_of_requestid, executeuntilstate=9999, source="listener"))

            # Delete the node using the path provided in the event
            root.child(event.path).delete()
        else:
            print("Data format unexpected:", event.data)
    except Exception as e:
        print(e)


Run("", 214, executeuntilstate=9999, source="listener")


# Attach the listener to a specific node (in this case, 'users')
users_ref = root.child('users')
users_ref.listen(listener)


# This listener will keep the program running and print event details 
# every time there's a change in the 'users' node of the database.


# This listener will keep the program running and print event details 
# every time there's a change in the 'users' node of the database.
