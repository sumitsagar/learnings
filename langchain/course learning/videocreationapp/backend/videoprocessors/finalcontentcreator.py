from moviepy.editor import VideoFileClip, AudioFileClip
import os

def add_audio_to_video(final_video_path, audio_path, video_path):
    # Load video and audio files
    video_clip = VideoFileClip(video_path)
    audio_clip = AudioFileClip(audio_path)
    
    # Get lengths of video and audio
    video_duration = video_clip.duration
    audio_duration = audio_clip.duration
    
    # Trim audio or video to be no longer than the other
    min_duration = min(video_duration, audio_duration)
    
    video_clip = video_clip.subclip(0, min_duration)
    audio_clip = audio_clip.subclip(0, min_duration)
    
    # Set audio to the video
    video_with_audio = video_clip.set_audio(audio_clip)
    
    # Save final video
    video_with_audio.write_videofile(final_video_path, codec='libx264')
    
    print(f"Final video saved as {final_video_path}")
 
# Example usage
# add_audio_to_video("test/finalwithaudio.mp4", "test/1.wav", "test/combined_video_with_sub.mp4")


# Example usage
# add_audio_to_video("test")
