from pydub import AudioSegment
import cv2
import numpy as np
from moviepy.editor import VideoFileClip, AudioFileClip
import os

def create_video_from_folder(folder_path):
    # List all the files in the folder
    folder_files = os.listdir(folder_path)
    
    # Separate the audio and image files
    audio_files = sorted([f for f in folder_files if f.endswith('.wav')])
    image_files = sorted([f for f in folder_files if f.endswith('.png')])
    
    # Read the first audio file to get its properties
    first_audio_path = os.path.join(folder_path, audio_files[0])
    first_audio = AudioSegment.from_wav(first_audio_path)
    
    # Initialize an empty audio segment for concatenation with the same properties as the first audio file
    concatenated_audio = first_audio[:0]
    
    # Concatenate audio files
    for audio_file in audio_files:
        audio_path = os.path.join(folder_path, audio_file)
        audio = AudioSegment.from_wav(audio_path)
        
        # Convert audio properties to match the first audio file if needed
        audio = audio.set_frame_rate(first_audio.frame_rate)
        audio = audio.set_channels(first_audio.channels)
        audio = audio.set_sample_width(first_audio.sample_width)
        
        concatenated_audio += audio

    # Calculate total audio length in seconds
    total_audio_length = len(concatenated_audio) // 1000  # pydub calculates length in milliseconds
    
    # Save the concatenated audio
    concatenated_audio_path = os.path.join(folder_path, "concatenated_audio.wav")
    concatenated_audio.export(concatenated_audio_path, format="wav")
    
    # Define the video settings for portrait orientation (9:16 aspect ratio)
    frame_width, frame_height = 540, 960  # 9:16 aspect ratio
    fps = 1  # Frames per second
    image_duration = 10  # Duration for which each image is displayed in seconds
    max_frames = total_audio_length  # Maximum number of frames to match audio length
    transition_duration = 1  # Duration of the blur transition in seconds
    
    # Initialize the video writer
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video_path = os.path.join(folder_path, "concatenated_video.mp4")
    out = cv2.VideoWriter(video_path, fourcc, fps, (frame_width, frame_height))
    
    current_frame = 0  # Counter for the current frame number
    
    # Write each image to the video file with 10-second duration and 1-second blur transition
    for i, image_file in enumerate(image_files):
        if current_frame >= max_frames:
            break  # Stop if the maximum number of frames is reached
        
        image_path = os.path.join(folder_path, image_file)
        img = cv2.imread(image_path)
        
        # Resize the image to match the video dimensions
        img_resized = cv2.resize(img, (frame_width, frame_height))
        
        # Write the frame to the video repeatedly for 10 seconds or until max_frames
        for _ in range(min(image_duration, max_frames - current_frame)):
            out.write(img_resized)
            current_frame += 1
        
        # Add a blur transition if this is not the last image and within max_frames
        if i < len(image_files) - 1 and current_frame < max_frames:
            next_image_path = os.path.join(folder_path, image_files[i + 1])
            next_img = cv2.imread(next_image_path)
            next_img_resized = cv2.resize(next_img, (frame_width, frame_height))
            
            # Create a fast blur transition from img_resized to next_img_resized within 1 second
            for alpha in np.linspace(0, 1, fps * transition_duration):
                if current_frame >= max_frames:
                    break  # Stop if the maximum number of frames is reached
                beta = 1 - alpha
                blended = cv2.addWeighted(img_resized, beta, next_img_resized, alpha, 0)
                out.write(blended)
                current_frame += 1
    
    # Release the video writer
    out.release()
    
    # Merge the video with the concatenated audio
    video = VideoFileClip(video_path)
    audio = AudioFileClip(concatenated_audio_path)
    final_video = video.set_audio(audio)
    
    # Save the final video
    final_video_path = os.path.join(folder_path, "final_video.mp4")
    final_video.write_videofile(final_video_path, codec="libx264")
    
    print(f"Final video created and saved at {final_video_path}")
