from moviepy.editor import VideoFileClip, concatenate_videoclips


def AddClipAtEndOfVideo(video1_path,video2_path,outputpath): 

    # Load video clips
    clip1 = VideoFileClip(video1_path)
    clip2 = VideoFileClip(video2_path)

    # Get video properties
    frame_rate1 = clip1.fps
    width1, height1 = clip1.size
    frame_rate2 = clip2.fps
    width2, height2 = clip2.size

    # Standardize frame rate and resolution
    standard_framerate = frame_rate1
    standard_resolution = (width1, height1)

    # Resize the second video while maintaining its aspect ratio
    aspect_ratio2 = width2 / height2
    new_height2 = height1
    new_width2 = int(new_height2 * aspect_ratio2)

    # Calculate the zoom factor needed for the second video
    zoom_factor_width = width1 / new_width2
    zoom_factor_height = height1 / new_height2
    zoom_factor = max(zoom_factor_width, zoom_factor_height)

    # Zoom in on the second video
    zoomed_width2 = int(new_width2 * zoom_factor)
    zoomed_height2 = int(new_height2 * zoom_factor)
    x_center = clip2.w / 2
    y_center = clip2.h / 2
    x_crop = max(0, x_center - zoomed_width2 // 2)
    y_crop = max(0, y_center - zoomed_height2 // 2)
    clip2_zoomed = clip2.crop(x1=x_crop, y1=y_crop, x2=x_crop + zoomed_width2, y2=y_crop + zoomed_height2)

    # Resize the zoomed clip to fit the dimensions of the first video
    clip2_zoomed_resized = clip2_zoomed.resize(newsize=(width1, height1))

    # Concatenate the original first clip with the zoomed and resized second clip
    final_clip_zoomed = concatenate_videoclips([clip1, clip2_zoomed_resized])
 
    # Write the result to a file
    final_clip_zoomed.write_videofile(outputpath)