from scipy.interpolate import CubicSpline
import cv2
import numpy as np
import os
from concurrent.futures import ThreadPoolExecutor

 
def crop_to_fit(img, target_width, target_height):
    h, w, _ = img.shape
    aspect_ratio = w / h
    target_aspect_ratio = target_width / target_height

    if aspect_ratio > target_aspect_ratio:
        new_width = int(target_height * aspect_ratio)
        new_height = target_height
    else:
        new_width = target_width
        new_height = int(target_width / aspect_ratio)

    img_resized = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_LANCZOS4)
    x_offset = (new_width - target_width) // 2
    y_offset = (new_height - target_height) // 2

    return img_resized[y_offset:y_offset + target_height, x_offset:x_offset + target_width]

def zoom_effect(img, zoom_factor):
    h, w, _ = img.shape
    center_x, center_y = w // 2, h // 2
    zoomed = cv2.resize(img, (0, 0), fx=zoom_factor, fy=zoom_factor, interpolation=cv2.INTER_LANCZOS4)
    zh, zw, _ = zoomed.shape
    new_center_x, new_center_y = zw // 2, zh // 2
    start_x = max(new_center_x - center_x, 0)
    start_y = max(new_center_y - center_y, 0)
    end_x = min(start_x + w, zw)
    end_y = min(start_y + h, zh)
    return zoomed[start_y:end_y, start_x:end_x]

def blend_frames(frame1, frame2, alpha):
    return cv2.addWeighted(frame1, 1 - alpha, frame2, alpha, 0)

def adjust_brightness_contrast(image, brightness=0., contrast=0.):
    img = np.int16(image)
    img = img * (contrast / 127 + 1) - contrast + brightness
    img = np.clip(img, 0, 255)
    img = np.uint8(img)
    return img

def cubic_interpolation(start, end, steps):
    steps = int(steps)  # Ensure steps is an integer
    x = np.array([0, steps])
    y = np.array([start, end])
    cs = CubicSpline(x, y)
    return cs(np.linspace(0, steps, steps))



def adjust_brightness_contrast(image, brightness=0., contrast=0.):
    img = np.int16(image)
    img = img * (contrast / 127 + 1) - contrast + brightness
    img = np.clip(img, 0, 255)
    img = np.uint8(img)
    return img

def process_frame(img_cropped, zoom_factor, previous_frame, blend_factor, fade_alpha=1.0):
    img_zoomed = zoom_effect(img_cropped, zoom_factor)
    if previous_frame is not None:
        img_zoomed = blend_frames(previous_frame, img_zoomed, blend_factor)
    img_zoomed = cv2.addWeighted(img_zoomed, fade_alpha, np.zeros_like(img_zoomed), 1 - fade_alpha, 0)
    return img_zoomed

def sigmoid_zoom_factors(start, end, steps):
    x = np.linspace(-6, 6, steps)
    y = 1 / (1 + np.exp(-x))
    y = y * (end - start) + start
    return y

def CreateVideo(folder_path, durations):
    frame_width, frame_height = 1080, 1920  # 9:16 aspect ratio, higher resolution
    fps = 30
    blend_factor = 0.5
    quick_zoom_steps = 15  # number of frames for quick zoom-in

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video_path = os.path.join(folder_path, "combined_video.mp4")
    out = cv2.VideoWriter(video_path, fourcc, fps, (frame_width, frame_height))

    image_files = [f for f in os.listdir(folder_path) if f.endswith('.png')]
    image_files = sorted(image_files, key=lambda x: int(x.split('.')[0]))

     # Check if the number of image files matches the number of durations
    if len(image_files) != len(durations):
        print(f"The number of image files ({len(image_files)}) does not match the number of durations ({len(durations)}).")
        return

    next_img_cropped = None  # Initialize variable to store the next image

    for idx, img_file in enumerate(image_files):
        img_path = os.path.join(folder_path, img_file)
        img = cv2.imread(img_path)
        img_cropped = crop_to_fit(img, frame_width, frame_height)

        start_time, end_time = durations[idx]
        total_frames_for_current_image = int(fps * (end_time - start_time))
        frames_for_main_zoom = int(total_frames_for_current_image - quick_zoom_steps)


        print(f"Processing {img_file}: total_frames={total_frames_for_current_image}, main_zoom_frames={frames_for_main_zoom}, quick_zoom_frames={quick_zoom_steps}")

        # For main zooming part, start with a higher zoom factor and end with a lower zoom factor
        main_zoom_factors = cubic_interpolation(1.2, 1, frames_for_main_zoom)

        # Quick zoom-in before each transition using sigmoid curve for smoothness
        quick_zoom_factors = sigmoid_zoom_factors(1, 1.2, quick_zoom_steps)

        # Create fade-out alpha values for the quick zoom
        fade_alphas = np.linspace(1, 0, quick_zoom_steps)

        # Prepare the next image if it exists
        if idx < len(image_files) - 1:
            next_img_path = os.path.join(folder_path, image_files[idx + 1])
            next_img = cv2.imread(next_img_path)
            next_img_cropped = crop_to_fit(next_img, frame_width, frame_height)

        previous_frame = None
        written_frames = 0

        for zoom_factor in main_zoom_factors:
            img_zoomed = process_frame(img_cropped, zoom_factor, previous_frame, blend_factor)
            out.write(img_zoomed)
            previous_frame = img_zoomed
            written_frames += 1

        print(f"Written main zoom frames: {written_frames}")

        # No blending during fast zoom, but fade into the next image
        for zoom_factor, fade_alpha in zip(quick_zoom_factors, fade_alphas):
            img_zoomed = process_frame(img_cropped, zoom_factor, None, 0, fade_alpha=fade_alpha)
            if next_img_cropped is not None:
                next_img_zoomed = process_frame(next_img_cropped, zoom_factor, None, 0, fade_alpha=1 - fade_alpha)
                img_zoomed = cv2.addWeighted(img_zoomed, fade_alpha, next_img_zoomed, 1 - fade_alpha, 0)
            out.write(img_zoomed)
            previous_frame = img_zoomed
            written_frames += 1

        print(f"Written total frames for {img_file}: {written_frames}")

    out.release()
    print("Modified video created.")





# Example usage:
# CreateVideo("test"
#             ,[[0.0, 4.0], [4.0, 8], [8, 11]])
 