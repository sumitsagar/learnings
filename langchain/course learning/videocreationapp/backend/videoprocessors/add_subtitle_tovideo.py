import cv2
import json
from PIL import Image, ImageDraw, ImageFont
import numpy as np
import textwrap


def add_subtitles(video_path, sub, output_path,subtitlelanguage):
    cap = cv2.VideoCapture(video_path)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = int(cap.get(cv2.CAP_PROP_FPS))

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, fps, (width, height))
    font = ImageFont.truetype("Arial_Bold.ttf", 68)  # Load arial TrueType font

    if subtitlelanguage == "hindi":
        font = ImageFont.truetype("Lohit-Devanagari.ttf", 74)  # Load arial TrueType font

    subtitles = json.loads(sub)
        

    index = 0
    subtitle_text = ""

    while True:
        ret, frame = cap.read()
        if not ret:
            break

        current_time = cap.get(cv2.CAP_PROP_POS_MSEC) / 1000.0  # Current time in seconds

        if index < len(subtitles):
            subtitle_data = subtitles[index]
            start, end = subtitle_data["duration"]

            if current_time >= start and current_time <= end:
                subtitle_text = subtitle_data["text"]
            elif current_time > end:
                index += 1
                subtitle_text = ""

        frame_pil = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        draw = ImageDraw.Draw(frame_pil)
        
        wrapped_text = textwrap.wrap(subtitle_text, width=20)
        y_pos = height // 3  # Start drawing text at the center of the screen's height

        for line in wrapped_text:
            text_bbox = draw.textbbox((0, 0), line, font=font)
            text_width = text_bbox[2] - text_bbox[0]
            x = (width - text_width) // 2
            y = y_pos

            # Draw text outline (border)
            outline_width = 3  # Width of the outline
            for dx in range(-outline_width, outline_width + 1):
                for dy in range(-outline_width, outline_width + 1):
                    draw.text((x + dx, y + dy), line, font=font, fill=(0, 0, 0))  # Black color for the border

            # Draw the actual text multiple times to increase width
            for dx in range(-1, 2):  # Change these numbers to control the width
                for dy in range(-1, 2):
                    draw.text((x + dx, y + dy), line, font=font, fill=(255, 255,255))  # Orange color for the text

            y_pos += text_bbox[3] - text_bbox[1] + 10  # Move y_pos down for next line



        frame = cv2.cvtColor(np.array(frame_pil), cv2.COLOR_RGB2BGR)

        out.write(frame)

    cap.release()
    out.release()
    cv2.destroyAllWindows()

# Example usage

# folder = "/home/sumitsagar/output/गणेश और चंद्रमा: एक शाप की कहानी | Lord Ganesha curse moon_14-09-14-34"
# add_subtitles(folder + '/combined_video.mp4',
#               folder + '/subtitle.json',
#                 folder + '/test_video_with_sub.mp4',
#                 subtitlelanguage= 'hindi')
