from functools import wraps
import json
import random
from flask import Flask, render_template, request, jsonify
from apicontroller import GenerateImageByPrompt, GetAlredyCreatedScriptByRequestID, GetBillController, GetImages, GetProfileDetails, GetRequest, GetRequests, GetScriptJsonFromScriptController, GetTransctiptionAndPrompts, SaveScript, Update_Script, UpdateImages, UpdateSubandPrompt
from flask_cors import CORS

from main import  GetScriptJsonFromTopic, Run
from statemanager.datamanager import insert_into_bill_table, insert_into_request_table
from google.oauth2 import id_token
from google.auth.transport import requests as google_requests
import jwt

from usercontroller import CreteUserAndGetToken
SECRET_KEY = "LKKJ7R2N2B22763VHIn84nyfr4879nfuhhnfnhfHH74H4H84HFKSDJNCC34397783209687NCCNUFU"

app = Flask(__name__)
CORS(app)

# Endpoint to render the HTML page
@app.route('/')

def index():
    return render_template('./index.html')

def token_required(f):
    print("dddddddddddddddddd")
    """Decorator function to protect endpoints."""
    @wraps(f) 
    def decorated(*args, **kwargs):
        print("eeeeeeeeeeeeeeeeeee")
        token = None

        if 'token' in request.headers:
            token = request.headers['token']

        if not token:
            return jsonify({'message': 'Token is missing!'}), 401

        try:
            data = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
            current_user_id = data['user_id'] 
            

        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Token has expired!'}), 401
        except Exception as e:
            print(e)
            return jsonify({'message': 'Token is invalid!'}), 401

        return f(current_user_id, *args, **kwargs)

    return decorated



@app.route('/getgenerations', methods=['GET'])
@token_required
def getgenerations(current_user_id): 
    
    script = GetRequests(limit =20) 
    return jsonify({"status": "success", "Data": script}), 200 


 
@app.route('/getgeneration/<int:requestid>', methods=['GET'])
@token_required
def getgeneration(current_user_id,requestid): 
    
    script = GetRequest(requestid) 
    return jsonify({"status": "success", "Data": script}), 200 



@app.route('/posttext', methods=['POST'])
@token_required
def post_text(current_user_id):
    print(current_user_id)
    # Get JSON payload
    payload = request.json
    requestid = insert_into_request_table("","")   

    # Ensure payload is not empty and contains required fields
    if payload and "data" in payload and "type" in payload:
        input_data = payload["data"]
        input_type = payload["type"]

        if input_type == "topic":
            scriptdict,scriptjsonasstring,price = GetScriptJsonFromTopic(input_data)
            insert_into_bill_table(requestid,price,"GPT-3.5","openai","GetScriptJsonFromTopic()")
            SaveScript(input_type,input_data,scriptdict,scriptjsonasstring,requestid)
            # processed_data = f"Received data: {input_data}, type: {input_type}"
            return jsonify({"status": "success","requestid" : requestid, "Data": scriptdict}), 200
         
        elif input_type == "script":
            scriptdict,scriptjsonasstring = GetScriptJsonFromScriptController(input_type,input_data,requestid) 
            # processed_data = f"Received data: {input_data}, type: {input_type}"
            return jsonify({"status": "success","requestid" : requestid, "Data": scriptdict}), 200

        else:  
            scriptobj = json.loads(input_data)
            scriptobj["script1"] = scriptobj["script"]
            asstr = json.dumps(scriptdict)
            Run(asstr,requestid=requestid)
            # Process the data (for demonstration, we are just echoing it)
            processed_data = f"Received data: {asstr}, type: {input_type}"

            # Return a JSON response
            return jsonify({"status": "success", "Data": processed_data}), 200
        
    else:
        return jsonify({"status": "error", "message": "Invalid payload"}), 400 



# GET API to return the status based on request ID
@app.route('/status/<string:requestid>', methods=['GET'])
def get_status(requestid):
    # Normally, you'd look up the status based on the request ID
    # For this example, returning a static response
    return jsonify({"status": 200, "message": "done", "data": {}})

# POST API to convert text to video
@app.route('/text-to-video', methods=['POST'])
def text_to_video():
    # Get JSON data from the client
    data = request.json
    text = data.get('text')
    showmeprompts = data.get('showmeprompts')

    # Normally, you'd convert the text to video here
    # For this example, returning a static response
    return jsonify({"status": 200, "message": "done", "data": {}})

@app.route('/gettranscriptionsandprompts/<int:requestid>', methods=['GET'])
def gettranscriptionsandprompts(requestid): 
 
    if requestid > 0: 
        
        script = GetAlredyCreatedScriptByRequestID(requestid)
        Run(scriptasargument=script,requestid=requestid,executeuntilstate=3)
        subtitleandimagepromptswithduration = GetTransctiptionAndPrompts(requestid)
        return jsonify({"status": "success", "Data": subtitleandimagepromptswithduration}), 200
        
    else:
        return jsonify({"status": "error", "message": "Invalid requestid"}), 400
    


@app.route('/generateimages/<int:requestid>', methods=['GET'])
def generateimages(requestid): 
 
    if requestid > 0: 
        
        Run(scriptasargument="",requestid = requestid,executeuntilstate=4) 
        images = GetImages(requestid=requestid) 
        if images != None:
            return jsonify({"status": "success","Data": json.loads(images)}), 200
        else:
            return jsonify({"status": "success","Data": None}), 200 
    



@app.route('/generatevideo/<int:requestid>', methods=['GET'])
def generatevideo(requestid): 
 
    if requestid > 0: 
        
        url,error,msg  = Run(scriptasargument="",requestid = requestid,executeuntilstate=99)  
        if error == "" and url != "":
            return jsonify({"status": "success","Data": url}), 200
        elif error != "":
            return jsonify({"status": "success","errormsg": error}), 400
        else: 
            return jsonify({"status": "success" ,"Data":msg}), 200
        
    else:
        return jsonify({"status": "error", "message": error}), 400
    

@app.route('/getimages/<int:requestid>', methods=['GET'])
def get_images(requestid): 
 
    if requestid > 0: 
        
        images = GetImages(requestid) 
        return jsonify({"status": "success", "Data": images}), 200
        
    else:
        return jsonify({"status": "error", "message": "error"}), 400
    


@app.route('/updatetranscriptionsandprompts', methods=['POST'])
def updatetranscriptionsandprompts(): 
    payload = request.json
    requestid = payload["requestid"]
    subtitle = payload["sub"]
    imageprompts = payload["imageprompts"]
    UpdateSubandPrompt(requestid,subtitle,imageprompts)
    return jsonify({"status": "success"}), 200




@app.route('/updatescript', methods=['POST'])
def UpdateScript():
    # Get JSON payload
    payload = request.json
    requestid = payload["requestid"]

    # Ensure payload is not empty and contains required fields
    if payload and "requestid" in payload and "script1" in payload:
        
        # requestid = payload.requestid
        # description = payload.Description
        # title = payload.Title
        # script1 = payload.script1
        Update_Script(payload,json.dumps(payload,ensure_ascii=False),requestid)
        return jsonify({"status": "success","requestid" : requestid}), 200
        
    else:
        return jsonify({"status": "error", "message": "Invalid payload"}), 400




@app.route('/getbill/<int:userid>/<int:page>', methods=['GET'])
def getbill(userid,page): 
    
    bill = GetBillController(userid,page,5) 
    response = jsonify({"status": "success", "Data": bill})
    response.headers.add('Content-Type', 'application/json; charset=utf-8')
    return response , 200


@app.route('/profile', methods=['GET'])
@token_required
def profile(userid): 
    
    profile = GetProfileDetails(userid) 
    response = jsonify({"status": "success", "Data": profile})
    response.headers.add('Content-Type', 'application/json; charset=utf-8')
    return response , 200



@app.route('/generateimage_byprompt', methods=['POST'])
def generateimage_byprompt():
    try:
        # Fetch request data
        data = request.get_json()

        # Fetch client_id and credential from request data
        prompt = data.get('prompt') 
        requestid = data.get('requestid')
        url = GenerateImageByPrompt(requestid,prompt) 
        # return jsonify({"status": "success", "url": "https://talentcarpet.s3.ap-southeast-1.amazonaws.com/ai/audio/20230921142354_175_21-09-14-23.png"}), 200
        return jsonify({"status": "success", "url": url}), 200
         

    except ValueError as e:
        return jsonify({"status": "failure", "message": str(e)}), 401

    except Exception as e:
        return jsonify({"status": "failure", "message": "Internal server error"}), 500


@app.route('/update_image', methods=['POST'])
def update_image():
    try:
        # Fetch request data
        data = request.get_json()

        # Fetch client_id and credential from request data
        requestid = data.get('requestid')
        images = data.get('images') 
        UpdateImages(requestid,images)
        return jsonify({"status": "success"}), 200
         

    except ValueError as e:
        return jsonify({"status": "failure", "message": str(e)}), 401

    except Exception as e:
        return jsonify({"status": "failure", "message": "Internal server error"}), 500


@app.route('/validate_google_login', methods=['POST'])
def validate_google_login():
    try:
        # Fetch request data
        data = request.get_json()

        # Fetch client_id and credential from request data
        client_id = data.get('clientId')
        token = data.get('credential')

        # Validate
        idinfo = id_token.verify_oauth2_token(
            token,
            google_requests.Request(),
            client_id
        )

        # Make sure the token is meant for this application
        if idinfo['aud'] == client_id and idinfo.get('email_verified'): 
            name = idinfo.get('name')
            email = idinfo.get('email')
            token = CreteUserAndGetToken(name,email,SECRET_KEY)
            return jsonify({"status": "success", "token": token})

    except ValueError as e:
        return jsonify({"status": "failure", "message": str(e)}), 401

    except Exception as e:
        return jsonify({"status": "failure", "message": "Internal server error"}), 500


if __name__ == '__main__':
    app.run(debug=True)
