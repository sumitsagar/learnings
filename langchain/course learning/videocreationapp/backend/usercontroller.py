import datetime

from aiohttp import request
from flask import jsonify
from statemanager.datamanager import insert_into_users, read_user_by_field
import jwt

class TokenValidationMiddleware:
    def __init__(self, secret_key):
        self.secret_key = secret_key

    def __call__(self, next):
        token = request.headers.get("Authorization")
        if not token or not token.startswith("Bearer "):
            return jsonify({"error": "Missing or invalid JWT token."}), 401

        try:
            payload = jwt.decode(token[7:], self.secret_key, algorithms=["HS256"])
        except jwt.ExpiredSignatureError:
            return jsonify({"error": "JWT token has expired."}), 401
        except jwt.InvalidTokenError:
            return jsonify({"error": "Invalid JWT token."}), 401

        # The user is authenticated!
        return next()
    


def CreteUserAndGetToken(name,emailid,SECRET_KEY):
    user = read_user_by_field("email",emailid)
    if user != None:
        token = generate_jwt(user["id"],SECRET_KEY)
        return token
    else:
        username = emailid
        name = name
        phone = ""
        isverified = False
        creditbalance = 0.0
        insertedcount = insert_into_users(username, emailid, name, phone, isverified, creditbalance)
        if insertedcount > 0:
            user = read_user_by_field("email",emailid)
            token = generate_jwt(user["id"],SECRET_KEY)
            print(token)
            return token


def generate_jwt(user_id,SECRET_KEY):
    # Your secret key. Keep this safe!
    # SECRET_KEY = "my_super_secret_key"
    
    # The data you want to embed in the token (payload)
    payload = {
        "user_id": user_id,
        "exp": datetime.datetime.utcnow() + datetime.timedelta(days=1)  # Optional: expiration time
    }
    
    # Generate the JWT token
    token = jwt.encode(payload, SECRET_KEY, algorithm="HS256")
    token_str = token.decode('utf-8')

    
    return token_str


