from pydub import AudioSegment
from scipy.interpolate import CubicSpline
import cv2
import numpy as np
from moviepy.editor import VideoFileClip, AudioFileClip
import os

def cubic_interpolation(start, end, steps):
    x = np.array([0, steps])
    y = np.array([start, end])
    cs = CubicSpline(x, y)
    return cs(np.linspace(0, steps, steps))

def zoom_effect(img, zoom_factor):
    h, w, _ = img.shape
    center_x, center_y = w // 2, h // 2
    zoomed = cv2.resize(img, (0, 0), fx=zoom_factor, fy=zoom_factor)
    zh, zw, _ = zoomed.shape
    new_center_x, new_center_y = zw // 2, zh // 2
    start_x = max(new_center_x - center_x, 0)
    start_y = max(new_center_y - center_y, 0)
    end_x = min(start_x + w, zw)
    end_y = min(start_y + h, zh)
    return zoomed[start_y:end_y, start_x:end_x]

def blend_frames(frame1, frame2, alpha):
    return cv2.addWeighted(frame1, 1 - alpha, frame2, alpha, 0)

# Your existing audio processing code here

# Video settings
frame_width, frame_height = 540, 960  # 9:16 aspect ratio
fps = 30  # Frames per second

# Initialize the video writer
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
video_path = "concatenated_video.mp4"
out = cv2.VideoWriter(video_path, fourcc, fps, (frame_width, frame_height))

# Read sample image and resize it
img_path = '1.png'  # Replace with your image path
img = cv2.imread(img_path)
img_resized = cv2.resize(img, (frame_width, frame_height))

# Initialize variables for frame blending
previous_frame = None
blend_factor = 0.2  # Adjust this between 0 and 1 for stronger or weaker blending

# Generate zoom factors with cubic interpolation
zoom_factors = cubic_interpolation(1, 1.4, 300)

# Loop over zoom factors to create frames
for zoom_factor in zoom_factors:
    img_zoomed = zoom_effect(img_resized, zoom_factor)

    if previous_frame is not None:
        img_zoomed = blend_frames(previous_frame, img_zoomed, blend_factor)

    out.write(img_zoomed)
    previous_frame = img_zoomed

# Release the video writer
out.release()

# Your existing code for merging audio and video

print("Zoom test video created.")
