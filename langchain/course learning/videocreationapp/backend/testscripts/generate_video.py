from pydub import AudioSegment
import cv2
import numpy as np
from scipy.interpolate import CubicSpline
import os
from moviepy.editor import VideoFileClip, AudioFileClip

def cubic_interpolation(start, end, steps):
    x = np.array([0, steps])
    y = np.array([start, end])
    cs = CubicSpline(x, y)
    return cs(np.linspace(0, steps, steps))

def crop_to_fit(img, target_width, target_height):
    h, w, _ = img.shape
    aspect_ratio = w / h
    target_aspect_ratio = target_width / target_height

    if aspect_ratio > target_aspect_ratio:
        new_width = int(target_height * aspect_ratio)
        new_height = target_height
    else:
        new_width = target_width
        new_height = int(target_width / aspect_ratio)

    img_resized = cv2.resize(img, (new_width, new_height))
    x_offset = (new_width - target_width) // 2
    y_offset = (new_height - target_height) // 2

    return img_resized[y_offset:y_offset + target_height, x_offset:x_offset + target_width]

def zoom_effect(img, zoom_factor):
    h, w, _ = img.shape
    center_x, center_y = w // 2, h // 2
    zoomed = cv2.resize(img, (0, 0), fx=zoom_factor, fy=zoom_factor)
    zh, zw, _ = zoomed.shape
    new_center_x, new_center_y = zw // 2, zh // 2
    start_x = max(new_center_x - center_x, 0)
    start_y = max(new_center_y - center_y, 0)
    end_x = min(start_x + w, zw)
    end_y = min(start_y + h, zh)
    return zoomed[start_y:end_y, start_x:end_x]

def blend_frames(frame1, frame2, alpha):
    return cv2.addWeighted(frame1, 1 - alpha, frame2, alpha, 0)

def create_zoom_video_with_audio(folder_path):
    frame_width, frame_height = 540, 960
    fps = 30
    seconds_per_image = 10
    frames_per_image = fps * seconds_per_image

    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    video_path = os.path.join(folder_path, "combined_video.mp4")
    out = cv2.VideoWriter(video_path, fourcc, fps, (frame_width, frame_height))

    image_files = sorted([f for f in os.listdir(folder_path) if f.endswith('.jpg')], key=lambda x: int(x.split('.')[0]))

    for img_file in image_files:
        img_path = os.path.join(folder_path, img_file)
        img = cv2.imread(img_path)
        img_cropped = crop_to_fit(img, frame_width, frame_height)

        zoom_factors = cubic_interpolation(1, 1.2, frames_per_image)
        previous_frame = None
        blend_factor = 0.2

        for zoom_factor in zoom_factors:
            img_zoomed = zoom_effect(img_cropped, zoom_factor)

            if previous_frame is not None:
                img_zoomed = blend_frames(previous_frame, img_zoomed, blend_factor)

            out.write(img_zoomed)
            previous_frame = img_zoomed

    out.release()

    audio_files = sorted([f for f in os.listdir(folder_path) if f.endswith('.wav')], key=lambda x: int(x.split('.')[0]))
    combined_audio = AudioSegment.empty()
    for audio_file in audio_files:
        audio_path = os.path.join(folder_path, audio_file)
        audio = AudioSegment.from_wav(audio_path)
        combined_audio += audio

    combined_audio_path = os.path.join(folder_path, "combined_audio.wav")
    combined_audio.export(combined_audio_path, format="wav")

    video = VideoFileClip(video_path)
    audio = AudioFileClip(combined_audio_path)
    final_video = video.set_audio(audio)
    final_video_path = os.path.join(folder_path, "final_video.mp4")
    final_video.write_videofile(final_video_path)

    print(f"Zoom video with audio created at {final_video_path}")

# Usage
# Replace "your_folder_path_here" with the actual folder path containing images and audio files
create_zoom_video_with_audio((os.path.join("output","Girl Accidentally Enters Haunted Building and Tries to Escape_26-08-21-44")))
