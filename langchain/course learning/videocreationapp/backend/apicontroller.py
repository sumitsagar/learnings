from datetime import datetime
from decimal import Decimal
import json
import os
from ai.texttoimage.text_to_image_clipboardco import generate_image_from_text
from helpers.s3.helper import upload_file_tos3
from main import GetScriptJsonFromScript
from statemanager.datamanager import get_data_by_requestid_from_request_table, get_data_from_request_table, get_request_and_bill_info_by_userid, insert_into_bill_table, read_user_by_field, update_request_table_by_requestid
from statemanager.statemanager import insert_metadata, read_metadata_by_requestid

def GetAlredyCreatedScriptByRequestID(requestid):
       
    data = get_data_by_requestid_from_request_table(requestid)
    if data != None:
          return data["script"]
    else:
        return ""
    

def GetImages(requestid):
       
    data = get_data_by_requestid_from_request_table(requestid)
    if data != None:
          return data["image_url"]
    else:
        return ""
    

def SaveScript(input_type,inputdata,scriptdict,scriptstr,requestid):
       
    # metadata = read_metadata_by_requestid(requestid)
    timestamp = datetime.now().strftime("%d-%m-%H-%M")  # current date and time
    output_folder_path = os.path.expanduser("~/output")
    title = scriptdict["Title"]
    folder_name = os.path.join(output_folder_path,f"{title}_{timestamp}")
    os.makedirs(folder_name, exist_ok=True)

    update_request_table_by_requestid(requestid,"inputtype",input_type)
    update_request_table_by_requestid(requestid,"inputtext",inputdata)
    update_request_table_by_requestid(requestid,"script",scriptstr)
    update_request_table_by_requestid(requestid,"localfolderpath",folder_name)

    insert_metadata(requestid, folder_name)
    
def UpdateSubandPrompt(requestid,sub,imageprompt):
    if sub != None and sub != "":
        update_request_table_by_requestid(requestid,"sub",sub)
    
    if imageprompt != None and imageprompt != "":
        update_request_table_by_requestid(requestid,"image_prompt",imageprompt)


def GetTransctiptionAndPrompts(requestid):

    data = get_data_by_requestid_from_request_table(requestid)
    sub = data["sub"]
    imageprompts = data["image_prompt"]
    if data != None:          
        return { "subtitle" : json.loads(sub) ,"imagepromptswithduration" : json.loads(imageprompts) }
    else:
        return { "subtitle" : "NOT_FOUND" ,"imagepromptswithduration" : "NOT_FOUND" }

 


def Update_Script(scriptjsonobject,scriptstring,requestid):
    update_request_table_by_requestid(requestid,"script",scriptstring)
    # metadata = read_metadata_by_requestid(requestid)
    # if metadata:
    #         print(f"Folder Path: {metadata[0]}, Timestamp: {metadata[1]}")
    #         folder_name = metadata[0]
    #         with open(os.path.join(folder_name,"script.txt"), "w") as f:
    #             f.write(scriptstring)    

def GetRequests(limit):
       
    rows = get_data_from_request_table(limit)

    new_rows = []
    for row in rows:
        try:
            # Initialize an empty dictionary
            filtered_row = {}

            # Add requestid first to ensure it's the first property
            filtered_row['requestid'] = row.get('requestid', None)

            # Check if 'script' exists and is not None
            script_str = row.get("script")
            if script_str:
                try:
                    scriptjson = json.loads(script_str)
                except json.JSONDecodeError:
                    # If JSON parsing fails, we default to an empty dictionary
                    scriptjson = {}
            else:
                scriptjson = {}

            # Add the remaining required fields with null checks
            filtered_row['script'] = scriptjson.get("script1", None)
            filtered_row['Title'] = scriptjson.get("Title", None)
            filtered_row['Description'] = scriptjson.get("Description", None)
            filtered_row['inputtext'] = row.get("inputtext", None)
            filtered_row['images'] = row.get("image_url", None)
            filtered_row['video_url'] = row.get("video_url", None)
            filtered_row['inputtype'] = row.get("inputtype", None)
            filtered_row['currentstatus'] = row.get("currentstatus", None)

            new_rows.append(filtered_row)
        except Exception as e:
            print(e)

    return new_rows




def GetRequest(requestid):
       
    rows = get_data_by_requestid_from_request_table(requestid)
    
    # Helper function to safely extract data
    def safe_get(data, key):
        return data.get(key) if data and key in data and data[key] not in (None, "") else None

    # Helper function to safely parse JSON
    def safe_json_parse(data):
        try:
            return json.loads(data)
        except :
            return None

    result = {} 
    result["audiopath"] = safe_get(rows, "audiopath")
    result["inputtext"] = safe_get(rows, "inputtext")
    result["inputtype"] = safe_get(rows, "inputtype")
    result["language"] = safe_get(rows, "language")
    result["localfolderpath"] = safe_get(rows, "localfolderpath")
    result["requestid"] = safe_get(rows, "requestid")
    result["s3path"] = safe_get(rows, "s3path")

    # For JSON fields
    result["image_url"] = safe_json_parse(safe_get(rows, "image_url"))
    result["image_prompt"] = safe_json_parse(safe_get(rows, "image_prompt"))
    result["script"] = safe_json_parse(safe_get(rows, "script"))
    result["sub"] = safe_json_parse(safe_get(rows, "sub"))
    result["sub_com"] = safe_json_parse(safe_get(rows, "sub_com"))
    result["currentstatus"] = safe_json_parse(safe_get(rows, "currentstatus"))

    return result

def GetScriptJsonFromScriptController(inputtype,script,requestid):
    jsonobj,jsonscriptstr,price = GetScriptJsonFromScript(script) 
    insert_into_bill_table(requestid,price,"GPT-3.5","openai","GetScriptJsonFromTopic()")
    jsonobj["script1"] = script
    finalscriptjsonasstr = json.dumps(jsonobj,ensure_ascii=False)
    SaveScript(inputtype,script,jsonobj,finalscriptjsonasstr,requestid)
    return jsonobj,finalscriptjsonasstr

 
def GetBillController(userid,page,perpage):
    # Call the previously defined method to get raw data
    raw_data = get_request_and_bill_info_by_userid(userid,page,perpage)
    
    if not raw_data:
        print("No data found.")
        return []

    # Initialize a dictionary to hold the requestid as the key and details as value
    request_dict = {}
    
    for row in raw_data:
        request_id = row["requestid"]
        
        # Check if script exists and is valid JSON
        script_title = None
        if row["script"]:
            try:
                script_json = json.loads(row["script"])
                script_title = script_json.get("Title", None)
            except json.JSONDecodeError:
                print(f"Invalid JSON in script for requestid: {request_id}")

        # If the request ID is already present, append the bill details
        if request_id in request_dict:
            request_dict[request_id]["bills"].append({
                "bill_id": row["bill_id"],
                "price": row["price"],
                "type": row["type"], 
                "createdon": row["createdon"]
            })
        # If the request ID is not already present, initialize
        else:
            request_dict[request_id] = {
                "requestid": request_id,
                "title": script_title,  # Use the extracted or null-checked script_title
                "bills": []
            }
            if row["bill_id"] is not None:
                request_dict[request_id]["bills"].append({
                    "bill_id": row["bill_id"],
                    "price": row["price"],
                    "type": row["type"], 
                    "createdon": row["createdon"]
                })

    # Convert dictionary to list to match the required format
    request_list = list(request_dict.values())

    return request_list
 

def GenerateImageByPrompt(requestid,prompttext):
    data = get_data_by_requestid_from_request_table(requestid)
    timestamp = datetime.now().strftime("%d-%m-%H-%M")  # current date and time
    filename = timestamp + ".png"
    imagepath = os.path.join(data["localfolderpath"],filename)
    generate_image_from_text(imagepath,prompt_text=prompttext)
    url = upload_file_tos3(imagepath,filename,requestid=requestid)
    return url


def UpdateImages(requestid,imagearray):
    data = get_data_by_requestid_from_request_table(requestid)
    update_request_table_by_requestid(requestid,"image_url",json.dumps(imagearray))
    return True

def GetProfileDetails(userid):
    userdetails = read_user_by_field("id",userid)
    retval = {}
    retval["username"] = userdetails["username"]
    retval["email"] = userdetails["email"]
    retval["name"] = userdetails["name"]
    retval["createdon"] = userdetails["createdon"]
    retval["phone"] = userdetails["phone"] 
    retval["creditbalance"] = userdetails["creditbalance"]
    
    return retval