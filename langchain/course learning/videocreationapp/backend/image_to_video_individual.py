from io import BytesIO
import random
from PIL import Image
import cv2
import numpy as np
import os

import requests

def crop_to_frame_size(image, frame_width=1080, frame_height=1920):
    img_w, img_h = image.size
    img_aspect = img_w / img_h
    frame_aspect = frame_width / frame_height
    
    if img_aspect > frame_aspect:
        new_width = int(img_h * frame_aspect)
        left = (img_w - new_width) / 2
        right = (img_w + new_width) / 2
        image = image.crop((left, 0, right, img_h))
    else:
        new_height = int(img_w / frame_aspect)
        top = (img_h - new_height) / 2
        bottom = (img_h + new_height) / 2
        image = image.crop((0, top, img_w, bottom))
    
    return image.resize((frame_width, frame_height), Image.LANCZOS)

def generate_zoom_frames(image, slow_zoom_duration=3, fast_zoom_duration=1, frame_rate=30,fast_zoom_enabled=True,
                            add_slow_zoom_buffer = 0 ,add_fast_zoom_buffer = 0):
    # Adding .25 seconds to both slow and fast zoom durations
    slow_zoom_duration += add_slow_zoom_buffer
    fast_zoom_duration += add_fast_zoom_buffer
    
    total_frames = int((slow_zoom_duration + fast_zoom_duration) * frame_rate)
    frames = []
    image = crop_to_frame_size(image)
    img_np = np.array(image.convert("RGB"))
    h, w, _ = img_np.shape
    slow_zoom_frames = int(slow_zoom_duration * frame_rate)
    fast_zoom_frames = int(fast_zoom_duration * frame_rate)
    slow_zoom_steps = np.linspace(1.2, 1, slow_zoom_frames)
    fast_zoom_steps = np.linspace(1, 1.5, fast_zoom_frames) 
    if fast_zoom_enabled:
        zoom_steps = np.concatenate([slow_zoom_steps, fast_zoom_steps])
    else:
        zoom_steps = slow_zoom_steps

    for zoom in zoom_steps:
        x1 = max(int((w - w / zoom) / 2), 0)
        y1 = max(int((h - h / zoom) / 2), 0)
        x2 = min(x1 + int(w / zoom), w)
        y2 = min(y1 + int(h / zoom), h)
        cropped_img = img_np[y1:y2, x1:x2]
        resized_img = cv2.resize(cropped_img, (w, h))
        frames.append(resized_img)

    return frames

def save_frames_as_video(frames, path, frame_size, frame_rate=30):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(path, fourcc, frame_rate, frame_size)
    for frame in frames:
        corrected_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        out.write(corrected_frame)
    out.release()

def add_timestamp(frame, timestamp_ms):
    font = cv2.FONT_HERSHEY_SIMPLEX
    position = (frame.shape[1]//2 - 50, frame.shape[0]//2)
    fontScale = 5
    color = (255, 255, 255)
    thickness = 4
    cv2.putText(frame, f"{timestamp_ms} ms", position, font, fontScale, color, thickness)


def CreateVideo(imageobj,folder_path, durations):
    frame_rate = 30
    frame_width, frame_height = 1080, 1920
    frame_size = (frame_width, frame_height)
    combined_frames = []
    overlap_frames = int(0.25 * frame_rate)  # 0.25 seconds * 30 FPS
    current_time_ms = 0  # Initialize the current time in milliseconds

    # Randomly choose an index for the slide-in effect
    slide_in_idx = random.randint(0, len(durations) - 2)

    for idx, (start_time, end_time) in enumerate(durations):
        # image = Image.open(os.path.join(folder_path, f"{idx+1}.png"))
        response = requests.get(imageobj[idx])
        image = Image.open(BytesIO(response.content))
        
        raw_duration = end_time - start_time  # The raw duration without extra time
        add_slow_zoom_buffer = 0
        add_fast_zoom_buffer = 0
        if idx == 0:
            fast_zoom_duration = .75  # clean fast zoom
            slow_zoom_duration = raw_duration - fast_zoom_duration  # No extra time
            add_fast_zoom_buffer = .25 # .25 will blend will next video , sothe ver last frame of 1 st video is unitl raw_duration
        elif idx == len(durations) - 1:
            add_slow_zoom_buffer = .1 #just to keep the video sligly longerthan audio sothat there is no repetation
            slow_zoom_duration = raw_duration  # No extra time
            add_slow_zoom_buffer = 0
        else:
            fast_zoom_duration = .75  # The fast zoom is 0.25 seconds longer for middle videos
            add_fast_zoom_buffer = 0.25
            slow_zoom_duration = raw_duration - fast_zoom_duration  # No extra time 

    #last .25 second of fast zoom of every video is blurred and next video starts .25 second earlier so that by the expected time
    # next video is clean and visible, so we add .25 secondd buffer to fast zoom duration for the transition to happen

        # Generate frames
        video_frames = generate_zoom_frames(image,
                                            slow_zoom_duration=slow_zoom_duration,
                                            fast_zoom_duration=fast_zoom_duration,
                                            fast_zoom_enabled=(fast_zoom_duration > 0),
                                            add_fast_zoom_buffer=add_fast_zoom_buffer,
                                            add_slow_zoom_buffer=add_slow_zoom_buffer,
                                            )
        
        # for frame in video_frames:
        #     add_timestamp(frame, current_time_ms)
        #     current_time_ms += int((1 / frame_rate) * 1000)
        # Save individual video

        individual_video_path = os.path.join(folder_path, f"individual_video_{idx+1}.mp4")
        save_frames_as_video(video_frames, individual_video_path, frame_size)
        
        # Apply slide-in effect if this index is selected
        if idx != 0 and idx %  2  == 0:
            slide_in_frames = []
            h, w, _ = video_frames[0].shape
            preloaded_frames = combined_frames[-overlap_frames:]

            for i in range(overlap_frames):
                slide_in_offset = int((1 - (i / overlap_frames)) * w)

                if i < len(preloaded_frames):
                    next_frame = preloaded_frames[i].copy()
                else:
                    next_frame = np.zeros((h, w, 3), dtype=np.uint8)

                next_frame[:, slide_in_offset:] = video_frames[i][:, :w - slide_in_offset]
                slide_in_frames.append(next_frame)

            combined_frames[-overlap_frames:] = slide_in_frames
            combined_frames.extend(video_frames[overlap_frames:])
        else:
            if idx == 0:
                combined_frames.extend(video_frames[:-overlap_frames])
            else:
                overlap_frames_list = []
                for i in range(overlap_frames):
                    alpha = i / overlap_frames
                    overlap_frame = cv2.addWeighted(
                        combined_frames[-(overlap_frames - i)], 1 - alpha,
                        video_frames[i], alpha, 0
                    )
                    overlap_frames_list.append(overlap_frame)
                
                combined_frames[-overlap_frames:] = overlap_frames_list
                combined_frames.extend(video_frames[overlap_frames:])

    # Save combined video
    save_frames_as_video(combined_frames, os.path.join(folder_path, "combined_video.mp4"), frame_size)



# Input parameter remains unchanged
# CreateVideo("/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/शिव पुराण: भगवान शिव के अनूठे रूप और उनका धार्मिक महत्व | Shiv Purana: The Unique Forms and Religious Significance of Lord Shiva_12-09-10-49", [
#         [
#             0,
#             3
#         ],
#         [
#             3,
#             8
#         ],
#         [
#             8,
#             13
#         ],
#         [
#             13,
#             16
#         ],
#         [
#             16,
#             19
#         ],
#         [
#             19,
#             22
#         ]
#     ])
