import os
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload
from google_auth_oauthlib.flow import InstalledAppFlow
import json
import io
from googleapiclient.http import MediaIoBaseUpload


# def get_authenticated_service():
#     flow = InstalledAppFlow.from_client_secrets_file('client_secrets.json', ['https://www.googleapis.com/auth/youtube.force-ssl'])
#     flow.run_console()
#     credentials = flow.credentials
#     return build('youtube', 'v3', credentials=credentials)
index = "1"
def load_credentials(channel): 
    credentialfile = ""
    
    if(channel == "horror_hinidi"):
            credentialfile = os.path.join("./youtubecredentials","credentials" + index + "_hindihorror.json")
    if(channel == "horror_english"):
            credentialfile = os.path.join("./youtubecredentials","credentials" + index + "_englishhorror.json")
    if(channel == "divine_stories"):
            credentialfile = os.path.join("./youtubecredentials","credentials" + index + "_divyakahaniya.json") 
    
    with open(credentialfile, 'r') as f:
    # with open('credentials.json', 'r') as f:
    # with open('credentials2.json', 'r') as f:
        creds_data = json.load(f)
        
    creds = Credentials(token=creds_data['token'], refresh_token=creds_data['refresh_token'], token_uri=creds_data['token_uri'], client_id=creds_data['client_id'], client_secret=creds_data['client_secret'], scopes=creds_data['scopes'])
    return creds

def upload_video(channel,file_path, title, description):
    
    try:
    
        credentials = load_credentials(channel)  #Credentials.from_authorized_user('credentials.json', ['https://www.googleapis.com/auth/youtube.force-ssl'])
        youtube = build('youtube', 'v3', credentials=credentials)

        body = {
            'snippet': {
                'title': title,
                'description': description, 
            },
            'status': {
                'privacyStatus': 'unlisted'
            }
        }

        media_body = MediaFileUpload(file_path, chunksize=-1, resumable=True)

        request = youtube.videos().insert(
            part="snippet,status",
            body=body,
            media_body=media_body
        )

        response = None
        while response is None:
            status, response = request.next_chunk()

        if 'id' in response:
            print(f'Uploaded video with ID: {response["id"]}')

            # # Upload captions
            # video_id = response["id"]

            # caption_file = io.BytesIO(caption_string.encode('utf-8'))
            # media_body = MediaIoBaseUpload(caption_file, mimetype="application/octet-stream", chunksize=-1, resumable=True)
            
            # request = youtube.captions().insert(
            #     part="snippet",
            #     body={
            #         'snippet': {
            #             'videoId': video_id,
            #             'language': caption_language,
            #             'name': caption_name,
            #             'trackKind': 'standard',
            #         },
            #     },
            #     media_body=media_body
            # )

            # response = None
            # while response is None:
            #     status, response = request.next_chunk()
                
            # if 'id' in response:
            #     print(f'Uploaded caption with ID: {response["id"]}')
            # else:
            #     print('An error occurred while uploading the caption.')
        else:
            print('An error occurred.')
            
    except Exception as e:
        print("error")
        print(e)
        return False,e
# if __name__ == '__main__':
#     os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
#     upload_video('output/60-Second Health- Myths vs Facts_27-08-23-08/final_video.mp4',
#                      'Your Title', 'Your Description', ['horror'])
