from typing import List, Dict
import json
from langchain import LLMChain, OpenAI, PromptTemplate


def ScriptSanityAndCorrectioninText(subtitlepath,script):
    
    subtitle = ""
    with open(subtitlepath, 'r') as file:
        subtitle = file.read() 
 
    # originalsubtitle = json.loads(subtitle) 
 
    imageprompttemplate = '''
        given below is subtitle of a video in json format with text and duration of text,verify the script to look for amy gramatical
        correction needed or some word need to be replaced with some other word,do the correction needed and return the corrected subtitle
        in same json format, do not modify the duration , do not merge the duration , the changes shoudl be as minimal as possible 
        subtitle json:
        {subtitle}
        actual script text : 
        '''
    imageprompttemplate += script
    
    prompt = PromptTemplate(
            input_variables=["subtitle"],
            template=imageprompttemplate,
        )
        # Import LLMChain and define chain with language model and prompt as arguments.

    llm=OpenAI(temperature=0.95, max_tokens=1000)
    chain = LLMChain(llm=llm, prompt=prompt,verbose=True) 
    response = chain.run(subtitle)
    print("subtitle after sanity : " + response)     
    return subtitle



def GenerateImagePromptsWithDuration(subtitle,Summary): 
    
    originalsubtitle = json.loads(subtitle)

    # compressedSubtitle = ""
    # try:
    #     compressedSubtitle = CompressSubtitle(originalsubtitle,2)
    # except:
    #     compressedSubtitle = originalsubtitle

    # Empty list to hold the transformed objects

    string = create_string_from_json(originalsubtitle) 
    prompts = GenerateImagePromptsForAsText(string,len(originalsubtitle))
    prompt_array = json.loads(prompts)    
    final_prompts = []
    final_image_durations = []

    # Iterate through the loaded prompts and associated durations
    for i in range(len(prompt_array)):
        # final_prompts.append('high quality color 3d effect photograph with 9:16 aspect ration , ' + prompt_array[i] + " context -" + Summary)
        final_prompts.append('high quality color 3d effect colorful photograph ' + prompt_array[i]+ " context -" + Summary)
        final_image_durations.append(originalsubtitle[i]['duration'])

    # Package the lists into a dictionary
    final_dict = {
        "prompts": final_prompts,
        "image_duration": final_image_durations
    }
    print(f"Length of final_prompts: {len(final_prompts)}")
    print(f"Length of final_image_durations: {len(final_image_durations)}")

    # Convert the dictionary to a JSON string and return it
    return json.dumps(final_dict)


def CompressSubtitle(subtitles: List[Dict[str, List[float]]], duration: float) -> List[Dict[str, List[float]]]:
    combined_subtitles = []
    i = 0
    n = len(subtitles)
    
    while i < n:
        current_sub = subtitles[i]
        
        # Initialize combined subtitle to be the current subtitle
        combined_sub = {
            'text': current_sub['text'],
            'duration': current_sub['duration'].copy()
        }
        
        # Loop to check if the next subtitle can be combined
        j = i + 1
        while j < n:
            next_sub = subtitles[j]
            
            # Check if the time frame is continuous
            if combined_sub['duration'][1] == next_sub['duration'][0]:
                
                # If either the current or next subtitle is empty, merge them
                if not combined_sub['text'].strip() or not next_sub['text'].strip():
                    combined_sub['text'] += ' ' + next_sub['text']
                    combined_sub['duration'][1] = next_sub['duration'][1]
                else:
                    # Check if the combined duration would be less than the specified duration
                    combined_duration = next_sub['duration'][1] - combined_sub['duration'][0]
                    if combined_duration < duration:
                        combined_sub['text'] += ' ' + next_sub['text']
                        combined_sub['duration'][1] = next_sub['duration'][1]
                    else:
                        break
                
                # Move to the next subtitle
                j += 1
            else:
                break
        
        # Append the combined subtitle
        combined_subtitles.append(combined_sub)
        
        # Update the loop counter
        i = j
    
    return combined_subtitles


def create_string_from_json(json_data):
    # Load JSON if json_data is a JSON-formatted string
    if isinstance(json_data, str):
        json_data = json.loads(json_data)
        
    output_str = ""
    for i, entry in enumerate(json_data):
        text = entry.get("text", "")
        output_str += f"{i+1}. {text}\n"

    return output_str

def GenerateImagePromptsForAsText(subtitle,length):
    imageprompttemplate = f'''
    given below are voice over for a video, write a descriptive image prompt for 3d looking artistic colorful images for each item, 
    you should strictly return valid json array of image promt for example ["prompt1","prompt2"] , the prompts should always be in english
    dont include any newline in your json response.
    prompt should be such that it genrates 3d looking photographic images
    the length of array of prompt should be exactly '''    + str(length) + ''' and the sequence of prompt should be same as asked and 
    response should be valid json'''
    imageprompttemplate  += ''' while genrating prompt do understand the overall contenxt of the script and generate prompt accordingly
    below is the subtitle:
    {subtitle}
    '''
 
    prompt = PromptTemplate(
        input_variables=["subtitle"],
        template=imageprompttemplate,
    )
    # Import LLMChain and define chain with language model and prompt as arguments.

    llm=OpenAI(temperature=0.95, max_tokens=1000)
    chain = LLMChain(llm=llm, prompt=prompt,verbose=True) 
    response = chain.run(subtitle)
    print(response)     
    return response


def merge_words_by_duration(formatted_words):
    merged_words = []
    i = 0

    while i < len(formatted_words):
        combined_text = ""
        start_time = formatted_words[i]['duration'][0]
        end_time = formatted_words[i]['duration'][1]
        duration = end_time - start_time

        while duration < 3 and i < len(formatted_words):
            combined_text += formatted_words[i]['text'] + " "
            end_time = formatted_words[i]['duration'][1]
            duration = end_time - start_time
            i += 1

        # If the duration exceeds 6 seconds, roll back one word to keep it within 3-6 seconds
        if duration > 6 and i > 0:
            i -= 1
            end_time = formatted_words[i - 1]['duration'][1]
            combined_text = combined_text.rsplit(' ', 2)[0]
            duration = end_time - start_time

        combined_text = combined_text.strip()
        if merged_words:
            last_end_time = merged_words[-1]['duration'][1]
            if start_time > last_end_time:
                start_time = last_end_time
        
        merged_words.append({
            "text": combined_text,
            "duration": [start_time, end_time]
        })

        # Skip to the next word that wasn't included in this merged group
        i += 1

    return merged_words