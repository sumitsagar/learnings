image_path = "/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/लक्ष्मण का 14 साल जागरण: भक्ति की सीमा | Lakshman's 14-Year Vigil: The Extent of Devotion_08-09-18-25/2.png"  # Replace this with your image path

# User-defined variable to control video length in seconds
video_length_in_seconds = 10  # You can change this value

import numpy as np
import cv2
from PIL import Image

def generate_depth_map(image):
    gray = cv2.cvtColor(np.array(image), cv2.COLOR_BGR2GRAY)
    depth_map = cv2.GaussianBlur(gray, (11, 11), 0) / 255.0
    return depth_map

def apply_bilinear_parallax(image, depth_map, shift_value):
    rows, cols, _ = image.shape
    shifted_image = np.zeros_like(image)
    
    # Calculate the new column indices for each pixel
    shift_values = (depth_map * shift_value).astype(int)
    new_cols = np.clip(np.arange(cols) + shift_values, 0, cols - 1)
    
    # Shift pixels based on the depth map
    for i in range(rows):
        shifted_image[i, new_cols[i]] = image[i, np.arange(cols)]

    # Use linear interpolation to fill in the gaps
    for i in range(rows):
        for c in range(3):  # For each color channel
            valid_cols = np.where(shifted_image[i, :, c] > 0)[0]
            if valid_cols.size > 0:
                full_cols = np.arange(cols)
                shifted_image[i, :, c] = np.interp(full_cols, valid_cols, shifted_image[i, valid_cols, c])

    return shifted_image

# Path to your image
# image_path = '/path/to/your/image.png'  # Replace with your image path
image = Image.open(image_path)
image_np = np.array(image)

depth_map = generate_depth_map(image)

# Initialize video writer
fourcc = cv2.VideoWriter_fourcc(*'mp4v')
video_path = 'parallax_effect_video_fast.mp4'  # Output video path
height, width, _ = image_np.shape

# Calculate the number of frames based on the video length
fps = 30  # Frames per second
num_frames = video_length_in_seconds * fps  # Total number of frames for the video

video_writer = cv2.VideoWriter(video_path, fourcc, fps, (width, height))

# Generate the video frames
for frame in range(num_frames):
    shift_value = np.sin(2 * np.pi * frame / num_frames) * 30.0
    shifted_frame = apply_bilinear_parallax(image_np, depth_map, shift_value)
    video_writer.write(cv2.cvtColor(shifted_frame, cv2.COLOR_RGB2BGR))

# Release the video writer
video_writer.release()

# import numpy as np
# import cv2
# from PIL import Image

# def generate_depth_map(image):
#     gray = cv2.cvtColor(np.array(image), cv2.COLOR_BGR2GRAY)
#     depth_map = cv2.GaussianBlur(gray, (11, 11), 0) / 255.0
#     return depth_map

# def apply_bilinear_parallax(image, depth_map, shift_value):
#     rows, cols, _ = image.shape
#     shifted_image = np.zeros_like(image)
    
#     # Calculate the new column indices for each pixel
#     shift_values = (depth_map * shift_value).astype(int)
#     new_cols = np.clip(np.arange(cols) + shift_values, 0, cols - 1)
    
#     # Shift pixels based on the depth map
#     for i in range(rows):
#         shifted_image[i, new_cols[i]] = image[i, np.arange(cols)]

#     # Use linear interpolation to fill in the gaps
#     for i in range(rows):
#         for c in range(3):  # For each color channel
#             valid_cols = np.where(shifted_image[i, :, c] > 0)[0]
#             if valid_cols.size > 0:
#                 full_cols = np.arange(cols)
#                 shifted_image[i, :, c] = np.interp(full_cols, valid_cols, shifted_image[i, valid_cols, c])

#     return shifted_image





# # image_path = 'path/to/your/image.jpg'  # Replace with your image path
# image = Image.open(image_path)
# image_np = np.array(image)

# depth_map = generate_depth_map(image)

# fourcc = cv2.VideoWriter_fourcc(*'mp4v')
# video_path = 'parallax_effect_video_1s_fast.mp4'  # Output video path
# height, width, _ = image_np.shape
# video_writer = cv2.VideoWriter(video_path, fourcc, 30, (width, height))

# num_frames = 5 * 15  # Number of frames for a 5-second video at 30fps

# for frame in range(num_frames):
#     shift_value = np.sin(2 * np.pi * frame / num_frames) * 15.0
#     shifted_frame = apply_bilinear_parallax(image_np, depth_map, shift_value)
#     video_writer.write(cv2.cvtColor(shifted_frame, cv2.COLOR_RGB2BGR))

# video_writer.release()


# import numpy as np
# import cv2
# import matplotlib.pyplot as plt
# from PIL import Image

# # Function to generate a simple depth map
# def generate_depth_map(image):
#     grayscale_image = cv2.cvtColor(np.array(image), cv2.COLOR_BGR2GRAY)
#     grayscale_image = grayscale_image / 255.0
#     depth_map = cv2.GaussianBlur(grayscale_image, (11, 11), 0)
#     return depth_map

# # Function to apply a smoother parallax effect using bilinear interpolation
# def apply_bilinear_parallax(image, depth_map, shift_value):
#     rows, cols, _ = image.shape
#     shifted_image = np.zeros_like(image)
    
#     for i in range(rows):
#         for j in range(cols):
#             shift = int(depth_map[i, j] * shift_value)
#             shifted_j = j + shift
            
#             if shifted_j >= 0 and shifted_j < cols:
#                 left_j = int(np.floor(shifted_j))
#                 right_j = int(np.ceil(shifted_j))
#                 alpha = shifted_j - left_j
                
#                 if left_j == right_j:
#                     shifted_image[i, shifted_j] = image[i, j]
#                 else:
#                     shifted_image[i, left_j] += (1 - alpha) * image[i, j]
#                     shifted_image[i, right_j] += alpha * image[i, j]
#     return shifted_image

# # Load the image
# image_path = '/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/गीता अध्याय 7: ज्ञान-विज्ञान योग का रहस्य | Gita Chapter 7: Secrets of Knowledge-Wisdom Yoga_05-09-23-00/3.png'  # Replace this with your image path
# image = Image.open(image_path)
# image_np = np.array(image)

# # Generate the depth map
# depth_map = generate_depth_map(image)

# # Initialize video writer
# fourcc = cv2.VideoWriter_fourcc(*'mp4v')
# video_path = 'parallax_effect_video_1s.mp4'  # Output video path
# height, width, _ = image_np.shape
# video_writer = cv2.VideoWriter(video_path, fourcc, 30, (width, height))

# # Number of frames for a 1-second video at 30fps
# num_frames_1s = 1 * 30

# # Generate video frames
# for frame in range(num_frames_1s):
#     shift_value = np.sin(2 * np.pi * frame / num_frames_1s) * 5.0
#     shifted_frame = apply_bilinear_parallax(image_np, depth_map, shift_value)
#     video_writer.write(cv2.cvtColor(shifted_frame, cv2.COLOR_RGB2BGR))

# # Release the video writer
# video_writer.release()
