image_path = '/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/गीता अध्याय 7: ज्ञान-विज्ञान योग का रहस्य | Gita Chapter 7: Secrets of Knowledge-Wisdom Yoga_05-09-23-00/3.png'  # Replace this with your image path
# User-defined variable to control video length in seconds
video_length_in_seconds = 5  # You can change this value

import numpy as np
import cv2
from PIL import Image

def generate_depth_map(image):
    gray = cv2.cvtColor(np.array(image), cv2.COLOR_BGR2GRAY)
    depth_map = cv2.GaussianBlur(gray, (11, 11), 0) / 255.0
    return depth_map

def apply_bilinear_parallax(image, depth_map, shift_value):
    rows, cols, _ = image.shape
    center_col = cols // 2
    
    # Calculate the shift for each pixel based on the depth map
    shift_values = np.interp(depth_map, [depth_map.min(), depth_map.max()], [-shift_value, shift_value])
    shift_from_center = (np.arange(cols) - center_col)[None, :] * shift_values[:, None]
    new_cols = np.clip(np.arange(cols)[None, :] + shift_from_center.astype(int), 0, cols - 1)
    
    # Shift the image using advanced indexing
    shifted_image = image[np.arange(rows)[:, None], new_cols]

    return shifted_image

# Path to your image
# image_path = '/path/to/your/image.png'  # Replace with your image path
image = Image.open(image_path)
image_np = np.array(image)

depth_map = generate_depth_map(image)

fourcc = cv2.VideoWriter_fourcc(*'mp4v')
video_path = 'parallax_effect_dolly_zoom.mp4'
height, width, _ = image_np.shape

fps = 30
num_frames = video_length_in_seconds * fps

video_writer = cv2.VideoWriter(video_path, fourcc, fps, (width, height))

# Generate the video frames
max_shift_value = 20.0  # You can adjust this value for a more or less pronounced effect
for frame in range(num_frames):
    shift_value = (frame / num_frames) * max_shift_value
    shifted_frame = apply_bilinear_parallax(image_np, depth_map, shift_value)
    video_writer.write(cv2.cvtColor(shifted_frame, cv2.COLOR_RGB2BGR))

video_writer.release()
