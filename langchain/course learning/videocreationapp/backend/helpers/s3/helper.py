from datetime import datetime
import os
import boto3
from botocore.exceptions import NoCredentialsError, PartialCredentialsError

def upload_to_s3(file_name, bucket, folder, original_file_name,requestid):
    s3 = boto3.client(
        's3',
        aws_access_key_id='AKIAIXKQ4T34LBE6NJYA',
        aws_secret_access_key='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',
        region_name='ap-southeast-1'  # specify the region
    )

    try:
        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        object_name = f"{folder}/{timestamp}_{requestid}_{original_file_name}"

        s3.upload_file(file_name, bucket, object_name)

        # Generate the URL of the uploaded object and return it
        url = f"https://{bucket}.s3.ap-southeast-1.amazonaws.com/{object_name}"
        return url

    except FileNotFoundError:
        print("The file was not found")
        return None
    except NoCredentialsError:
        print("Credentials not available")
        return None
    except PartialCredentialsError:
        print("Incomplete credentials")
        return None


def list_images_from_folder(folder_path):
    return [f for f in os.listdir(folder_path) if any(f.endswith(ext) for ext in ['.png', '.jpg', '.jpeg', '.gif', '.bmp'])]


def upload_images_from_folder_tos3(folder_path,requestid):
    images= []
    bucket_name = "talentcarpet"  # Replace with your bucket name
    s3_folder = "ai/images"  # The folder (prefix) you want to upload to

    image_files = list_images_from_folder(folder_path)
    
    for image_file in image_files:
        full_image_path = os.path.join(folder_path, image_file)
        url = upload_to_s3(full_image_path, bucket_name, s3_folder, image_file,requestid)  # Fixed this line
        images.extend([url])
    return images 


def upload_file_tos3(file_path,filename,requestid):
    bucket_name = "talentcarpet"  # Replace with your bucket name
    s3_folder = "ai/audio"  # The folder (prefix) you want to upload to
    url = upload_to_s3(file_path, bucket_name, s3_folder, filename,requestid)
    return url 


if __name__ == "__main__":
    folder_path = "/home/sumitsagar/output/शापित | Cursed rebirth of Jay and Vijay_15-09-23-59"  # Replace with the path to your folder
    images = upload_images_from_folder_tos3(folder_path)
    print(images)