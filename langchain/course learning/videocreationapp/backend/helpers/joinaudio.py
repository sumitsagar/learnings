from pydub import AudioSegment
import os

def combine_audio_files(folder_path, output_file_name="combined_audio.mp3"):
    folder_files = os.listdir(folder_path)
    
    # Separate the audio and image files
    audio_files = sorted([f for f in folder_files if f.endswith('.wav')])
     # Read the first audio file to get its properties
    first_audio_path = os.path.join(folder_path, audio_files[0])
    first_audio = AudioSegment.from_wav(first_audio_path)

    concatenated_audio = first_audio[:0]
    for audio_file in audio_files:
        audio_path = os.path.join(folder_path, audio_file)
        audio = AudioSegment.from_wav(audio_path)
        
        # Convert audio properties to match the first audio file if needed
        audio = audio.set_frame_rate(first_audio.frame_rate)
        audio = audio.set_channels(first_audio.channels)
        audio = audio.set_sample_width(first_audio.sample_width)
        
        concatenated_audio += audio

    # Calculate total audio length in seconds
    total_audio_length = len(concatenated_audio) // 1000  # pydub calculates length in milliseconds
    
    # Save the concatenated audio
    concatenated_audio_path = os.path.join(folder_path, "concatenated_audio.wav")
    concatenated_audio.export(concatenated_audio_path, format="wav")


# Example usage
# combine_audio_files("test")
