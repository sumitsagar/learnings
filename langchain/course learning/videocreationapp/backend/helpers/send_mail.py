import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

# https://app.elasticemail.com/api/settings/create-smtp
def send_email_with_attachment(file_path, body_text):
    # Elastic Email SMTP Server Configuration
    smtp_server = "smtp.elasticemail.com"
    smtp_port = 2525  # For Elastic Email

    # Sender and Receiver Email
    sender_email = "sumitsagar729@gmail.com"
    receiver_email = "sumitsagar731@gmail.com"
    sender_password = "1DA27470B1A3CE83215331596C4956F8B44D"

    # Create the email object
    msg = MIMEMultipart()
    msg['From'] = sender_email
    msg['To'] = receiver_email
    msg['Subject'] = "Your Subject Here"

    # Add email body
    msg.attach(MIMEText(body_text, 'plain'))

    # Add the attachment
    with open(file_path, 'rb') as f:
        attach = MIMEApplication(f.read(), Name="video.mp4")
    attach['Content-Disposition'] = 'attachment; filename="video.mp4"'
    msg.attach(attach)

    try:
        # Establish a secure session with Elastic Email's outgoing SMTP server
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.starttls()
        server.login(sender_email, sender_password)

        # Send email
        server.sendmail(sender_email, receiver_email, msg.as_string())
        print("Email sent successfully!")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        server.quit()

# Test the function
send_email_with_attachment("/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/कृष्ण के राक्षस वध: धर्म और शांति की स्थापना | Krishna's Slaying of Demons: The Establishment of Dharma and Peace_05-09-21-27/combined_video.mp4",
                            "This is the email body.")
