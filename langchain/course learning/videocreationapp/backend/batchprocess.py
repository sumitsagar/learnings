import json
from main import RunInBatch,SetParams


# template 
"""

[
  {
    "script1": "script in at least 150 characters ",
    "Description": "including has tags , long description",
    "Title": "both hindi and english , max 90 characters",
    "Summary": "summary of script",
    "tags": ["tag1", "tag2"]
  },  
  {
    "script1": "script in at least 150 characters ",
    "Description": "including has tags , long description",
    "Title": "both hindi and english , max 90 characters",
    "Summary": "summary of script",
    "tags": ["tag1", "tag2"]
  } 
]
"""


def process_object(obj):
    """
    Process each JSON object.
    """
    print(f"Processing object with Title: {obj['Title']}")
    # Convert each item to a string and print
    str = json.dumps(obj,ensure_ascii=False)
    print("--------------------------")

    RunInBatch(str)

def main():
    SetParams("english","english_horror") #divine_stories
    # Read JSON data from file
    try:
        with open('input_script.json', 'r') as f:
            data = json.load(f)
        
        # Check if the data is a list of objects
        if isinstance(data, list):
            for obj in data:
                process_object(obj)
        else:
            print("The JSON file does not contain a list of objects.")
    except Exception as e:
        print(f"An error occurred: {e}")

if __name__ == "__main__":
    main()
