import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

def PushData(requestid):
    # Check if Firebase app is already initialized
    if not firebase_admin._apps:
        # Initialize Firebase
        cred = credentials.Certificate('aitexttovideo-firebase.json')
        firebase_admin.initialize_app(cred, {
            'databaseURL': 'https://aitexttovideo-acf41-default-rtdb.asia-southeast1.firebasedatabase.app/'  # Your Realtime Database URL
        })

    # Reference to the database root
    root = db.reference()

    # Add data
    data = {
        "name": "requestid",
        "value": requestid
    }

    data_ready = prepare_for_firebase(data)
    root.child('users').push(data_ready)


def prepare_for_firebase(data):
        """
        Convert sets and other non-JSON-serializable types in the data dictionary to JSON-serializable types.
        """
        if isinstance(data, dict):
            return {key: prepare_for_firebase(value) for key, value in data.items()}
        elif isinstance(data, set):
            return list(data)
        return data