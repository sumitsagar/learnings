from decimal import Decimal, getcontext
import json
import traceback
from langchain import PromptTemplate
from langchain import OpenAI
from langchain.chains import LLMChain,SimpleSequentialChain 
import os
import ast
import re
# from text_to_speech_module import text_to_speech
from ai.speech.text_to_speech_google import synthesize_text_to_audo
from datetime import datetime
from firebasepublisher import PushData
from helpers.s3.helper import upload_file_tos3, upload_images_from_folder_tos3
from helpers.tokencounter import tokens_from_string
from main_helper import GenerateImagePromptsWithDuration, ScriptSanityAndCorrectioninText, merge_words_by_duration
from statemanager.datamanager import get_data_by_requestid_from_request_table, insert_into_bill_table, insert_into_request_table, update_request_table_by_requestid
from statemanager.statemanager import get_last_executed_state, insert_metadata, read_metadata_by_requestid, save_state, update_end_time  # To get current date and time
from videoprocessors.generate_video__witheffects import create_video_from_folder
from ai.speech.generate_subtitle_from_audio import GenerateSubtitle
from videoprocessors.add_subtitle_tovideo import add_subtitles
# from imagetovideo import CreateVideo
from image_to_video_individual import CreateVideo
from typing import List, Dict
from videoprocessors.concat_video_clip import AddClipAtEndOfVideo
from ai.texttoimage.text_to_image_clipboardco import generate_image_from_text 
from videoprocessors.finalcontentcreator import add_audio_to_video
from youtube.publish_to_youtube_upload import upload_video 

os.environ["OPENAI_API_KEY"] = "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"
output_folder_path = os.path.expanduser("~/output")
folder_name = "" 


language = "" #hindi
channel = "divine_stories" #"hindi_horror" # "divine_stories"


def SetParams(_language,_channel):
    global language, channel
    language = _language
    channel = _channel

def GetLanguagefromString(input_string): 
    for char in input_string:
        if '\u0900' <= char <= '\u097F':
            return "hindi"
    return "english"


def GetScriptJsonFromScript(script):

    # script = '''
    #         {
    #         "Description": "रास्ते के मुख पर एक जंगल था उसमें कुत्ता का एक अजीब सा अनुभव था | | A strange experience of a dog in ajungle was there. ",
    #         "Summary": "Strange dog experience in the jungle.",
    #         "Title": "एक भूतिया हवेली | A Strange Cave",
    #         "script1": "रास्ते के मुख पर एक जंगल था। उसमें अनेक जंगली जानवर रहते थे। एक शिकारक कुत्ता उस जंगल में इक्कीस "
    #         }
    #         '''
    # return json.loads(script),script

    template = ""
    language = GetLanguagefromString(script)
    if language == "hindi":
        template  = """
        Please write the description (in 20 word),title and summary for below script for a youtube video
          '{script}'. 
        The script,title and description text should be written in hindi text and language for example "एक भूतिया हवेली",  other fields like prompts,
        image_duration, summary should be in English.  
        Your response should be in JSON format and none of the fields should have any new lines. 
        The response should be in the following correct JSON format. The entire response should be wrapped in curly braces to return a parsable JSON. 
        "Description": "YouTube video description",// this will be pure hindi text then a pipe character "|" then english transaction of same text
        "Title": "YouTube video title" // this will be pure hindi text then a pipe character "|" then english transaction of same text ,overall keep the title short but to the point
        "Summary": "Summary of story in 5 to 8 words in english"
            """
    else:
        template  = """
        Please write the description (in 20 word),title and summary for below script for a youtube video
          '{script}'.  
        Your response should be in JSON format and none of the fields should have any new lines. 
        The response should be in the following correct JSON format. The entire response should be wrapped in curly braces to return a parsable JSON. 
        "Description": "YouTube video description",
        "Title": "YouTube video title" overall keep the title short but to the point
        "Summary": "Summary of story in 5 to 8 words in english"
            """

    prompt = PromptTemplate(
        input_variables=["script"],
        template=template,
    )
    
    # Run the chain only specifying the input variable.
    llm=OpenAI(temperature=0.95, max_tokens=1000)
    chain = LLMChain(llm=llm, prompt=prompt,verbose=True)
    # Run the chain only specifying the input variable.
 
    inputtokens = tokens_from_string(template + script,"gpt-3.5-turbo")
    response = chain.run(script)
    outputtokens = tokens_from_string(response,"gpt-3.5-turbo")
    
    inputprice_pertoken = 0.0015/1000
    outputprice_pertoken = 0.002/1000
    totalpriceindollar = (inputprice_pertoken * inputtokens) + (outputprice_pertoken * outputtokens)

    print(response)
    
    # Step 1: Remove newlines and excessive spaces
    cleaned_new_str = re.sub(r'\s+', ' ', response).strip()    
    
    # Step 3: Parse the cleaned string into a dictionary using ast.literal_eval
    parsed_new_dict = ast.literal_eval(cleaned_new_str)
    return parsed_new_dict,response,totalpriceindollar

def GetScriptJsonFromTopic(conceptarg):

    # script = '''
    #         {
    #         "Description": "रास्ते के मुख पर एक जंगल था उसमें कुत्ता का एक अजीब सा अनुभव था | | A strange experience of a dog in ajungle was there. ",
    #         "Summary": "Strange dog experience in the jungle.",
    #         "Title": "एक भूतिया हवेली | A Strange Cave",
    #         "script1": "रास्ते के मुख पर एक जंगल था। उसमें अनेक जंगली जानवर रहते थे। एक शिकारक कुत्ता उस जंगल में इक्कीस "
    #         }
    #         '''
    # return json.loads(script),script

    template = ""
    if language == "hindi":
        template  = """
            Please write the text for an audio script of a 150 words story video on '{concept}'. 
            The script,title and description text should be written in hindi text and language for example "एक भूतिया हवेली", other fields like prompts,
            image_duration, and tags should be in English.  
            Your response should be in JSON format and none of the fields should have any new lines. Make sure the story has a meaningful and catcy beginning and an interesting end.
            Finally, include a title and description suitable for sharing the video on YouTube. 
            The response should be in the following correct JSON format. The entire response should be wrapped in curly braces to return a parsable JSON.
            "script1": "This contains the script", // this will be pure hindi text 
            "Description": "YouTube video description",// this will be pure hindi text then a pipe character "|" then english transaction of same text
            "Title": "YouTube video title" // this will be pure hindi text then a pipe character "|" then english transaction of same text ,overall keep the title short but to the point
            "Summary": "Summary of story in 5 to 8 words in english",
                """
    else:
        template  = """
        Please write the text for an audio script of a 150 words story video on '{concept}'.  
        Your response should be in JSON format and none of the fields should have any new lines. Make sure the story has a meaningful and catcy beginning and an interesting end.
        Finally, include a title and description suitable for sharing the video on YouTube. 
        The response should be in the following correct JSON format. The entire response should be wrapped in curly braces to return a parsable JSON.
        "script1": "This contains the script",  
        "Description": "YouTube video description",
        "Title": "YouTube video title" overall keep the title short but to the point
        "Summary": "Summary of story in 5 to 8 words",
            """
        
    prompt = PromptTemplate(
        input_variables=["concept"],
        template=template,
    )
    
    # Run the chain only specifying the input variable.
    llm=OpenAI(temperature=0.95, max_tokens=1000)
    chain = LLMChain(llm=llm, prompt=prompt,verbose=True)
    # Run the chain only specifying the input variable.

    inputtokens = tokens_from_string(template + conceptarg,"gpt-3.5-turbo")
    response = chain.run(conceptarg)
    outputtokens = tokens_from_string(response,"gpt-3.5-turbo") 
    
    inputprice_pertoken = 0.0015/1000
    outputprice_pertoken = 0.002/1000
    totalpriceindollar = (inputprice_pertoken * inputtokens) + (outputprice_pertoken * outputtokens)
    print(response)
 

    # Parse the string into a Python dictionary
    # Parse the string into a Python dictionary using ast.literal_eval
    
    # Step 1: Remove newlines and excessive spaces
    cleaned_new_str = re.sub(r'\s+', ' ', response).strip()    
    
    # Step 3: Parse the cleaned string into a dictionary using ast.literal_eval
    parsed_new_dict = ast.literal_eval(cleaned_new_str)
    return parsed_new_dict,response,totalpriceindollar


def AddSubscribeRequestVideo(inputvideo,outputvideo): 
    print("adding subscribe video")
    if channel == "divine_stories":
        AddClipAtEndOfVideo(inputvideo,"./genericassets/hindi_divyakahani_subscribe.mp4",outputvideo)    
    else:
        print("no subscrbver video!") 


def RunInBatch(script):
    Run(script)

def Run(scriptasargument,requestid,executeuntilstate=999,source = "api"):

    last_state =  -1
    isexistingrequestid = False
    if requestid > 0:
        result = get_data_by_requestid_from_request_table(requestid)
        language = result["language"]
        if result == None:
            print("request id not found in db")
            return 
        else:
            isexistingrequestid = True
    else:
        requestid = insert_into_request_table("","")    
        
    last_state = get_last_executed_state(requestid)
    try:

        parsed_new_dict = []
        scriptstring = scriptasargument
        if scriptstring != "":
            cleaned_new_str = re.sub(r'\s+', ' ', scriptstring).strip()       
            parsed_new_dict = ast.literal_eval(cleaned_new_str) 
            lang = GetLanguagefromString(scriptstring)
            language = lang
            update_request_table_by_requestid(requestid,"language",lang)
            # insert_into_bill_table(requestid,price,"GPT-3.5","openai","GetLanguagefromString()")
        else:
            if(result["script"] == ""):
                parsed_new_dict,scriptstring,price = GetScriptJsonFromTopic() 
                insert_into_bill_table(requestid,price,"GPT-3.5","openai","GetScriptJsonFromTopic()")
                
            else:
                script = result["script"]
                parsed_new_dict = json.loads(script)
            

        script = parsed_new_dict['script1']
        script_text =  "" #parsed_new_dict['script_text']
        # tag = parsed_new_dict['tags']
        desc = parsed_new_dict['Description']
        title = parsed_new_dict['Title']
        Summary = parsed_new_dict['Summary']
        # Access and print the elements
        print("Description:", desc)
        print("Title:", title)
        print("Script:", script_text)
        # print("tag:", tag)
        print("title:", title) 

         
        if isexistingrequestid:
            folderpath = result["localfolderpath"]
            # print(f"Folder Path: {folderpath}, Timestamp: {metadata[1]}")
            folder_name = folderpath


        else:
            print("No metadata found for this request ID.")
            # Create a folder to store the files and save script initally
            timestamp = datetime.now().strftime("%d-%m-%H-%M")  # current date and time
            folder_name = os.path.join(output_folder_path,f"{title}_{timestamp}")
            os.makedirs(folder_name, exist_ok=True) 
            update_request_table_by_requestid(requestid,"localfolderpath",folder_name)
            # with open(os.path.join(folder_name,"script.txt"), "w") as f:
            #     f.write(scriptstring)
            
            update_request_table_by_requestid(requestid,"script",scriptstring)
            insert_metadata(requestid, folder_name)

        
        if (last_state is None or last_state < 1) and executeuntilstate >= 1:
            #start of state 1
            # Generate sound files and save them in the folder
            id = save_state(requestid, 1)
            print("Generating sound files now ...")
            filename = "1.wav"
            audiopath = os.path.join(folder_name, filename)
            synthesize_text_to_audo(script, audiopath,language=language)

            getcontext().prec = 10
            price_of_1_million_character = Decimal(16)
            price_of_1_character = price_of_1_million_character / Decimal(1000000)

            script = "Your very long script here ..."
            price = price_of_1_character * Decimal(len(script))
            insert_into_bill_table(requestid,price,"texttospeech","google","synthesize_text_to_audo()")
            
            url = upload_file_tos3(audiopath,filename,requestid=requestid)
            update_request_table_by_requestid(requestid,"audiopath",url)
            update_end_time(id,requestid, 1)
            #endof state 1

        if (last_state is None or last_state < 2) and executeuntilstate >= 2:
            #start of state 2
            id = save_state(requestid, 2)
            data = get_data_by_requestid_from_request_table(requestid)
            audiopath = os.path.join(data["localfolderpath"], "1.wav")
            sub,sub_compressed,audiolength = GenerateSubtitle(folder_name,"subtitle.json","compressedsubtitle_for_imageprompt.json",
                                            audiopath,language,minaudiochunklength=1000,maxaudiochunklength=4000,minsilencelength=500) #'output/Test_30-08-17-12/subtitle.json'
            update_request_table_by_requestid(requestid,"sub",sub)
            update_request_table_by_requestid(requestid,"sub_com",sub_compressed)

            audiolengthinseconds = round(audiolength / 1000, 4)
            # https://cloud.google.com/speech-to-text/pricing
            priceperminute = Decimal('0.024')
            price = priceperminute * audiolengthinseconds
            insert_into_bill_table(requestid,price,"speechtotext","google","GenerateSubtitle()")


            # if language == "hindi":
            #     clenedsubtitlejson = ScriptSanityAndCorrectioninText(subtitlepath,script)
            #     subtitlepath = os.path.join(folder_name,"subtitle_cleaned.txt")
            #     with open(subtitlepath, "w") as f:
            #         f.write(clenedsubtitlejson)
            update_end_time(id,requestid, 2)
        #end of state 2

        if (last_state is None or last_state < 3) and executeuntilstate >= 3:
            #start of state 3
            id = save_state(requestid, 3)
            # subtitlepath = os.path.join(folder_name,"subtitle.json")

            # subtitle = ""
            # with open(subtitlepath, 'r') as file:
            #     subtitle = file.read() 
        
            # subtitle = json.loads(subtitle)
            # compressedcubtitlepath = os.path.join(folder_name,"compressedsubtitle_for_imageprompt.json")
            # compressedsubtitleforimageprompt = merge_words_by_duration(subtitle)

            # with open(compressedcubtitlepath, "w") as f:
            #     f.write(json.dumps(compressedsubtitleforimageprompt, indent=4,ensure_ascii=False))

            # subtitleforimageprompt = os.path.join(folder_name,"compressedsubtitle_for_imageprompt.json")
            data = get_data_by_requestid_from_request_table(requestid)
            response = GenerateImagePromptsWithDuration(data["sub_com"],Summary)
            # Convert the string back to a Python object
            response_object = json.loads(response)

            # Convert the Python object back to a formatted JSON string
            formatted_response = json.dumps(response_object, indent=4)
            update_request_table_by_requestid(requestid,"image_prompt",formatted_response)
            # with open(os.path.join(folder_name, "ImagePromptsWithDuration.txt"), "w") as f:
            #     f.write(formatted_response)
            print(response)
            update_end_time(id,requestid, 3)
            #end of state 3
        
        if (last_state is None or last_state < 4) and executeuntilstate >= 4:
            # start of state 4
            id = save_state(requestid, 4)
            # content = ""
            # with open(os.path.join(folder_name, "ImagePromptsWithDuration.txt"), 'r') as file:
            #     content = file.read()
            #     print(content)
            reqdata = get_data_by_requestid_from_request_table(requestid)
            imagepromptresponse = json.loads(reqdata["image_prompt"])
            imageprompts = imagepromptresponse['prompts']

            for i, prompt in enumerate(imageprompts, 1):
                print(f"Generating image for Prompt-{i}: {prompt}")
                generate_image_from_text(os.path.join(folder_name, f"{i}.png"),prompt_text=prompt)
                insert_into_bill_table(requestid,0.029,"texttoimage","clipboardco","generate_image_from_text()")

            
            images = upload_images_from_folder_tos3(folder_name,requestid=requestid)
            update_request_table_by_requestid(requestid,"image_url",json.dumps(images))

            update_end_time(id,requestid, 4)
            #end of state 4

        if (last_state is None or last_state < 5) and executeuntilstate >= 5:

            #all code below is state 5      
            id = save_state(requestid, 5)  
            print("geenrating video now .")

            # content = ""
            # with open(os.path.join(folder_name, "ImagePromptsWithDuration.txt"), 'r') as file:
            #     content = file.read()
            #     print(content)

            reqdata = get_data_by_requestid_from_request_table(requestid)
            if((reqdata["currentstatus"]) == "generating_video"):
                return "",'Video generation already in progress!',""
            if((reqdata["currentstatus"]) == "generating_video_success"):
                return "",'Video generation is already done!',""
            
            # if source == 'api':
            #     PushData(requestid)
            #     return "","","video generation has been initiated!"
            
            imagepromptresponse = json.loads(reqdata["image_prompt"])
             
            try: 
                update_request_table_by_requestid(requestid,"currentstatus","generating_video")
                images = reqdata["image_url"]
                imagesobj = json.loads(images)
                CreateVideo(imagesobj,folder_path=folder_name,durations=imagepromptresponse['image_duration']) 
            except Exception as e:
                print(e)
                traceback_str = traceback.format_exc()
                update_request_table_by_requestid(requestid,"error",traceback_str)
                update_request_table_by_requestid(requestid,"currentstatus","generating_video_error")
                return "",traceback_str ,""


            video_path = os.path.join(folder_name, "combined_video.mp4")
            videowithsub_path = os.path.join(folder_name,"combined_video_with_sub.mp4")
            # subtitlepath = os.path.join(folder_name,"subtitle.json")
            add_subtitles(video_path,reqdata["sub"],videowithsub_path,language)

            finalvideopath = os.path.join(folder_name,title + ".mp4")
            add_audio_to_video(finalvideopath,audio_path=os.path.join(folder_name,"1.wav"),video_path=videowithsub_path)

            filename = title  + "_withsub.mp4"
            finalvideopath_withsubsreq = os.path.join(folder_name,filename)
            AddSubscribeRequestVideo(finalvideopath,finalvideopath_withsubsreq)
            url = upload_file_tos3(finalvideopath_withsubsreq,filename,requestid=requestid)
            update_request_table_by_requestid(requestid,"video_url",url)
            update_request_table_by_requestid(requestid,"currentstatus","generating_video_success")
            upload_video(channel,finalvideopath_withsubsreq,title,desc) #,tag, script_text, 'en', 'English Subtitles')
            update_end_time(id,requestid, 5)
            return url,"",""

    except Exception as e:
        print("error")
        print(e)
        traceback_str = traceback.format_exc()
        update_request_table_by_requestid(requestid,"error",traceback_str)
        return False,e


script = '''
{
  "script1": "रति, कामदेव की अनजानी पत्नी, लोकों में प्रेम और सौंदर्य का महत्व बताने की तलाश में एक अजनबी लोक में चली जाती हैं। वहां 'रशा' नाम से जानी जाती हैं, और उन्होंने लोगों को प्रेम का महत्व सिखाया। एक दिन, एक असुर 'भयांकर' प्रेम और सौंदर्य को नष्ट करने आता है, लेकिन रति उसे अपनी दिव्य शक्तियों से हरा देती हैं। जब वे वापस आती हैं, कामदेव उनके अद्वितीय कार्य की प्रशंसा करते हैं। इससे रति समझती हैं कि उनका भी जीवन में कुछ विशेष भूमिका है।",
  "Description": "Did you know that Teej is also celebrated in Nepal as a national holiday? Explore this unique aspect. #Teej #Nepal #CulturalUnity",
  "Title": "रति",
  "Summary": "beautiful sundari in indian mythology",
  "tags": ["Teej", "Nepal", "CulturalUnity"]
}

'''

# Run(script,requestid=0,executeuntilstate = 9999)

























# subtitle = ""
# with open('output/The Haunted Mirror: A Terrifying Tale_31-08-00-26/subtitle.json', 'r') as file:
#     subtitle = file.read() 

# originalsubtitle = json.loads(subtitle)
# compressedSubtitle = CompressSubtitle(originalsubtitle)
# compressedSubtitlestring = json.dumps(compressedSubtitle)
# r = GenerateImagePromptsWithDuration('/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/The Witch at the Ocean - A Tale of Hope_31-08-01-05/subtitle.json')
# print(r)

# add_subtitles("/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/कॉर्पोरेट जगत में भगवद गीता से सिखे_31-08-21-35/combined_video.mp4",
#               '/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/कॉर्पोरेट जगत में भगवद गीता से सिखे_31-08-21-35/subtitle.json',
#               '/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/कॉर्पोरेट जगत में भगवद गीता से सिखे_31-08-21-35/combined_video_with_sub.mp4',
#               "hindi")

# subtitlepath = GenerateSubtitle('/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/Bhootiya Haveli aur Raju ka Saahas_31-08-21-03',
#                                 '/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/Bhootiya Haveli aur Raju ka Saahas_31-08-21-03/1.wav',"hindi") #

 
# response = GenerateImagePromptsWithDuration("/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/गंगा अवतरण: शिव की जटाओं में संजोया गंगा | Ganga Descent: Shiva's Locks, Earth's Savior_10-09-20-45/subtitle.json",
#                                             "this is summary")

# clenedsubtitlejson = ScriptSanityAndCorrectioninText("/home/sumitsagar/Code/weiggle/learnings/langchain/course learning/videocreationapp/output/गंगा अवतरण: शिव की जटाओं में संजोया गंगा | Ganga Descent: Shiva's Locks, Earth's Savior_10-09-20-45/subtitle.json")

