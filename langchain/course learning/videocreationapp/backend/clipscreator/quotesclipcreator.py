import ast
from datetime import datetime
import os
import random
import re
import string
import cv2
import numpy as np
import textwrap
from langchain import PromptTemplate
from langchain import OpenAI
from langchain.chains import LLMChain,SimpleSequentialChain 
from PIL import Image, ImageDraw, ImageFont
from moviepy.editor import *
from pydub import AudioSegment
import random

def AddOverLay(input_path, output):
    cap = cv2.VideoCapture(input_path)

    # Get video information
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = int(cap.get(cv2.CAP_PROP_FPS))

    # Create output writer
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output, fourcc, fps, (width, height))

    while True:
        ret, frame = cap.read()
        
        if not ret:
            break
        
        # Add Gaussian blur
        blurred_frame = cv2.GaussianBlur(frame, (15, 15), 0)
        
        # Create an overlay (same size as frame but black)
        overlay = np.zeros_like(frame)
        
        # Add the overlay to the frame with 0.6 alpha for the frame and 0.4 alpha for the overlay
        weighted_frame = cv2.addWeighted(blurred_frame, 0.5, overlay, 0.5, 0)
        
        # Write frame to output
        out.write(weighted_frame)
        
        # Display output (Optional)
        cv2.imshow("Output", weighted_frame)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release resources
    cap.release()
    out.release()
    cv2.destroyAllWindows()

# Define the main function to add text to a video
def add_text_to_video(video_path, output_path, str1, str2, str3, str4, str5):
    
    # Initialize a VideoCapture object for the input video
    cap = cv2.VideoCapture(video_path)
    
    # Get video dimensions and frame rate
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    
    # Define the codec and create a VideoWriter object for the output video
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, fps, (width, height))
    
    max_text_width = int(width * 0.9)  # 90% of video width
    max_text_width_80 = int(width * 0.8)  # 80% of video width

    # Define a function to add fading text to a video frame
    def add_fading_text(frame, text, y, frame_count, fade_start_frame, fade_duration,font, color=(255, 255, 255), 
                        bg_color=None, apply_fade=True,shadow=True):
        
        frame_pil = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        draw = ImageDraw.Draw(frame_pil)

        # average_char_width = sum(draw.textsize(c, font=font)[0] for c in string.ascii_uppercase) / 26.0
        # max_chars_per_line = int(max_text_width_80 / average_char_width)

        wrapped_text = textwrap.wrap(text, width=21) #width is max character per line
        y_pos = y


        fade_factor = 1.0 
        if frame_count >= fade_start_frame:
            if apply_fade:
                fade_factor = max(0, min(1, 1 - (frame_count - fade_start_frame) / fade_duration))
            else:
                fade_factor = 1.0  # No fade

        for line in wrapped_text:
            textbbox_val = draw.textbbox((0, 0), line, font=font)
            text_width, text_height = textbbox_val[2] - textbbox_val[0], textbbox_val[3] - textbbox_val[1]
            x = (width - text_width) // 2
            
            padding = 10  # Amount of padding around the text

            if bg_color is not None:
                # Adjust the coordinates of the rectangle to be behind the text, with padding
                rect_x_left = x - padding
                rect_y_top = y_pos - padding
                rect_x_right = x + text_width + padding
                rect_y_bottom = y_pos + text_height + padding + 2

                draw.rectangle([rect_x_left, rect_y_top, rect_x_right, rect_y_bottom], fill=bg_color)

            # Draw the text in the center of the rectangle
            draw.text((x, y_pos), line, font=font, fill=color)

            if shadow:
                shadow_offset = 2
                draw.text((x + shadow_offset, y_pos + shadow_offset), line, font=font, fill=(0, 0, 0))
            
            draw.text((x, y_pos), line, font=font, fill=color)


            y_pos += text_height + 10  # Move y_pos down for next line

        text_frame = cv2.cvtColor(np.array(frame_pil), cv2.COLOR_RGB2BGR)

        cv2.addWeighted(text_frame, fade_factor, frame, 1 - fade_factor, 0, frame)

        return frame


    # Initialize frame count
    frame_count = 0
    
    # Initialize font and set the maximum text width
    fontsmall = ImageFont.truetype("Arial_Bold.ttf", 24)  # Load arial TrueType font
    font = ImageFont.truetype("Arial_Bold.ttf", 28)  # Load arial TrueType font
    font_timesroman = ImageFont.truetype("Times_New_Roman_Bold.ttf",30)
    fontlarge = ImageFont.truetype("Arial_Bold.ttf", 32)  # Load arial TrueType font
    # Main loop to read and write each frame
    while True:
        ret, frame = cap.read()
        if not ret:
            break  # Break the loop if the video ends

        frame_count += 1  # Increment frame count

        # Add watermark text to the bottom of each frame
        # frame = add_fading_text(frame, "@TangentTruths", int(height * 0.97), frame_count, 0, 1)
        # frame = add_fading_text(frame, "@TangentTruths", int(height * 0.8), frame_count, (6 * fps) - fps, fps)

        # Add strings str1, str2, str3, str4, str5 at specified times
        if 0 <= frame_count <= 2 * fps:
            frame = add_fading_text(frame, str1, height // 6, frame_count, (2 * fps) - fps, fps,font_timesroman, color=(0, 0, 0) ,
                                    bg_color=(255, 255, 255),shadow=False)
        elif 2 * fps < frame_count <= 6 * fps:
            frame = add_fading_text(frame, str2, height // 3, frame_count, (6 * fps) - fps, fps,fontlarge)
        elif 6 * fps < frame_count <= 10 * fps:
            frame = add_fading_text(frame, str3, height // 3, frame_count, (10 * fps) - fps, fps,fontlarge)
        elif 10 * fps < frame_count <= 14 * fps:
            frame = add_fading_text(frame, str4, height // 3, frame_count, (14 * fps) - fps, fps,fontlarge)
        elif 14 * fps < frame_count <= 18 * fps:
            frame = add_fading_text(frame, str5, height // 3, frame_count, (18 * fps) - fps, fps,fontlarge)
        

        frame = add_fading_text(frame, "@TangentTruths", int(height * 0.8), frame_count, (6 * fps) - fps, fps,font=fontsmall, apply_fade=False)
        # Write the frame to the output video
        out.write(frame)

    # Release the video capture and writer objects and destroy any OpenCV windows
    cap.release()
    out.release()
    cv2.destroyAllWindows()

    
def Create():

    # add_text_to_video('/home/sumitsagar/Documents/youtubeproject/quotes/outputshorts/Life_Is_Full_Of_Surprises_-_Learning_to_Adapt_to_Change_28-08-22-34/22.mp4',
    #                    '/home/sumitsagar/Documents/youtubeproject/quotes/outputshorts/Life_Is_Full_Of_Surprises_-_Learning_to_Adapt_to_Change_28-08-22-34/22_final.mp4',
    #                 'Life Lessons','Life is full of surprises', 'expect the unexpected and accept what comes your way',
    #                   'embrace change and remember to stay flexible', 'seek out lessons to guide you on your journey')

    # return
    os.environ["OPENAI_API_KEY"] = "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"
    template  = """
    {concept}
    choose one of the below topic randomly and give me four-part aphorism that captures your philosophy on them.
    1.Life Lessons
    2.Relationship Wisdom
    3.Career Advice
    4.Personal Growth
    5.Emotional Intelligence
    6.Financial Literacy
    7.Social Dynamics
    8.Health and Well-being
    9.Philosophical Insights
    10.Spiritual Enlightenment   

    and give me response , be creative and dont return same response every time and dont pick same topic each time.
    the response should also include a title and description for youtube video based on the response you provide,over all response
    should be a json with below fields
    1. category - category of the fact in 2 word like "life fact","work fact" etc.
    2. fact_part_1 - part one of the four part 
    3. fact_part_2 - part two of the four part 
    4. fact_part_3 - part three of the four part 
    5. fact_part_4 - part four of the four part 
    6. title - title for youtube video
    7. description - description for youtube video
 
    for fact_part_1 to fact_part_3 strictly dont add semicolon at end of string
    """
    prompt = PromptTemplate(
        input_variables=["concept"], #dummy
        template=template,
    )
    # Import LLMChain and define chain with language model and prompt as arguments.

    llm=OpenAI(temperature=0.9, max_tokens=1000)
    chain = LLMChain(llm=llm, prompt=prompt,verbose=True)
    # Run the chain only specifying the input variable.
    # Create a four-part aphorism that captures your philosophy on work-life balance.
    # Write a  four-part proverbial saying that offers advice on dealing with adversity.
    # Develop a  four-part motivational aphorism that explains your views on the relationship between risk and reward.
    # Compose a four-part saying that captures how you feel about the role of relationships in a fulfilling life.
    # Craft an  four-part aphoristic statement about the importance of personal growth, broken down into four interconnected parts.
    response = chain.run("")
    print(response) 
    cleaned_new_str = re.sub(r'\s+', ' ', response).strip()
    parsed_new_dict = ast.literal_eval(cleaned_new_str)    
    desc = parsed_new_dict['description']
    title = parsed_new_dict['title'] 

    category = parsed_new_dict['category'] 
    fact_part_1 = parsed_new_dict['fact_part_1'] 
    fact_part_2 = parsed_new_dict['fact_part_2'] 
    fact_part_3 = parsed_new_dict['fact_part_3'] 
    fact_part_4 = parsed_new_dict['fact_part_4'] 

    random_number = random.randint(3, 70)
    # Convert the integer to a string
    random_number_str = str(random_number)
    inputvideo = os.path.join('/home/sumitsagar/Documents/youtubeproject/quotes/clips',random_number_str + ".mp4")

    # Create a folder to store the output
    timestamp = datetime.now().strftime("%d-%m-%H-%M")  # current date and time
    folder_name = os.path.join("/home/sumitsagar/Documents/youtubeproject/quotes/outputshorts",f"{title.replace(' ','_')}_{timestamp}")

    os.makedirs(folder_name, exist_ok=True)
    with open(os.path.join(folder_name,"script.txt"), "w") as f:
        f.write(response)

    outputvideo = os.path.join(folder_name,random_number_str + ".mp4")
    AddOverLay(inputvideo,outputvideo)
    finalvideo = os.path.join(folder_name,random_number_str + "_final.mp4")
    add_text_to_video(outputvideo,finalvideo,
                    category,fact_part_1, fact_part_2, fact_part_3, fact_part_4)


   #now adding audio to video
   # Load the video and audio files
    video = VideoFileClip(finalvideo)
    audio = AudioSegment.from_mp3("sound.mp3")

    # Get the duration of video in milliseconds
    video_duration_ms = video.duration * 1000  # Convert to milliseconds

    # Randomly select starting point and extract audio segment
    random_start = random.randint(0, len(audio) - video_duration_ms)
    audio_segment = audio[random_start: random_start + video_duration_ms]

    # Convert PyDub audio to MoviePy audio
    audio_segment.export("temp_audio.wav", format="wav")
    audio_clip = AudioFileClip("temp_audio.wav")

    # Combine the audio segment with the video
    final_video = video.set_audio(audio_clip)

    finalvideowithaudio = os.path.join(folder_name,random_number_str + title + ".mp4")
    # Export the final video
    final_video.write_videofile(finalvideowithaudio)


    # video_path = "/home/sumitsagar/youtubeproject/clips/2.mp4"
Create()