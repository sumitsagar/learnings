from moviepy.editor import VideoFileClip

def split_video(input_path, output_folder):
    with VideoFileClip(input_path) as clip:
        # Get video duration in seconds
        duration = clip.duration
        
        # Exclude the first 3 minutes (180 seconds) and last 1 minute (60 seconds)
        start_time = 180  # 3 minutes = 180 seconds
        end_time = duration - 60  # Last 1 minute
        
        clip_index = 1
        
        for start in range(start_time, int(end_time), 18):
            end = min(start + 18, end_time)
            
            # Cut video segment
            new_clip = clip.subclip(start, end)
            
            # Define output file name
            output_file = f"{output_folder}/{clip_index}.mp4"
            
            # Write video segment to a file
            new_clip.write_videofile(output_file, codec="libx264")
            
            print(f"Saved {output_file}")
            
            clip_index += 1

# Path to the input video
input_path = "/home/sumitsagar/youtubeproject/4kvideo.mp4"

# Folder where to save the output clips
output_folder = "/home/sumitsagar/youtubeproject/clips"

# Execute the split function
split_video(input_path, output_folder)
