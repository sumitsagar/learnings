import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './bill.css';
import { TopMenuBar } from './TopMenuBar';

function Bill() {
  const [data, setData] = useState([]);
  const [pageIndex, setPageIndex] = useState(1);
  const [showPopup, setShowPopup] = useState(false);
  const [popupContent, setPopupContent] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const userId = 1;
      const response = await axios.get(`http://localhost:5000/getbill/${userId}/${pageIndex}`);
      if (response.data.status === 'success') {
        setData(response.data.Data || []);
      }
    };
    fetchData();
  }, [pageIndex]);

  const handleRowClick = (entry) => {
    setPopupContent(entry.bills || []);
    setShowPopup(true);
  };

  const renderTable = () => (
    <div>
      <table>
        <thead>
          <tr>
            <th>Title</th>
            <th>Total Price ($)</th>
            <th>Total Price (INR)</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          {data.map((entry, index) => {
            const totalPrice = entry.bills?.reduce((acc, bill) => acc + parseFloat(bill.price), 0) || 0;
            const createdOn = entry.bills?.[0]?.createdon || 'N/A';
            return (
              <tr key={index} onClick={() => handleRowClick(entry)}>
                <td style={{width:'40%'}}>{entry.title || 'N/A'}</td>
                <td>{totalPrice.toFixed(2)}</td>
                <td>{(parseFloat(totalPrice.toFixed(2)) * 83.27).toFixed(2)}</td>
                <td>{createdOn}</td>
              </tr>
            );
          })}
        </tbody>
          </table>
          {showPopup && (
              <div className="popup">
                  <table>
                      <thead>
                          <tr>
                              <th>Type</th>
                              <th>Price ($)</th>
                              <th>Price (INR)</th>
                          </tr>
                      </thead>
                      <tbody>
                          {popupContent.map((bill, index) => (
                              <tr key={index}>
                                  <td>{bill.type}</td>
                                  <td>{parseFloat(bill.price).toFixed(3)}</td>
                                  <td>{(parseFloat(bill.price) * 83.27).toFixed(2)}</td>
                              </tr>
                          ))}
                      </tbody>
                  </table>
                  <button className="close-button" onClick={() => setShowPopup(false)}>X</button>
             </div>
      )}
    </div>
  );

  return (
    <div className="App">
      <TopMenuBar />
      <h1>Bill Information</h1>
      {renderTable()}
      <div className="pagination">
        <button onClick={() => setPageIndex(Math.max(1, pageIndex - 1))}>Previous</button>
        <span className="pagenumber">Page no. {pageIndex}</span>
        <button onClick={() => setPageIndex(pageIndex + 1)}>Next</button>
      </div>
    </div>
  );
}

export default Bill;
