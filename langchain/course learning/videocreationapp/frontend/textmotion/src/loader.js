// Loader.js
const Loader = () => {
    const loaderStyle = {
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      backgroundColor: 'rgba(0, 0, 0, 0.3)',
      zIndex: 1000,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column'
    };
  
    return (
      <div style={loaderStyle}>
        <img src="https://icaengineeringacademy.com/wp-content/uploads/2019/01/ajax-loading-gif-transparent-background-2.gif" alt="loading" />
        <p>processing...</p>
      </div>
    );
  };
  
  export default Loader;
  