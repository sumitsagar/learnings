import React, { useState, useEffect,useRef } from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import ScriptList from './ScriptList';
import Editor from './Editor';
import { googleLogout } from '@react-oauth/google'; 
import Login from './Login';
import Bill from './bill';
import ProfilePage from './profile';


const ProtectedRoute = ({ isLoggedIn, initiateGoogleLogin, element }) => {
  // debugger;
  if (isLoggedIn) {
    return element;
  } else {
    return <Navigate to="/login" replace />;
  }
};


const routeConfig = [
  {
    path: '/',
    component: ScriptList
  },
  {
    path: '/editor',
    component: Editor
  },
  {
    path: '/bill',
    component: Bill
  },
  {
    path: '/profile',
    component: ProfilePage
  },
  // Add other protected routes here
];

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);    
  const [isLoading, setIsLoading] = useState(true); // Add this line


useEffect(() => {
    
    const token = localStorage.getItem('token');
    // debugger;
    if (token) {
      // debugger;
      setIsLoggedIn(true);
    }else{
      // alert("loggingout");
      try{
        // debugger;
        googleLogout();//since token is missing so we will logut user and ask to signin again
      }catch(e){
        alert(e);
      }
    }
    setIsLoading(false); // Set this to false once you're done checking
  }, []);

 
  const responseOutput = (response) => {  
  
    // Send data to your API
    fetch("http://127.0.0.1:5000/validate_google_login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        credential: response.credential,
        clientId: response.clientId,
        select_by: response.select_by,
      }),
    })
    .then((res) => res.json())
    .then((data) => {
      console.log("API Response:", data); 
      localStorage.setItem('token', data.token);
      setIsLoggedIn(true);
      window.location = "/"

    })
    .catch((error) => {
      console.log("API Error:", error);
    });
  };

  const errorOutput = (error) => {
    alert(error);
    console.log('Error during login:', error);
    setIsLoggedIn(false);
  };
  

  if (isLoading) {
    return <div>Loading...</div>; // Or your own loading indicator
  }

  return (
    <Router>
      <div>
        <nav>
          {!isLoggedIn &&
            <Login responseOutput={responseOutput} errorOutput={errorOutput} />
          
          // : (
          //   <p></p>
          // )
          }
        </nav>
        <Routes>
          {routeConfig.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              element={
                route.path === '/login' ? (
                  <route.component responseOutput={responseOutput} errorOutput={errorOutput} />
                ) : (
                  <ProtectedRoute
                    isLoggedIn={isLoggedIn}
                    element={<route.component />}
                  />
                )
              }
            />
          ))}
        </Routes>
      </div>
    </Router>
  );
};

export default App;
