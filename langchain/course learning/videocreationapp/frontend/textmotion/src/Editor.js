import React, { useState, useEffect ,useRef } from 'react';
import './App.css';
import { json, useLocation } from 'react-router-dom';
import { TopMenuBar } from './TopMenuBar';

import * as styles from './styles';  
import Loader from './loader'; 
import ImageSubtitleAudio from './videopreview';
import PromptList from './imagedurationeditor';
import SubtitleEditor from './subtitleeditor';
import ImageGallery from './ImageGallery'; 
import Popup from './Popup';

function Editor() {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const rid = searchParams.get('rid');

  const [isLoading, setIsLoading] = useState(false);
  const [inputText, setInputText] = useState(""); 
  const [rationselectiontype, setrationselectiontype] = useState(""); 
  const [showscriptbreakup, setshowscriptbreakup] = useState(false);
  const [showscriptandprompt, setshowscriptandprompt] = useState(false);
  const[scriptsaved,setscriptsaved] = useState(true);

  const [subtitle, setsubtitle] = useState("");
  const [imageprompts, setimageprompts] = useState(""); 


  const [description, setdescription] = useState("");
  const [summary, setsumary] = useState("");
  const [title, settitle] = useState("");
  const [script, setscript] = useState("");
  const [requestid,setrequestid] = useState(rid);
  
  const [inputType, setInputType] = useState("jsonData"); // "jsonData" or "topic"

  const [scriptbreakupedited, setscriptbreakupedited] = useState(false);
  const [SubAndPromptedited,setSubAndPromptedited]= useState(false);


  const [selectedImage, setSelectedImage] = useState(null);
  const [imageUrls, setimageUrls] = useState(null);


  const [progress,setprogress]= useState(0);
  const [imagegenerated,setimagegenerated] =  useState(0);

  const [isSubtitleExpanded, setSubtitleExpanded] = useState(false);
  const [isImagePromptExpanded, setImagePromptExpanded] = useState(false);

  const [casousaldata, setcasousaldata] = useState({});
  const [iscasousaldataset, setiscasousaldataset] = useState(false);
  const [imageurlarray, setimageurlarray] = useState({});
  const [imageDurations, setimageDurations] = useState(false);
  const [Audiourl, setAudiourl] = useState(false);
  const [showvideoPopup, setShowvideoPopup] = useState(false);

   
  
  const audioRef = useRef(null);

  const truncate = (str) => {
    return str.length > 50 ? str.substring(0, 50) + "..." : str;
  };

  // alert(rid);


  useEffect(() => {
    GetDetailsByRequestID(); 
  }, []);

  useEffect(() => {
    let timer;
    // setcasousaldata(sampleData);

    return () => {
      clearInterval(timer);
    };
  }, []);
 


  const GetScript = () => { 
    setshowscriptbreakup(false);
    console.log("Submit button clicked with input:", inputText);
  
    setIsLoading(true);
    fetch('http://localhost:5000/posttext', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      },
      body: JSON.stringify({
        data: inputText, // or whatever data you want to send
        type: inputType // the selected radio value
      })
    })
      .then(response => response.json())
      .then(data => {
        console.log('Success:', data);
        setdescription(data.Data.Description);
        setsumary(data.Data.Summary);
        settitle(data.Data.Title);
        setscript(data.Data.script1); 
        setrequestid(data.requestid);
        setshowscriptbreakup(true);
        setprogress(1);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error('Error:', error);
        setIsLoading(false);
      });
  };

  // function constructJsonForCarousal(imgarr,imageDurations,sub) {
  //   const output = [];

  //   var sub = JSON.parse(sub); 
  //   // var prompts = JSON.parse(imageprompts).prompts;
  //   for (let i = 0; i < imageDurations.length; i++) {
  //     const duration = imageDurations[i];
  //     const url = imgarr[i];
  //     // const prompt = prompts[i];
  
  //     const obj = {
  //       url,
  //       duration,
  //       // prompt
  //     };
  
  //     output.push(obj);
  //   }
  
  //   var _casousaldata = {};
  //   _casousaldata.images = output;
  //   _casousaldata.subtitle = sub;
  //   console.log(_casousaldata);
  //   setcasousaldata(_casousaldata);
  //   setiscasousaldataset(true);
  // }


  const handleVideoGeneration = () => {
    console.log("Proceed with video generation button clicked");
    // Your logic for video generation here
  };

  const GenerateImage = async () => {

    try { 
      setIsLoading(true);
      const response = await fetch("http://localhost:5000/generateimages/" + requestid, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'image/png',
          'Authorization': 'Bearer 8ef92cce43aa8972419f8b62944f774df203bdbdc25cf5647d385e7b568429a84e596cbb64fbeaf4300e2f3de343c597'
        }, 
      }); 
      if (response.ok) {
        const data = await response.json(); 
        if(data.Data !== undefined){
          setimageUrls(data.Data);
          setimagegenerated(true);
          setprogress(3); 

          var imageDurations = imageprompts.image_duration;  
          setimageDurations(imageDurations); 
          setiscasousaldataset(true); 
          setIsLoading(false);
        }
      } else {
        console.log("Failed to save the script:");
        alert("error getting data");
        setIsLoading(false);
      }
    } catch (error) {
      setIsLoading(false);
      console.error("An error occurred while saving the script:", error);
    }

  }

  const GetDetailsByRequestID = async () => {
    if (requestid === null){
      return;
    }
    try { 
      setIsLoading(true);
      const response = await fetch("http://localhost:5000/getgeneration/" + requestid, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'image/png',
          'token': localStorage.getItem('token')
        }, 
      }); 
      if (response.ok) {
        const data = await response.json(); 
        if(data.Data !== undefined){
          // debugger;
          setIsLoading(false);
          if(data.Data.script !== null && data.Data.script !== "" && data.Data.script !== undefined)
          {
            var scriptobj = data.Data.script
            setsumary(scriptobj.Summary);
            setdescription(scriptobj.Description);
            settitle(scriptobj.Title);
            setscript(scriptobj.script1);
            setshowscriptbreakup(true); 
            setprogress(1);
            setInputText(data.Data.inputtext);
            setInputType(data.Data.inputtype);
          }
          if(data.Data.sub !== null && data.Data.sub !== undefined && data.Data.image_prompt !== undefined &&  data.Data.image_prompt !== null){
            setsubtitle((data.Data.sub));
            setimageprompts(data.Data.image_prompt);
            setshowscriptandprompt(true);
            setprogress(2);
          }
          if(data.Data.image_url !== null && data.Data.image_url !== undefined && (data.Data.image_url !== "")){
            // debugger;
            setimageUrls(data.Data.image_url);
            setimagegenerated(true);
            setAudiourl(data.Data.audiopath)
            setprogress(3);
            
            // var sub = JSON.parse(data.Data.sub); 
            // setcasousaldata(casousaldata);
            // constructJsonForCarousal(JSON.parse(data.Data.image_url),data.Data.image_prompt,data.Data.sub);
            
            
            var imageDurations = data.Data.image_prompt.image_duration; 
            // var imageurlarr = JSON.parse(data.Data.image_url);
            setimageDurations(imageDurations);
            // alert(JSON.stringify(imageurlarr)); 
            // imageurlarray(imageurlarr); 
            
            setiscasousaldataset(true); 
            setIsLoading(false);
          }
 
        }
      } else {
        setIsLoading(false);
        console.log("Failed to save the script:");
        alert("error getting data");
      }
    } catch (error) {
      setIsLoading(false);
      console.error("An error occurred while saving the script:", error);
    }

  }


  const GetSubtitleAndImagePrompts = async () => {

    try { 
      setIsLoading(true);
      const response = await fetch("http://localhost:5000/gettranscriptionsandprompts/" + requestid, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'image/png',
          'Authorization': 'Bearer 8ef92cce43aa8972419f8b62944f774df203bdbdc25cf5647d385e7b568429a84e596cbb64fbeaf4300e2f3de343c597'
        }, 
      }); 
      if (response.ok) {
        setIsLoading(false);
        const data = await response.json(); 
        if(data.Data.imagepromptswithduration !== undefined){
          setsubtitle(data.Data.subtitle);
          setimageprompts(data.Data.imagepromptswithduration);
          setshowscriptandprompt(true);
          setprogress(2); 
          casousaldata.subtitle = data.Data.subtitle; 
          setcasousaldata(casousaldata);
          setiscasousaldataset(true);
        }
      } else {
        setIsLoading(false);
        console.log("Failed to save the script:");
        alert("error getting data");
      }
    } catch (error) {
      setIsLoading(false);
      console.error("An error occurred while saving the script:", error);
    }

  }

  const saveScript = async () => { 
    const requestBody = JSON.stringify({
      Description: description, 
      Title: title,
      script1: script,
      requestid: requestid,
      Summary : summary
    });
  
    try {
      setIsLoading(false);
      const response = await fetch("http://localhost:5000/updatescript", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'image/png',
          'Authorization': 'Bearer 8ef92cce43aa8972419f8b62944f774df203bdbdc25cf5647d385e7b568429a84e596cbb64fbeaf4300e2f3de343c597'
        },
        body: requestBody
      }); 
      if (response.ok) { 
        setIsLoading(false);
        const data = await response.json();
        if(data.requestid !== undefined && data.requestid > 0){
          // alert(data.requestid);
          setrequestid(data.requestid);
          console.log("Successfully saved the script:", data);
          setscriptbreakupedited(false);
        }
      } else {
        setIsLoading(false);
        console.log("Failed to save the script:", response.status, response.statusText);
        alert("error saving script");
      }
    } catch (error) {
      setIsLoading(false);
      console.error("An error occurred while saving the script:", error);
    }
  };

  const savesubAndPrompts = async () => { 
    const requestBody = JSON.stringify({
      sub: subtitle, 
      imageprompts: imageprompts,
      requestid: requestid
    });
  
    try {
      setIsLoading(true);
      const response = await fetch("http://localhost:5000/updatetranscriptionsandprompts", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'image/png',
          'Authorization': 'Bearer 8ef92cce43aa8972419f8b62944f774df203bdbdc25cf5647d385e7b568429a84e596cbb64fbeaf4300e2f3de343c597'
        },
        body: requestBody
      }); 
      setIsLoading(false);
      if (response.ok) { 
          setSubAndPromptedited(false);
      } else {
        console.log("Failed to save");
        alert("error saving script");
      }
    } catch (error) {
      setIsLoading(false);
      console.error("An error occurred while saving the script:", error);
    }
  };
  
  const handleImageClick = (url) => {
    setSelectedImage(url);
  };

  const handleClose = () => {
    setSelectedImage(null);
  };

  
  const GenerateVideo = async () => {
    try { 
      setIsLoading(true);
      const response = await fetch("http://localhost:5000/generatevideo/" + requestid, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'image/png',
          'Authorization': 'Bearer 8ef92cce43aa8972419f8b62944f774df203bdbdc25cf5647d385e7b568429a84e596cbb64fbeaf4300e2f3de343c597'
        }, 
      }); 
      setIsLoading(false);
      debugger;
      if (response.ok) {
        console.log(response);
        var data = await response.json();
        alert(data.Data);
      } else {
        console.log("Failed to save the script:");
        var data = await response.json();
        alert(data.errormsg);
      }
    } catch (error) {
      setIsLoading(false);
      console.error("An error occurred while saving the script:", error);
    }
  };

  const HandleRadioSelection = async (e) => {
    setrationselectiontype(e.target.value); 
    // alert(e.target.value);
  } 

  return (
    <div>
       <div>
        <TopMenuBar />
        {/* Your Routes */}
      </div>
      <div>
        {isLoading ? <Loader /> : <div></div>}
      </div>
      <div  style={styles.containerStyle}>
        <div style={styles.leftSectionStyle}>
          <div style={styles.frameContainerStyle}>
            <div style={styles.topSectionStyle}>
              Your Input for content
            </div>
            {progress == 0 &&
              <div style={styles.framecontentstyle}>
                <textarea
                  style={styles.textAreaStyle}
                  value={inputText}
                  onChange={(e) => setInputText(e.target.value)}
                  placeholder="Enter text here..."
                />
                <div style={styles.radioGroupStyle}>
                  <div style={styles.radioStyle}>
                    <input
                      style={styles.enlargedRadio}
                      type="radio"
                      id="jsonData"
                      name="inputType"
                      value="jsonData"
                      checked={inputType === "jsonData"}
                      onChange={(e) => {
                        setInputType(e.target.value);
                        console.log(e.target.value);
                        HandleRadioSelection(e);
                      }}
                    />
                    <label style={styles.radioLabelStyle} htmlFor="jsonData">Enter json data</label>
                  </div>
                  <div style={styles.radioStyle}>
                    <input
                      style={styles.enlargedRadio}
                      type="radio"
                      id="topic"
                      name="inputType"
                      value="topic"
                      checked={inputType === "topic"}
                      onChange={(e) => {
                        setInputType(e.target.value);
                        console.log(e.target.value);
                        HandleRadioSelection(e);
                      }}
                    />
                    <label style={styles.radioLabelStyle} htmlFor="topic">Enter a topic</label>
                  </div>
                  <div style={styles.radioStyle}>
                    <input
                      style={styles.enlargedRadio}
                      type="radio"
                      id="script"
                      name="inputType"
                      value="script"
                      checked={inputType === "script"}
                      onChange={(e) => {
                        setInputType(e.target.value);
                        console.log(e.target.value);
                        HandleRadioSelection(e);
                      }}
                    />
                    <label style={styles.radioLabelStyle} htmlFor="jsonData">Enter Script</label>
                  </div>
                </div>
                {rationselectiontype !== 'jsonData' &&
                  <button style={styles.buttonStyle} onClick={GetScript}>Get Script</button>
                }
                {rationselectiontype === 'jsonData' &&
                <button style={styles.buttonStyle} onClick={GetScript}>Generate subtitle + image prompts</button>
                }
                {/* <button style={styles.buttonStyle} onClick={GetScript}>Proceed with Video</button>  */}

              </div>
              }   
            {progress > 0 &&
              <div  style={styles.framecontentstyle}>
                <h6>Your Input</h6> 
                <br></br> 
                <h5>({inputType}){inputText}</h5>
              </div> 
            }  
          </div>
          {showscriptbreakup &&

            
            <div style={styles.frameContainerStyle}>
              <div style={styles.topSectionStyle}>
                Details and metadata of content
              </div>
              {progress == 1 &&
                <div  style={styles.framecontentstyle}>
                  <div>
                    <label htmlFor="title">Title</label>
                    <textarea  value={title}  id="title" 
                    onChange={(e) => { settitle(e.target.value); setscriptbreakupedited(true);} } style={styles.textAreaStyle}></textarea>
                  </div>
                  <div style={{ marginTop: '10px' }}>
                    <label htmlFor="script">Script</label>
                    <textarea  value={script}  id="script" onChange={(e) => {setscript(e.target.value); setscriptbreakupedited(true);} } 
                    style={{...styles.textAreaStyle,height:'150px'}}
                    ></textarea>
                  </div>
                  <div style={{ marginTop: '10px' }}>
                    <label htmlFor="description">Description</label>
                    <textarea value={description} id="description" onChange={(e) => {setdescription(e.target.value); setscriptbreakupedited(true);}} style={styles.textAreaStyle}></textarea>
                  </div>
                  <div style={{ marginTop: '10px' }}>
                    <label htmlFor="summary">Summary</label>
                    <textarea value={summary} id="summary" onChange={(e) => {setsumary(e.target.value); setscriptbreakupedited(true);}} style={styles.textAreaStyle}></textarea>
                  </div>
                  {scriptbreakupedited &&
                    <button style={styles.buttonStyle} onClick={saveScript}>
                      Save
                    </button>
                  }

                  <div>
                        <button style={styles.buttonStyle} onClick={GetSubtitleAndImagePrompts}>
                          Generate subtitle and image promptss
                        </button>
                        {/* <button style={styles.buttonStyle} onClick={handleVideoGeneration}>
                          Proceed with video
                        </button> */}
                  </div>
                </div>
              }
              {progress > 1 &&
                <div  style={styles.framecontentstyle}>
                <h6>Title</h6> 
                <h5>{title}</h5>
                <br></br>
                <h6>Script</h6> 
                <h5>{script}</h5>
                <br></br>
                <h6>Description</h6> 
                <h5>{description}</h5>
                <br></br>
                <h6>Summary</h6> 
                <h5>{summary}</h5>
              </div> 
              }
            </div>
            
          }
          {showscriptandprompt && progress === 2 &&

            <div style={styles.frameContainerStyle}>
              <div style={styles.topSectionStyle}>
                Subtitle and Prompts for image assets
              </div>
              <div style={styles.framecontentstyle}>
                {/* <div>
                  <label htmlFor="subtitle">Subtitle</label>
                  <textarea value={subtitle} id="title" onChange={(e) => { setsubtitle(e.target.value); setSubAndPromptedited(true); }} style={styles.textAreaStyle}></textarea>
                </div>
                <div style={{ marginTop: '10px' }}>
                  <label htmlFor="imageprompts">imageprompts</label>
                  <textarea value={imageprompts} id="script" onChange={(e) => { setimageprompts(e.target.value); setSubAndPromptedited(true); }} style={styles.textAreaStyle}></textarea>
                </div> */}
                <SubtitleEditor subtitle={subtitle} requestid={requestid}  progress={progress}></SubtitleEditor>
                <PromptList 
                 dataasstr={imageprompts}  requestid={requestid} progress={progress}/>
                {/* {SubAndPromptedited &&
                  <button style={styles.buttonStyle} onClick={savesubAndPrompts}>
                    Save
                  </button>
                } */}
                <button style={{...styles.buttonStyle,width:'400px',height:'50px',fontSize:'16px'}} onClick={GenerateImage} >
                  Proceed with Image Generation
                </button>
              </div>
            </div> 
          }
          {progress > 2 &&
            <div style={styles.frameContainerStyle}>
              <div style={styles.topSectionStyle}>
              Subtitle and Prompts for image assets 
              </div>
              <div  style={styles.framecontentstyle}>                
                <SubtitleEditor subtitle={subtitle} requestid={requestid}  progress={progress}></SubtitleEditor>
                <PromptList dataasstr={imageprompts} requestid={requestid}  progress={progress}/>
              </div>
            </div>
          }

          {imagegenerated && progress > 2 &&
          
            <div style={styles.frameContainerStyle}>
              <div style={styles.topSectionStyle}>
                Images generated from Prompts, this will be used for your video
              </div>
              <div style={styles.framecontentstyle}>
                {/* <span>Images</span> */}
                <ImageGallery images={imageUrls} requestId={requestid} imageprompts={imageprompts} />


                <div style={styles.imageGallery}>
                  {selectedImage && (
                    <div style={styles.expandedImageWrapper}>
                      <img src={selectedImage} alt="Expanded" style={styles.expandedImage} />
                      <button style={styles.closeButton} onClick={handleClose}>
                        Close
                      </button>
                    </div>
                  )}
                  <button style={styles.buttonStyle} onClick={GenerateVideo}>
                    Generate Video
                  </button>
                  <button style={styles.buttonStyle} onClick={() => setShowvideoPopup(true)}>Preview Video</button>
                </div>
              </div>
            </div>
            
          }
        </div>

        {progress > 2 && iscasousaldataset &&
            <Popup isVisible={showvideoPopup} onClose={() => setShowvideoPopup(false)}>
              {progress > 2 && iscasousaldataset &&
                <ImageSubtitleAudio
                  Audiourl={Audiourl}
                  imageurlarray={imageUrls}
                  imageDurations={imageDurations}
                  subtitle={subtitle}
                />
              }
            </Popup>
          }
      </div>
    </div>
  );
}

  
export default Editor;
