import React, { useState, useEffect } from 'react';

const TextareaComponent = () => {
    const textareaStyle = {
      width: '100%',
      padding: '12px 20px',
      margin: '8px 0',
      boxSizing: 'border-box',
      border: '2px solid #ccc',
      borderRadius: '4px',
      resize: 'vertical',
      fontFamily: 'Arial, sans-serif',
      fontSize: '16px',
    };
  
    const focusedTextareaStyle = {
      ...textareaStyle,
      borderColor: '#007bff',
      outline: 'none',
    };
  
    const [isFocused, setFocus] = React.useState(false);
  
    return (
        <textarea
        style={isFocused ? focusedTextareaStyle : textareaStyle}
        placeholder="Type your text here..."
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
      ></textarea>
    );
  };
  
  export default TextareaComponent;