import React from 'react';
import { GoogleLogin } from '@react-oauth/google';

const styles = {
  container: {
    textAlign: 'center',
    position: 'relative',
  },
  bannerImage: {
    width: '100%',
    height: 'auto',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
  },
  overlay: {
    position: 'absolute',
    top: '200px',
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(128, 128, 128, 0.4)',
    zIndex: 2,
  },
  content: {
    position: 'relative',
    zIndex: 3,
    padding: '2rem',
    
  },
  title: {
    fontSize: '3.5rem',
    margin: '1rem 0',
    color: '#fff',
    marginTop:'20%',
  },
  description: {
    fontSize: '1.2rem',
    color: '#fff',
    maxWidth: '800px',
    margin: '1rem auto',
    lineHeight: '1.5',
  },
  loginButtonWrapper: {
    marginTop: '2rem',
    marginLeft:'40%'
  },
};

const Login = ({ responseOutput, errorOutput }) => {
  return (
    <div style={styles.container}>
      <img style={styles.bannerImage} src="https://www.thedigitalspeaker.com/content/images/2022/10/What-is-the-Future-of-Artificial-Intelligence.jpg" alt="Banner" />
      <div style={styles.overlay}></div>
      
      <div style={styles.content}>
        <h1 style={styles.title}>Text2Video: Your Imagination, Visualized</h1>
        
        <p style={styles.description}>
          Welcome to Text2Video, the revolutionary platform that transforms your written words into captivating video content. 
          Whether you're a storyteller, a marketer, or just someone looking to bring ideas to life, our platform simplifies 
          the process. With auto-generated audio, customizable subtitles, and engaging illustrations, you're just a text input 
          away from creating shareable, impactful, and visually stunning videos. Join us and unleash your creativity!
        </p>
        
        <div style={styles.loginButtonWrapper}>
          <GoogleLogin
            clientId="your-google-client-id"
            buttonText="Login with Google"
            onSuccess={credentialResponse => {
              console.log(credentialResponse);
              responseOutput(credentialResponse);
            }}
            onFailure={() => {
              console.log('Login Failed');
              errorOutput('Login Failed');
            }}
            cookiePolicy={'single_host_origin'}
          />
        </div>
      </div>
    </div>
  );
};

export default Login;
