import React, { useState, useEffect } from 'react';
import { TopMenuBar } from './TopMenuBar';

const ProfilePage = () => {
  const [userData, setUserData] = useState(null);

  useEffect(() => {
    const fetchProfile = async () => {
      const token = localStorage.getItem('token');

      try {
        const response = await fetch('http://localhost:5000/profile', {
          method: 'GET',
          headers: {
            'token': token
          }
        });

        const data = await response.json();
        if (data.status === 'success') {
          setUserData(data.Data);
        } else {
          console.error('Error fetching profile data:', data);
        }
      } catch (error) {
        console.error('Error fetching profile:', error);
      }
    };

    fetchProfile();
  }, []);

  return (
    <div>
        <TopMenuBar />
    <div style={styles.darkContainer}>
      {userData ? (
        <table style={styles.table}>
          <tbody>
            <tr>
              <td>Name:</td>
              <td>{userData.name}</td>
            </tr>
            <tr>
              <td>Email:</td>
              <td>{userData.email}</td>
            </tr>
            <tr>
              <td>Username:</td>
              <td>{userData.username}</td>
            </tr>
            <tr>
              <td>Phone:</td>
              <td>{userData.phone || 'N/A'}</td>
            </tr>
            <tr>
              <td>Created On:</td>
              <td>{userData.createdon}</td>
            </tr>
            <tr>
              <td>Credit</td>
              <td>{userData.creditbalance}</td>
            </tr>
          </tbody>
        </table>
      ) : (
        <p style={styles.loadingText}>Loading...</p>
      )}
    </div>
    </div>
  );
};

const styles = {
    darkContainer: {
      backgroundColor: '#3d3d3d',
      color: '#f1f1f1',
      fontSize: '18px',
      minHeight: '100vh',
      padding: '20px',
      display: 'flex',
      justifyContent: 'center',
      // Removed alignItems to align content to the top
    },
    table: {
      width: '50%',
      height :'40vh',
      borderCollapse: 'collapse',
      backgroundColor: '#444',
      borderRadius: '8px',
      overflow: 'hidden',
    },
    td: {
      border: '1px solid #f1f1f1',
      padding: '12px 16px',
    },
    loadingText: {
      fontSize: '20px',
      fontWeight: 'bold',
    }
  };

export default ProfilePage;
