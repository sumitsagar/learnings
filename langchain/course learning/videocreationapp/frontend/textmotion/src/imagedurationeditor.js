import React, { useState, useEffect } from 'react';
import Loader from './loader'; 

const PromptList = ({ dataasstr,requestid,progress }) => {
    debugger;
    // alert(progress);
    // debugger;
    // alert(requestid);
    var data = {
    prompts: [
      // Your prompt strings here
    ],
    image_duration: [
      // Your time arrays here
    ],
  };
  if(dataasstr !== undefined){
    data = dataasstr
  }
  const [rows, setRows] = useState(() => {
    if (!data || !data.prompts || !Array.isArray(data.prompts) || 
        !data.image_duration || !Array.isArray(data.image_duration)) {
      return [];
    }
    return data.prompts.map((prompt, index) => {
      const duration = data.image_duration[index];
      return {
        prompt,
        // startTime: duration && duration[0] ? duration[0] : null,
        // endTime: duration && duration[1] ? duration[1] : null,
        startTime: duration && (duration[0] !== null && duration[0] !== undefined) ? duration[0] : null,
        endTime: duration && (duration[1] !== null && duration[1] !== undefined) ? duration[1] : null,

      };
    });
  });
  
  const [isModified, setIsModified] = useState(false);
  const [isEditMode, setIsEditMode] = useState(false);
  const [updatedPromptDat, setupdatedPromptDat] = useState();
  const [initialRows, setInitialRows] = useState([...rows]);
  const [isLoading, setIsLoading] = useState(false);

  const savePrompts = async (imagepromt) => { 
    setIsLoading(true);
    const requestBody = JSON.stringify({
      sub: null, 
      imageprompts: JSON.stringify(imagepromt),
      requestid: requestid
    });
  
    try {
      setIsLoading(true);
      debugger;
      const response = await fetch("http://localhost:5000/updatetranscriptionsandprompts", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'image/png',
          'Authorization': 'Bearer 8ef92cce43aa8972419f8b62944f774df203bdbdc25cf5647d385e7b568429a84e596cbb64fbeaf4300e2f3de343c597'
        },
        body: requestBody
      }); 
      setIsLoading(false);
      if (response.ok) { 
        //   setSubAndPromptedited(false);
      } else {
        console.log("Failed to save");
        alert("error saving script");
      }
    } catch (error) {
      setIsLoading(false);
      console.error("An error occurred while saving the script:", error);
    }
  };

  const handleTextChange = (index, field, e) => {
    const newRows = [...rows];
    newRows[index][field] = e.target.value;
    setRows(newRows);
    setIsModified(true);
  };

  const handleAddRow = () => {
    setRows([...rows, { prompt: '', startTime: 0, endTime: 0 }]);
    setIsModified(true);
  };

  const handleRemoveRow = (index) => {
    const newRows = [...rows];
    newRows.splice(index, 1);
    setRows(newRows);
    setIsModified(true);
  };

  const handleSave = () => {
    const updatedData = {
      prompts: rows.map(row => row.prompt),
      image_duration: rows.map(row => [parseFloat(row.startTime), parseFloat(row.endTime)]),
    };
    debugger;
    setupdatedPromptDat(updatedData);
    console.log('Updated JSON:', JSON.stringify(updatedData, null, 2));
    setIsModified(false);
    setIsEditMode(false);
    setInitialRows([...rows]);
    savePrompts(updatedData);
  };
  const handleCancel = () => {
    setRows([...initialRows]);
    setIsModified(false);
    setIsEditMode(false);
  };

  const handleEdit = () => {
    setIsEditMode(true);
  };

  return (
      <div>
          <div>
              {isLoading ? <Loader /> : <div></div>}
          </div>
          <br></br>
          <h4>Image Prompts</h4>
          <table border="1">
              <thead>
                  <tr>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th>ImagePrompt</th> 
                      <th>Action</th> 
                  </tr>
              </thead>
              <tbody>
                  {rows.map((row, index) => (
                      <tr key={index}>
                          {isEditMode ? (
                              <>
                                  <td  style={{ width: "50px" }}>
                                      <input
                                          type="number"
                                          value={row.startTime}
                                          style={{background:'#222',color:'white' }}
                                          onChange={(e) => handleTextChange(index, 'startTime', e)}
                                      />
                                  </td>
                                  <td  style={{ width: "50px" }}>
                                      <input
                                          type="number"
                                          value={row.endTime}
                                          style={{background:'#222',color:'white' }}
                                          onChange={(e) => handleTextChange(index, 'endTime', e)}
                                      />
                                  </td>
                                  <td style={{ width: "800px" }}>
                                      <textarea
                                          rows="4"
                                          cols="50"
                                          value={row.prompt}
                                          onChange={(e) => handleTextChange(index, 'prompt', e)}
                                          style={{ width: "100%", height: "100%",background:'#222',color:'white' }}
                                      ></textarea>
                                  </td>
                                  <td>
                                      {isEditMode && <button onClick={() => handleRemoveRow(index)}>Remove</button>}
                                  </td>
                              </>
                          ) : (
                              <>
                                  <td>{row.startTime}</td>
                                  <td>{row.endTime}</td>
                                  <td>{row.prompt}</td>
                              </>
                          )}
                      </tr>
                  ))}
              </tbody>
          </table>
          {isEditMode && progress === 2 && <button onClick={handleAddRow}>Add Row</button>}
          {isEditMode && isModified && <button onClick={handleSave}>Save</button>}
          {isEditMode && <button onClick={handleCancel}>Cancel</button>}
          {!isEditMode && progress === 2 && <button onClick={handleEdit}>Edit</button>}
      </div>

  );
};

export default PromptList;








