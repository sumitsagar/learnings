
  // const generateNewImage = async () => {
  //   // const response = await callAPI();
  //   // if (response.status === 'success') {
  //   //   setNewImages([...newImages, response.imageurl]);
  //   //   setSelectedImage(originalImage); // Revert selection
  //   //   setShowSelectMessage(false);
  //   // }
  //   setNewImages([...newImages, "https://m.media-amazon.com/images/I/81gxiU-w93L._AC_UF1000,1000_QL80_.jpg"]);
  //   setSelectedImage(originalImage); // Revert selection
  //   setShowSelectMessage(false);
  // };
import React, { useState ,useEffect} from 'react';
import './gallerystyle.css'; // Assuming you saved the CSS in a file named 'gallerystyle.css'
import 'font-awesome/css/font-awesome.min.css';

const callAPI = async (promptValue, requestId) => {
  const endpoint = 'http://localhost:5000/generateimage_byprompt';
  const data = {
    prompt: promptValue,
    requestid: requestId
  };

  try {
    const response = await fetch(endpoint, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const json = await response.json();

    // Assuming the response JSON contains a 'url' field.
    return json.url;
  } catch (error) {
    console.error('There was an error with the API call', error);
    return {
      status: 'error',
      message: error.message
    };
  }
};

const ImageGallery = ({ images,imageprompts,requestId }) => {
  // debugger;
  var imagepromptsarr = imageprompts;
  const [selectedImageIndex, setSelectedImageIndex] = useState(null); // Added this state
  const [originalImage, setOriginalImage] = useState(null);
  const [imageArray, setImageArray] = useState(images);
  const [showPopup, setShowPopup] = useState(false);
  const [newImages, setNewImages] = useState([]);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const [showSelectMessage, setShowSelectMessage] = useState(false);
  const [textAreaValue, setTextAreaValue] = useState({});



  useEffect(() => {
    if (selectedIndex !== undefined) {
      setTextAreaValue(JSON.stringify(imagepromptsarr.prompts[selectedIndex]));
    }
  }, [selectedIndex]);

  const handleTextAreaChange = (e) => {
    setTextAreaValue(e.target.value);
    // Optionally, you can update your original data or handle this value differently
}
  const generateNewImage = async () => {
    // Removed selectedImage state and added selectedImageIndex
    // setNewImages([...newImages, "https://m.media-amazon.com/images/I/81gxiU-w93L._AC_UF1000,1000_QL80_.jpg"]);
    const newimageurl = await callAPI(textAreaValue, requestId);
    setNewImages([...newImages,newimageurl]);
    setSelectedImageIndex(null); // Resetting selected image index
    setShowSelectMessage(false);
  };

  const saveImage = () => {
    var updatedArray = null;
    if (selectedImageIndex !== null) {
      const selectedImage = newImages[selectedImageIndex];
      updatedArray = imageArray.map((img, index) =>
        index === selectedIndex ? selectedImage : img
      );
      setImageArray(updatedArray);
      console.log('Updated Array:', updatedArray);
      closePopup();
    } else {
      setShowSelectMessage(true);
    }
    fetch('http://localhost:5000/update_image', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        requestid: requestId, // or whatever data you want to send
        images: updatedArray // the selected radio value
      })
    })
      .then(response => response.json())
      .then(data => {
        console.log('Success:', data);
        
      })
      .catch((error) => {
        console.error('Error:', error); 
      });
  };

  const closePopup = () => {
    setShowPopup(false);
    setSelectedImageIndex(null); // Resetting selected image index
    setNewImages([]);
    setShowSelectMessage(false);
  };

  return (
    <div>
      <h4>Images</h4>
      <div className="image-list">
        {imageArray.map((image, index) => (
          <div className="image-item" key={index}>
            <img src={image} alt={`img-${index}`} style={{ height: '450px', width: 'auto' }} />
            <i
              className="fa fa-pencil"
              onClick={() => {
                console.log(`Editing image at index: ${index}`); 
                setShowPopup(true);
                setSelectedIndex(index);
                setOriginalImage(image);
                setNewImages([]);
                setShowSelectMessage(false);
              }}
            ></i>
          </div>
        ))}
      </div>
      {showPopup && (
        <div className="popup-overlay">
          <div className="popup">

            <div style={{ background: '#edf5ef', height: '170px', padding: '10px', display: 'flex', alignItems: 'center' }} className="prompt-section">
              <img src={originalImage} alt="Selected" width="100" />
              <textarea
                style={{ width: '600px', height: '80px', margin: '20px' }}
                value={textAreaValue}
                onChange={handleTextAreaChange}
              />
              <button onClick={generateNewImage}>Generate New Image</button>
              <button onClick={closePopup}>Cancel</button>
            </div>
            

            {newImages.length > 0 && <p>Click on an image to select</p>}
            {showSelectMessage && <p style={{ color: 'red' }}>Please select an image</p>}

            <div>
              {newImages.map((newImage, i) => (
                <img
                  src={newImage}
                  key={i}
                  alt={`new-img-${i}`}
                  style={{ objectFit: 'cover', margin: '7px', height: '450px', width: 'auto' }}
                  className={selectedImageIndex === i ? 'selected-image' : ''}
                  onClick={() => setSelectedImageIndex(i)}
                />
              ))}
            </div>

            <div className="button-group">
              {newImages.length > 0 && <button onClick={saveImage}>Save</button>}
             
            </div>
          </div>
   </div>
   
    
      
      )}
    </div>
  );
};

export default ImageGallery;
