import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { GoogleOAuthProvider } from '@react-oauth/google';
import { AuthProvider } from './AuthContext';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render( 
    <GoogleOAuthProvider clientId='604593922734-t8b5lf0s0b00hhfao3u9hfeuc8nao02o.apps.googleusercontent.com'>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </GoogleOAuthProvider > 
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
