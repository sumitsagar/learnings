import React, { useState } from 'react';

const Popup = ({ isVisible, onClose, children }) => {
    return (
        <>
            {isVisible && (
                <div style={{
                    position: 'fixed',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'rgba(0,0,0,0.7)',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    zIndex: 1000
                }}>
                    <div style={{
                        backgroundColor: '#fff',
                        padding: '20px',
                        borderRadius: '10px',
                        position: 'relative',
                        // width: '80%',  // remove this line
                        maxWidth: '90%',  // To ensure it doesn't overflow the viewport
                        maxHeight: '95%',
                        overflowY: 'auto'
                    }}>
                        <button 
                            onClick={onClose} 
                            style={{
                                position: 'absolute',
                                top: 10,
                                right: 10,
                                background: 'red',
                                color: 'white',
                                border: 'none',
                                borderRadius: '50%',
                                padding: '5px 10px',
                                cursor: 'pointer',
                                zIndex: 2  // ensure it's above other content
                            }}
                        >
                            X
                        </button>
                        {children}
                    </div>
                </div>
            )}
        </>
    );
}

export default Popup;
