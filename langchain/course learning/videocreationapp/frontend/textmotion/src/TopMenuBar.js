import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { googleLogout } from '@react-oauth/google'; 

const menuBarStyle = {
  display: 'flex',
  justifyContent: 'flex-end', 
  alignItems: 'center',
  backgroundColor: '#333',     
  color: '#FFF',               
  padding: '20px 10px', // Increased vertical padding from '10px' to '20px'
};

const buttonStyle = {
  margin: '0 10px',
  backgroundColor: '#444',
  padding: '5px 15px',
  color: '#FFF',
  textDecoration: 'none',
  border: 'none',
  borderRadius: '5px', // Added for a rounded button effect
};

const buttonStylered = {
  margin: '0 10px',
  backgroundColor: 'red',
  padding: '5px 15px',
  color: '#FFF',
  textDecoration: 'none',
  border: 'none',
  borderRadius: '5px', // Added for a rounded button effect
};

const dropdownContentStyle = {
  display: 'none',
  position: 'absolute',
  backgroundColor: '#444',
  minWidth: '160px',
  boxShadow: '0px 8px 16px 0px rgba(0,0,0,0.7)',  
  zIndex: 1, 
  flexDirection: 'column',
  borderRadius: '5px', // Rounded corners for dropdown
  marginTop: '10px',   // Added to separate dropdown from button
};

const dropdownStyle = {
  position: 'relative',
  display: 'inline-block',
  marginRight: '10px', // Margin to separate from the edge
};

const dropdownButtonStyle = {
  ...buttonStyle,
};

export const TopMenuBar = () => {
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);

  return (
    <div style={menuBarStyle}>
      <div style={dropdownStyle}>
        <button style={buttonStylered} onClick={() => setIsDropdownVisible(!isDropdownVisible)}>
          Profile
        </button>
        {isDropdownVisible && (
          <div style={{ ...dropdownContentStyle, display: 'flex' }}>
            <Link to="/profile" style={buttonStyle}>My Profile</Link>
            <Link to="/bill" style={buttonStyle}>Bills</Link>
            <Link to="/settings" style={buttonStyle}>Settings</Link>
            <Link style={buttonStyle} onClick={() => {
              googleLogout();
              localStorage.setItem("token", "");
              window.location.href = "/login";
            }}>
              Logout
            </Link>
          </div>
        )}
        <Link to="/" style={buttonStylered}>Creations</Link>
        <Link to="/editor" style={buttonStylered}>Editor</Link>
      </div>
    </div>
  );
};
