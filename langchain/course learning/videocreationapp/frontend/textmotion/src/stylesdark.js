// styles.js

// Dark Theme Styles
export const containerStyle = {
    display: 'flex',
    width: '100%',
    height: '100vh',
    backgroundColor: '#333'
};

export const leftSectionStyle = {
    flex: 3,
    padding: '20px',
    borderRight: '1px solid #666',
    backgroundColor: '#444',
};

export const rightSectionStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#444',
};

export const textAreaStyle = {
    width: '100%',
    minHeight: '50px',
    padding: '12px 20px',
    margin: '8px 0',
    boxSizing: 'border-box',
    border: '2px solid #666',
    borderRadius: '4px',
    fontFamily: 'Arial, sans-serif',
    fontSize: '16px',
    transition: 'border-color 0.3s ease',
    outline: 'none',
    resize: 'vertical',
    backgroundColor: '#555',
    color: 'white'
};

export const frameContainerStyle = {
    width: '100%',
    margin: '0 auto',
    marginTop: '20px',
    border: '2px solid #666',
    borderRadius: '10px',
    boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.3)',
    backgroundColor: '#444',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    gap: '10px'
};

export const topSectionStyle = {
    height: '50px',
    backgroundColor:'#555',
    display: 'flex',
    alignItems: 'center',
    padding:'5px',
    justifyContent: 'left',
    color:'white',
    borderRadius: '8px 8px 0 0'
};

export const framecontentstyle = {
    padding:'40px',
    backgroundColor: '#444',
};

export const focusedTextAreaStyle = {
    ...textAreaStyle,
    borderColor: '#007bff'
};

export const buttonStyle = {
    backgroundColor: '#007bff',
    color: 'white',
    padding: '6px 10px',
    border: 'none',
    borderRadius: '4px',
    cursor: 'grabbing',
    fontSize: '14px',
    margin: '15px 5px'
};

export const imgWrapperStyle = {
    position: 'relative',
    width: '400px',
    height: '600px',
    backgroundColor: '#555'
};

export const imgStyle = {
    width: '100%',
    height: '100%',
    objectFit: 'cover'
};

export const subtitleStyle = {
    position: 'absolute',
    bottom: '40%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    color: 'white',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    padding: '5px',
    borderRadius: '5px',
    fontSize:'25px'
};

export const controlsStyle = {
    marginTop: '2px',
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    backgroundColor: '#555'
};

export const imageGallery = {
    textAlign: 'center',
    backgroundColor: '#444'
};

export const imageGrid = {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    backgroundColor: '#444'
};

export const image = {
    width: '150px',
    height: '150px',
    margin: '5px',
    cursor: 'pointer',
    transition: 'transform 0.2s ease',
};

export const expandedImageWrapper = {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1000
};

export const expandedImage= {
    maxWidth: '80%',
    maxHeight: '80%'
};

export const closeButton= {
    position: 'absolute',
    top: '20px',
    right: '20px',
    backgroundColor: '#666',
    border: 'none',
    padding: '10px',
    cursor: 'pointer'
};
export const tableStyle = {
    width: '98%',
    borderCollapse: 'collapse',
    fontFamily: 'Arial, sans-serif',
    boxShadow: '0px 14px 16px rgba(0, 0, 0, 0.4)', // Slightly darker shadow
    margin: '20px',
    padding: '45px',
    backgroundColor: '#333', // Darker background
};

export const tableHeader = {
    background: '#444', // Darker background
    padding: '10px',
    textAlign: 'left',
    color: 'white',
    border: 'none'
    // borderBottom: '1px solid #555' // Darker border
};

export const tableRow = {
    borderBottom: '1px solid #555', // Darker border
    color: 'white'
};

export const tableRowAlt = {
    borderBottom: '1px solid #555', // Darker border
    backgroundColor: '#444', // Darker alternate background
    color: 'white'
};

export const tableCell = {
    padding: '10px',
    textAlign: 'left',
    color: 'white',
    border: 'none'  // No border for individual table cells
};
export const actionCell = {
    display: 'flex',
    flexDirection: 'row',  // ensures horizontal alignment
    justifyContent: 'space-around',
    alignItems: 'center',
    border:'0px',
};

export const actionButton = {
    padding: '0px',
    margin: '5px',
    cursor: 'pointer',
    backgroundColor: '#007bff',
    boxShadow: '0 4px 6px rgba(0,0,0,0.3)',
    color: 'white' ,
    height: '40px',
    width: '40px'
};

export const deleteButton = {
    padding: '10px',
    margin: '5px',
    cursor: 'pointer',
    backgroundColor: '#ff4757',
    color: 'white',
    height: '40px',
    width: '40px'
};

export const downloadButton = {
    padding: '10px',
    margin: '5px',
    cursor: 'pointer',
    backgroundColor: '#2ed573',
    color: 'white',
    height: '40px',
    width: '40px'
};

export const radioStyle = {
    display: 'inline-block',
    margin: '0 10px',
    padding: '5px',
    color: 'white'
};

export const radioLabelStyle = {
    fontWeight: '500',
    fontSize: '14px',
    color: 'white'
};

export const radioGroupStyle = {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
    backgroundColor: '#444'
};
