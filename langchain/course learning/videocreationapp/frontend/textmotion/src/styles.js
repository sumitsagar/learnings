const darkBackground = '#212121';
const lightText = '#FFFFFF';
const lightBorder = '#444444';
const lightShadow = 'rgba(255, 255, 255, 0.1)';

export const containerStyle = {
    display: 'flex',
    width: '100%',
    height: '100vh',
    color:'#c7c7c7',
    background:'black',
    backgroundColor: darkBackground
};

export const leftSectionStyle = {
    flex: 3,
    padding: '20px',
    // borderRight: `1px solid ${lightBorder}`
};

export const rightSectionStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
};

export const textAreaStyle = {
    width: '100%',
    minHeight: '50px',
    padding: '12px 20px',
    margin: '8px 0',
    boxSizing: 'border-box',
    border: `2px solid ${lightBorder}`,
    borderRadius: '4px',
    fontFamily: 'Arial, sans-serif',
    fontSize: '18px',
    transition: 'border-color 0.3s ease',
    outline: 'none',
    resize: 'vertical',
    backgroundColor: '#333131',
    color: lightText
};

export const frameContainerStyle = {
    width: '100%',
    margin: '0 auto',
    marginTop: '20px',
    border: `2px solid ${lightBorder}`,
    borderRadius: '10px',
    boxShadow: `0px 4px 8px ${lightShadow}`,
    backgroundColor: darkBackground,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    gap: '10px'
};

export const topSectionStyle = {
    height: '50px',
    backgroundColor: '#333',
    display: 'flex',
    alignItems: 'center',
    padding: '5px',
    justifyContent: 'left',
    color: 'white',
    borderRadius: '8px 8px 0 0'
};

export const framecontentstyle = {
    padding: '40px',
};

export const focusedTextAreaStyle = {
    ...textAreaStyle,
    borderColor: '#007bff'
};

export const buttonStyle = {
    backgroundColor: 'red',
    color: 'white',
    padding: '6px 10px',
    border: 'none',
    borderRadius: '4px',
    cursor: 'grabbing',
    fontSize: '14px',
    margin: '15px 5px',
    minWidth:'200px',
    boxShadow: `0 4px 6px ${lightShadow}`
};

export const imgWrapperStyle = {
    position: 'relative',
    width: '400px',
    height: '600px'
};

export const imgStyle = {
    width: '100%',
    height: '100%',
    objectFit: 'cover'
};

export const subtitleStyle = {
    position: 'absolute',
    bottom: '40%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    color: 'white',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    padding: '5px',
    borderRadius: '5px',
    fontSize: '25px',
};

export const controlsStyle = {
    marginTop: '2px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
};

export const imageGallery = {
    textAlign: 'center',
};

export const imageGrid = {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
};

export const image = {
    width: '150px',
    height: '150px',
    margin: '5px',
    cursor: 'pointer',
    transition: 'transform 0.2s ease',
};

export const expandedImageWrapper = {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1000,
};

export const expandedImage = {
    maxWidth: '80%',
    maxHeight: '80%',
};

export const closeButton = {
    position: 'absolute',
    top: '20px',
    right: '20px',
    backgroundColor: '#fff',
    border: 'none',
    padding: '10px',
    cursor: 'pointer',
};

export const tableStyle = {
    width: '98%',
    borderCollapse: 'collapse',
    fontFamily: 'Arial, sans-serif',
    boxShadow: `0px 14px 16px ${lightShadow}`,
    margin: '20px',
    padding: '45px',
    backgroundColor: darkBackground
};

export const tableHeader = {
    borderBottom: '2px solid #ddd',
    background: '#333',
    padding: '10px',
    textAlign: 'left',
};

export const tableRow = {
    borderBottom: '1px solid #ddd',
};

export const tableRowAlt = {
    ...tableRow,
    backgroundColor: '#282828'
};

export const tableCell = {
    padding: '10px',
    textAlign: 'left',
    color: lightText
};

export const actionCell = {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
};

export const actionButton = {
    padding: '10px',
    margin: '5px',
    cursor: 'pointer',
    backgroundColor: 'blue',
    color: lightText,
    boxShadow: `0 4px 6px ${lightShadow}`
};

export const deleteButton = {
    ...actionButton,
    backgroundColor: '#ff4747'
};

export const downloadButton = {
    ...actionButton,
    backgroundColor: '#00a600'
};

export const radioStyle = {
    display: 'inline-block',
    margin: '0 10px',
    padding: '5px',
    color: lightText,
};

export const enlargedRadio = {
    transform: 'scale(1.5)', // Increase as needed
    marginRight: '5px',
};

export const radioLabelStyle = {
    fontWeight: '500',
    fontSize: '14px',
    color: '#adadad'
};

export const radioGroupStyle = {
    display: 'flex',
    justifyContent: 'left',
    alignItems: 'center',
};