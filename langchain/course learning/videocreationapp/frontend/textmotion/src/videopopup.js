import React, { useState } from 'react';

const VideoPopup = ({ videoUrl, onClose }) => { 
  const videoPopupStyle = {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    zIndex: 1000,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  };

  const videoStyle = {
    maxWidth: '80%',
    maxHeight: '80%'
  };

  const closeButtonStyle = {
    position: 'absolute',
    top: '15px',
    right: '20px',
    backgroundColor: '#fff',
    color: '#000',  // Add this line for the text color
    border: '1px solid #ccc', // Add some border so it's not just a white square
    borderRadius: '50%',  // Make the button round
    padding: '10px',
    cursor: 'pointer',
    height:'40px',
    width:'40px'
  };
  return (
    <div style={videoPopupStyle}>
      <video controls style={videoStyle} >
        <source src={videoUrl} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
      <button style={closeButtonStyle} onClick={onClose}>X</button> 
    </div>
  );
};

export default VideoPopup;
