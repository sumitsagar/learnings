import React, { useState, useEffect } from 'react';

const SubtitleEditor = ({ subtitle,requestid,progress }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [jsonArray, setJsonArray] = useState(subtitle || []);
  const [originalArray, setOriginalArray] = useState(subtitle || []);

  const handleEdit = () => {
    setIsEditing(true);
    setOriginalArray([...jsonArray]); // Save the original state
  };

  const handleSave = () => {
    setIsEditing(false);
    saveSubtitle();
  };

  const handleCancel = () => {
    setIsEditing(false);
    setJsonArray([...originalArray]); // Restore to the original state
  };

  const handleChange = (index, e) => {
    const newArr = [...jsonArray];
    newArr[index].text = e.target.value;
    setJsonArray(newArr);
  };


  const saveSubtitle = async () => { 
    // setIsLoading(true);
    const requestBody = JSON.stringify({
      sub: JSON.stringify(jsonArray), 
      imageprompts: null,
      requestid: requestid
    });
  
    try {
    //   setIsLoading(true);
    //   debugger;
      const response = await fetch("http://localhost:5000/updatetranscriptionsandprompts", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'image/png',
          'Authorization': 'Bearer 8ef92cce43aa8972419f8b62944f774df203bdbdc25cf5647d385e7b568429a84e596cbb64fbeaf4300e2f3de343c597'
        },
        body: requestBody
      }); 
    //   setIsLoading(false);
      if (response.ok) { 
        //   setSubAndPromptedited(false);
      } else {
        console.log("Failed to save");
        alert("error saving script");
      }
    } catch (error) {
    //   setIsLoading(false);
      console.error("An error occurred while saving the script:", error);
    }
  };


  useEffect(() => {
    // Optional: If you want to do something with the modified array, like sending it to a server.
    // console.log(JSON.stringify(jsonArray));
  }, [jsonArray]);

  return (
    <div>
      <h6>Subtitle</h6>
      {isEditing && jsonArray !== null && jsonArray !== undefined ?  (
        jsonArray.map((obj, index) => (
          <textarea
            key={index}
            value={obj.text}
            style={{background:'#333',color:'white',padding:'1px',margin:'1px'}}
            onChange={(e) => handleChange(index, e)}
          />
        ))
      ) : (
        <div>
          {jsonArray.map((obj, index) => (
            <span style={{fontSize:'20px'}} key={index}>
              {obj.text}
              {' '}
            </span>
          ))}
        </div>
      )}
          {progress === 2 &&
              <button onClick={isEditing ? handleSave : handleEdit}>
                  {isEditing ? 'Save' : 'Edit'}
              </button>
          }
      {isEditing && (
        <button onClick={handleCancel}>
          Cancel
        </button>
      )}
    </div>
  );
};

export default SubtitleEditor;
