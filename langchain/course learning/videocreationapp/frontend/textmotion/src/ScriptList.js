import React, { useState, useEffect } from 'react';
import axios from 'axios';
// import './App.css';
// import './ScriptListDarkStyle.css'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import Editor from './Editor';
import 'font-awesome/css/font-awesome.min.css';
// import * as styles from './styles'; 
import * as styles from './stylesdark'; 
import { TopMenuBar } from './TopMenuBar';
import Loader from './loader';
import VideoPopup from './videopopup'; 




const truncate = (str) => (str.length > 150 ? str.substring(0, 100) + '...' : str);

const ImageModal = ({ images, show, close, index, setIndex }) => {
  if (!show) return null;

  const nextImage = () => setIndex((prevIndex) => (prevIndex + 1) % images.length);
  const prevImage = () => setIndex((prevIndex) => (prevIndex - 1 + images.length) % images.length);

  return (
    <div className="modal">
      <div className="modal-content">
        <img src={images[index]} alt="big-view" style={{ maxWidth: '100%', maxHeight: '70vh', objectFit: 'contain' }} />
        <div>
          <button onClick={prevImage}>Previous</button>
          <button onClick={nextImage}>Next</button>
          <button onClick={close}>Close</button>
          <span>{`${index + 1}/${images.length}`}</span> {/* Image numbering */}
        </div>
      </div>
    </div>
  );
};


const App = () => { 
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);
  const [selectedRow, setSelectedRow] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [showImageModal, setShowImageModal] = useState(false);
  const [currentImages, setCurrentImages] = useState([]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isVideoPopupVisible, setVideoPopupVisible] = useState(false);
  const [currentVideoUrl, setCurrentVideoUrl] = useState('');

  const handleShowVideo = (url) => { 
    setVideoPopupVisible(true);
    setCurrentVideoUrl(url); 
  };

  const handleCloseVideo = () => { 
    setVideoPopupVisible(false);
  };


  useEffect(() => {
    const token = localStorage.getItem('token');
    
    setIsLoading(true);
    axios.get('http://127.0.0.1:5000/getgenerations', {
      headers: {
        'token': token
      }
    })
    .then((response) => {
      if (response.data.status === 'success') {
        setData(response.data.Data);
        setIsLoading(false);
      }
    })
    .catch((error) => {
      console.error('There was an error!', error);
      setIsLoading(false);
    });
}, []);


  const handleImageClick = (event,images, index) => {
    event.stopPropagation();  // This stops the event from propagating to parent elements
    setCurrentImages(images);
    setCurrentIndex(index);
    setShowImageModal(true);
    
  };

  const handleRowClick = (row) => {
    setSelectedRow(row);
    setShowModal(true);
  };

  return (
    // <Router>
    <div style={{background:'#282829'}}>
      <div>
        {isLoading ? <Loader /> : <div></div>}
      </div>
      <div>
        <TopMenuBar />
        {/* Your Routes */}
      </div>
      {/* <h1>Data Table</h1> */}
      <table style={styles.tableStyle}>
        <thead>
          <tr style={styles.tableRow}>
            <th style={{...styles.tableHeader,width:'25%'}}>Input Text</th>
            {/* <th style={styles.tableHeader}>inputtype</th> */}
            {/* <th style={{...styles.tableHeader,width:'25%'}}>Script</th> */}
            <th style={styles.tableHeader}>Title</th>
            <th style={styles.tableHeader}>Description</th>
            <th style={styles.tableHeader}>Images</th>
            <th style={styles.tableHeader}>Actions</th>
          </tr>
        </thead>
        <tbody>
          {data.map((row, index) => (
            <tr key={index} style={index % 2 === 0 ? styles.tableRow : styles.tableRowAlt}>
              <td style={{ ...styles.tableCell, width: '25%' }} onClick={() => handleRowClick(row)}>
                {
                  <span>
                    <strong>{row.inputtype ? `[${row.inputtype}]` + ' ' : ''}</strong>
                    {truncate(String(row.inputtext))}
                  </span>
                }
              </td>
              {/* <td style={styles.tableCell} onClick={() => handleRowClick(row)}>{truncate(String(row.inputtype))}</td> */}
              {/* <td style={{...styles.tableCell,width:'25%'}} onClick={() => handleRowClick(row)}>{truncate(String(row.script))}</td> */}
              <td style={styles.tableCell} onClick={() => handleRowClick(row)}>{truncate(String(row.Title))}</td>
              <td style={styles.tableCell} onClick={() => handleRowClick(row)}>{truncate(String(row.Description))}</td>
              <td style={{...styles.tableCell,width:'15%'}} onClick={() => handleRowClick(row)}>
                {typeof row.images === 'string' && row.images.startsWith('[') ? (
                  <div>
                    {JSON.parse(row.images).map((url, imgIndex) => (
                      <img
                        key={imgIndex}
                        src={url}
                        alt="small-view"
                        style={{cursor:'grab' ,width: '50px', height: '50px', objectFit: 'contain', margin: '4px',borderRadius:'10px', border:'1px solid #9da19e' }}
                        onClick={(event) => handleImageClick(event, JSON.parse(row.images), imgIndex)}
                        />
                    ))}
                  </div>
                ) : (
                  truncate(String(row.images))
                )}
              </td>
              <td style={styles.actionCell}>
                {row.currentstatus !== "generating_video" &&
                  <div>
                    {
                      (() => {
                        // console.log('Debug:', row.video_url);  // Debug line
                        if (row.video_url !== null && row.video_url !== undefined && row.video_url !== "") {
                          return (
                            <div style={styles.actionCell}>

                              <button style={styles.deleteButton}>
                                <i className="fa fa-trash"></i>
                              </button>
                              <button style={styles.downloadButton}>
                                <Link style={{ color: 'white' }} to={`${row.video_url}`}>
                                  <i className="fa fa-download"></i>
                                </Link>
                              </button>
                              <button style={styles.downloadButton}>
                                <Link style={{ color: 'white' }} onClick={() => { handleShowVideo(row.video_url); }}>
                                  <i className="fa fa-play"></i>
                                </Link>
                                {isVideoPopupVisible &&
                                  <VideoPopup videoUrl={currentVideoUrl} onClose={handleCloseVideo} />
                                }
                              </button>
                            </div>
                          );
                        } else {
                          return (
                            <div style={styles.actionCell}>

                              <button style={styles.deleteButton}>
                                <i className="fa fa-trash"></i>
                              </button>
                              <button style={styles.actionButton}>
                                <Link style={{ color: 'white' }} to={`/editor?rid=${row.requestid}`}>
                                  <i className="fa fa-pencil"></i>
                                </Link>
                              </button>
                              <button style={{ ...styles.actionButton, visibility: 'hidden' }}>
                                <Link style={{ color: 'white' }} to={`#`}>
                                  <i className="fa fa-pencil"></i>
                                </Link>
                              </button>
                            </div>
                          );
                        }
                      })()
                    }
                  </div>
                }
                {row.currentstatus === "generating_video" &&
                  <div>
                    <img height={50} src="https://icaengineeringacademy.com/wp-content/uploads/2019/01/ajax-loading-gif-transparent-background-2.gif" alt="Loading..." />
                    <h6>Generating video...</h6>
                  </div>
                }
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {showModal && (
        <div className="modal">
          <div className="modal-content">
            <h2>Detail View</h2><br></br>
            {Object.entries(selectedRow).map(([key, value], index) => {
              // Check if the key is one of the desired columns
              if (['Description', 'Title', 'script', 'video_url'].includes(key)) {
                return (
                  <p key={index}>
                    <h4>{key}</h4>
                     {value}
                    <br></br>
                  </p>
                );
              }
              // If the key is not one of the desired columns, don't render anything for it
              return null;
            })}
            <button className="modal-close-button" onClick={() => setShowModal(false)}>Close</button>
          </div>
        </div>

      )}

      <ImageModal
        images={currentImages}
        show={showImageModal}
        close={() => setShowImageModal(false)}
        index={currentIndex}
        setIndex={setCurrentIndex}
      />
    </div>
  // </Router>
  );
};

export default App;
