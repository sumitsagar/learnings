import React, { useRef,useState,useEffect } from 'react';
import * as styles from './styles';  


const ImageSubtitleAudio = ({ Audiourl,imageurlarray,imageDurations,subtitle }) => {

  // console.log(imageurlarray);
  const [buttonText, setButtonText] = useState("Play");
  const [playing, setPlaying] = useState(false);
  const [elapsedTime, setElapsedTime] = useState(0);
  const audioRef = useRef(null);
  const [currentSubtitle, setCurrentSubtitle] = useState("");
  const [currentImage, setcurrentImage] = useState("");
  const [casousaldata, setcasousaldata] = useState({});


  function constructJsonForCarousal(imgarr,imageDurations,subtitlestr) {
    const output = [];

    // debugger; 
    var sub = subtitlestr; 
    // var prompts = JSON.parse(imageprompts).prompts;
    for (let i = 0; i < imageDurations.length; i++) {
      const duration = imageDurations[i];
      const url = imgarr[i];
      // const prompt = prompts[i];
  
      const obj = {
        url,
        duration,
        // prompt
      };
  
      output.push(obj);
    }
  
    var _casousaldata = {};
    _casousaldata.images = output;
    _casousaldata.subtitle = sub;
    // console.log(_casousaldata);
    setcasousaldata(_casousaldata); 
  }


  useEffect(() => {
    
    constructJsonForCarousal(imageurlarray,imageDurations,subtitle);
    
  }, [imageurlarray,imageDurations,subtitle]);


  useEffect(() => {
    // alert(JSON.stringify(casousaldata));
    let timer;
    // setcasousaldata(sampleData);
    if (playing) {
      timer = setInterval(() => {
        setElapsedTime((prevTime) => prevTime + 1);
      }, 1000);
    }

    return () => {
      clearInterval(timer);
    };
  }, [playing]);


  useEffect(() => { 
    // debugger; 
    if(casousaldata.subtitle !== undefined){  
      const applicableSubtitle = casousaldata.subtitle.find((sub) => 
        elapsedTime >= sub.duration[0] && elapsedTime < sub.duration[1]
      );
      setCurrentSubtitle(applicableSubtitle ? applicableSubtitle.text : "");
    }

    if(casousaldata.images !== undefined){  
      var _currentImage = casousaldata.images.find((img) => 
      elapsedTime >= img.duration[0] && elapsedTime < img.duration[1]
      ) || casousaldata.images[0];
        setcurrentImage(_currentImage);
    } 
  }, [elapsedTime, playing,casousaldata]);

  const playAudio = () => {
    audioRef.current.play();
    setButtonText("Pause");
  };
  
  const pauseAudio = () => {
    audioRef.current.pause();
    audioRef.current.currentTime = 0;  // This line will make sure the audio starts from the beginning when played again
    setButtonText("Play");
  };
  const handleButtonClick = () => {
    if (buttonText === "Play") {
      playAudio();
    } else {
      pauseAudio();
    }
  };

  return (
      <div>
          {currentImage !== "" &&
              <div>
                  <div style={styles.imgWrapperStyle}>
                      <img style={styles.imgStyle} src={currentImage.url} alt="Display" />
                      <div style={styles.subtitleStyle}>{currentSubtitle}</div>
                  </div>
                  <div style={styles.controlsStyle}>
                      <button style={{ ...styles.buttonStyle, background: 'green' }} onClick={() => { setPlaying(true); setElapsedTime(0); handleButtonClick(); }}>
                          <i className="fa fa-play"></i>
                      </button>
                      <button style={{ ...styles.buttonStyle, background: 'red' }} onClick={() => { setPlaying(false); handleButtonClick(); }}>
                          <i className="fa fa-stop"></i>
                      </button>
                      <div>
                          <audio ref={audioRef} src={Audiourl}></audio>
                          {/* <button onClick={handleButtonClick}>{buttonText}</button> */}
                      </div>
                  </div>
              </div>
          }
      </div>
  );
};

export default ImageSubtitleAudio;
