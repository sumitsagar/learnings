import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import Editor from './Editor';
import 'font-awesome/css/font-awesome.min.css';
import * as styles from './styles'; 
import { TopMenuBar } from './TopMenuBar';
import Loader from './loader';
import VideoPopup from './videopopup';




const truncate = (str) => (str.length > 150 ? str.substring(0, 250) + '...' : str);

const ImageModal = ({ images, show, close, index, setIndex }) => {
  if (!show) return null;

  const nextImage = () => setIndex((prevIndex) => (prevIndex + 1) % images.length);
  const prevImage = () => setIndex((prevIndex) => (prevIndex - 1 + images.length) % images.length);

  return (
    <div className="modal">
      <div className="modal-content">
        <img src={images[index]} alt="big-view" style={{ maxWidth: '100%', maxHeight: '80vh', objectFit: 'contain' }} />
        <div>
          <button onClick={prevImage}>Previous</button>
          <button onClick={nextImage}>Next</button>
          <button onClick={close}>Close</button>
          <span>{`${index + 1}/${images.length}`}</span> {/* Image numbering */}
        </div>
      </div>
    </div>
  );
};


const App = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);
  const [selectedRow, setSelectedRow] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [showImageModal, setShowImageModal] = useState(false);
  const [currentImages, setCurrentImages] = useState([]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isVideoPopupVisible, setVideoPopupVisible] = useState(false);
  const [currentVideoUrl, setCurrentVideoUrl] = useState('');

  const handleShowVideo = (url) => { 
    setVideoPopupVisible(true);
    setCurrentVideoUrl(url); 
  };

  const handleCloseVideo = () => { 
    setVideoPopupVisible(false);
  };


  useEffect(() => {
    setIsLoading(true);
    axios.get('http://127.0.0.1:5000/getgenerations')
      .then((response) => {
        if (response.data.status === 'success') {
          setData(response.data.Data);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.error('There was an error!', error);
        setIsLoading(false);
      });
  }, []);

  const handleImageClick = (images, index) => {
    setCurrentImages(images);
    setCurrentIndex(index);
    setShowImageModal(true);
  };

  const handleRowClick = (row) => {
    setSelectedRow(row);
    setShowModal(true);
  };

  return (
    // <Router>
    <div>
      <div>
        {isLoading ? <Loader /> : <div></div>}
      </div>
      <div>
        <TopMenuBar />
        {/* Your Routes */}
      </div>
      {/* <h1>Data Table</h1> */}
      <table  style={styles.tableStyle}>
        <thead>
        <tr  style={styles.tableRow}>
          {data.length > 0 && Object.keys(data[0]).map((key, index) => (
            key !== 'video_url' ? <th  style={styles.tableHeader} key={index}>{key}</th> : null
          ))}
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {data.map((row, index) => (
          <tr key={index} style={index % 2 === 0 ? styles.tableRow : styles.tableRowAlt}>

            {Object.entries(row).map(([key, value], i) => (
              key !== 'video_url' ? (
                <td  style={styles.tableCell} key={i} onClick={() => handleRowClick(row)}>
                  {typeof value === 'string' && value.startsWith('[') ? (
                    <div>
                        {JSON.parse(value).map((url, imgIndex) => (
                        <img
                          key={imgIndex}
                          src={url}
                          alt="small-view"
                          style={{ width: '50px', height: '50px', objectFit: 'contain', margin: '2px' }}
                          onClick={() => handleImageClick(JSON.parse(value), imgIndex)}
                        />
                      ))}
                    </div>
                  ) : (
                    truncate(String(value))
                  )}
                </td>
              ) : null
            ))}
            <td style={styles.actionCell}>

             <button style={styles.deleteButton}>
                <i className="fa fa-trash"></i> 
              </button>
              {
                (() => {
                  console.log('Debug:', row.video_url);  // Debug line
                  if (row.video_url !== null && row.video_url !== undefined && row.video_url !== "") {
                    return (
                      <div style={styles.actionCell}>
                        <button style={styles.downloadButton}>
                        <Link style={{ color: 'white' }} to={`${row.video_url}`}>
                          <i className="fa fa-download"></i> 
                        </Link>
                      </button>
                      <button style={styles.downloadButton}>
                        <Link style={{ color: 'white' }}  onClick={()=>{handleShowVideo(row.video_url);}}>
                          <i className="fa fa-play"></i> 
                        </Link>
                          {isVideoPopupVisible &&
                            <VideoPopup videoUrl={currentVideoUrl} onClose={handleCloseVideo} />
                          }
                      </button>
                      </div>
                      
                    );
                  } else {
                    console.log('Going to else');  // Debug line
                    return (
                      <div>
                        <button style={styles.actionButton}>
                        <Link style={{ color: 'white' }} to={`/editor?rid=${row.requestid}`}>
                          <i className="fa fa-pencil"></i> 
                        </Link>
                      </button>
                      <button style={{...styles.actionButton, visibility: 'hidden'}}>
                        <Link style={{ color: 'white' }} to={`#`}>
                          <i className="fa fa-pencil"></i> 
                        </Link>
                      </button>
                      </div>
                    );
                  }
                })()
              }
            </td>
            </tr>
          ))}
        </tbody>
      </table>

      {showModal && (
        <div className="modal">
          <div className="modal-content">
            <h2>Detail View</h2>
            {Object.entries(selectedRow).map(([key, value], index) => (
              <p key={index}>
                <strong>{key}:</strong> {value}
              </p>
            ))}
            <button className="modal-close-button" onClick={() => setShowModal(false)}>Close</button>
          </div>
        </div>
      )}

      <ImageModal
        images={currentImages}
        show={showImageModal}
        close={() => setShowImageModal(false)}
        index={currentIndex}
        setIndex={setCurrentIndex}
      />
    </div>
  // </Router>
  );
};

export default App;
