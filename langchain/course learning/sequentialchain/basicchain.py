from langchain import PromptTemplate
from langchain import OpenAI
from langchain.chains import LLMChain,SequentialChain
from agents.agents import lookup
import os
from chains.custom_chains import get_ice_breaker_chain ,get_interests_chain,get_summary_chain
from output_parser import summary_parser, ice_breaker_parser, topics_of_interest_parser

os.environ["OPENAI_API_KEY"] = "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"

 
# linkedinprofileurl = lookup(name="sumit")
linkedindata = "{\r\n  \"name\": \"Sumit\",\r\n  \"skills\": [\r\n    \"Web Development\",\r\n    \"Machine Learning\",\r\n    \"Data Analysis\",\r\n    \"Cloud Computing\",\r\n    \"Mobile App Development\"\r\n  ],\r\n  \"address\": {\r\n    \"street\": \"123 Main St\",\r\n    \"city\": \"Bangalore\",\r\n    \"state\": \"Karnataka\",\r\n    \"postalCode\": \"560001\",\r\n    \"country\": \"India\"\r\n  },\r\n  \"companies\": [\r\n    {\r\n      \"name\": \"TechCorp Ltd.\",\r\n      \"position\": \"Software Engineer\",\r\n      \"duration\": \"2018-2020\"\r\n    },\r\n    {\r\n      \"name\": \"WebWorks Inc.\",\r\n      \"position\": \"Lead Developer\",\r\n      \"duration\": \"2021-Present\"\r\n    },\r\n    {\r\n      \"name\": \"DataDive Solutions\",\r\n      \"position\": \"Data Scientist\",\r\n      \"duration\": \"2020-2021\"\r\n    }\r\n  ],\r\n  \"languages\": [\"English\", \"Hindi\", \"maghi\"]\r\n}\r\n"

tweets = "[{\"tweetId\": \"001\", \"content\": \"Exploring the beauty of nature this weekend! #WeekendVibes #NatureLover\", \"likes\": 210, \"retweets\": 53}, {\"tweetId\": \"002\", \"content\": \"Just finished reading 'The Great Book'. Highly recommend! #BookRecommendation\", \"likes\": 130, \"retweets\": 40}, {\"tweetId\": \"003\", \"content\": \"Starting a new project today. Excited for the challenges ahead! #DeveloperLife\", \"likes\": 87, \"retweets\": 12}, {\"tweetId\": \"004\", \"content\": \"Attending a virtual conference on AI. Can't wait to learn and network. #AICon2023\", \"likes\": 152, \"retweets\": 58}, {\"tweetId\": \"005\", \"content\": \"Grateful for the little moments. #ThoughtOfTheDay\", \"likes\": 211, \"retweets\": 65}]"


# summary chain
summary_chain = get_summary_chain()
summary_and_facts = summary_chain.run(
        information=linkedindata, twitter_posts=tweets
    )
summary_and_facts = summary_parser.parse(summary_and_facts) 
print(summary_and_facts.summary) 

# we got the summary , now this will be passed to sequential chain as input


# intereset chain
interests_chain =   get_interests_chain()
# icebreaker chain 
ice_breaker_chain = get_ice_breaker_chain() 
 

sequentialchain  = SequentialChain(chains=[interests_chain,ice_breaker_chain],
                                   input_variables=["summary"],
                                   output_variables=["interest","icebreaker"],
                                   verbose=True)
response = sequentialchain(summary_and_facts.summary)
print(response)


