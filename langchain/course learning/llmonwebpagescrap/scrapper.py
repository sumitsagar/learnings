import os
import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin

BASE_URL = "https://python.langchain.com/docs/use_cases/"
SAVE_DIR = "scraped_pages"
os.environ["OPENAI_API_KEY"] = "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"
os.environ["PINCONE_API_KEY"] = "9f03f26f-bf0e-4895-9916-219695e87790"
os.environ["PINCONE_ENVOIRNMENT_REGION"] = "gcp-starter"

# Ensure the save directory exists
if not os.path.exists(SAVE_DIR):
    os.makedirs(SAVE_DIR)

def save_content(url, content):
    """Save the content to a file named after the URL."""
    filename = os.path.join(SAVE_DIR, url.replace("https://", "").replace("/", "_") + ".html")
    with open(filename, 'w', encoding='utf-8') as f:
        f.write(content)

def scrape_page(url):
    """Scrape the content of a single page."""
    response = requests.get(url)
    if response.status_code == 200:
        save_content(url, response.text)
        return response.text
    else:
        print(f"Failed to retrieve {url}")
        return ""

def get_all_links(html_content, base_url):
    """Extract all internal links from the page content."""
    soup = BeautifulSoup(html_content, 'html.parser')
    links = [urljoin(base_url, a['href']) for a in soup.find_all('a', href=True)]
    # Deduplicate links
    return list(set(links))

# Scrape the main page
main_page_content = scrape_page(BASE_URL)

# Extract all internal links from the main page
all_links = get_all_links(main_page_content, BASE_URL)

# Scrape all the links
for link in all_links:
    scrape_page(link)

print("Scraping completed!")
