import requests
from bs4 import BeautifulSoup
import csv
import os
from urllib.parse import urlparse, urljoin



# 1. URLs extraction

# Set the starting URL
base_url = 'https://python.langchain.com/docs/use_cases'

# Initialize the set of visited URLs and add the base URL
visited_urls = set()
visited_urls.add(base_url)

# Initialize the set of URLs to be visited and add the base URL
urls_to_visit = set()
urls_to_visit.add(base_url)

# Parse the base URL to get the domain name
base_domain = urlparse(base_url).netloc

# Initialize the CSV writer
csv_file = open('links.csv', 'w', newline='')
csv_writer = csv.writer(csv_file)

# Loop through the URLs to be visited
while urls_to_visit:
    current_url = urls_to_visit.pop()

    try:
        response = requests.get(current_url)

        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')

            for link in soup.find_all('a'):
                link_url = link.get('href')

                if link_url is not None and link_url != '':
                    link_url = urljoin(current_url, link_url)
                    link_domain = urlparse(link_url).netloc

                    if link_domain == base_domain:
                        if link_url not in visited_urls:
                            urls_to_visit.add(link_url)

                        visited_urls.add(link_url)
                        csv_writer.writerow([link_url])
        else:
            print('Error: ' + str(response.status_code))
    except requests.exceptions.RequestException as e:
        print('Error: ' + str(e))

csv_file.close()

# 2. Reading the URLs and extracting the data

if not os.path.exists('output'):
    os.makedirs('output')

counter = 0

with open('links.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        url = row[0]
        response = requests.get(url)

        if response is not None:
            soup = BeautifulSoup(response.content, 'html.parser')
            main_tag = soup.find('main', id='main-content', class_='bd-main')
            if main_tag is not None:
                main_text = main_tag.get_text(separator='\n', strip=True)
                filename = url.replace('https://', '').replace('http://', '').replace('/', '_').replace(':', '') + '.txt'

                with open('output/' + filename, 'w') as f:
                    print(counter)
                    counter += 1
                    f.write(main_text)
            else:
                print(f"No main tag found in {url}")
        else:
            print(f"Error fetching {url}")

print("Web scraping complete!")
