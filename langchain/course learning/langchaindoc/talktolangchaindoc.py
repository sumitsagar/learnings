import os

os.environ["OPENAI_API_KEY"] = "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"
from langchain.document_loaders import PyPDFLoader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.chains import RetrievalQA
from langchain.llms import OpenAI

if __name__ == "__main__":
    print("hi")
    pdf_path = "./langchain.pdf"

    loader = PyPDFLoader(file_path=pdf_path)
    documents = loader.load()
    text_splitter = CharacterTextSplitter(
        chunk_size=1000, chunk_overlap=30, separator="\n"
    )
    docs = text_splitter.split_documents(documents=documents)

    savepath = "/home/sumitsagar/langchain/course git/langchaindoc/vectoroutput/langchain"
    embeddings = OpenAIEmbeddings()
    vectorstore = FAISS.from_documents(docs, embeddings)
    vectorstore.save_local(savepath)

    new_vectorstore = FAISS.load_local(savepath, embeddings)
    qa = RetrievalQA.from_chain_type(
        llm=OpenAI(), chain_type="stuff", retriever=new_vectorstore.as_retriever()
    )
    res = qa.run("what is langchain?")
    print(res)