import torch
from PIL import Image
from diffusers import StableDiffusionImg2ImgPipeline
img=Image.open("5.jpg")
model = "runwayml/stable-diffusion-v1-5"
type = torch.float16
pipe = StableDiffusionImg2ImgPipeline.from_pretrained(model,torch_dtype=type)
pipe = pipe.to("cuda")
prompt = "scared kid hiding in a corner of a room, a man peeping in from the window next to the corner of room."
image = pipe(prompt,image=img).images[0]
image.save("img1.png")