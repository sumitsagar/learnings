# main.py

import asyncio
from fromhuggingface import generate_and_save_image

async def main():
    # Create a list of prompts
    prompts = ["white boat in a river", "sunset over mountains", "field of flowers"]
    
    # List to hold the task references
    tasks = []
    
    for idx, prompt in enumerate(prompts):
        folder = "my_folder"
        file_name = f"image_{idx}.png"
        task = asyncio.ensure_future(generate_and_save_image(folder, file_name, prompt))
        tasks.append(task)
        
    # Wait until all tasks are completed
    await asyncio.gather(*tasks)


asyncio.run(main())
