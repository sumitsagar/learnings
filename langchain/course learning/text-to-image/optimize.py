import re
from diffusers import DiffusionPipeline
import torch
from torch import inference_mode
import deepspeed
from time import perf_counter
import numpy as np

HF_MODEL_ID="CompVis/stable-diffusion-v1-4"
HF_TOKEN="" # your hf token: https://huggingface.co/settings/tokens

pipe = DiffusionPipeline.from_pretrained(HF_MODEL_ID, torch_dtype=torch.float16 ,use_auth_token=HF_TOKEN).to("cuda")

#Used to conduct timings
def measure_latency(pipe, prompt):
    latencies = []
    # warm up
    pipe.set_progress_bar_config(disable=True)
    for _ in range(2):
        _ =  pipe(prompt)
    # Timed run
    for _ in range(10):
        start_time = perf_counter()
        _ = pipe(prompt)
        latency = perf_counter() - start_time
        latencies.append(latency)
    # Compute run statistics
    time_avg_s = np.mean(latencies)
    time_std_s = np.std(latencies)
    time_p95_s = np.percentile(latencies,95)
    return f"P95 latency (seconds) - {time_p95_s:.2f}; Average latency (seconds) - {time_avg_s:.2f} +\- {time_std_s:.2f};", time_p95_s


prompt = "a photo of an astronaut riding a horse on mars"

with torch.inference_mode():
  deepspeed.init_inference(
      model=getattr(pipe,"model", pipe),      # Transformers models
      mp_size=1,        # Number of GPU
      dtype=torch.float16, # dtype of the weights (fp16)
      replace_method="auto", # Lets DS autmatically identify the layer to replace
      replace_with_kernel_inject=False, # replace the model with the kernel injector
  )
  ds_results = measure_latency(pipe,prompt)

  print(f"DeepSpeed model: {ds_results[0]}")   