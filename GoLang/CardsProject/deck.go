package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

type deck []string

func newDeck() deck {

	cards := deck{}

	joiner := " of "
	cardSuits := []string{"Spades", "Diamond", "Hearts", "Club"}
	cardValue := []string{"Ace", "Two", "Three", "Four"}

	for _, _cardValue := range cardValue {
		for _, _cardSuites := range cardSuits {
			cards = append(cards, _cardValue+joiner+_cardSuites)
		}
	}
	return cards
}

func newDeckWithStructList() []CardStruct {
	cards := []CardStruct{}
	cardItem1 := CardStruct{number: "one", name: "spades"}
	cardItem2 := CardStruct{number: "one", name: "heart"}
	cardItem3 := CardStruct{number: "one", name: "clubs"}
	cards = append(cards, cardItem1)
	cards = append(cards, cardItem2)
	cards = append(cards, cardItem3)
	return cards
}

func newDeckFromFile(fileName string) deck {

	cards := deck{}
	byteSlice, err := ioutil.ReadFile(fileName)
	if err == nil {
		csvString := string(byteSlice)
		cards = strings.Split(csvString, ",")
	} else {
		return cards
	}
	return cards
}

func (d deck) Print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}

func (d deck) saveToFile(fileName string) error {

	bytes := []byte(d.toString())
	return ioutil.WriteFile(fileName, bytes, 0666)
}
