package main

import "fmt"

type CardStruct struct {
	number string
	name   string
}

func (card CardStruct) Print() {
	fmt.Println(card.number + " of " + card.name)
}

func (card *CardStruct) UpdateStructValue() {
	(*card).name = "updated Name"
}
