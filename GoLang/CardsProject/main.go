package main

import "fmt"

var filePath string

func main() {

	filePath = "D:\\handcart.txt"

	cardsWithListOfStruct := newDeckWithStructList()
	for _, card := range cardsWithListOfStruct {
		//cardPointer := &card
		card.UpdateStructValue()
		card.Print()
	}

	return

	cardsFromFile := newDeckFromFile(filePath)
	cardsFromFile.Print()

	cards := newDeck()
	hand, remainingCards := deal(cards, 5)
	hand.Print()
	remainingCards.Print()
	err := cards.saveToFile("D:\\handcart.txt")

	if err != nil {
		fmt.Print("error is : ", err.Error())
	} else {
		fmt.Print("successfully saved document...")
	}
}
