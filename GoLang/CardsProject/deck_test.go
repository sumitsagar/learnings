package main

import "testing"

func TestNewDeck(t *testing.T) {
	deck := newDeck()
	if len(deck) != 16 {
		t.Errorf("invalid length of deck...expected 16 got : %v", len(deck))
	}
}
