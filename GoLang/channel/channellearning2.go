package main

import (
	"fmt"
	"time"
)

func main() {
	NonBufferedExample()
}

func NonBufferedExample() {
	message := make(chan string)

	//we have to spawn a go routing becase sending msg to channel is blocked until some one is listening
	go func() { // sending goroutine
		time.Sleep(time.Second * 1)
		fmt.Println("sending message to goroutin!")
		message <- "Hello from goroutine!"
		fmt.Println("sent to go routine!")
	}()

	fmt.Println("waitng for message .....")
	//if below code is not present then the line of code which sends message will be blocked for ever hence blocking the go routine
	msg := <-message // receiving in main goroutine
	fmt.Println(msg)
}

func BufferedChannel() {
	// Create a buffered channel
	bufferedChannel := make(chan int, 3)

	// Start 5 workers
	for i := 1; i <= 5; i++ {
		go worker(i, bufferedChannel)
		bufferedChannel <- i // Fill the channel slot to represent a worker in-progress
	}

	// Wait for all workers to finish
	// This is a simple way to wait; in real-world scenarios, you might use sync.WaitGroup or similar
	time.Sleep(7 * time.Second)
}
func worker(id int, ch chan int) {
	// Pretend we're doing some work
	fmt.Printf("Worker %d started\n", id)
	time.Sleep(time.Second)
	fmt.Printf("Worker %d finished\n", id)
	<-ch // Signal we're done
}
