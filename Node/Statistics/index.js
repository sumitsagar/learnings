const express = require('express');
const app = express();

//routes
const route_graph = require('./Routes/Graph');

app.use(express.json());
app.use('/api/graph',route_graph);

//app.use("view engine",'pug');
app.set('views','./views');

const port = 3000;
app.listen(port,()=> console.log(`listening on port ${port}`));