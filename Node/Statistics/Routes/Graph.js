//imports
const express = require("express");
const path = require('path');
const router = express.Router();

//bdl
const bdl_exception = require('../BDL/exceptionBDL')
const bdl_generic = require('../BDL/genericBDL') 

//model
const schema_Exec = require('./../Model/Schema/Exception');
 

router.get('/exceptions', async (req, res) => {
    var allExceptions = await bdl_exception.GetExceptions();
    var exceptionCountByName = await bdl_exception.GetExceptionCountByName();
    res.send(exceptionCountByName);    
});

router.get('/collections', async (req, res) => {
    var allCollectons = await generic.GetAllCollections()
    res.send(allCollectons);
});

module.exports = router;  