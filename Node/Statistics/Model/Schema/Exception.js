var joi = require('joi');

var exceptionSchema = joi.object().keys({ 
  Source: joi.string().required(),
});

function Validate(exception){
    var err = exceptionSchema.validate(exception).error;
    return err;
}

module.exports.Validate = Validate;