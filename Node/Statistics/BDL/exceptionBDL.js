const db = require('../Database/Arango/Connection/ArangoConnection');

 async function GetExceptions(){
    var collection = db.collection("Logs")
    var docs = await collection.all(); 
    var filtered = [];
    if (parseInt(docs._result.length) > 0) {
        docs._result.forEach(element => {
            //var validate = schema_Exec.Validate(element);
            filtered.push(element);        
        });  
    }
    else { 
    }
    return filtered;
}
async function GetExceptionCountByName(){
    //var retval = [];
    var result = await db.query(
    'FOR u IN Logs  COLLECT status = u.Source WITH COUNT INTO numUsers RETURN { status,numUsers }');
    var res = result.all();
    // res.forEach(element => {
    //     retval.push(element);
    // });
    return res;
}


module.exports.GetExceptions = GetExceptions;
module.exports.GetExceptionCountByName = GetExceptionCountByName; 