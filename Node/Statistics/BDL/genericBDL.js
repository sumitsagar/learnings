const db = require('../Database/Arango/Connection/ArangoConnection');

async function GetAllCollections() {

    var collections = await db.listCollections();
    return collections;
    // collections.forEach((coll, i) => {
    //     console.log(`${i + 1}. ${coll.name} (ID=${coll.id}, system=${coll.isSystem})`)
    // }); 
}

module.exports.GetAllCollections = GetAllCollections;