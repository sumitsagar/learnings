import requests
import hashlib

def get_enc_key():
    url = 'https://ant.aliceblueonline.com/rest/AliceBlueAPIService/api/customer/getAPIEncpkey'
    headers = {
        'Content-Type': 'application/json',
        'Cookie': '__cf_bm=mPADSrPPbcSqOISaFz0IfQ7h7l1_bOxkHgg8zA153D8-1704962571-1-AWop/qVAAlmK38gmw6W/tceVoARcRLrH/cifzDbuUk/5F+XxGJMMFpBglI3flnaL8MirxXxNFtAirD+qSTefq5k='
    }
    data = '{"userId": "520769"}'
    response = requests.post(url, headers=headers, data=data)
    return response.json()['encKey']

def get_user_sid(enc_hash):
    url = 'https://ant.aliceblueonline.com/rest/AliceBlueAPIService/api/customer/getUserSID'
    headers = {
        'Content-Type': 'application/json',
        'Cookie': '__cf_bm=mPADSrPPbcSqOISaFz0IfQ7h7l1_bOxkHgg8zA153D8-1704962571-1-AWop/qVAAlmK38gmw6W/tceVoARcRLrH/cifzDbuUk/5F+XxGJMMFpBglI3flnaL8MirxXxNFtAirD+qSTefq5k='
    }
    data = '{"userId":"520769", "userData":"' + enc_hash + '"}'
    response = requests.post(url, headers=headers, data=data)
    return response.json()['sessionID']

def GetSessionId():
    user_id = "520769"
    apikey = "7Ad0lYxAXpuytmmytCpbeHSxxIrzhvnUXCSdo0RgOcUj7ieIThPmF7TeILNHPMU03C8uGn0BAc9WaBiyhQyk1nnREClrlv92P7HNbQKZkmZRyH4ZRaf9RE8cbEze0H5J"
    
    # Step 1: Get the encKey
    enc_key = get_enc_key()

    # Step 2: Compute SHA256 hash
    concat_string = user_id  + apikey + enc_key
    enc_hash = hashlib.sha256(concat_string.encode()).hexdigest()

    # Step 3: Get the user SID
    session_id = get_user_sid(enc_hash)
    return session_id

 