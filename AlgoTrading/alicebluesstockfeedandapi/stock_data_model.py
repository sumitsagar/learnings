# Save this code in stock_data_model.py

class stockdata:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

class PreviousDayClosePrice:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

class StockPriceChange:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
