import mysql.connector
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, HourLocator, MinuteLocator
from matplotlib.patches import Rectangle
import matplotlib.dates as mdates

# Connect to the MySQL database
conn = mysql.connector.connect(user='root', password='password',
                               host='localhost', database='stockdata')

# Define the time range for the query
start_time = '2024-01-12 14:54:47'
end_time = '2024-01-12 14:59:52'

# Query the data
query = """
SELECT
    FROM_UNIXTIME(FLOOR(UNIX_TIMESTAMP(exchange_time_stamp)/60)*60) as Minute,
    MIN(last_trade_price) as Low,
    MAX(last_trade_price) as High,
    SUBSTRING_INDEX(MIN(CONCAT(ts_epoc, '_', last_trade_price)), '_', -1) as Open,
    SUBSTRING_INDEX(MAX(CONCAT(ts_epoc, '_', last_trade_price)), '_', -1) as Close
FROM
    stockdata
WHERE
    exchange_time_stamp BETWEEN %s AND %s AND NAME = 'INFY-EQ'
GROUP BY
    Minute
ORDER BY
    Minute
"""
df = pd.read_sql_query(query, conn, params=[start_time, end_time])

# Close the database connection
conn.close()

# Convert 'Minute' to datetime and map for plotting
df['Minute'] = pd.to_datetime(df['Minute'])
df['Minute'] = df['Minute'].map(mdates.date2num)

# Plotting the candlestick chart
fig, ax = plt.subplots()
# ax.xaxis.set_major_locator(MinuteLocator(interval=30))  # Adjust interval as needed
# ax.xaxis.set_major_formatter(DateFormatter('%H:%M'))

ax.xaxis.set_major_locator(HourLocator(interval=1))  # Adjust the interval as needed
ax.xaxis.set_major_formatter(DateFormatter('%Y-%m-%d %H:%M'))

# Define the width of the candlestick elements
width = 1 / (24 * 60)  # width for 1 minute

df['Open'] = pd.to_numeric(df['Open'])
df['High'] = pd.to_numeric(df['High'])
df['Low'] = pd.to_numeric(df['Low'])
df['Close'] = pd.to_numeric(df['Close'])


for idx, row in df.iterrows():
    color = 'g' if row["Close"] > row["Open"] else 'r'
    ax.plot([row["Minute"], row["Minute"]], [row["Low"], row["High"]], color=color)
    # Ensure that row["Minute"] is a numeric value for x-coordinate
    x_coord = mdates.date2num(row["Minute"])  # Converts datetime to a numeric value suitable for plotting
    ax.add_patch(Rectangle((x_coord - width/2, row["Open"]), width, row["Close"] - row["Open"], color=color))

ax.autoscale_view()
ax.xaxis_date()
ax.set_xlabel("Time")
ax.set_ylabel("Price")
ax.set_title("1-Minute Candlestick Chart for Stock Data")
# plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')

plt.show()
