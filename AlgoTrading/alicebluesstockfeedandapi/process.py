#  doc  https://github.com/krishnavelu/alice_blue

# pip install cryptography==35.0.0
# pip install alice_blue
# pip install confluent_kafka


import logging
import time
from alice_blue_local import *
import requests
import json
# from confluent_kafka import Producer
import datetime
from get_session import GetSessionId
from dbhelper import DBInsert

# p = Producer({'bootstrap.servers': 'localhost:9092'})
 

logging.basicConfig(level=logging.INFO)
 
session_id = GetSessionId()
# session_id = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIyam9lOFVScGxZU3FTcDB3RDNVemVBQkgxYkpmOE4wSDRDMGVVSWhXUVAwIn0.eyJleHAiOjE3MTAxNDY2MjMsImlhdCI6MTcwNDk2MjYyMywianRpIjoiNjRkYjZhNWItODI2My00ZmY3LTg4ODktNTBjOWQ4Y2M2NGUzIiwiaXNzIjoiaHR0cHM6Ly9hYjEuYW1vZ2EudGVjaC9hbXNzby9yZWFsbXMvQWxpY2VCbHVlIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6ImY5YjUyNDRkLTNkZTMtNDFlYS1iYzdiLTZmODIzYzYxYjVmZSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImFsaWNlLWtiIiwic2Vzc2lvbl9zdGF0ZSI6IjRkZWQyYjZkLTdjYTMtNGEzZS1hNDU1LTQ5NmYzODg1MTZkNiIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovL2xvY2FsaG9zdDozMDAyIiwiaHR0cDovL2xvY2FsaG9zdDo1MDUwIiwiaHR0cDovL2xvY2FsaG9zdDo5OTQzIiwiaHR0cDovL2xvY2FsaG9zdDo5MDAwIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsImRlZmF1bHQtcm9sZXMtYWxpY2VibHVla2IiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFsaWNlLWtiIjp7InJvbGVzIjpbIkdVRVNUX1VTRVIiLCJBQ1RJVkVfVVNFUiJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiNGRlZDJiNmQtN2NhMy00YTNlLWE0NTUtNDk2ZjM4ODUxNmQ2IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInVjYyI6IjUyMDc2OSIsImNsaWVudFJvbGUiOlsiR1VFU1RfVVNFUiIsIkFDVElWRV9VU0VSIl0sIm5hbWUiOiJTVU1JVCBTQUdBUiBTQUdBUiIsIm1vYmlsZSI6IjcyNjM5NDYxMDYiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiI1MjA3NjkiLCJnaXZlbl9uYW1lIjoiU1VNSVQgU0FHQVIiLCJmYW1pbHlfbmFtZSI6IlNBR0FSIiwiZW1haWwiOiJzdW1pdHNhZ2FyNzI5QGdtYWlsLmNvbSJ9.H2tiDGhXa5gVLjdjSMiVmiyWS7GQV3PgB3fT5pMsBz0-DnLc3KrvL3KwIh444O5jrheLWmV1BkBGclSgTS0ajf-P8Pk2GiS-zgC_AdY_fvtd3z2O2OJXj0WULoRE3gNEVURykZvhNBfpxN5X1mVj27wNS7Gz4U5BpcDHo9jCHZKXXaEMIGGMHx1QZg3A2MoHYuCJy-2f60tMnuptAHWBuKsbJTQ-ZCXg1y-JhZcrC9rccKk9_wF2UPj2FZ-CNuo9sGhMsoR29QodMKezivGlwHgXLsnSYsdCMGe4MavthC7GZumJCUb4-_N-FaA08Qu06Sa3ePpaaudCtwOZCDoxvg'
 
print(session_id) 
alice = AliceBlue(username = "520769", session_id = session_id)


# print(alice.get_balance()) # get balance / margin limits
print(alice.get_profile()) # get profile
# print(alice.get_daywise_positions()) # get daywise positions
# print(alice.get_netwise_positions()) # get netwise positions
# print(alice.get_holding_positions()) # get holding positions

global open_price
global high_price
global close_price
global low_price

eventcount = 0
eventcounttoskip = 2000

socket_opened = False
skipevent = False

# loop = asyncio.get_event_loop()


def event_handler_quote_update(message):

    # print(f"quote update {message}")
    # d = json.loads(message)
    # print(d.token)
    job_name = 'stockrate3'
    instance_name = 'pythonproducer'
    # provider = 'hetzner'
    payload_key = 'stockrate3'
    # payload_value = message['best_bid_price']
    global eventcount
    eventcount = eventcount + 1
    # print(message)
  
    token = message['instrument'].token
    exchange = message['instrument'].exchange
    name = message['instrument'].symbol
    open_price = message['open']
    high_price = message['high']
    low_price = message['low']
    close_price = message['close']
    last_trade_price = message['ltp']
    change_value = message['change_value']
    percent_change = message['percent_change']
    volume = message['volume']
    exchange_time_stamp = message['exchange_time_stamp']
    atp = message['atp']
    tick_increment = message['tick_increment']
    lot_size = message['lot_size']
    best_bid_price = message['best_bid_price']
    best_ask_price = message['best_ask_price']
    best_bid_quantity = message['best_bid_quantity']
    best_ask_quantity = message['best_ask_quantity']
    price_precision = message['price_precision']
    total_open_interest = message['total_open_interest']

    x = {
    "token": token,
    "exchange": exchange,
    "name": name,
    "open_price": open_price,
    "high_price": high_price,
    "low_price": low_price,
    "close_price": close_price,
    "last_trade_price": last_trade_price,
    "change_value": change_value,
    "percent_change": percent_change,
    "volume": volume,
    "exchange_time_stamp": exchange_time_stamp,
    "atp": atp,
    "tick_increment": tick_increment,
    "lot_size": lot_size,
    "best_bid_price": best_bid_price,
    "best_ask_price": best_ask_price,
    "best_bid_quantity": best_bid_quantity,
    "best_ask_quantity": best_ask_quantity,
    "price_precision": price_precision,
    "total_open_interest": total_open_interest,
    "ts_epoc": int(time.time()),
    "ts": datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
}
    
    # y = json.dumps(x)
    # print(y)
    global skipevent
    try:
        print(x)
        DBInsert(x)
    except Exception as e:
        print(e)

    print("event process  is -- ", eventcount)


def open_callback():
    eventcount = 0
    global socket_opened
    socket_opened = True
    print("socket Opened")


def post(url, data):
    response = requests.post(url, data=data)
    print(response.status_code)


sosocket_opened = False

alice.start_websocket(subscribe_callback=event_handler_quote_update,
                      socket_open_callback=open_callback)

#  while(socket_opened == False):
#     pass
 
for x in range(6000):
  print(x)

alice.subscribe(
    [alice.get_instrument_by_symbol('NSE', 'TATASTEEL-EQ'),
     alice.get_instrument_by_symbol('NSE', 'HINDALCO-EQ'),
     alice.get_instrument_by_symbol('NSE', 'BPCL-EQ'),
     alice.get_instrument_by_symbol('NSE', 'SHREECEM-EQ'),
     alice.get_instrument_by_symbol('NSE', 'TATACONSUM-EQ'),
     alice.get_instrument_by_symbol('NSE', 'TATAMOTORS-EQ'),
     alice.get_instrument_by_symbol('NSE', 'JSWSTEEL-EQ'),
     alice.get_instrument_by_symbol('NSE', 'GRASIM-EQ'),
    #  alice.get_instrument_by_symbol('NSE', 'HDFC-EQ'),
     alice.get_instrument_by_symbol('NSE', 'NESTLEIND-EQ'),
     alice.get_instrument_by_symbol('NSE', 'BAJAJ-AUTO-EQ'),
     alice.get_instrument_by_symbol('NSE', 'HINDUNILVR-EQ'),
     alice.get_instrument_by_symbol('NSE', 'M&M-EQ'),
     alice.get_instrument_by_symbol('NSE', 'HEROMOTOCO-EQ'),
     alice.get_instrument_by_symbol('NSE', 'EICHERMOT-EQ'),
     alice.get_instrument_by_symbol('NSE', 'DIVISLAB-EQ'),
     alice.get_instrument_by_symbol('NSE', 'IOC-EQ'),
     alice.get_instrument_by_symbol('NSE', 'BRITANNIA-EQ'),
     alice.get_instrument_by_symbol('NSE', 'UPL-EQ'),
     alice.get_instrument_by_symbol('NSE', 'INDUSINDBK-EQ'),
     alice.get_instrument_by_symbol('NSE', 'COALINDIA-EQ'),
     alice.get_instrument_by_symbol('NSE', 'SBILIFE-EQ'),
     alice.get_instrument_by_symbol('NSE', 'DRREDDY-EQ'),
     alice.get_instrument_by_symbol('NSE', 'INFY-EQ'),
     alice.get_instrument_by_symbol('NSE', 'POWERGRID-EQ'),
     alice.get_instrument_by_symbol('NSE', 'TECHM-EQ'),
     alice.get_instrument_by_symbol('NSE', 'CIPLA-EQ'),
     alice.get_instrument_by_symbol('NSE', 'NTPC-EQ'),
     alice.get_instrument_by_symbol('NSE', 'RELIANCE-EQ'),
     alice.get_instrument_by_symbol('NSE', 'TCS-EQ'),
     alice.get_instrument_by_symbol('NSE', 'HDFCBANK-EQ'),
     alice.get_instrument_by_symbol('NSE', 'INFY-EQ'),
    #  alice.get_instrument_by_symbol('NSE', 'HDFC-EQ'),
     alice.get_instrument_by_symbol('NSE', 'ICICIBANK-EQ'),
     alice.get_instrument_by_symbol('NSE', 'KOTAKBANK-EQ'),
     alice.get_instrument_by_symbol('NSE', 'SBIN-EQ'),
     alice.get_instrument_by_symbol('NSE', 'BAJFINANCE-EQ'),
     alice.get_instrument_by_symbol('NSE', 'BHARTIARTL-EQ'),
     alice.get_instrument_by_symbol('NSE', 'ITC-EQ'),
     alice.get_instrument_by_symbol('NSE', 'HCLTECH-EQ'),
     alice.get_instrument_by_symbol('NSE', 'ASIANPAINT-EQ'),
     alice.get_instrument_by_symbol('NSE', 'WIPRO-EQ'),
     alice.get_instrument_by_symbol('NSE', 'AXISBANK-EQ'),
     alice.get_instrument_by_symbol('NSE', 'MARUTI-EQ'),
     alice.get_instrument_by_symbol('NSE', 'LT-EQ'),
     alice.get_instrument_by_symbol('NSE', 'ULTRACEMCO-EQ'),
     alice.get_instrument_by_symbol('NSE', 'DMART-EQ'),
     alice.get_instrument_by_symbol('NSE', 'ADANIGREEN-EQ'),
     alice.get_instrument_by_symbol('NSE', 'BAJAJFINSV-EQ'),
     alice.get_instrument_by_symbol('NSE', 'SUNPHARMA-EQ'),
     alice.get_instrument_by_symbol('NSE', 'ADANIPORTS-EQ'),
     alice.get_instrument_by_symbol('NSE', 'HDFCLIFE-EQ'),
     alice.get_instrument_by_symbol('NSE', 'TITAN-EQ'),
     alice.get_instrument_by_symbol('NSE', 'ONGC-EQ'),
     alice.get_instrument_by_symbol('NSE', 'HINDZINC-EQ'),
     alice.get_instrument_by_symbol('NSE', 'ADANIENT-EQ'), ],
    LiveFeedType.TICK_DATA)
 

time.sleep(50000)
