import pymysql
import json
from datetime import datetime

# Hardcoded JSON data
# json_data = '{"token": 3499, "exchange": "NSE", "open_price": 134.85, "high_price": 135.65, "low_price": 134.2, "close_price": 134.1, "last_trade_price": 134.85, "name": "TATASTEEL-EQ", "ts_epoc": 1704964247, "ts": "11-01-2024 14:40:47"}'

# Function to convert date to long
def convert_date_to_long(date):
    return date.timetuple().tm_yday + (date.year * 1000)

# Function to convert date to long
def convert_date_to_long(date):
    return date.timetuple().tm_yday + (date.year * 1000)

# Function to fetch the last trade price of the previous day
def get_previous_day_last_trade_price(stock_name, connection):
    cursor = connection.cursor()
    previous_day = convert_date_to_long(datetime.now()) - 1
    query = "SELECT last_trade_price FROM stockdata WHERE name = %s AND daynum = %s ORDER BY id DESC LIMIT 1"
    cursor.execute(query, (stock_name, previous_day))
    result = cursor.fetchone()
    cursor.close()
    return result['last_trade_price'] if result else None

# Function to insert stock data into MySQL
def format_datetime_for_mysql(dt):
    return dt.strftime("%Y-%m-%d %H:%M:%S")

def insert_stock_data(stock_event, connection):
    cursor = connection.cursor()
    ts_processed = datetime.now()
    ts_epoc_processed = int(datetime.timestamp(ts_processed) * 1000)
    daynum = convert_date_to_long(ts_processed)

    # Convert ts to the correct format
    ts_for_db = format_datetime_for_mysql(datetime.strptime(stock_event["ts"], "%d-%m-%Y %H:%M:%S"))

    query = """
    INSERT INTO stockdata (
        token, exchange, name, open_price, high_price, low_price, close_price, 
        last_trade_price, change_value, percent_change, volume, 
        exchange_time_stamp, atp, tick_increment, lot_size, 
        best_bid_price, best_ask_price, best_bid_quantity, best_ask_quantity, 
        price_precision, total_open_interest, ts_epoc, ts, ts_epoc_processed, 
        ts_processed, daynum
    ) 
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """

    values = (
        stock_event["token"], stock_event["exchange"], stock_event["name"],
        stock_event["open_price"], stock_event["high_price"], stock_event["low_price"], 
        stock_event["close_price"], stock_event["last_trade_price"], 
        stock_event["change_value"], stock_event["percent_change"], stock_event["volume"], 
        stock_event["exchange_time_stamp"], stock_event["atp"], stock_event["tick_increment"], 
        stock_event["lot_size"], stock_event["best_bid_price"], stock_event["best_ask_price"], 
        stock_event["best_bid_quantity"], stock_event["best_ask_quantity"], 
        stock_event["price_precision"], stock_event["total_open_interest"], 
        stock_event["ts_epoc"], ts_for_db, ts_epoc_processed, 
        ts_processed, daynum
    )

    # print(values)
    
    cursor.execute(query, values)
    connection.commit()
    cursor.close()

# Function to insert price change data into MySQL
def insert_price_change_data(stock_event, connection, previous_day_last_trade_price):
    cursor = connection.cursor()
    ts_processed = datetime.now()
    ts_epoc_processed = int(datetime.timestamp(ts_processed) * 1000)
    daynum = convert_date_to_long(ts_processed)

    # Calculate the difference amount and percent change
    difference_amount = stock_event["last_trade_price"] - previous_day_last_trade_price
    percent_change = (difference_amount / previous_day_last_trade_price) * 100 if previous_day_last_trade_price != 0 else 0

    query = "INSERT INTO StockPriceChange (name, exchange, differenceamount, percentchange, ts_epoc_processed, ts_processed, daynum) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    values = (stock_event["name"], stock_event["exchange"], difference_amount, percent_change, ts_epoc_processed, ts_processed, daynum)

    cursor.execute(query, values)
    connection.commit()
    cursor.close()

# Main function
def DBInsert(stock_event):
    # stock_event = json.loads(json_data)

    # Database connection parameters
    connection = pymysql.connect(host='localhost', user='root', password='root', db='stockdata', charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)

    try:
        insert_stock_data(stock_event, connection)
        previous_day_price = 120 #get_previous_day_last_trade_price(stock_event["name"], connection)
        if previous_day_price is not None:
            insert_price_change_data(stock_event, connection, previous_day_price)
            # print("Data inserted successfully")
        else:
            print("Previous day's last trade price not found for the stock")

        # print("Data inserted successfully")
    except Exception as e:
        print(f"An error occurred: {e}")
    finally:
        connection.close()

# if __name__ == "__main__":
#     main()


def query_candlestick_data(time_frame, start_time, end_time):
    """
    Query candlestick data aggregated by the given time frame.
    :param time_frame: Time frame in minutes (e.g., 5, 15)
    :param start_time: Start time as a string (YYYY-MM-DD HH:MM:SS)
    :param end_time: End time as a string (YYYY-MM-DD HH:MM:SS)
    :return: Aggregated candlestick data as a list of dictionaries
    """
    query = f"""
        SELECT 
            FLOOR(UNIX_TIMESTAMP(exchange_time_stamp) / ({time_frame} * 60)) AS time_group,
            MIN(last_trade_price) AS open_price,
            MAX(high_price) AS high_price,
            MIN(low_price) AS low_price,
            MAX(last_trade_price) AS close_price,
            SUM(volume) AS volume
        FROM stockdata
        WHERE exchange_time_stamp BETWEEN %s AND %s
        GROUP BY time_group
        ORDER BY time_group
    """
    connection = pymysql.connect(
        host='localhost',
        user='root',
        password='root',
        db='stockdata',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor
    )

    cursor = connection.cursor()
    cursor.execute(query, (start_time, end_time))
    rows = cursor.fetchall()

    # Format the data into a list of dictionaries
    candlestick_data = []
    for row in rows:
        candlestick_data.append({
            "time_group": row["time_group"],
            "open_price": row["open_price"],
            "high_price": row["high_price"],
            "low_price": row["low_price"],
            "close_price": row["close_price"],
            "volume": row["volume"]
        })

    cursor.close()
    connection.close()
    return candlestick_data
