# initialize_tracker:

# This function ensures that all symbols in stockdata are added to the candle_processing_tracker table.
# It inserts symbols with the earliest exchange_time_stamp from stockdata.
# Handling Empty candle_processing_tracker:

# If the table is empty, all symbols from stockdata are added with their earliest timestamps.
# Graceful Backfilling:

# For symbols already in candle_processing_tracker, the script continues processing from last_processed_time.

import time
import pymysql
from datetime import datetime, timedelta
 
def initialize_tracker(db_config):
    """
    Ensure that all symbols in stockdata are present in candle_processing_tracker.
    For each token, use the latest processed time from candlestick_1min if available,
    otherwise, fall back to the earliest time in stockdata.
    """
    try:
        connection = pymysql.connect(**db_config)
        cursor = connection.cursor()

        # Step 1: Check for missing tokens in candle_processing_tracker
        missing_tokens_query = """
        SELECT DISTINCT token
        FROM stockdata
        WHERE token NOT IN (SELECT token FROM candle_processing_tracker);
        """
        cursor.execute(missing_tokens_query)
        missing_tokens = cursor.fetchall()

        for (token,) in missing_tokens:
            # Step 2: Check latest processed time in candlestick_1min for this token
            latest_time_query = """
            SELECT MAX(start_time)
            FROM candlestick_1min
            WHERE token = %s;
            """
            cursor.execute(latest_time_query, (token,))
            latest_time = cursor.fetchone()[0]

            if latest_time:
                # Step 3: Use the latest time from candlestick_1min
                last_processed_time = latest_time
            else:
                # Step 4: Fallback to the earliest time in stockdata
                earliest_time_query = """
                SELECT MIN(exchange_time_stamp)
                FROM stockdata
                WHERE token = %s;
                """
                cursor.execute(earliest_time_query, (token,))
                last_processed_time = cursor.fetchone()[0]

            if last_processed_time:
                # Step 5: Insert into candle_processing_tracker
                insert_tracker_query = """
                INSERT INTO candle_processing_tracker (token, last_processed_time)
                VALUES (%s, %s);
                """
                cursor.execute(insert_tracker_query, (token, last_processed_time))

        connection.commit()

    except pymysql.MySQLError as e:
        print(f"Error initializing tracker: {e}")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()


def process_candlestick_data(db_config):
    """
    Process 1-minute candlesticks for symbols in candle_processing_tracker.
    """
    try:
        connection = pymysql.connect(**db_config)
        cursor = connection.cursor()

        # Get all symbols and their last processed times
        cursor.execute("SELECT token, last_processed_time FROM candle_processing_tracker")
        trackers = cursor.fetchall()

        for token, last_processed_time in trackers:
            start_time = last_processed_time
            end_time = start_time + timedelta(minutes=1)  # Process 1-minute interval

            # Query to aggregate 1-minute candles
            query = f"""
            INSERT INTO candlestick_1min (token, exchange, name, start_time, end_time, open_price, high_price, low_price, close_price, volume)
            SELECT 
                s.token,
                s.exchange,
                s.name,
                s.interval_start AS start_time,
                MAX(s.interval_end) AS end_time,
                MIN(s.open_price) AS open_price,
                MAX(s.high_price) AS high_price,
                MIN(s.low_price) AS low_price,
                MAX(s.close_price) AS close_price,
                SUM(s.volume) AS volume
            FROM (
                SELECT
                    token,
                    name,
                    exchange,
                    TIMESTAMP(DATE(exchange_time_stamp), SEC_TO_TIME(FLOOR(TIME_TO_SEC(TIME(exchange_time_stamp)) / 60) * 60)) AS interval_start,
                    TIMESTAMP(DATE(exchange_time_stamp), SEC_TO_TIME(FLOOR(TIME_TO_SEC(TIME(exchange_time_stamp)) / 60) * 60) + INTERVAL 1 MINUTE) AS interval_end,
                    open_price, high_price, low_price, close_price, volume,
                    exchange_time_stamp
                FROM stockdata
                WHERE exchange_time_stamp BETWEEN %s AND %s
            ) s
            LEFT JOIN candlestick_1min c
            ON s.token = c.token AND s.interval_start = c.start_time
            WHERE c.start_time IS NULL
            GROUP BY s.token, s.exchange, s.name, s.interval_start;
            """
            cursor.execute(query, (start_time, end_time))

            # Update the tracker
            update_tracker_query = """
            UPDATE candle_processing_tracker
            SET last_processed_time = %s
            WHERE token = %s
            """
            cursor.execute(update_tracker_query, (end_time, token))

        connection.commit()

    except pymysql.MySQLError as e:
        print(f"Error processing candlestick data: {e}")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

# Example usage
db_config = {
    'host': 'localhost',
    'user': 'root',
    'password': 'root',
    'database': 'stockdata'
}

while(True):
    # Step 1: Initialize tracker if empty
    initialize_tracker(db_config)

    # Step 2: Process candlestick data
    process_candlestick_data(db_config)
    time.sleep(5)
