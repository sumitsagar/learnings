#  doc  https://github.com/krishnavelu/alice_blue

import logging
import time
from alice_blue import *
import requests
import json
from confluent_kafka import Producer
import datetime

p = Producer({'bootstrap.servers': '34.125.93.96:9092'})

# j = "{'exchange': 'NSE', 'token': 3499, 'ltp': 1154.3, 'ltt': 1639643633, 'ltq': 1, 'volume': 2958623, 'best_bid_price': 1154.2, 'best_bid_quantity': 83, 'best_ask_price': 1154.3, 'best_ask_quantity': 7, 'total_buy_quantity': 218243, 'total_sell_quantity': 437136, 'atp': 1160.12, 'exchange_time_stamp': 1639643633, 'open': 1164.0, 'high': 1168.8, 'low': 1153.7, 'close': 1154.05, 'yearly_high': 1534.5, 'yearly_low': 585.0, 'instrument': Instrument(exchange='NSE', token=3499, symbol='TATASTEEL', name='TATA STEEL LIMITED', expiry=None, lot_size=None)}"
# j = j.replace("\'", "\"")
# d = json.loads(j)
# print(d)
# time.sleep(50000)

logging.basicConfig(level=logging.INFO)

access_token = "dwsS0lIgho9-IxqRCCR5qkVnHjVjIMCZn9Cn-QkqiQE.7yMgNpQdSJIRJL2KSx5Vp1hWc6P12aXANB27Rl-m5Z4"
access_token = AliceBlue.login_and_get_access_token(
    username='520769', password='Fadesigns6#', twoFA='1990',
    api_secret='4HqHfluuWayb51G96MwbKtNxCfnHjLlQlMkjslpFbsCLL4ZnQ4cRn4GFbsSq4O7Y',
    redirect_url='https://ant.aliceblueonline.com/plugin/callback', app_id='mmvevNYBWD',)
print(access_token)
alice = AliceBlue(username='mmvevNYBWD',
                  password='4HqHfluuWayb51G96MwbKtNxCfnHjLlQlMkjslpFbsCLL4ZnQ4cRn4GFbsSq4O7Y', access_token=access_token)

# print(alice.get_balance()) # get balance / margin limits
# print(alice.get_profile()) # get profile
# print(alice.get_daywise_positions()) # get daywise positions
# print(alice.get_netwise_positions()) # get netwise positions
# print(alice.get_holding_positions()) # get holding positions


global open_price
global high_price
global close_price
global low_price

eventcount = 0
eventcounttoskip = 2000

socket_opened = False
skipevent = False

# loop = asyncio.get_event_loop()


def event_handler_quote_update(message):

    print(f"quote update {message}")
    # d = json.loads(message)
    # print(d.token)
    job_name = 'stockrate3'
    instance_name = 'pythonproducer'
    # provider = 'hetzner'
    payload_key = 'stockrate3'
    # payload_value = message['best_bid_price']
    global eventcount
    eventcount = eventcount + 1

    token = message['token']
    exchange = message['exchange']
    name = message['instrument'].symbol
    open_price = message['open']
    high_price = message['high']
    low_price = message['low']
    close_price = message['close']
    lasttraeprice = message['ltp']

    x = {
        "token": token,
        "exchange": exchange,
        "open_price": open_price,
        "high_price": high_price,
        "low_price": low_price,
        "close_price": close_price,
        "last_trade_price": lasttraeprice,
        "name": name,
        "ts_epoc": int(time.time()),
        "ts": datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    }
    y = json.dumps(x)
    print(y)
    global skipevent
    try:
        if(skipevent == False):
            p.produce('stockevents', y.encode('utf-8'))
        else:
            global eventcounttoskip
            print("skipping event as quue was full, msg to wait - " + eventcounttoskip)
            eventcounttoskip = eventcounttoskip - 1
            if(eventcounttoskip <= 0):
                skipevent = False
    except:
        print("kafka local queue full.. will wait for 1000 events ....")
        skipevent = True
        eventcounttoskip = 2000  # resetting count on exception

    # below code from https://blog.ruanbekker.com/blog/2019/05/17/install-pushgateway-to-expose-metrics-to-prometheus/
    # /open_price/{o}/high_price/{p}/low_price/{q}/close_price/{r}
    # response = requests.post(, )
    # print(response.status_code)

    # task = post('http://weiggle.com:9091/metrics/job/{j}/instance/{k}/token/{l}/exchange/{m}/name/{n}'.format(
    #     j=job_name, k=instance_name, l=token, m=exchange, n=name, o=open_price, p=high_price,
    #     q=low_price, r=close_price), data='{k} {v}\n'.format(k=payload_key, v=lasttraeprice))
    # post('http://weiggle.com:9091/metrics/job/{j}/instance/{k}/token/{l}/exchange/{m}/name/{n}'.format(
    #     j=job_name, k=instance_name, l=token, m=exchange, n=name, o=open_price, p=high_price,
    #     q=low_price, r=close_price), data='{k} {v}\n'.format(k=payload_key, v=lasttraeprice))
    print("event process  is -- ", eventcount)


def open_callback():
    eventcount = 0
    global socket_opened
    socket_opened = True
    print("socket Opened")


def post(url, data):
    response = requests.post(url, data=data)
    print(response.status_code)


sosocket_opened = False

alice.start_websocket(subscribe_callback=event_handler_quote_update,
                      socket_open_callback=open_callback,
                      run_in_background=True)

while(socket_opened == False):
    pass

alice.subscribe(
    [alice.get_instrument_by_symbol('NSE', 'TATASTEEL'),
     alice.get_instrument_by_symbol('NSE', 'HINDALCO'),
     alice.get_instrument_by_symbol('NSE', 'BPCL'),
     alice.get_instrument_by_symbol('NSE', 'SHREECEM'),
     alice.get_instrument_by_symbol('NSE', 'TATACONSUM'),
     alice.get_instrument_by_symbol('NSE', 'TATAMOTORS'),
     alice.get_instrument_by_symbol('NSE', 'JSWSTEEL'),
     alice.get_instrument_by_symbol('NSE', 'GRASIM'),
     alice.get_instrument_by_symbol('NSE', 'HDFC'),
     alice.get_instrument_by_symbol('NSE', 'NESTLEIND'),
     alice.get_instrument_by_symbol('NSE', 'BAJAJ-AUTO'),
     alice.get_instrument_by_symbol('NSE', 'HINDUNILVR'),
     alice.get_instrument_by_symbol('NSE', 'M&M'),
     alice.get_instrument_by_symbol('NSE', 'HEROMOTOCO'),
     alice.get_instrument_by_symbol('NSE', 'EICHERMOT'),
     alice.get_instrument_by_symbol('NSE', 'DIVISLAB'),
     alice.get_instrument_by_symbol('NSE', 'IOC'),
     alice.get_instrument_by_symbol('NSE', 'BRITANNIA'),
     alice.get_instrument_by_symbol('NSE', 'UPL'),
     alice.get_instrument_by_symbol('NSE', 'INDUSINDBK'),
     alice.get_instrument_by_symbol('NSE', 'COALINDIA'),
     alice.get_instrument_by_symbol('NSE', 'SBILIFE'),
     alice.get_instrument_by_symbol('NSE', 'DRREDDY'),
     alice.get_instrument_by_symbol('NSE', 'INFY'),
     alice.get_instrument_by_symbol('NSE', 'POWERGRID'),
     alice.get_instrument_by_symbol('NSE', 'TECHM'),
     alice.get_instrument_by_symbol('NSE', 'CIPLA'),
     alice.get_instrument_by_symbol('NSE', 'NTPC'),
     alice.get_instrument_by_symbol('NSE', 'RELIANCE'),
     alice.get_instrument_by_symbol('NSE', 'TCS'),
     alice.get_instrument_by_symbol('NSE', 'HDFCBANK'),
     alice.get_instrument_by_symbol('NSE', 'INFY'),
     alice.get_instrument_by_symbol('NSE', 'HDFC'),
     alice.get_instrument_by_symbol('NSE', 'ICICIBANK'),
     alice.get_instrument_by_symbol('NSE', 'KOTAKBANK'),
     alice.get_instrument_by_symbol('NSE', 'SBIN'),
     alice.get_instrument_by_symbol('NSE', 'BAJFINANCE'),
     alice.get_instrument_by_symbol('NSE', 'BHARTIARTL'),
     alice.get_instrument_by_symbol('NSE', 'ITC'),
     alice.get_instrument_by_symbol('NSE', 'HCLTECH'),
     alice.get_instrument_by_symbol('NSE', 'ASIANPAINT'),
     alice.get_instrument_by_symbol('NSE', 'WIPRO'),
     alice.get_instrument_by_symbol('NSE', 'AXISBANK'),
     alice.get_instrument_by_symbol('NSE', 'MARUTI'),
     alice.get_instrument_by_symbol('NSE', 'LT'),
     alice.get_instrument_by_symbol('NSE', 'ULTRACEMCO'),
     alice.get_instrument_by_symbol('NSE', 'DMART'),
     alice.get_instrument_by_symbol('NSE', 'ADANIGREEN'),
     alice.get_instrument_by_symbol('NSE', 'BAJAJFINSV'),
     alice.get_instrument_by_symbol('NSE', 'SUNPHARMA'),
     alice.get_instrument_by_symbol('NSE', 'ADANIPORTS'),
     alice.get_instrument_by_symbol('NSE', 'HDFCLIFE'),
     alice.get_instrument_by_symbol('NSE', 'TITAN'),
     alice.get_instrument_by_symbol('NSE', 'ONGC'),
     alice.get_instrument_by_symbol('NSE', 'HINDZINC'),
     alice.get_instrument_by_symbol('NSE', 'ADANIENT'), ],
    LiveFeedType.MARKET_DATA)

# alice.subscribe(
#     [
#         alice.get_instrument_by_symbol('NSE', 'INFY'), ],
#     LiveFeedType.MARKET_DATA)


# market status
# socket_opened_ms = False


# def market_status_messages(message):
#     print(f"market status messages {message}")


# def exchange_messages(message):
#     print(f"exchange messages {message}")


# def open_callback():
#     global socket_opened_ms
#     socket_opened_ms = True


# alice.start_websocket(market_status_messages_callback=market_status_messages,
#                       exchange_messages_callback=exchange_messages,
#                       socket_open_callback=open_callback,
#                       run_in_background=True)
# while(socket_opened_ms == False):
#     pass
# alice.subscribe_market_status_messages()
# alice.subscribe_exchange_messages()


time.sleep(50000)
