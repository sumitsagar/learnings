import random
from datetime import datetime, timedelta

from dbhelper import DBInsert, insert_stock_data


import time

def simulate_stock_data():
    """
    Simulates stock data based on the provided sample characteristics.
    """
    current_time = datetime.now()
    base_price = 146.50  # Starting price close to your sample data
    high_price = round(base_price + random.uniform(0.1, 1.5), 2)  # Ensure high > base_price
    low_price = round(base_price - random.uniform(0.1, 1.7), 2)   # Ensure low < base_price
    close_price = round(random.uniform(low_price, high_price), 2)  # Close price within range
    last_trade_price = round(random.uniform(low_price, high_price), 2)  # Realistic randomization
    volume = random.randint(18100000, 18200000)  # Match your provided volume range
    
    simulated_event = {
        "token": 3499,  # Fixed token based on your data
        "exchange": "NSE",
        "name": "TEST-TATASTEEL-EQ",
        "open_price": base_price,
        "high_price": high_price,
        "low_price": low_price,
        "close_price": close_price,
        "last_trade_price": last_trade_price,
        "change_value": round(close_price - base_price, 2),  # Difference between close and base
        "percent_change": round(((close_price - base_price) / base_price) * 100, 2),  # Percent change
        "volume": volume,
        "exchange_time_stamp": current_time.strftime("%Y-%m-%d %H:%M:%S"),
        "atp": round(random.uniform(145.80, 146.90), 2),  # Average traded price range
        "tick_increment": 0.01,
        "lot_size": 1,
        "best_bid_price": round(random.uniform(low_price, close_price), 2),
        "best_ask_price": round(random.uniform(close_price, high_price), 2),
        "best_bid_quantity": random.randint(10, 5000),  # Adjusted for reasonable quantity
        "best_ask_quantity": random.randint(10, 5000),  # Adjusted for reasonable quantity
        "price_precision": 2,
        "total_open_interest": random.randint(520000000, 522000000),  # Match range in your data
        "ts_epoc": int(current_time.timestamp()),
        "ts": current_time.strftime("%d-%m-%Y %H:%M:%S"),
        "ts_epoc_processed": int(current_time.timestamp() * 1000),
        "ts_processed": current_time,
        "daynum": current_time.timetuple().tm_yday
    }
    return simulated_event

def run_simulation_and_insert():
    """
    Simulates stock data and inserts it into the database.
    """
    for _ in range(100000):  # Simulate 10 events for demonstration
        stock_event = simulate_stock_data()
        DBInsert(stock_event)  # Insert into the database
        print(f"Inserted stock event: {stock_event['name']} at {stock_event['ts']}")
        time.sleep(1)  # Wait 1 second before generating the next event


run_simulation_and_insert()
