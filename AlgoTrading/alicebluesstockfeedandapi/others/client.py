from pya3 import *

from dbhelper import DBInsert
alice = Aliceblue(user_id='520769',api_key='7Ad0lYxAXpuytmmytCpbeHSxxIrzhvnUXCSdo0RgOcUj7ieIThPmF7TeILNHPMU03C8uGn0BAc9WaBiyhQyk1nnREClrlv92P7HNbQKZkmZRyH4ZRaf9RE8cbEze0H5J')
print(alice.get_session_id()) # Get Session ID
# print(alice.get_balance()) # get balance / margin limits
# print(alice.get_profile()) # get profile
# print(alice.get_daywise_positions()) # get daywise positions
# print(alice.get_netwise_positions()) # get all netwise positions
# print(alice.get_holding_positions()) # get holding positions


LTP = 0
socket_opened = False
subscribe_flag = False
subscribe_list = []
unsubscribe_list = []
eventcount = 0

def socket_open():  # Socket open callback function
    print("Connected")
    global socket_opened
    socket_opened = True
    if subscribe_flag:  # This is used to resubscribe the script when reconnect the socket.
        alice.subscribe(subscribe_list)

def socket_close():  # On Socket close this callback function will trigger
    global socket_opened, LTP
    socket_opened = False
    LTP = 0
    print("Closed")

def socket_error(message):  # Socket Error Message will receive in this callback function
    global LTP
    LTP = 0
    print("Error :", message)

def feed_data(message):  # Socket feed data will receive in this callback function
    global LTP, subscribe_flag
    print()
    feed_message = json.loads(message)
    if feed_message["t"] == "ck":
        print("Connection Acknowledgement status :%s (Websocket Connected)" % feed_message["s"])
        subscribe_flag = True
        print("subscribe_flag :", subscribe_flag)
        print("-------------------------------------------------------------------------------")
        pass
    elif feed_message["t"] == "tk":
        print("Token Acknowledgement status :%s " % feed_message)
        print("-------------------------------------------------------------------------------")
        pass
    else:
        print("Feed :", feed_message)
        LTP = feed_message[
            'lp'] if 'lp' in feed_message else LTP  # If LTP in the response it will store in LTP variable
    event_handler_quote_update(message=message)


def event_handler_quote_update(message):

    print(f"quote update {message}")
    # d = json.loads(message)
    # print(d.token)
    job_name = 'stockrate3'
    instance_name = 'pythonproducer'
    # provider = 'hetzner'
    payload_key = 'stockrate3'
    # payload_value = message['best_bid_price']
    global eventcount
    eventcount = eventcount + 1
    print(message)
  
    token = message['instrument'].token
    exchange = message['instrument'].exchange
    name = message['instrument'].symbol
    open_price = message['open']
    high_price = message['high']
    low_price = message['low']
    close_price = message['close']
    last_trade_price = message['ltp']
    change_value = message['change_value']
    percent_change = message['percent_change']
    volume = message['volume']
    exchange_time_stamp = message['exchange_time_stamp']
    atp = message['atp']
    tick_increment = message['tick_increment']
    lot_size = message['lot_size']
    best_bid_price = message['best_bid_price']
    best_ask_price = message['best_ask_price']
    best_bid_quantity = message['best_bid_quantity']
    best_ask_quantity = message['best_ask_quantity']
    price_precision = message['price_precision']
    total_open_interest = message['total_open_interest']

    x = {
    "token": token,
    "exchange": exchange,
    "name": name,
    "open_price": open_price,
    "high_price": high_price,
    "low_price": low_price,
    "close_price": close_price,
    "last_trade_price": last_trade_price,
    "change_value": change_value,
    "percent_change": percent_change,
    "volume": volume,
    "exchange_time_stamp": exchange_time_stamp,
    "atp": atp,
    "tick_increment": tick_increment,
    "lot_size": lot_size,
    "best_bid_price": best_bid_price,
    "best_ask_price": best_ask_price,
    "best_bid_quantity": best_bid_quantity,
    "best_ask_quantity": best_ask_quantity,
    "price_precision": price_precision,
    "total_open_interest": total_open_interest,
    "ts_epoc": int(time.time()),
    "ts": datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
}
    
    # y = json.dumps(x)
    # print(y)
    global skipevent
    try:
        print(x)
        DBInsert(x)
    except Exception as e:
        print(e)

    print("event process  is -- ", eventcount)


# Socket Connection Request
alice.start_websocket(socket_open_callback=socket_open, socket_close_callback=socket_close,
                      socket_error_callback=socket_error, subscription_callback=feed_data, run_in_background=True,market_depth=False)

while not socket_opened:
    pass

subscribe_list = [alice.get_instrument_by_token('INDICES', 26000)]
alice.subscribe(subscribe_list)

print(datetime.now())
sleep(10)
print(datetime.now())
# unsubscribe_list = [alice.get_instrument_by_symbol("NSE", "RELIANCE")]
# alice.unsubscribe(unsubscribe_list)
# sleep(8)

# Stop the websocket
alice.stop_websocket()
sleep(10)
print(datetime.now())

# Connect the socket after socket close
alice.start_websocket(socket_open_callback=socket_open, socket_close_callback=socket_close,
                      socket_error_callback=socket_error, subscription_callback=feed_data, run_in_background=True)
