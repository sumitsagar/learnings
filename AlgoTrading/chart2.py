import pandas as pd
import mplfinance as mpf
from alicebluesstockfeedandapi.dbhelper import query_candlestick_data

def plot_candlestick(time_frame, start_time, end_time):
    # Initialize the database
    # db = StockDataDB()

    # Query data for the given time frame and date range
    candlestick_data = query_candlestick_data(time_frame, start_time, end_time)

    # Convert data to a DataFrame
    df = pd.DataFrame(candlestick_data)
    df['time_group'] = pd.to_datetime(df['time_group'])
    df.set_index('time_group', inplace=True)

    # Rename columns to match mplfinance expectations
    df.rename(columns={
        "open_price": "Open",
        "high_price": "High",
        "low_price": "Low",
        "close_price": "Close",
        "volume": "Volume"
    }, inplace=True)

    # Plot candlestick chart
    mpf.plot(df, type='candle', volume=True, style='yahoo')

    # Close the database connection
    db.close()

if __name__ == "__main__":
    # Example usage
    time_frame = 5  # 5-minute candles
    start_time = "2024-12-05 09:15:00"
    end_time = "2024-12-05 15:30:00"
    plot_candlestick(time_frame, start_time, end_time)
