using system;

namespace StockKafkaEventConsumer
{
    class LinkedList
    {

        public class Node
        {
            public string Data { get; set; }
            public Node next { get; set; }

            public Node(int x)
            {
                val = x;
            }

        }

        public void ReverseLinkedList(Node head)
        {
            // Initialize three pointers prev as NULL, curr as head and next as NULL.
            Node prev = null;
            Node curr = head;
            Node next = null;

            while (curr != null)
            { 
                //reverse the pointers
                curr.next = prev

                //for next node , current node will become prev node as we move forward
                //so set curnote as prevnode 
                prev = curr
                 //set curnode as next node
                curr = curr.next 

            }
            // At the end of the loop, prev is the new head of the reversed list
            head = prev;


        }

        static void Test()
        {
            // Create the nodes of the linked list
            Node node1 = new Node(1);
            Node node2 = new Node(2);
            Node node3 = new Node(3);
            Node node4 = new Node(4);

            // Link the nodes together to form a list (1 -> 2 -> 3 -> 4)
            node1.next = node2;
            node2.next = node3;
            node3.next = node4;

            // Create an instance of the Solution class
            Solution solution = new Solution();

            // Call the ReverseList method and pass the head of the list
            Node reversedHead = solution.ReverseList(node1);

            // Print the reversed list
            Node current = reversedHead;
            while (current != null)
            {
                Console.WriteLine(current.val);
                current = current.next;
            }
        }
    }
}