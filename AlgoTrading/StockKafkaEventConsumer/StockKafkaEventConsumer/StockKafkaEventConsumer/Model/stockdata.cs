﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;

namespace StockKafkaEventConsumer.Model
{
    [PrimaryKey("id")]

    public class stockdata
    {
        public long id { get; set; }
        public string token { get; set; }
        public string exchange { get; set; }
        public decimal open_price { get; set; }
        public decimal high_price { get; set; }
        public decimal low_price { get; set; }
        public decimal close_price { get; set; }
        public decimal last_trade_price { get; set; }
        public string name { get; set; }
        public long ts_epoc { get; set; }
        public string ts { get; set; }
        public long ts_epoc_processed { get; set; }
        public DateTime ts_processed { get; set; }
        public long daynum { get; set; }
    }

    public class PreviousDayClosePrice
    {
        public decimal last_trade_price { get; set; }
        public string name { get; set; }
    }

    public class StockPriceChange
    {

        public long id { get; set; }
        public string name { get; set; }
        public string exchange { get; set; }
        public decimal differenceamount { get; set; }
        public decimal percentchange { get; set; }
        public long ts_epoc_processed { get; set; }
        public DateTime ts_processed { get; set; }
        public long daynum { get; set; }
    }
}
