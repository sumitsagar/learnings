﻿using StockKafkaEventConsumer.DataConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace StockKafkaEventConsumer
{
    public class PaaSDataCollectionForML
    {
        Dictionary<string, UserTransaction> usertransations_dict = new Dictionary<string, UserTransaction>();
        List<UserTransaction> usertransations = new List<UserTransaction>();
        string connectionString_paas = "server=prcreplica.redbus.com;Database=RB_Central_Payment_dbo;Persist Security Info=True;User ID=prc_rd_sh_user;Password=xwku9cmw;Connect Timeout=200";

        public async Task Init()
        {
            //string dateOfRequestInLong = DateTime.Now.Year + DateTime.Now.AddDays(-1).DayOfYear.ToString().PadLeft(3, '0');
            long startdate = 2022001;
            long enddate = startdate + 10;
            for (long i = startdate; i < enddate; i++)
            {
                await Process(i);
            }

            string csv = ToCsv<UserTransaction>(",", usertransations);
            System.IO.File.WriteAllText("D:\\mlpaasdata\\" + DateTime.Now.ToShortDateString() + DateTime.Now.Ticks + ".csv", csv);
        }

        public async Task Process(long dateOfRequestInLong)
        {
            MinMaxTxn minMaxTxn = await GetTxnRange(dateOfRequestInLong);
            string query = "SELECT p.transactionid,BankName 'PGType', FinalResult,ptd.haspreferredpayment,ptd.ispreferredpayment," +
                                    "SUBSTR(requestdata, POSITION('UDF1\":\"redbus' IN requestdata) + 7, 20) userid " +
                                    @"FROM PaymentTransactionArchive p
                                  JOIN ClientConfiguration c ON p.ClientId = c.ClientId
                                  JOIN Banks b ON b.bankid = p.bankid
                                  JOIN PaymentTransactionDetailsArchive ptd ON ptd.transactionid = p.transactionid 
                                  JOIN PaymentRequestArchive pr ON pr.orderid = p.orderid
                                  WHERE p.ClientId IN (236) " +
                                      "AND requestdata LIKE '%\"UDF1\":\"redbus%'" +
                                      @"AND p.gatewayid NOT IN (SELECT gatewayid FROM PaymentGateways WHERE pgtypeid = (3)) 
                                  AND p.transactionid BETWEEN {0} AND {1}
                                  UNION ALL
                                  SELECT p.transactionid,'Net Banking' AS 'PGType', FinalResult,ptd.haspreferredpayment,ptd.ispreferredpayment," +
                                    "SUBSTR(requestdata, POSITION('UDF1\":\"redbus' IN requestdata) + 7, 20) userid " +
                                    @"FROM PaymentTransactionArchive p
                                  JOIN ClientConfiguration c ON p.ClientId = c.ClientId
                                  JOIN Banks b ON b.bankid = p.bankid
                                  JOIN PaymentTransactionDetailsArchive ptd ON ptd.transactionid = p.transactionid 
                                  JOIN PaymentRequestArchive pr ON pr.orderid = p.orderid
                                  WHERE p.ClientId IN (236) " +
                                      "AND requestdata LIKE '%\"UDF1\":\"redbus%'" +
                                      @"AND p.gatewayid  IN (SELECT gatewayid FROM PaymentGateways WHERE pgtypeid = 3) 
                                 AND p.transactionid BETWEEN {0} AND {1} ";
            query = string.Format(query, minMaxTxn.min, minMaxTxn.max);
            Console.WriteLine("getting data for " + dateOfRequestInLong);
            List<PreferredTransaction> result = await new MySqlConnector(connectionString_paas).FetchAsync<PreferredTransaction>(query);
            //var a = result.Select(x => x.PGType).Distinct().ToList();
            //string json = Newtonsoft.Json.JsonConvert.SerializeObject(a);
            int cnt = 0;
            int fountcount = result.Count();
            foreach (var item in result)
            {
                Console.WriteLine(string.Format("processing txn {0} of {1} for date {2}", cnt++, fountcount, dateOfRequestInLong));
                var usertransaction = usertransations_dict.ContainsKey(item.CleanUserId) ? usertransations_dict[item.CleanUserId] : null;  //usertransations.Where(x => x.UserId == item.CleanUserId).FirstOrDefault();
                if (usertransaction == null)
                {
                    usertransaction = new UserTransaction();
                    usertransations_dict[item.CleanUserId] = usertransaction;
                }
                usertransaction.UserId = item.CleanUserId;
                usertransaction.LastUsed = item.PGType;

                if (item.PGType.Contains("Amazon"))
                {
                    usertransaction.AmazonPay = usertransaction.AmazonPay + 1;
                }
                else if (item.PGType.Contains("PhonePe"))
                {
                    usertransaction.PhonePe = usertransaction.PhonePe + 1;
                }
                else if (item.PGType.Contains("Google"))
                {
                    usertransaction.GooglePay = usertransaction.GooglePay + 1;
                }
                else if (item.PGType.Contains("Debit"))
                {
                    usertransaction.DebitCard = usertransaction.DebitCard + 1;
                }
                else if (item.PGType.Contains("Credit"))
                {
                    usertransaction.CreditCard = usertransaction.CreditCard + 1;
                }
                else if (item.PGType.Contains("Paytm"))
                {
                    usertransaction.Paytm = usertransaction.Paytm + 1;
                }
                else if (item.PGType.Contains("UPI QR"))
                {
                    usertransaction.UPIQR = usertransaction.UPIQR + 1;
                }
                else if (item.PGType.Contains("OlaMoney"))
                {
                    usertransaction.OlaMoneyPostpaid = usertransaction.OlaMoneyPostpaid + 1;
                }
                else if (item.PGType.Contains("Add UPI"))
                {
                    usertransaction.AddUPI = usertransaction.AddUPI + 1;
                }
                else if (item.PGType.Contains("Banking"))
                {
                    usertransaction.NB = usertransaction.NB + 1;
                }
                usertransations.Add(usertransaction);
            }
        }

        private async Task<MinMaxTxn> GetTxnRange(long dateinlong)
        {
            string query = string.Format(@"SELECT MIN(transactionid) AS MIN,MAX(transactionid) " +
                        " AS MAX FROM PaymentTransactionArchive WHERE dateofrequestinlong = {0}", dateinlong);
            MinMaxTxn result = await new MySqlConnector(connectionString_paas).FirstOrDefaultAsync<MinMaxTxn>(query);
            return result;
        }

        public static string ToCsv<T>(string separator, IEnumerable<T> objectlist)
        {
            Type t = typeof(T);
            //FieldInfo[] fields = t.GetFields(BindingFlags.Public | BindingFlags.Instance); 
            PropertyInfo[] fields = t.GetProperties();

            string header = String.Join(separator, fields.Select(f => f.Name).ToArray());

            StringBuilder csvdata = new StringBuilder();
            csvdata.AppendLine(header);

            foreach (var o in objectlist)
                csvdata.AppendLine(ToCsvFields(separator, fields, o));

            return csvdata.ToString();
        }
        public static string ToCsvFields(string separator, PropertyInfo[] fields, object o)
        {
            StringBuilder linie = new StringBuilder();

            foreach (var f in fields)
            {
                if (linie.Length > 0)
                    linie.Append(separator);

                var x = f.GetValue(o);

                if (x != null)
                    linie.Append(x.ToString());
            }

            return linie.ToString();
        }
    }

    public class MinMaxTxn
    {
        public long min { get; set; }
        public long max { get; set; }

    }
    public class PreferredTransaction
    {
        public long transactionid { get; set; }
        public string PGType { get; set; }
        public string finalresult { get; set; }
        //public string requestdata { get; set; }
        public string userid { get; set; }
        public string CleanUserId
        {
            get
            {
                return RemoveGarbaseSuffixFromUserID(userid);
            }
        }
        public bool HaspreferredPayment { get; set; }
        public bool IsPreferredPayment { get; set; }

        private string RemoveGarbaseSuffixFromUserID(string userid)
        {
            int indexofDoubleQuote = userid.IndexOf('\"', 10);
            if (indexofDoubleQuote > 0)
            {
                string userid_formatted = userid.Substring(0, indexofDoubleQuote);
                return userid_formatted;
            }
            else
            {
                return userid;
            }
        }
    }
    public class UserTransaction
    {
        public string UserId { get; set; }
        public int PhonePe { get; set; }
        public int CreditCard { get; set; }
        public int DebitCard { get; set; }
        public int GooglePay { get; set; }
        public int AddUPI { get; set; }
        public int OlaMoneyPostpaid { get; set; }
        public int UPIQR { get; set; }
        public int Airtel { get; set; }
        public int Simpl { get; set; }
        public int AmazonPay { get; set; }
        public string LastUsed { get; set; }
        public int Paytm { get; internal set; }
        public int NB { get; internal set; }
    }
}
