﻿using Confluent.Kafka;
using Newtonsoft.Json;
using StockKafkaEventConsumer.DataConnector;
using StockKafkaEventConsumer.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StockKafkaEventConsumer
{
    public class StockDataKafkaConcumer
    {
        public static DateTimeOffset UnixStartTime = new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.FromHours(0));
        private static Dictionary<string, decimal> keyValuePairs = new Dictionary<string, decimal>();
        string connectionString_local = "server=localhost;Database=stock;Persist Security Info=True;User ID=root;Password=root;";

        public async Task Start()
        {
            keyValuePairs = await GetLastDayTradePrice();
            Consume();
        }

        private async void Consume()
        {
            var config = new ConsumerConfig
            {
                BootstrapServers = "34.125.93.96:9092",
                //GroupId = "stockeventconsumer_dev_01",
                GroupId = "stockeventconsumer_dev_temp",
                AutoOffsetReset = AutoOffsetReset.Latest,
                EnableAutoCommit = false
            };

            while (true)
            {
                using (var consumer = new ConsumerBuilder<string, string>(config).Build())
                {
                    consumer.Subscribe("stockevents");

                    while (true)
                    {
                        var consumeResult = consumer.Consume();
                        await Process(consumeResult.Message.Key, consumeResult.Message.Value);
                        //Task.Factory.StartNew(() => this.Post(consumeResult.Message.Key, consumeResult.Message.Value));

                    }

                }
            }
        }

        private async Task Process(string key, string value)
        {
            stockdata stockEvent = JsonConvert.DeserializeObject<stockdata>(value);
            await InsertToMysqlDB(stockEvent);
            await ProcessPriceChange(stockEvent);
            Console.WriteLine(stockEvent.name + ":" + stockEvent.last_trade_price);

            //string url = "http://weiggle.com:9091";
            //string path = string.Format( "/metrics/job/{0}/instance/{1}/token/{2}/exchange/{3}/name/{4}"
            //    ,"stockeventscrapper","netcoreeventconsumer",stockEvent.token,stockEvent.exchange,stockEvent.name);
            //var client = new RestClient(url);
            //var request = new RestRequest(path, Method.POST);
            //string jsonToSend = "stockevent " + stockEvent.last_trade_price + "\n";
            ////jsonToSend += "sfcount " + sfcount + "\n";

            //request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
            //request.RequestFormat = DataFormat.Json;

            //try
            //{
            //    client.Timeout = 10000;
            //    var response = await client.ExecuteAsync(request);
            //    Console.WriteLine(response.StatusCode + " " + response.Content);
            //}
            //catch (Exception error)
            //{
            //    Console.WriteLine(error.Message + error.StackTrace);
            //}
        }

        private async Task<object> ProcessPriceChange(stockdata stockEvent)
        {
            StockPriceChange spc = new StockPriceChange();
            if (keyValuePairs.ContainsKey(stockEvent.name))
            {
                decimal previousdaylasttradeprice = keyValuePairs[stockEvent.name];

                spc.differenceamount = stockEvent.last_trade_price - previousdaylasttradeprice;
                spc.percentchange = (spc.differenceamount / previousdaylasttradeprice) * 100;
            }
            else
            {
                spc.differenceamount = 0;
                spc.percentchange = 0;
            }
            spc.name = stockEvent.name;
            spc.ts_processed = DateTime.Now;
            spc.ts_epoc_processed = Convert.ToInt64(DateTimeOffset.Now.ToUniversalTime().Subtract(UnixStartTime).TotalMilliseconds);
            spc.exchange = stockEvent.exchange;
            spc.daynum = convertDateToLong(DateTime.Now);
            var result = await new MySqlConnector(connectionString_local).InsertAsync<StockPriceChange>(spc);
            return result;
        }

        private async Task<object> InsertToMysqlDB(stockdata stockEvent)
        {
            stockEvent.ts_epoc_processed = Convert.ToInt64(DateTimeOffset.Now.ToUniversalTime().Subtract(UnixStartTime).TotalMilliseconds);
            stockEvent.ts_processed = DateTime.Now;
            stockEvent.daynum = convertDateToLong(DateTime.Now);
            var result = await new MySqlConnector(connectionString_local).InsertAsync<stockdata>(stockEvent);
            return result;
        }
        public long convertDateToLong(DateTime date)
        {
            return date.DayOfYear + (date.Year * 1000);
        }

        public async Task<Dictionary<string, decimal>> GetLastDayTradePrice()
        {
            keyValuePairs = new Dictionary<string, decimal>();
            long previousdayinlong = convertDateToLong(DateTime.Now.AddDays(-2));
            string query = string.Format("SELECT last_trade_price,name FROM stockdata " +
                            " where id IN (SELECT MAX(id) FROM stockdata where daynum = {0} GROUP BY name);", previousdayinlong);

            List<PreviousDayClosePrice> result = await new MySqlConnector(connectionString_local).FetchAsync<PreviousDayClosePrice>(query);
            foreach (var item in result)
            {
                keyValuePairs.Add(item.name, item.last_trade_price);
            }
            return keyValuePairs;
        }
    }
}
