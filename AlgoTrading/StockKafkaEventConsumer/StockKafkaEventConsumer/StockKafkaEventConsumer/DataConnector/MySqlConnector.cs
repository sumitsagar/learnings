﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StockKafkaEventConsumer.DataConnector
{
    public class MySqlConnector
    {
        IDatabase connection;
        public MySqlConnector(string connectionString)
        {
             connection = new Database(connectionString, DatabaseType.MySQL, MySql.Data.MySqlClient.MySqlClientFactory.Instance);
             connection.OneTimeCommandTimeout = 200000;
        }
        public async Task<List<T>> ExecuteQuery<T>(string query)
        {
            List<T> result = await connection.FetchAsync<T>(query);
            return result;
        }
        public async Task<T> FirstAsync<T>(string query)
        {
            T result = await connection.FirstAsync<T>(query);
            return result;
        }
        public async Task<int?> ExecuteScalarAsync(string query)
        {
            try
            {
                var result = await connection.ExecuteScalarAsync<int>(query);
                return result;
            }
            catch (Exception e)
            {

            }
            return -1;
        }
        public async Task<T> FirstOrDefaultAsync<T>(string query, params object[] parameters)
        {
            T result = await connection.FirstOrDefaultAsync<T>(query, parameters);
            return result;
        }
        public async Task<int> ExecuteAsync<T>(string query, object parameters)
        {
            int c = await connection.ExecuteAsync(query, parameters);
            return c;
        }
        public async Task<List<T>> FetchAsync<T>(string query, params object[] parameters)
        {
            List<T> result = await connection.FetchAsync<T>(query, parameters);
            return result;
        }

        public async Task<object> InsertAsync<T>(T row)
        {
            var result = await connection.InsertAsync<T>(row);
            return result;
        }
    }
}
